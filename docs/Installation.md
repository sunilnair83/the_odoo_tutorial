# Sushumna Installation

Odoo development in Isha is standardized on *Ubuntu Desktop*. Automation
scripts, helpers, etc are all designed and tested in Ubuntu.

If you are using a Windows / Mac machine, please see the [Ubuntu VM Setup Gudie](#ubuntu-vm-setup-guide) Section below.

**It is possible to develop Sushumna on a non-Linux environment.  There are nuances of Python
that are hard to debug when encountered.  Hence, non-linux platforms are discouraged for development.**



## Installing Odoo for Development

### Before you begin

You will need to authenticate into BitBucket from the console to get the Sushumna repository.  If you have not
set up a BitBucket user account and password yet:
1. Go to [https://bitbucket.com](https://bitbucket.com])
1. Log in with Google and use the Isha Foundation Account
1. Click on the Profile icon on the top right corner of the page.  Your username is shown under your name.
_Note that username does not include the `@` prefix_
1. In the menu, select `Settings` (Icon on the top right corner > Settings)
1. Select `Passwords` menu on the left.  Create a password.

### Installing Odoo using Easy Installer

1. Download  [Easy Install bash script](https://bitbucket.org/ishafoundation/sushumna/raw/master/bin/easy-install.sh) to the default `~/Downloads` directory. For mac, download [Easy Install mac bash script](https://bitbucket.org/ishafoundation/sushumna/raw/master/bin/easy-install-mac.sh)
_(Note that some browsers may rename the file to `.txt` for security reasons)._
1. Execute the downloaded script.  _(Note that you may need to change the sample command below based on the *filename* you saved the download as, and the *path* it is saved under)._
```bash
# Run:
#    bash <downloaded script file>
# Example:
     bash ~/Downloads/easy-install.sh
```

## Running Odoo from the Terminal

Login Credentials:

* Email: `admin`
* Password: `admin`

#### Odoo application in browser
```bash
~/isha/sushumna/run-odoo
```

The script should launch the browser: [http://localhost:8069/web/login?debug=1](http://localhost:8069/web/login?debug=1)

#### Interactive Odoo Shell

To enter the Odoo Shell (Python REPL with Odoo):
```bash
~/isha/sushumna/run-odoo shell
```

## Developing in PyCharm

1. Launch PyCharm (installed using the easy-install script)
1. Go to *File > Open*
1. In path, paste `~/isha/sushumna` and select *OK*

#### Configuring Project Interpreter

1. Got to *File > Settings > Project: sushumna > Project Interpreter*
1. From the `Project Interpreter` drop down, select `Show All`
1. Add Interpreter Path: `~/isha/sushumna/env/bin/python`.   You can follow [this guide](https://www.jetbrains.com/help/pycharm/configuring-python-interpreter.html?keymap=primary_default_for_gnome#add-existing-interpreter)
 to add the virtual environment created by the easy installer.
1. Select the newly added interpreter in the `Project Interpreter` dropdown.


#### Configuring Project Structure

1. Got to *File > Settings > Project: sushumna > Project Structure*
1. Content Root: `~/isha/sushumna`
1. To mark the following as Sources: `custom_addons`, `third_party_addons`, `odoo`,
select each of these folders, and click the blue *Sources* icon on top.

#### Running Odoo

Locate the *configuration drop-down* on top right corner with the Play and Bug button.

Default Configurations:

* `Sushumna` : Launches the Odoo Application in Browser
* `Sushumna (Shell)` : Launches Interactive Odoo Shell where you can directly interact with
Odoo on the Python console
* `Install Addon PIP Requiremnts` : Install or Update Python PIP dependencies for all the
addons

#### Resetting Sushumna Application

In the event that the database gets corrupted, and you'd like to
start from the stable master, run:

```bash
~/isha/sushumna/bin/reset-sushumna.sh
```

You can also select the `Reset Sushumna` configuration from Pycharm.


## Ubuntu VM Setup Guide

### Setting Ubuntu in Virtual Box

Download the following:
    * [Oracle Virtual Box](https://www.oracle.com/virtualization/technologies/vm/downloads/virtualbox-downloads.html)
    * [Ubuntu Desktop 19.10 Installer ISO](https://ubuntu.com/download/desktop/thank-you/?version=19.10&architecture=amd64)
    * [Odoo Development VM Appliance](https://drive.google.com/a/ishafoundation.org/file/d/1-taMjiNQBctqz2VGNvE-E9i8kkxTxmhU/view?usp=sharing)


1. Disble HyperV & Virtualization - Windows Hosts Only (Admin Mode)
2.
    * Launch *Command Prompt* in *Admin mode*, and run: `bcdedit /set hypervisorlaunchtype off`
    * Launch *Windows Turn On and Turn Off features*, and turn OFF *HyperV*


1. Install Oracle Virutal Box (Admin Mode)

1. Import the VM Appliance into Virtual Box (`File` > `Import Appliance`).  If you are creating the VM
configuration yourself, you can se the recommended configuration below.

1. Adding Ubuntu Installer Image to the VM
    1. Go to the VM `Settings` > `Storage`
    1. Under `Controller: IDE`, select the `Empty` entry
    1. Next to the `Optical Drive` drop down, click on the `CD` Icon > `Choose Virtual Optical Disk` file
    1. Select the Ubuntu Installer ISO
1. Start the VM

1. When the CD boots, choose `Install Ubuntu`

1. Go through the prompts, and select `Minimal Installation` to avoid installing unnecessary software

##### VM Coniguration
The VM Appliance has the recommended specs configured.  In the event you are creating your own VM,
the VM specs are:

* 2 processor cores (4 recommended)
* 4 GB memory (8 GB recommended)
* 20 GB virtual disk (40 GB recommended with dynamic allocation)

## Troubleshooting

#### Ubuntu VM Does not start after installation
There are cases where after the first installtion, the Ubuntu VM fails to start.
Under `Settings > Display`, set *Graphics Controller* to `VBoxSVGA`.

#### Installation Failures
The installation may partially fail because of connectivity issues / download failures.
Simply launch the installer again, and it will resume from where it left off.

Note that the easy-install re-initializes the Odoo instance with the base modules every run.

#### Lower than required Python 3 version.

If you are installing on Ubuntu 18.04 LTS or older versions of Linux, the APT repository
may not have python 3.7.  In that case, add the Python PPA:

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
```

#### INotify Watches Limit

You may encounter an Inotify Watches Limit error when debugging with
PyCharm.  [Follow these instructions](https://confluence.jetbrains.com/display/IDEADEV/Inotify+Watches+Limit)
to raise the limit.

## Running older versions of Odoo

The legacy installation requires Sushumna to be installed first.
* Odoo 12:
    * Install: `bash ~/isha/sushumna/bin/easy-install-12.sh`
    * Run: `~/isha/legacy/run-odoo`
    * Web Application: [http://localhost:8059/](http://localhost:8059/web/login?debug=1)
    * Database: `odoo12`

* Odoo 11:
    * Install: `bash ~/isha/sushumna/bin/easy-install-11.sh`
    * Run: `~/isha/legacy-11/run-odoo`
    * Web Application: [http://localhost:8049/](http://localhost:8049/web/login?debug=1)
    * Database: odoo11

##### NOTE: To run different versions of odoo in parallel, please use different browsers.

## Database Administration/Exploration

#### Exploring the Odoo Database
* Launch PgAdmin using the terminal command: `pgadmin4`
* First time - add the local server:
    * Host: localhost
    * Port: 5432
    * Username: odoo
    * Password: odoo
    * Save password for quicker access subsequently


