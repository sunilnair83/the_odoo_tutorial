[//]: # (Author: Indradeep Biswas, Created: Jan 5, 2019)

# Sushumna Development Guide

The Sushumna platform is a set of tools, conventions and utilities around the Odoo Framework
to facilitate rapid application development and high-level of code-reuse.

## Application Development

The Sushumna repository houses a variety of applications. For all of them to play well together
we need to abide by a set of constraints to avoid conflicts.

### Branching Strategy

![Branching Strategy](images/dev-branching-strategy.jpg)

#### The `master` branch

The `master` branch is considered gold, and must always be release-ready.  

**Conventions:**

* NO direct code push into the `master` branch.  All code MUST be merged into `master` through
merge requests.

* Only the maintainers have write access to the`master` branch (at the the time of writing, 
they are [Lokesh Kumar](lokesh.kumar@ishafoundation.org), 
[Sutharson Mathialagan](sutharson.mathialagan@ishafoundation.org), 
[Kirtan Gajjar](kirtan.gajjar@ishafoundation.org) and 
[Indradeep Biswas](indradeep.biswas@ishafoundation.org))

* The maintainer accepting the pull request is directly responsible for any regression that
may appear in the live application.  The maintainer must take utmost caution and is 
encouraged to consult others for review.

* All changes must be tested and approved by appropriate QA responsible for the quality of the
release build

* All pull requests / bug fixes must merge to master through `integration`.  Only critical
fixes made to fix a production error can be merged directly into master.  

#### The `*/release` branch

The release branches (`ishangam/release` or `erp/release`) are the branches where active 
development from different applications are rolled up
for integration testing prior to release.  This is a staging environment that houses
live data from master for pre-prod testing.

The release branch is stabilized and then tagged prior to release.

**Conventions:**

* It is best to avoid direct code push into the `*/release` branch.  All code should be merged through
pull requests.

* The integration branch is expected to be stable, and is the branch that QA regularly tests
for feature integrity and regression.

* All feature additions must be merged from application master branches e.g. `crm/master`

#### The `*/master` branches

The Sushumna repository houses a whole series of applications. Often these applications are
developed by independent teams working on their own release cadence. Each such application 
development train maintains its own source tree.

* Only urgent fixes should be made to `<application>/master`.  

* All feature development should happen on `<application>/feature/<feature-name>` branches.

* The application development team is responsible for managing the lifecycle of this branch.

* Application masters should merge regularly from the upstream `*/release` branches to get 
changes that are being added from other applications.

* Configuration changes made specifically for the application must not be merged up

#### The `<application>/feature/<feature-name>` branch

The feature branches under an application tree contain the source code for any feature additions.

* One feature branch should be created for one unit of work.

* These are short-lived and must be deleted upon merge to `<application>/master`.

### Branch Maintenance Practices

With many developers on the same repository, it is easy for the number of branches to explode.
Here are some best practices to keep the repository clean and maintainable:

* When merging a `<application>/feature/<feature-name>`, ensure that you `squash commit` 
and `delete source branch`.

* Any branch that has not seen a commit in the last 14 days will be deleted automatically.


## Code Promotion Workflow

### Common Promotion Workflow Steps

Code is promoted using pull requests in Git. The following steps must be followed:
1. Check out destination branch and pull the latest changes
1. Check out source branch and rebase to destination branch
1. Resolve any conflicts that may arise
1. Commit the latest changes to source branch
1. Raise a pull request in Git from source branch to destination branch.

##### Exmaple:
```bash

# Get the latest code from destination
git checkout <destination-branch>
git pull

# Pull the latest destination into your source branch
git checkout <source-branch>
git rebase <destination-branch>

# Reconcile any conflicts on rebase
# git add <files changed during resolution>
# git commit -m "<resolution message>"

# Push the final changes to origin
git push

# Raise Pull Request
```


### Workflows 

Promotion | Source Branch | Destination Branch | Requester | Reviewer
--- | --- | --- | --- | ---
Feature | `<application> /feature/<feature-name>` | `<application> /master` | Feature Developer | Application Owner 
CRM Applications | `<application>/master` | `ishangam/release` | Application Owner | Sushumna Maintainer
Ishangam Live | `ishangam/release` | `master` | Sushumna Maintainer | Sushumna Maintainer, Ops (optional)
ERP Applications | `<application>/master` | `erp/release` | Application Owner | Sushumna Maintainer
ERP Live | `erp/release` | `master` | Sushumna Maintainer | Sushumna Maintainer, Ops (optional)
Live Hotfix | `hotfix/<hotifx-name>` | `*/release` | Hotfix Developer | Sushumna Maintainer, Ops (optional)

##### Steps:
1. Pull the latest master
1. Check out master into `<ishangam/erp>/hotfix/<hotifx-name>` branch
1. Make the fix on `<ishangam/erp>/hotfix/<hotifx-name>`
1. Raise pull request from `<ishangam/erp>/hotfix/<hotifx-name>` to `<ishangam/erp>/release`
1. Once pull request is merged to `*/release`, delete hotfix branch
