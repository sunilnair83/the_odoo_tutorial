# Sushumna

Sushumna is the application development and delivery platform for Odoo based applications.

## Goals

* Provide easy onboarding for new developers:
    * Automated installation of packages and tools
    * Automated setup of the Sushumna application base with Odoo 13
    
* Standardize development:
    * Common IDE, Database and run configurations
    * Easy run configurations, click-through debugging support
    * Established code lifecycle mechanics - branching, review, promotion
 
* Standardize delivery:
    * Continuous build and qa environment for each application
    * Integration environment for testing application prior to production 


## New to sushumna?

Getting started on Sushuma is easy.  Follow the [Installation & Setup Guide](docs/Installation.md) 
to get your enviornment setup and coding.  It is best that you familiarize with the 
[development practice and conventions](docs/Development.md) before you start committing code.


## Index

1. [Installation & Setup Guide](docs/Installation.md)
1. [Development Guide](docs/Development.md)
1. [Deployment Guide](docs/Deployment.md)
1. [Configuration Management](docs/Configuration.md)

## Projects

Project | Apps | Status | Branch | Owner
--- | --- | --- | --- | ---
CRM | Santosha, Sangam | Live | `crm/master` | [Lokesh Kumar](lokesh.kumar@ishafoundation.org)
IMS | - | Ingtegration |  `ims/master` | [Omkar Deekonda](omkar.deekonda@ishafoundation.org)
VRS | - | Onboarding | `vrs/master` | [Vijay Munusamy](munusamy.k@ishafoundation.org)
Inventory | - | Onboarding | `inventory/master` | [Kirtan Gajjar](kirtan.gajjar@ishafoundation.org)
Akshaya | - | Onboarding | `akshaya/master` | [Vijay Munusamy](munusamy.k@ishafoundation.org)
Cottage | - | Onboarding | `cottage/master` | [Rohan Naik](rohan.naik@ishafoundation.org)
Telecom | - | Onboarding | `telecom/master` | -



## Support

The best way to get support for Sushumna is on Slack.  Join the channel `#odoo-training`.
  You can also contact the relevant team member from the list below

## Core Team
#### Program / Project Management
* [Kirthi Siva](kirthi.siva@ishafoundation.org)
* [Sneha Magapu](sneha.magapu@ishafoundation.org)

#### Platform Engineering
* [Indradeep Biswas](indradeep.biswas@ishafoundation.org)
* [Kirtan Gajjar](kirtan.gajjar@ishafoundation.org)

#### Application Engineering 
* [Lokesh Kumar](lokesh.kumar@ishafoundation.org)
* [Sutharson Mathialagan](sutharson.mathialagan@ishafoundation.org)
* [Vijay Munusamy](munusamy.k@ishafoundation.org)

#### Operations (Dev & Prodution)
* [Senthil Kumar Venkatesan](senthilkumar.v@ishafoundation.org)


