#!/bin/bash

set -e
set -u

function create_user() {
	local usr=$1
	echo "  Creating user '$usr'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $usr WITH PASSWORD '$usr';
            ALTER USER $usr WITH SUPERUSER;
EOSQL
}

if [ -z ${POSTGRES_MULTIPLE_USERS} ]; then
	echo "Multiple user creation requested: $POSTGRES_MULTIPLE_USERS"
	for user in $(echo $POSTGRES_MULTIPLE_USERS | tr ',' ' '); do
		create_user $user
	done
	echo "Multiple users created"
fi

