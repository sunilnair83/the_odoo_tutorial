# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import datetime
import logging
import re
import uuid

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError

from dateutil.relativedelta import relativedelta

email_validator = re.compile(r"[^@]+@[^@]+\.[^@]+")
_logger = logging.getLogger(__name__)


def dict_keys_startswith(dictionary, string):
    """Returns a dictionary containing the elements of <dict> whose keys start with <string>.
        .. note::
            This function uses dictionary comprehensions (Python >= 2.7)
    """
    return {k: v for k, v in dictionary.items() if k.startswith(string)}


class CallQueueUserInput(models.Model):
    """ Metadata for a set of one user's answers to a particular call_queue """

    _name = "call_queue.user_input"
    _rec_name = 'registration_id'
    _description = 'CallQueue User Input'
    _order = "write_date desc"

    # description
    call_queue_id = fields.Many2one('call_queue.call_queue', string='CallQueue', required=True,
                                    readonly=True, ondelete='cascade')
    queue_status = fields.Selection(related='call_queue_id.state', store=True, string='Queue Status')
    is_sensitive = fields.Boolean(related='call_queue_id.is_sensitive', store=True, string='Sensitive Call Queue')
    partner_phone = fields.Char('Phone', related='registration_id.sp_phone')
    done_time = fields.Datetime('Called time', readonly=True, copy=False)

    state = fields.Selection([
        ('new', 'Not started yet'),
        ('skip', 'Skipped'),
        ('cant_call', 'Do not call today'),
        ('did_not_pick', 'Did not pick'),
        ('done', 'Completed')], string='Status', default='new', copy=False)

    caller_id = fields.Many2one('call_queue.caller', 'Allocation')
    caller_user_id = fields.Many2one('res.users', related='caller_id.caller_id.user_id', store=True)
    caller_dialer_code = fields.Char('Dialer Code', related='caller_id.caller_id.dialer_code', store=True)

    registration_id = fields.Many2one('sp.registration', string='Contact', readonly=True)
    reg_name = fields.Char(related='registration_id.sp_name', string="Name")

    # call_status = fields.Selection([('ring_no_action', 'Ringing, No answer'),
    #                                 ('call_completed', 'Call Completed'),
    #                                 ('call_later', 'Call Later'),
    #                                 ('wrong_num', 'Wrong Number'),
    #                                 ('dnd', 'Don’t Disturb'),
    #                                 ('not_interested', 'Not Interested'),
    #                                 ('not_reachable', 'Not Reachable'),
    #                                 ('switched_off', 'Swtiched Off')], string='Call Status', copy=False)
    call_feedback = fields.Selection([('warm', 'Warm'),
                                      ('luke_warm', 'Luke Warm'),
                                      ('cold', 'Cold'),
                                      ('not_interested', 'Not Interested')],
                                     string="Call Feedback", copy=False)
    contact_remark = fields.Text(string="Remarks", copy=False)
    did_not_pick_comment = fields.Text()

    sms_sent = fields.Boolean('Follow up SMS sent', default=False, copy=False)
    prev_remark = fields.Text(string="Previous remarks", store=False, compute='_compute_prev_feedback')

    # Track last time the contact is called
    track_count = fields.Integer(string='Track Count', store=False, compute='_get_track_past_week')
    track_queue_details = fields.Char(string='Queue Track Details', store=False, compute='_get_track_past_week')

    # answers
    user_input_line_ids = fields.One2many('call_queue.user_input_line', 'user_input_id', string='Answers', copy=False)
    # Pre-defined questions
    question_ids = fields.Many2many('call_queue.question', string='Predefined Questions', readonly=True, copy=False)

    def send_sms(self):
        self.ensure_one()

        if self.call_queue_id.follow_up_sms_text:
            self.env['sms.api']._send_smcountry_sms({
                'number': self.call_queue_id.sms_test_number or self.partner_phone,
                'message': self.call_queue_id.follow_up_sms_text
            })
            self.write({'sms_sent': True})

    def _get_track_past_week(self):
        self.ensure_one()
        _logger.info('Track queue History start')
        track_period = self.call_queue_id.queue_track_period if self.call_queue_id.queue_track_period else 7
        track_till = datetime.datetime.now() - datetime.timedelta(days=track_period)
        track_till_str = track_till.strftime('%Y-%m-%d 00:00:00')
        track_list = self.search([('registration_id', '=', self.registration_id.id),
                                  ('write_date', '>=', track_till_str)]).filtered(
            lambda r: r.state == 'completed' and r.id != self.id and r.done_time and r.done_time >= track_till
        ).sorted(
            key=lambda r: r.done_time, reverse=True)
        _logger.info('Track queue History end')
        if len(track_list) > 0:
            track_history = []
            for rec in track_list:
                track_history.append(" / ".join(
                    filter(None, [rec.call_queue_id.title, str(rec.done_time.strftime("%d-%m-%Y"))])))
            self.track_count = len(track_list)
            self.track_queue_details = "^".join(track_history)
            print(self.track_queue_details)
        else:
            self.track_count = 0
            self.track_queue_details = ''
        return

    def _compute_prev_feedback(self):
        self.ensure_one()
        prev_list = self.search([('registration_id', '=', self.registration_id.id), ('state', '=', 'done')]).sorted(
            key=lambda r: r.write_date, reverse=True)
        if len(prev_list) > 0:
            for rec in prev_list:
                if rec.id != self.id and rec.contact_remark:
                    self.prev_remark = " ".join(
                        filter(None, [rec.call_queue_id.title, str(rec.write_date.strftime("%d-%m-%Y")), rec.contact_remark]))
                    return
        self.prev_remark = ''
        return

    def action_open_application(self):
        self.ensure_one()
        context = dict(self.env.context or {})
        context.update({
            'create': False,
            'write': False,
            'readonly': True,
            'edit': False,
            'no_edit': True,
            'no_write': True,
        })
        if self.registration_id:
            return {
                'name': 'Registration information',
                'view_type': 'form',
                'res_model': 'sp.registration',
                'view_mode': 'form',
                'view_id': self.env.ref('isha_sp_registration.sp_registration_form').id,
                'type': 'ir.actions.act_window',
                'res_id': self.registration_id.id,
                'context': context,
                'flags': {'mode': 'readonly'},
                'target': 'new'
            }


class CallQueueUserInputLine(models.Model):
    _name = 'call_queue.user_input_line'
    _description = 'CallQueue User Input Line'
    _rec_name = 'user_input_id'
    _order = 'question_sequence,id'

    # call_queue data
    user_input_id = fields.Many2one('call_queue.user_input', ondelete='cascade', required=True)
    call_queue_id = fields.Many2one(related='user_input_id.call_queue_id', string='CallQueue', store=True,
                                    readonly=False)
    caller_id = fields.Many2one(related='user_input_id.caller_id', string='Caller')
    question_id = fields.Many2one('call_queue.question', string='Question', ondelete='cascade', required=True)
    question_category_id = fields.Many2one(string='Question Category', related='question_id.question_category_id',
                                           store=True)
    page_id = fields.Many2one(related='question_id.page_id', string="Section", readonly=False)
    question_sequence = fields.Integer('Sequence', related='question_id.sequence', store=True)
    partner_phone = fields.Char(related='user_input_id.partner_phone')
    # caller
    skipped = fields.Boolean('Skipped')
    answer_type = fields.Selection([
        ('text', 'Text'),
        ('number', 'Number'),
        ('date', 'Date'),
        ('datetime', 'Datetime'),
        ('free_text', 'Free Text'),
        ('suggestion', 'Suggestion')], string='Answer Type')
    value_text = fields.Char('Text answer')
    value_number = fields.Float('Numerical answer')
    value_date = fields.Date('Date answer')
    value_datetime = fields.Datetime('Datetime answer')
    value_free_text = fields.Text('Free Text answer')
    value_suggested = fields.Many2one('call_queue.label', string="Suggested answer")
    value_suggested_row = fields.Many2one('call_queue.label', string="Row answer")
    answer_score = fields.Float('Score')
    answer_is_correct = fields.Boolean('Correct', compute='_compute_answer_is_correct')

    @api.depends('value_suggested', 'question_id')
    def _compute_answer_is_correct(self):
        for answer in self:
            if answer.value_suggested and answer.question_id.question_type in ['simple_choice', 'multiple_choice']:
                answer.answer_is_correct = answer.value_suggested.is_correct
            else:
                answer.answer_is_correct = False

    @api.constrains('skipped', 'answer_type')
    def _answered_or_skipped(self):
        for uil in self:
            if not uil.skipped != bool(uil.answer_type):
                raise ValidationError(_('This question cannot be unanswered or skipped.'))

    @api.constrains('answer_type')
    def _check_answer_type(self):
        for uil in self:
            fields_type = {
                'text': bool(uil.value_text),
                'number': (bool(uil.value_number) or uil.value_number == 0),
                'date': bool(uil.value_date),
                'free_text': bool(uil.value_free_text),
                'suggestion': bool(uil.value_suggested)
            }
            if not fields_type.get(uil.answer_type, True):
                raise ValidationError(_('The answer must be in the right type'))

    @api.model
    def save_lines(self, user_input_id, question, post, answer_tag):
        """ Save answers to questions, depending on question type

            If an answer already exists for question and user_input_id, it will be
            overwritten (in order to maintain data consistency).
        """
        try:
            saver = getattr(self, 'save_line_' + question.question_type)
        except AttributeError:
            _logger.error(question.question_type + ": This type of question has no saving function")
            return False
        else:
            saver(user_input_id, question, post, answer_tag)

    @api.model
    def save_line_free_text(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False,
        }
        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'free_text', 'value_free_text': post[answer_tag]})
        else:
            vals.update({'answer_type': None, 'skipped': True})
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def save_line_textbox(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'text', 'value_text': post[answer_tag]})
        else:
            vals.update({'answer_type': None, 'skipped': True})
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def save_line_numerical_box(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'number', 'value_number': float(post[answer_tag])})
        else:
            vals.update({'answer_type': None, 'skipped': True})
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def save_line_date(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'date', 'value_date': post[answer_tag]})
        else:
            vals.update({'answer_type': None, 'skipped': True})
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def save_line_datetime(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'datetime', 'value_datetime': post[answer_tag]})
        else:
            vals.update({'answer_type': None, 'skipped': True})
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def save_line_simple_choice(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        old_uil.sudo().unlink()

        if answer_tag in post and post[answer_tag].strip():
            vals.update({'answer_type': 'suggestion', 'value_suggested': int(post[answer_tag])})
        else:
            vals.update({'answer_type': None, 'skipped': True})

        # '-1' indicates 'comment count as an answer' so do not need to record it
        if post.get(answer_tag) and post.get(answer_tag) != '-1':
            self.create(vals)

        comment_answer = post.pop(("%s_%s" % (answer_tag, 'comment')), '').strip()
        if comment_answer:
            vals.update({'answer_type': 'text', 'value_text': comment_answer, 'skipped': False, 'value_suggested': False})
            self.create(vals)

        return True

    @api.model
    def save_line_multiple_choice(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        old_uil.sudo().unlink()

        ca_dict = dict_keys_startswith(post, answer_tag + '_')
        comment_answer = ca_dict.pop(("%s_%s" % (answer_tag, 'comment')), '').strip()
        if len(ca_dict) > 0:
            for key in ca_dict:
                # '-1' indicates 'comment count as an answer' so do not need to record it
                if key != ('%s_%s' % (answer_tag, '-1')):
                    val = ca_dict[key]
                    vals.update({'answer_type': 'suggestion', 'value_suggested': bool(val) and int(val)})
                    self.create(vals)
        if comment_answer:
            vals.update({'answer_type': 'text', 'value_text': comment_answer, 'value_suggested': False})
            self.create(vals)
        if not ca_dict and not comment_answer:
            vals.update({'answer_type': None, 'skipped': True})
            self.create(vals)
        return True

    @api.model
    def save_line_matrix(self, user_input_id, question, post, answer_tag):
        vals = {
            'user_input_id': user_input_id,
            'question_id': question.id,
            'call_queue_id': question.call_queue_id.id,
            'skipped': False
        }
        old_uil = self.search([
            ('user_input_id', '=', user_input_id),
            ('call_queue_id', '=', question.call_queue_id.id),
            ('question_id', '=', question.id)
        ])
        old_uil.sudo().unlink()

        no_answers = True
        ca_dict = dict_keys_startswith(post, answer_tag + '_')

        comment_answer = ca_dict.pop(("%s_%s" % (answer_tag, 'comment')), '').strip()
        if comment_answer:
            vals.update({'answer_type': 'text', 'value_text': comment_answer})
            self.create(vals)
            no_answers = False

        if question.matrix_subtype == 'simple':
            for row in question.labels_ids_2:
                a_tag = "%s_%s" % (answer_tag, row.id)
                if a_tag in ca_dict:
                    no_answers = False
                    vals.update({'answer_type': 'suggestion', 'value_suggested': ca_dict[a_tag], 'value_suggested_row': row.id})
                    self.create(vals)

        elif question.matrix_subtype == 'multiple':
            for col in question.labels_ids:
                for row in question.labels_ids_2:
                    a_tag = "%s_%s_%s" % (answer_tag, row.id, col.id)
                    if a_tag in ca_dict:
                        no_answers = False
                        vals.update({'answer_type': 'suggestion', 'value_suggested': col.id, 'value_suggested_row': row.id})
                        self.create(vals)
        if no_answers:
            vals.update({'answer_type': None, 'skipped': True})
            self.create(vals)
        return True
