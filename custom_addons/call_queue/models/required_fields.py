# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging

from odoo import models, fields

_logger = logging.getLogger(__name__)


class RequiredFields(models.Model):
    _name = 'required.fields'
    _description = 'Required Fields'

    model_id = fields.Many2one('ir.model', string='Applies to', required=True,
                               domain=['&', ('is_mail_thread_sms', '=', True), ('transient', '=', False)],
                               default=lambda self: self.env['ir.model'].sudo().search(
                                   [('model', '=', 'sp.registration')]))
    model_object_field_ids = fields.Many2many('ir.model.fields', string="Fields", domain="[('model_id','=',model_id)]")
