# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import re
from odoo.http import request
from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class IshaCallQueueContactAllocation(models.TransientModel):
    """ wizard to convert contacts into Queue Leads """

    _name = 'isha.call_queue.contact.allocation'
    _description = 'Contact convert to Lead'

    queue_id = fields.Many2one('call_queue.call_queue', string='Queue', required=True)
    caller_id = fields.Many2one('call_queue.caller', string='Pre-assign Caller', required=True)
    selected_count = fields.Integer(string='Selected count', readonly=True)

    def get_valid_reg_recs_set(self):
        reg_recs = []
        if self._context.get('active_model') == 'call_queue.user_input':
            reg_recs = [x.registration_id for x in
                        self.env['call_queue.user_input'].browse(self._context.get('active_ids'))]
        elif self._context.get('active_model') == 'call_queue.user_input_line':
            # reg_recs = self.env['call_queue.user_input'].browse(self._context.get('active_ids')).registration_id
            # no filtering as of now on call queue output
            reg_recs = [x.user_input_id.registration_id for x in
                        self.env['call_queue.user_input_line'].browse(self._context.get('active_ids'))]
        elif self._context.get('active_model') == 'sp.registration':
            reg_recs = self.env['sp.registration'].browse(self._context.get('active_ids'))

        valid_reg_recs = [x.id for x in reg_recs]
        return set(valid_reg_recs)

    @api.model
    def default_get(self, fields):
        result = super(IshaCallQueueContactAllocation, self).default_get(fields)
        result['selected_count'] = len(self.get_valid_reg_recs_set())
        return result

    def action_contacts_to_leads(self):
        UserInput = self.env['call_queue.user_input']
        create_list = []
        for registration_id in self.get_valid_reg_recs_set():
            # if not already exists, add it
            userinput_id = UserInput.search(
                [('call_queue_id', '=', self.queue_id.id), ('registration_id', '=', registration_id)], limit=1)
            if not userinput_id or userinput_id.state == 'done':
                create_list.append({'call_queue_id': self.queue_id.id, 'registration_id': registration_id,
                                    'caller_id': self.caller_id.id})
            elif self.caller_id:
                userinput_id.write({'caller_id': self.caller_id.id})
        UserInput.create(create_list)

        return {
            "type": "ir.actions.act_window",
            "res_model": "call_queue.call_queue",
            "views": [[False, "form"]],
            "res_id": self.queue_id.id
        }
