# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import datetime
from datetime import date, datetime
from odoo import api, fields, models, _

class MaintenanceManagement(models.Model):

    _name = "maintenance.checklist"
    _description = "Maintenance Checklist"

    name = fields.Char(String="Checklist Name")
    is_validate = fields.Boolean(string="Is Validate",default=False)
    description = fields.Text(String="Description")


class MaintenanceProduct(models.Model):
    _name = "maintenance.product"

    product_id = fields.Many2one('product.template',string='Product Name')
    quantity = fields.Integer('Quantity',default=1)
    product_uom_id = fields.Many2one('uom.uom',string="UOM",related="product_id.uom_id",store=True)
    maintenance_product_id = fields.Many2one('maintenance.equipment',string="Maintenance Product")
    maintenance_req_id = fields.Many2one('maintenance.request',"Maintenance Request")


class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.equipment'

    equipment_checklist_ids = fields.Many2many('maintenance.checklist',String="Checklist Name")
    equipment_material_line = fields.One2many('maintenance.product','maintenance_product_id',string="Product")


class MaintenanceMaterialPurchaseRequisition(models.Model):
    _inherit = 'material.purchase.requisition'

    maintenance_request_id = fields.Many2one('maintenance.request',string="Maintenance Request")
    job_material_requisition_id = fields.Many2one('job.order')
    req_location_id = fields.Many2one('stock.location',string="Destination Location")


    def confirm_requisition(self):
        res = super(MaintenanceMaterialPurchaseRequisition, self).confirm_requisition()
        if self.maintenance_request_id:
            request = self.env['maintenance.request'].browse(self.maintenance_request_id.id)
            name = request.name
            job = self.env['job.order'].search([('name','=',name)])
            purchase_requisition_list = []

            purchase_requisition_list.append((0,0,{
                'sequence' : self.sequence,
                'employee_id' : self.employee_id.id,
                'department_id' : self.department_id.id,
                'requisition_date' : self.requisition_date or False,
                'requisition_deadline_date' : self.requisition_deadline_date or False,
                'company_id' : self.company_id.id,
                'state' : self.state,
                }))
            job.write({
                'material_requisition_line' : [(6,0,self.ids)]
                })
        return res



class MaintenanceJobOrder(models.Model):
    _inherit = 'job.order'

    material_requisition_line = fields.One2many('material.purchase.requisition','job_material_requisition_id',string="Purchase Requisition")


class MaintenanceEquipment(models.Model):
    _inherit = 'maintenance.request'    

    is_job_order = fields.Boolean(string="Is a job",default=False)
    is_material = fields.Boolean(string="Is Material",default=False)

    @api.depends('equipment_id')
    def _checklist(self):
        
        for record in self:
            request =  self.search([('name','=',record.name)])
            equipment_checklist = []
            equipment_material = []
            if request.equipment_id:
                for checklist in request.equipment_id.equipment_checklist_ids:
                    if checklist:
                        equipment_checklist.append(checklist.id)
            request.request_checklist_ids = [(6,0,equipment_checklist)]
            if request.equipment_id:     
                for checklist in request.equipment_id.equipment_material_line:
                    if checklist:
                        equipment_material.append((0,0,{
                            'product_id' : checklist.product_id.id,
                            'quantity' : checklist.quantity,
                            'product_uom_id' : checklist.product_uom_id.id,
                            }))
            request.request_material_line = equipment_material

    request_checklist_ids = fields.Many2many('maintenance.checklist',string="Checklist Name",compute='_checklist',store=True)
    request_material_line = fields.One2many('maintenance.product','maintenance_req_id',string="Product",compute='_checklist',store=True,)
    location_id = fields.Many2one('stock.location',string="Destination Location")
    material_requisition_employee_id = fields.Many2one('res.users',string="Material Requisition Employee")
    
    def equipment_material_requisition(self):
        purchase_requisition = self.env['material.purchase.requisition']
        material_requisition = []
        for material in self.request_material_line:
            if material:
                pro_id = self.env['product.product'].search([('name','=',material.product_id.name),('product_tmpl_id','=',material.product_id.id)],limit=1)
                material_requisition.append((0,0,{
                    'product_id' : pro_id.id,
                    'qty' : material.quantity,
                    'uom_id' : material.product_uom_id.id,
                    }))
        requisition = purchase_requisition.create({
            'employee_id' : self.employee_id.id,
            'department_id' : self.employee_id.department_id.id or '',
            'requisition_date' : date.today(),
            'requisition_responsible_id' : self.material_requisition_employee_id.id or '',
            'company_id' : self.company_id.id,
            'maintenance_request_id' : self.id,
            'req_location_id' : self.location_id.id or '',
            'requisition_line_ids' : material_requisition,
            })  
        self.write({'is_material' : True,})
        views = [(self.env.ref('bi_material_purchase_requisitions.material_purchase_requisition_tree_view').id, 'tree'), (self.env.ref('bi_material_purchase_requisitions.material_purchase_requisition_form_view').id, 'form')]
        return {
            'name': _('Purchase Requisition'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'material.purchase.requisition',
            'view_id': False,
            'views': views,
            'domain': [('id', '=', requisition.id)],
        }


    def equipment_job_order(self):
        job_order = self.env['job.order']

        job = job_order.create({
            'name' : self.name,
            'user_id' : self.material_requisition_employee_id.id,
            'project_id' : '',
            'description' : self.equipment_id.note, 
            })
        views = [(self.env.ref('bi_odoo_job_costing_management.job_order_kanban_view').id, 'kanban'), (self.env.ref('bi_odoo_job_costing_management.job_order_form_view').id, 'form')]
        self.write({'is_job_order' : True,})
        return {
            'name': _('Job Order'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'kanban,form',
            'res_model': 'job.order',
            'view_id': False,
            'views': views,
            'domain': [('id', '=', job.id)],
        }


    def job_equipment_request(self):
        views = [(self.env.ref('bi_odoo_job_costing_management.job_order_kanban_view').id, 'kanban'), (self.env.ref('bi_odoo_job_costing_management.job_order_form_view').id, 'form')]
        job = self.env['job.order'].search([('name','=',self.name)])
        for order in job:
            return {
                'name': _('Job Order'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'kanban,form',
                'res_model': 'job.order',
                'view_id': False,
                'views': views,
                'domain': [('id', '=', order.id)],
            }


    def material_requisition_request(self):
        views = [(self.env.ref('bi_material_purchase_requisitions.material_purchase_requisition_tree_view').id, 'tree'), (self.env.ref('bi_material_purchase_requisitions.material_purchase_requisition_form_view').id, 'form')]
        requisition = self.env['material.purchase.requisition'].search([('maintenance_request_id','=',self.id)])
        for order in requisition:
            return {
                'name': _('Purchase Requisition'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'material.purchase.requisition',
                'view_id': False,
                'views': views,
                'domain': [('id', '=', order.id)],
            }
