# -*- coding: utf-8 -*-
# Copyright (C) 2016-2019 Artem Shurshilov <shurshilov.a@yandex.ru>
# License LGPL-3 or later (http://www.gnu.org/licenses/agpl).
{
    'name': 'Widget Image Preview',
    'summary': """Preview image in popup""",
    'description': """
This is extension for widget image
========================================================
* How it works:
    * Use widget="image" in image field. Example <field name="profile_photo" widget="image"/>
    * click on image to open popup
    * you can zoom in/out, rotate picture, print picture
    * click elsewhere to close popup or press esc to close
    
""",
    'author': 'IP Bastola',
    'website': "https://isha.sadhguru.org/",
    
    # Categories can be used to filter modules in modules listing
    'category': "Tools",
    'version': '13.1.0.1',
    # any module necessary for this one to work correctly
    'depends': ['web','mail'],
    "license": "LGPL-3",
    'data': ['views/assets_template.xml', ],
    'qweb': ['static/src/xml/image_template.xml', ],
    'installable': True,
    'application': False,
    # If it's True, the modules will be auto-installed when all dependencies
    # are installed
    'auto_install': False,
}