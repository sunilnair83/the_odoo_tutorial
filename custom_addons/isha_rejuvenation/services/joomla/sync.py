import os
import odoo
from .joomla import JoomlaInterface
from datetime import datetime as dt
from collections import Counter


def now():
    return dt.now().isoformat()


def get_odoo_environment():
    user_id = odoo.SUPERUSER_ID
    registry = odoo.registry()
    env = odoo.api.Environment(registry._db.cursor(), user_id, dict())

    return env


def sync_joomla(env):
    print(now(), "Starting schedule sync with Joomla...")

    if not env:
        env = get_odoo_environment()
    # switch this to prod when going live
    base_url = env['ir.config_parameter'].sudo().get_param('rejuvenation.joomlaurl')
    uid = env['ir.config_parameter'].sudo().get_param('rejuvenation.joomlauid')
    pwd = env['ir.config_parameter'].sudo().get_param('rejuvenation.joomlakey')
    #base_url = "https://beta.ishafoundation.org/administrator/"

    # ideal
    # uid = os.environ['JOOMLA_USER']
    # pwd = os.environ['JOOMLA_PASSWORD']

    # not recommended
    #uid = "swami.kalitha@ishafoundation.org"
    #pwd = "eM@^3v^6cXU"

    j = JoomlaInterface(uid=uid, pwd=pwd, lurl=base_url)
    ja = j.authenticate()

    if not ja:
        print(now(), "Joomla authentication failed. Exiting...")
        return


    program_schedules_model = env['rejuvenation.program.schedule']

    c = Counter()

    for ps in program_schedules_model.search([("pgmschedule_online_code", "in", [None, False, ""])]):
        ps_core = ps.get_core_parameters()
        print(now(), "Creating program for ", ps_core.program_title)

        new_code = j.create_new_program(ps_core)

        if new_code.startswith("FAILED"):
            c['skipped'] += 1
            print(now(), "Create program for", ps_core.program_title, "failed. Moving on...")
            continue

        ps.write({"pgmschedule_online_code": new_code})
        c['processed'] += 1

    print(now(), "{} Processed, {} Skipped".format(c['processed'], c['skipped']))
    env.cr.commit()
