function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}

function jspgmregfmdcoumentready() {
  $(document).ready(function () {
$('#prs-iframe').attr("scrolling","yes");
jsprgregseamlessinit();
jspgmregfmishaautofillclick();
jspgmregmsigninasdiffuser();
jspgmregviewprofinfo();
});
}
defer(jspgmregfmdcoumentready);

function add_table_delete_behaviour(add_button, table) {
var clonetrvar = $($(`#${table} tr`)[1]).clone();
    $(`#${add_button}`).click(function () {
        var appendedelevar =$(clonetrvar).clone().appendTo(`#${table}`);
        $("td a i.fa.fa-trash", appendedelevar).show();
    });

    $(`#${table}`).on("click", "tr td a i.fa.fa-trash", function () {
        $($(this).parents(`#${table} tr`)[0]).remove();
    });
}

function jsprgreghideaddbuttonfunction(tableselectorvar,buttonselectorvar){
        $(buttonselectorvar).hidden = true;
$(tableselectorvar).on('change', 'tr select,tr input', function() {
    var emptyflg = false;
console.log(tableselectorvar+ " input"+", "+tableselectorvar+ " select");
    $(tableselectorvar+ " input"+", "+tableselectorvar+ " select").each(function() {
        if ($(this).val() == "") {
            emptyflg = true;
        }
    });
    if (emptyflg) {
        $(buttonselectorvar)[0].hidden = true;
    } else {
        $(buttonselectorvar)[0].hidden = false;
    }
 });
}
var seamlessvar;
function jsprgregseamlessinit(){
             seamlessvar = new Seamless({child: true, selector: 'body', acceptFrom:['preprod.sadhguru.org', 'isha.sadhguru.org'],  // the parent iframe url
              onStart: function() {
                $("#formcontainer").css("transition", "width 1s, height 1s, background-color 1s").css("visibility", "hidden");
                //$("body").css("background-color", "transparent");
                //jf.scrollSelector = null;
                // Just in case, if the onInit does not fire
                setTimeout(function() { $("#formcontainer").show().css("visibility", "visible"); }, 2000);
              },
              onInit: function() {
                $("#formcontainer").css("overflow", "hidden").hide().css("visibility", "visible").fadeIn(900);
              }
            });
}

function jspgmregfmishaautofillclick(){
    $('#jspgmregfautofillisha').click(function(){
    seamlessvar.sendMessage({type: 'ssologin', legal_entity: "IF", force_consent: "1"});
    });

}

function jspgmregmsigninasdiffuser(){
    $("#jspgmregfmsigninasdifferentuser").click(function(){
       seamlessvar.sendMessage({type: 'ssosignout', params: ""});
    });
}

function jspgmregviewprofinfo(){
    $("#jspgmregfviewprofile").click(function(){
        seamlessvar.sendMessage({type: 'ssomyprofile', params: "afm=1"});
    });
}