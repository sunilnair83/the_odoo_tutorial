# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.tools.translate import html_translate

class KnowledgePost(models.TransientModel):
    _name = "knowledge.post"
    _description = "knowledge post"

    web_question = fields.Char(string="Question",required=True,default=None)
    web_solution = fields.Html('Solution', translate=html_translate)

    def post_a_question(self):
        ticket_id = self.env['support.ticket'].browse(self.env.context.get('active_id'))
        knowledge_obj = self.env['knowledge.bases']
        vals = None
        for value in self:
            vals = {
            'name' : value.web_question,
            'query_solution' : value.web_solution,
            'helpdest_id' : ticket_id.id,
            }
        knowledge_obj.create(vals)

class KnowledgeSearch(models.TransientModel):
    _name = "knowledge.search"
    _description = "knowledge search"

    knowledge_search_ids = fields.Many2many('knowledge.bases',string="Knowledge Base")

    def knowledge_search(self):
        url_list = []
        for value in self:
            for question in value.knowledge_search_ids:
                if question:
                    url_list.append(question.question_url)