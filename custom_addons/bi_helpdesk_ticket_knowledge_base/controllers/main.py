# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import SUPERUSER_ID, http, tools, _
from odoo import models, fields, api, _
from odoo.http import request
from datetime import datetime
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from odoo.exceptions import UserError, AccessError


class bi_website_knowledge_bases(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(bi_website_knowledge_bases, self)._prepare_portal_layout_values()

        knowledge = request.env['knowledge.bases']
        
        knowledge_count = knowledge.sudo().search_count([])

        values.update({
            'knowledge_count': knowledge_count,
        })

        return values

    @http.route(['/my/query', '/my/query/page/<int:page>'], type='http', auth="user", website=True)
    def portal_my_knowledge(self, page=1, date_begin=None, date_end=None, **kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        knowledge_base = request.env['knowledge.bases']
        
        domain = []

        archive_groups = self._get_archive_groups('knowledge.bases', domain)
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]
        
        # count for pager
        knowledge_count = knowledge_base.sudo().search_count(domain)
        # pager
        knowledge_category = request.env['knowledge.category'].sudo().search([])
        category_list = []
        
        pager = request.website.pager(
            url="/my/query",
            url_args={'date_begin': date_begin, 'date_end': date_end},
            total=knowledge_count,
            step=self._items_per_page
        )
        
        knowledge = knowledge_base.sudo().search(domain, limit=self._items_per_page, offset=(page - 1) * self._items_per_page)
        
        values.update({
            'date': date_begin,
            'knowledge': knowledge,
            'page_name': 'Knowledge Bases',
            'pager': pager,
            'kw' : kw,
            'archive_groups': archive_groups,
            'default_url': '/my/query',
        })
        return request.render("bi_helpdesk_ticket_knowledge_base.portal_my_knowlegde", values)

    @http.route(['/my/query/search'], type='http', auth="user", website=True)
    def portal_search_knowledge(self, page=1, date_begin=None, date_end=None, **kw):
        values = self._prepare_portal_layout_values()
        knowledge_base = request.env['knowledge.bases']
        knowledge_category = request.env['knowledge.category']
        knowledge_tags = request.env['knowledge.tags']
        post_name = str(kw.get('search'))
        base_line_domain = []
        if post_name:
            base_line_domain += [('name', 'ilike', post_name)]

        category_lines = knowledge_category.sudo().search(base_line_domain)
        tag_lines = knowledge_tags.sudo().search(base_line_domain)
        knowledge_lines = knowledge_base.sudo().search(['|','|',('name', 'ilike', post_name),('category_id','in',category_lines.ids),('tag_ids','in',tag_lines.ids)])
        
        values.update({'knowledges' : knowledge_lines,})

        return request.render("bi_helpdesk_ticket_knowledge_base.search_knowledge_base", values)


    @http.route(['/knowledge/view/detail/<model("knowledge.bases"):knowledge>'],type='http',auth="public",website=True)
    def knowledge_view(self, knowledge, category='', search='', **kwargs):
        context = dict(request.env.context or {})
        knowledge_obj = request.env['knowledge.bases']
        context.update(active_id=knowledge.id)
        knowledge_data_list = []
        knowledge_data = knowledge_obj.sudo().browse(int(knowledge))
        
        for items in knowledge_data:
            knowledge_data_list.append(items)
            
        return http.request.render('bi_helpdesk_ticket_knowledge_base.portal_knowledge_detail_view',{
            'knowledge_data_list': knowledge
        })    


    @http.route(['/my/query/category/<model("knowledge.category"):bases>','/my/query/category/<model("knowledge.category"):bases>/page/<int:page>'],type='http',auth="public",website=True)
    def knowledge_category_view(self, bases,  page=1, date_begin=None, date_end=None, category='', search='', **kw):
        values = self._prepare_portal_layout_values()
        context = dict(request.env.context or {})
        bases_obj = request.env['knowledge.bases']
        context.update(active_id=bases.id)
        category_data_list = []
        
        knowledge_base = request.env['knowledge.bases']
        
        domain = []#('partner_id', '=', partner.id),('state', 'in', ['draft', 'confirmed', 'approved', 'return'])
        

        archive_groups = self._get_archive_groups('knowledge.bases', domain)
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]
        
        # count for pager
        knowledge_count = knowledge_base.sudo().search_count(domain)

        pager = request.website.pager(
            url="/my/query/category/%s-%s" % (str((bases.name).lower()),str(bases.id)),
            url_args={'date_begin': date_begin, 'date_end': date_end},
            total=knowledge_count,
            step=self._items_per_page
        )

        category_data = bases_obj.sudo().search([('category_id', '=', bases.id)],limit=self._items_per_page, offset=(page - 1) * self._items_per_page)


        values.update({
            'page_name': 'Knowledge Bases',
            'pager': pager,
            'kw' : kw,
            'category_data_list': category_data,
            'categories':bases
            })

        return http.request.render('bi_helpdesk_ticket_knowledge_base.portal_my_knowlegde',values)