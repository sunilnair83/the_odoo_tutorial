# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Sales Contract Subscription and Recurring Invoice Odoo',
    'version': '13.0.0.0',
    'category': 'Sales',
    'summary': 'Apps helps to create Subscription contract Recurring Invoice from sales order subscription invoice documents Sale Contract Subscription contract Recurring invoice sale order subscription process sales order subscription',
    'description': """
	Sales Contract and Recurring Invoice, Subscription management, Recurring invoice on Sales, Recurring invoice on contract, contract subscription management, subscription invoice, sale contact, invoice contract, recurring invoice, sales invoice contract, contract invoice , mutiple invoice on contract, subscription invoice contract.Sales Contract management, Sales recurring invoice, sales monthly invoice, sales invoice interval.
	Sales Subscription
	auto contract subscription invoice
	Recurring invoice
	sales Recurring invoice
	contract Recurring invoice
	sales subscription contract management
	invoice recurring document
	subscription invoice documents
	sales recurring document
    subscription document management
    subscription process
    subscription automatic invoice contract subscription contract
	product for subscription subscription product automatically create contract subscription.
	Sales order Subscription/Contract, Sales order Contractor.
	Setup recurring period on Subscription and invoice.
	automate invoice creation process by system
    Sales Contract Subscription and Recurring Invoice 
    Sale Subscription product Recurring Invoice
    Sale Contract Subscription Recurring Invoice 
	Quotation Template
	Sales Order Template
	Recurring Invoice from Contract
    This Odoo apps used to maintain the contracts and subcription for the product sold by the company.
     It is also allowing you to create recurring invoice from Subscription contract based on period configured
      on sales subscription contract.With help of this Odoo module/apps you can easily create subscription product 
      from the product view and easily generate the contract for the subscription product. 
      This subscription contract is used for create subscription sales order which will allow to generate recurring invoice process 
      with linked Project/Analytic Account in Odoo ERP.
    This Odoo apps also helps to print project contract reports sales subscription Process
    Verkaufsvertrag und wiederkehrende Rechnung, Abonnementverwaltung, wiederkehrende Rechnung für den Verkauf, wiederkehrende Rechnung für den Vertrag, Vertragsabonnementverwaltung, Abonnementrechnung, Verkaufskontakt, Rechnungsvertrag, wiederkehrende Rechnung, Verkaufsrechnungsvertrag, Vertragsrechnung, Mehrfachrechnung auf Vertrag, Abonnement-Rechnungsvertrag .Verkaufsvertragsverwaltung, wiederkehrende Verkaufsrechnung, monatliche Verkaufsrechnung, Intervall für Verkaufsrechnung.
Verkaufsabonnement
Abo-Rechnung
Wiederkehrende Rechnung
Verkauf Wiederkehrende Rechnung
Vertrag Wiederkehrende Rechnung
Abonnementvertragsverwaltung
wiederkehrendes Dokument in Rechnung stellen
Abonnement-Rechnungsdokumente
Verkauf wiederkehrendes Dokument

Produkt für Abonnement, Abonnementprodukt, automatisch Vertragsabonnement erstellen.
Kundenauftrag Abonnement / Vertrag, Kundenauftrag Kontrakt.
Wiederholungszeitraum für Abonnement und Rechnung einrichten
Automatisieren Sie den Rechnungsstellungsprozess nach System
Angebotsvorlage
Kundenauftragsvorlage
Wiederkehrende Rechnung aus dem Vertrag
Contrat de vente et facture récurrente, Gestion des abonnements, Facture récurrente sur les ventes, Facture récurrente sur contrat, gestion des abonnements, facture d'abonnement, contact de vente, contrat de facture, facture récurrente, contrat de facture, facture contractuelle, contrat de facture d'abonnement Gestion des contrats Ventes, facture récurrente des ventes, facture mensuelle des ventes, intervalle de facturation des ventes.
Abonnement aux ventes
facture d'abonnement
Facture récurrente
ventes Facture récurrente
contrat Facture récurrente
Gestion des contrats d'abonnement
facture facture récurrente
documents de facture d'abonnement
document récurrent de vente

produit pour l'abonnement, produit d'abonnement, créer automatiquement un abonnement au contrat.
Commande client Abonnement / contrat, commande client Contractr.
Configurez la période récurrente sur l'abonnement et la facture.
automatiser le processus de création de facture par système
Modèle de devis
Modèle de commande de vente
Facture récurrente du contrat
Contrato de venda e fatura recorrente, gerenciamento de assinatura, fatura recorrente em vendas, fatura recorrente em contrato, gerenciamento de assinatura de contrato, fatura de assinatura, contato de venda, contrato de fatura, fatura recorrente, contrato de fatura de venda, fatura de contrato, fatura múltipla em contrato Gerenciamento de contratos de vendas, fatura recorrente de vendas, fatura mensal de vendas, intervalo de fatura de vendas.
Assinatura de Vendas
fatura de assinatura
Fatura recorrente
fatura recorrente de vendas
fatura recorrente
Gerenciamento de contrato de assinatura
documento recorrente da fatura
documentos de fatura de assinatura
documento recorrente de vendas

produto para assinatura, produto de assinatura, criar automaticamente a assinatura do contrato.
Ordem de venda Subscription / Contract, Sales order Contractr.
Configure o período recorrente em Assinatura e fatura.
automatizar o processo de criação de faturas pelo sistema
Modelo de Cotação
Modelo de ordem de vendas
Fatura recorrente do contrato

Contrato de Ventas y Factura Recurrente, Gestión de suscripciones, Factura recurrente en Ventas, Factura recurrente en contrato, gestión de suscripción de contrato, factura de suscripción, contacto de venta, contrato de factura, factura recurrente, contrato de factura de venta, factura de contrato, factura múltiple en contrato, contrato de factura de suscripción . Gestión de contratos de ventas, factura recurrente de ventas, factura mensual de ventas, intervalo de facturación de ventas.
Suscripción de ventas
factura de suscripción
Factura recurrente
ventas Factura recurrente
contrato factura recurrente
Gestión de contratos de suscripción
factura de documento recurrente
documentos de factura de suscripción
documento recurrente de ventas

producto de suscripción, producto de suscripción, crear automáticamente suscripción de contrato.
Pedido de venta Suscripción / contrato, pedido de cliente Contractr.
Configurar el período recurrente en Suscripción y factura.
automatizar proceso de creación de factura por sistema
Plantilla de cotización
Plantilla de pedido de venta
Factura recurrente del contrato
عقد البيع والفاتورة المتكررة ، إدارة الاشتراكات ، الفاتورة المتكررة على المبيعات ، الفاتورة المتكررة على العقد ، إدارة الاشتراك في العقد ، فاتورة الاشتراك ، جهة اتصال البيع ، عقد الفاتورة ، الفاتورة المتكررة ، عقد بيع الفاتورة ، فاتورة العقد ، فاتورة المودعة على العقد ، عقد فواتير الاشتراك . مبيعات إدارة العقود ، مبيعات فاتورة متكررة ، فاتورة المبيعات الشهرية ، فواتير المبيعات الفاصل.
اشتراك المبيعات
فاتورة الاشتراك
فاتورة متكررة
مبيعات فاتورة متكررة
عقد الفواتير المتكررة
إدارة عقد الاشتراك
فاتورة وثيقة متكررة
وثائق فاتورة الاشتراك
وثيقة مبيعات متكررة

منتج للاشتراك ، منتج الاشتراك ، تلقائيا إنشاء عقد الاشتراك.
أمر المبيعات الاشتراك / العقد ، أمر المبيعات Contractr.
إعداد فترة متكررة على الاشتراك والفاتورة.
أتمتة عملية إنشاء الفاتورة حسب النظام
قالب التسعير
نموذج طلب المبيعات
فاتورة متكررة من العقد
eaqad albaye walfatwrt almutakarirat , 'iidarat alaishtirakat , alfatwrt almutakarirat ealaa almabieat , alfatwrt almutakarirat ealaa aleaqd , 'iidarat alaishtirak fi aleuqad , faturat alaishtirak , jihat aitisal albaye , eaqd alfatwrt , alfatwrt almutakarirat , eaqd baye alfatwrt , faturat aleaqd , faturt almuadaeat ealaa aleaqd , eaqd fawatir alaishtirak . mabieat 'iidarat aleuqud , mabieat faturat mutakarirat , faturat almubayeat alshahriat , fawatir almubayeat alfasil.
aishtirak almubieat
fatwrat alaishtirak
faturat mutakarira
mabieat faturat mutakarira
eaqad alfawatir almutakarira
'iidarat eaqd alaishtirak
fatwrat wathiqat mutakarira
wathayiq faturat alaishtirak
wathiqat mabieat mutakarira
muntij lilaishtirak , muntij alaishtirak , talqayiyaaan 'iinsha' eaqd alaishtirak.
'amr almubayeat alaishtirak / aleaqd , 'amr almabieat Contractr.
'iiedad fatrat mutakarirat ealaa alaishtirak walfaturat.
'atamtat eamaliat 'iinsha' alfatwrt hsb alnizam
qalib altaseir
namudhaj talab almubieat
faturat mutakarirat min aleuqad

""",
    'author': 'BrowseInfo',
    'price': '29',
    'currency': "EUR",
    'website': 'https://www.browseinfo.in',
    'live_test_url':'https://youtu.be/_aSMtGDL_2A',
    'depends': ['base','web','sale_management','project','analytic','account'],
    'data': [
    		 'security/ir.model.access.csv',
             'views/data.xml',
             'views/product.xml',
             'views/sale_view.xml',
             'views/account_analytic_account_view.xml',
             "views/report.xml",
             "views/report_contract.xml",
             
             
             
             
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
	"images":['static/description/Banner.png']
}
