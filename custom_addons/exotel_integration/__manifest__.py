# -*- coding: utf-8 -*-
{
    'name': "Exotel Integration",

    'summary': """
        Exotel Integration""",

    'description': """
        Exotel Integration with odoo
    """,

    'author': "Isha",
    'website': "http://www.ishafoundation,.org",
    'category': 'app',
    'version': '13.0.0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','portal','bi_website_support_ticket'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],

}
