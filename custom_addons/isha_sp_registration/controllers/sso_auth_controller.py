import logging
import time

from odoo import http
from odoo.http import request
from odoo.addons.auth_oauth.controllers.main import fragment_to_query_string

from ..isha_importer import SSOAuthController, Configuration, sso_get_user_profile, hash_hmac
from .sp_reg_controller_util import render_error_template_for_public_user

_logger = logging.getLogger(__name__)


class SpSSOAuthController(SSOAuthController):
    @http.route()
    @fragment_to_query_string
    def sso_auth_signin(self, **kw):
        url = request.httprequest.form['request_url']
        sso_config = Configuration('SSO')

        secret = sso_config['SSO_API_SECRET']
        api_key = sso_config['SSO_API_KEY']
        session_id = request.httprequest.form['session_id']
        hash_val = hash_hmac({'session_id': session_id}, secret)

        data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
        # time.sleep(3)
        sso_user_profile = eval(sso_get_user_profile(sso_config['SSO_LOGIN_INFO_ENDPOINT'], data).text)

        # Update context with user profile to avoid additional call to get user profile in crm/sso_auth_endpoint
        self._update_context({
            'sso_user_profile': sso_user_profile
        })
        # todo
        if 'sp-profile' in url or 'ha-form' in url or 'trial-form' in url:
            user_email = sso_user_profile['autologin_email']
            sp_record = request.env['sp.registration'].sudo().search([('sp_email', '=', user_email),
                                                                      ('registration_batch.status', '=', 'current')])
            if not sp_record.id:
                return render_error_template_for_public_user(
                    'Please login with email used for pre-registration application.')

        return super(SpSSOAuthController, self).sso_auth_signin(**kw)

    def _update_context(self, value):
        context = request.env.context.copy()
        context.update(value)
        request.env.context = context
