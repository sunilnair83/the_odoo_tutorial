# -*- coding: utf-8 -*-

from odoo import models, fields


class SpInterviewerSuggestion(models.Model):
    _name = 'sp.interviewer.suggestion'
    _description = 'SP Interviewer Suggestion'

    _sql_constraints = [('question_unique', 'UNIQUE (name)', 'Suggestion already exists!')]

    name = fields.Char(string='Suggestion', required=True)
    active = fields.Boolean(string='Active', default=True)
    color = fields.Integer('Color Index')
