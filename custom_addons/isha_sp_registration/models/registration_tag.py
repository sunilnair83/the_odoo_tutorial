# -*- coding: utf-8 -*-
from odoo import models, fields


class RegistrationTag(models.Model):
    _name = 'registration.tag'
    _description = 'Registration Tag'

    _sql_constraints = [('name_unique', 'UNIQUE (name,tag_type)', 'You can not have two tags with the same name!')]

    name = fields.Char(string='Tag', required=True)
    tag_type = fields.Many2one('registration.tag.type', string='Type')
    active = fields.Boolean(string='Active', default=True)


class TagType(models.Model):
    _name = 'registration.tag.type'
    _description = 'Registration Tag Type'

    _sql_constraints = [('name_unique', 'UNIQUE (name)', 'You can not have two tag types with the same name!')]

    name = fields.Char(string='Type', required=True)
    active = fields.Boolean(string='Active', default=True)
