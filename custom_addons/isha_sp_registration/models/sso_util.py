import base64
import json
import logging
import uuid
from datetime import date, datetime
from odoo.http import request
import jwt
import requests
import hashlib
import hmac
from odoo import api, models, fields
from ..isha_importer import Configuration

_logger = logging.getLogger(__name__)


class SSO(models.AbstractModel):
    _name = 'isha_sp_registration.ssoapi'

    # SSO Logout
    def sso_logout(self):
        sso_config = Configuration('SSO')
        ret = sso_config['SSO_API_SECRET']

        cururls = str(request.httprequest.url).replace("http://", "http://", 1)
        ret_list = {'request_url': str(cururls),
                    "callback_url": str(request.httprequest.host_url) + "web/session/logout",
                    "api_key": sso_config['SSO_API_KEY'],
                    }
        data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
        data_enc = data_enc.replace("/", "\\/")
        signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                             digestmod=hashlib.sha256).hexdigest()
        ret_list["hash_value"] = signature
        ret_list['sso_logouturl'] = sso_config['SSO_LOGIN_URL'] + 'cm'
        print(ret_list)
        return ret_list
