# -*- coding: utf-8 -*-
from odoo import models, fields, api


class ApplicationProcessingPriority(models.Model):
    _name = 'application.processing.priority'
    _description = 'Application Processing Priority'
    _order = 'sequence'

    _sql_constraints = [
        ('name_unique', 'UNIQUE (name)', 'You can not have two Priority with the same name!'),
        ('sequence_unique', 'UNIQUE (sequence,name)', 'Sequence no. must be unique!')
    ]

    name = fields.Char(string='Priority', required=True)
    active = fields.Boolean(string='Active', default=True)
    sequence = fields.Integer('Sequence', default=1)

    @api.model
    def create(self, values):
        values['sequence'] = self.env['ir.sequence'].next_by_code('application.processing.priority')
        return super(ApplicationProcessingPriority, self).create(values)
