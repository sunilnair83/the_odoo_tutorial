# -*- coding: utf-8 -*-
from odoo import models, api


class IrAttachment(models.Model):
    _inherit = 'ir.attachment'

    @api.model
    def check(self, mode, values=None):
        if mode == 'read' and self._is_sp_reg_group_member():
            return True

        return super(IrAttachment, self).check(mode, values)

    def _is_sp_reg_group_member(self):
        user = self.env.user
        sp_reg_module = self.env['ir.module.category'].sudo().search(
            [('id', '=', self.env.ref('isha_sp_registration.module_sp_registration').id)])
        sp_reg_module.ensure_one()
        sp_reg_groups = self.env['res.groups'].sudo().search([('category_id', '=', sp_reg_module.id)])
        user_sp_groups = set(sp_reg_groups.ids).intersection(set(user.groups_id.ids))
        return user_sp_groups and len(user_sp_groups) > 0
