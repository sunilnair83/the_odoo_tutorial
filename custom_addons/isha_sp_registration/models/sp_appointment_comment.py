# -*- coding: utf-8 -*-

from odoo import models, fields


class AppointmentCommentType(models.Model):
    _name = 'appointment.comment.type'
    _description = 'SP Appointment Comment Types'

    _sql_constraints = [('type_unique', 'UNIQUE (name)', 'Comment type already exists!')]

    name = fields.Char(string='Type')


class SpAppointmentComment(models.Model):
    _name = 'sp.appointment.comment'
    _description = 'SP Appointment Comment'

    name = fields.Text(string='Comment', required=True)
    type = fields.Many2one('appointment.comment.type', required=True)
    appointment_id = fields.Many2one('sp.appointment', required=True)
