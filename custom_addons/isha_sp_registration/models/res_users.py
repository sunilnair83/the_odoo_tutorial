from odoo import models, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    interviewer_id = fields.One2many('sp.interviewer', 'user_id')

    def write(self, values):
        res = super(ResUsers, self).write(values)
        if self.has_group('isha_sp_registration.group_sp_appointment_interviewer'):
            interviewer_id = self.env['sp.interviewer'].sudo().search([('user_id', '=', self.id)])
            if not interviewer_id:
                interviewer_id = self.env['sp.interviewer'].sudo().create({'user_id': self.id}).id
        elif not self.has_group('isha_sp_registration.group_sp_appointment_interviewer'):
            interviewer_id = self.env['sp.interviewer'].sudo().search([('user_id', '=', self.id)])
            if interviewer_id:
                interviewer_id.unlink()
        return res
