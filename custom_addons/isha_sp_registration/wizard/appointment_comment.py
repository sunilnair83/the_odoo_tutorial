# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AppointmentComment(models.TransientModel):
    _name = 'appointment.comment'
    _description = 'Appointment Comment'
    _order = 'id asc'

    name = fields.Text('Comment', required=True)

    @api.model
    def create(self, values):
        res = super(AppointmentComment, self).create(values)
        if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
            type = self.env.ref('isha_sp_registration.appointment_comment_type_scheduler').id
        else:
            # if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interviewer'):
            type = self.env.ref('isha_sp_registration.appointment_comment_type_interviewer').id
        self.env['sp.appointment.comment'].sudo().create({
            'appointment_id': self._context.get('active_id'),
            'name': res.name,
            'type': type,
        })
        return res
