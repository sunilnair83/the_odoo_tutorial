# -*- coding: utf-8 -*-

from datetime import timedelta

from odoo import models, fields, api


class AppointmentReassign(models.TransientModel):
    _name = 'appointment.reassign'
    _description = 'Appointment Reassign'
    _order = 'id desc'

    appointment_id = fields.Many2one('sp.appointment', string='Appointment', readonly=True)

    def _get_interviewer_id_domain(self):
        domain = []
        interviewer_id = []
        # This is to get Appointment timeslot's time and find the interviewer list for reassign
        if self._context.get('time_slot_id'):
            calendar_event_id = self.env['calendar.event'].browse(self._context.get('time_slot_id'))
            domain = [("start", ">", calendar_event_id.start.strftime('%Y-%m-%d %H:00:00'))]
            domain += [("start", "<", (calendar_event_id.start + timedelta(hours=25)).strftime('%Y-%m-%d %H:00:00'))]
            calendar_event_ids = self.env['calendar.event'].search(domain)
            for calendar_event_id in calendar_event_ids:
                interviewer_id += calendar_event_id.interviewer_id.ids
            domain = [("interviewer_id", "in", interviewer_id)]
        return domain

    interviewer_id = fields.Many2one('sp.interviewer', string='Assign to', domain=_get_interviewer_id_domain)

    def action_reassign_interviewer(self):
        appoint_id = self.env['sp.appointment'].browse(self.appointment_id.id)
        appoint_id.sudo().write({
            'assign_interviewer_id': self.interviewer_id,
        })
        if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
            appoint_id.sudo().write({
                'is_re_assigned_by_scheduler': True,
            })
        appoint_id.registration_id.sudo().write({
            'appointment_state': 're_assign',
        })

    def action_reassign_scheduler(self):
        appoint_id = self.env['sp.appointment'].browse(self.appointment_id.id)
        appoint_id.sudo().write({
            'interviewer_id': self.interviewer_id,
        })
        appoint_id.registration_id.sudo().write({
            'appointment_state': 're_assign',
        })

    @api.onchange('interviewer_id')
    def onchange_interviewer_id(self):
        return {'domain': {'interviewer_id': [('id', '!=', self.appointment_id.interviewer_id.id)]}}
