import pymysql, time
from datetime import datetime
from confluent_kafka import Producer
import json, traceback
import logging
import sys, csv

# MYsql properties
hostname = 'non-prod-comm-psql-mysql.internal.isha.in'
username = 'suvyaApp_2'
password = 'bY94eKY2fV'
database = 'suvya_master_clone_db'

#logging.basicConfig(filename='connector.log', level=logging.DEBUG)

# Kafka config
kafka_topic = 'crm-sp-qa-suvya-odoo-mar18'
p = Producer({
    'bootstrap.servers': 'qa-kafka.isha.in:9093',
    'security.protocol': 'SSL',
    'ssl.ca.location': 'CARoot.pem',
    'ssl.certificate.location': 'certificate.pem',
    # 'ssl.key.location': 'key.pem'
})


def delivery_report(err, msg):
    print('Hello')
    if err is not None:
        logger.debug('Message delivery failed: {}'.format(err))


def doQuery(conn):
    cur = conn.cursor()
    logging.info('Mysql connected')
    print('Mysql connected')
    query = "select i.id, i.first_name, i.last_name, i.email, i.mobile, " \
            "i.city, i.state, i.country, p.postal_code, i.sadhanapada_year, i.nationality, p.gender, p.personal_email, p.education," \
            " p.phone, p.address, p.date_of_birth, p.passport_country, p.country_of_residence, p.program_history, " \
            " p.primary_emergency_contact_name, p.primary_emergency_contact_address, p.primary_emergency_contact_phone, " \
            " p.primary_emergency_contact_email, p.primary_emergency_contact_relation, p.secondary_emergency_contact_name," \
            " p.secondary_emergency_contact_address, p.secondary_emergency_contact_phone, p.secondary_emergency_contact_email," \
            " p.secondary_emergency_contact_relation, p.tags, p.work_experience, p.languages, i.person_id, i.ie_status," \
            " i.ie_month_year, i.arrival_date, i.departure_date, i.sadhanapada_status, p.mother_tongue, p.professional_details," \
            " p.references_professional, p.skills_hobbies_awards, p.employed_in_india, p.personal_skills, p.knowledge_based_skills," \
            " p.transferable_skills, p.computer_skills, p.references_volunteering, p.teacher_training, p.volunteeringAtIYCOption," \
            " p.volunteering_iyc, p.stay_duration_during_volunteering, p.volunteeringLocalOption, p.volunteering_local," \
            " p_extn.skillyouHave, p_extn.ishaConnect, p_master.sp_remarks, p.person_uid, p.marital_status," \
            " p.spouses_name, p.fathers_name, p.mothers_name,p.mental_ailments, p.mental_ailments_in_family, p.current_medication," \
            " p.past_medication, p.drug_usage, p.drugTreatment, p.surgery_details, p.physical_disabilities," \
            " p.hospitalized, p.family_details, p.yatra_history, p.event_history, inter.interviewer_assessment," \
            " inter.action_item, inter.redFlags, inter.redFlagsOther, inter.difficult_situation, inter.additionalObservation," \
            " inter.additionalObservationother, i.person_interviewed, i.interview_date_time, inter.interviewerInput," \
            " inter.vroFeedback, trial_master.trial_period_feedback, greatest(i.temp_ts, p.temp_ts," \
            " p_extn.temp_ts) as greatest_modified_date, sp_comment.spComments, " \
            " sp_health.doctor_approval, sp_health.doctor_assigned, sp_health.doctor_screening, sp_medical.height, sp_medical.sp_weight, " \
            " p_extn.areasContribute, i.call_status, i.pre_reg_cancellation_date, i.pre_reg_cancellation_reason, " \
            " p.criminal_history, i.createdAt, i.reg_email_status, p_extn.expertiseAreas, i.full_form_status, i.ha_form_sent, " \
            " i.ha_form_submitted, sp_health.submitted_on, p.incarceration_details, i.ie_program_check, i.ie_location, i.ie_online_status, " \
            " i.interview_status, inter.interviewerOpinion, p_extn.legalCaseExist, i.is_matched, i.countryCode, inter.noResponseCounter, " \
            " p_extn.otherLiabilities, p.past_medication, i.application_stage_pending_reason, p.physical_ailments, i.pre_registration_status, " \
            " inter.reviewStatus, i.sp_form_sent_date, i.sdp_tagged_comments, i.sdp_tagged_status, p_extn.expertiseAreas, p_extn.skillyouHave, " \
            " i.sp_reference_id, i.trail_period_status, i.utmCampaign, i.utmSource, i.utmMedium, i.ie_teacher_name, " \
            " inter.vroFeedback, i.zip_code, p.passport_no, p.passport_city_of_issue, p.passport_country_of_issue, p.passport_issue_date, " \
            " p.passport_expiry_date, p.other_practices, i.user_agent, i.user_registered_ip, i.registration_source, i.utmTerm, i.utmContent " \
            " from sadhanapada_interest_people i left join isha_yogacenter_person p " \
            " on i.person_id = p.id left join isha_yogacenter_person_extn p_extn on i.id = p_extn.sadhanapadaid left join " \
            " isha_yogacenter_sadhanapada_participant_master p_master on i.id = p_master.sadhanapada_id left join" \
            " isha_yogacenter_sadhanapada_interviewer inter on i.id = inter.sadhanapadaid " \
            " left join isha_yogacenter_sadhanapada_trial_master trial_master on i.id = trial_master.sadhanapada_id " \
            " left join isha_yogacenter_sadhanapada_comments sp_comment on i.id = sp_comment.sadhanapadaid " \
            " left join isha_yogacenter_sadhanapada_health_master sp_health on i.id = sp_health.sadhanapada_id " \
            " left join isha_yogacenter_sadhanapada_medical_details sp_medical on i.id = sp_medical.sadhanapadaid "
    # print('query: ' + query)
    logging.info('Mysql query printed')
    cur.execute(query)
    return cur.fetchall()


def converter(o):
    if isinstance(o, datetime):
        return o.__str__()


# Push messages to kafka
def pushToKafka(data):
    for x in data:
        if x['arrival_date']:
            x['arrival_date'] = str(x['arrival_date'])
        if x['departure_date']:
            x['departure_date'] = str(x['departure_date'])
        if x['interview_date_time']:
            x['interview_date_time'] = str(x['interview_date_time'])
        x = json.dumps(x, default=converter)
        p.produce(kafka_topic, x.encode('utf-8'), callback=delivery_report)
        p.flush(3)


# Log the Push status
def logStats():
    logging.info('time: ' + str(datetime.now()))
    logging.info('\n')

i = 0
while True:
    try:
        if i == 0:
            myConnection = pymysql.connect(host=hostname, user=username, passwd=password, db=database,
                                                     cursorclass=pymysql.cursors.DictCursor)
            data = doQuery(myConnection)
            print('# records found: ' + str(len(data)))
            pushToKafka(data)
            logStats()
            time.sleep(200)
            myConnection.close()
            i = 1
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        logging.info(tb_ex)
        print(tb_ex)
        pass
