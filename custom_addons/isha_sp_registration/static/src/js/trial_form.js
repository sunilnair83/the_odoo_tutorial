odoo.define('isha_sp_registration.TrialForm', function (require) {
	"use strict";

	var BaseForm = require('isha_website_utils.BaseForm');
	var TrialForm = BaseForm.extend({});
	var trialForm = new TrialForm({});

	$('.btn-save').on('click', function() {
		let $form = $('form');
		trialForm.validateAndSaveForm($form, false);
	});
});
