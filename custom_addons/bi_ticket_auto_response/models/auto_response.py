# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

class AutoResponse(models.Model):
    _name = "ticket.autoresponse"

    name = fields.Char(string='Stage Name')
    stage_id = fields.Many2one('support.stage',string='Stages',required=True,track_visibility='onchange',index=True)
    support_team = fields.Many2one('support.team',string='Helpdesk Team')
    is_folded = fields.Boolean(string='Folded in Helpdesk',default=False)
    auto_response = fields.Boolean(string='Needed AutoResponse??',default=False)
    response_template_id = fields.Many2one('mail.template',string='Response Template',required=True,\
        default=lambda self: self.env['mail.template'].search([('name','=','Assinged Ticket stages')]))
    is_customer = fields.Boolean(string='Send to Customer??',default=False)
    is_follower = fields.Boolean(string='Send to Followers??',default=False)
    is_user = fields.Boolean(string='Send to Users??',default=False)
    internal_user = fields.Many2many('res.users',string='Internal Users??',default=False)


class HelpdeskSupport(models.Model):
    _inherit = 'support.ticket'

    mail_list = fields.Many2many('res.partner',string='list',default=False)

    def write(self, vals):
        autoresponse_ids = self.env['ticket.autoresponse'].search([])
        if 'stage_id' in vals:
            stage = vals['stage_id']
            value = self.env['support.stage'].browse(stage)     
            for autoresponse in autoresponse_ids:
                email_to = []
                if autoresponse.auto_response == True:
                    if autoresponse.stage_id.id == value.id:
                        if autoresponse.is_customer == True:
                            if self.partner_id:
                                email_to.append(self.partner_id.id)
                                
                        if autoresponse.is_follower == True:
                            for follower in self.message_follower_ids:
                                email_to.append(follower.partner_id.id)
                                
                        if autoresponse.is_user == True:
                            if self.user_id:
                                email_to.append(self.user_id.partner_id.id)
                                
                            if self.team_leader_id:
                                email_to.append(self.team_leader_id.partner_id.id)
                                
                        if autoresponse.support_team.id == self.support_team_id.id:
                            for partner in autoresponse.support_team.team_member:
                                if partner.user_id:
                                    email_to.append(partner.user_id.partner_id.id)
                                    
                        if autoresponse.internal_user:
                            for user in autoresponse.internal_user:
                                email_to.append(user.partner_id.id)
                        
                        val = self.env['support.stage'].search([('name','=',autoresponse.stage_id.name)])
                        
                        if autoresponse.is_folded:
                            if value.name == val.name:
                                val.write({'fold' : True})
                        old_stage = self.stage_id.name
                        new_stage = value.name
                        ticket = self.sequence
                        auther = self.user_id.partner_id
                        recipient_ids = list(set(email_to))
                        template_id = autoresponse.response_template_id
                        send = template_id.sudo().with_context(auther=auther,ticket=ticket,old_stage=old_stage,new_stage=new_stage,recipient_ids=recipient_ids).send_mail(self.id,force_send=True)
        return super(HelpdeskSupport,self).write(vals)


class MailTemplate(models.Model):
    _inherit = 'mail.template'

    def send_mail(self, res_id, force_send=False, raise_exception=False, email_values=None):
        res = super(MailTemplate, self).send_mail(res_id, force_send=False, raise_exception=False, email_values=None)
        
        if self._context.get('recipient_ids'):
            ticket = self._context.get('ticket')
            old = self._context.get('old_stage')
            new = self._context.get('new_stage')            
            self.env['mail.mail'].sudo().browse(res).recipient_ids = [(6,0,self._context.get('recipient_ids'))]
            self.env['mail.mail'].sudo().browse(res).author_id = self._context.get('auther').id # [(6,0,[self._context.get('attachment').id])]
            self.env['mail.mail'].sudo().browse(res).body_html  = ("""<p>Hello</p><p>Ticket number %s has been change from %s to %s. </p>""" % (str(ticket),str(old),str(new)))
        return res
