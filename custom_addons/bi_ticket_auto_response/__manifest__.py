# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk support Ticket Auto Response in Odoo',
    'version': '13.0.0.1',
    'category' : 'Website',
    'summary': 'Helpdesk Ticket Auto Response System Ticket Email Auto Notification Helpdesk Support Tickets by Stages support ticket email auto notification auto response to issue  issue auto notification ticket automated response automated support email auto notify',
    'description': """Helpdesk Ticket Auto Response System
	
	Ticket Email Auto Notification
Helpdesk Support Tickets by Stages
support tickect email auto notification
auto response to issue 
issue auto notification
automated response
automated answer
automated support email

auto notification support ticket

	
	
	 """,
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 29,
    'currency': 'EUR',
    'depends': ['base','website',
                # 'website_sale',
                'bi_website_support_ticket'],
    'data':[
        'security/ir.model.access.csv',
        'data/auto_response_email_data.xml',
        'views/auto_response_views.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url': 'https://youtu.be/eFCMi68HaA4',
    'images':['static/description/Banner.png'],
}
