
from odoo import api, fields, models, tools, _

import logging
_logger = logging.getLogger(__name__)

class SupportCategory(models.Model):
    _name = "support.ticket.category"
    _description = "Event"

    color = fields.Integer(string='Color Index')
    name = fields.Char('Event', required=True, translate=True)
    user_ids = fields.Many2many('res.users',string='Events')
