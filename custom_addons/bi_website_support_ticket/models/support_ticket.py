# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
import logging
import random
import threading
import traceback
from datetime import datetime

from odoo import api, fields, models, tools, _, registry, SUPERUSER_ID
from odoo.exceptions import UserError, AccessError
from odoo.tools.misc import split_every

_logger = logging.getLogger(__name__)


class support_ticket(models.Model):

    _name = "support.ticket" 
    _inherit = ['portal.mixin', 'mail.thread', 'utm.mixin', 'rating.mixin', 'mail.activity.mixin']
    _order = 'date_create desc, id desc'
    _description = "Support Ticket"     
    
    def set_to_close(self):
        stage_obj = self.env['support.stage'].search([('name','=','Closed')])
        a = self.write({'stage_id':stage_obj.id,'is_ticket_closed':True,'date_close':datetime.now()})
        return a
        
    def set_to_reset(self):
        stage_obj = self.env['support.stage'].search([('name','=','New')])
        a = self.write({'stage_id':stage_obj.id,'is_ticket_closed':False})
        return a        
        
    @api.model 
    def default_get(self, flds): 
        result = super(support_ticket, self).default_get(flds)
        stage_nxt1 = self.env['ir.model.data'].xmlid_to_object('bi_website_support_ticket.support_stage') 
        result['stage_id'] = stage_nxt1.id
        result['is_ticket_closed'] = False
        return result
        
    def stage_find(self):
        return self.env['support.stage'].search([], limit=1).id
            
    def _get_attachment_count(self):
        for ticket in self:
            attachment_ids = self.env['ir.attachment'].search([('support_ticket_id','=',ticket.id)])
            ticket.attachment_count = len(attachment_ids)
        
    def attachment_on_support_ticket_button(self):
        self.ensure_one()
        return {
            'name': 'Attachment.Details',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'ir.attachment',
            'domain': [('support_ticket_id', '=', self.id)],
        }
    
    def _get_invoice_count(self):
        for ticket in self:
            invoice_ids = self.env['account.move'].search([('invoice_support_ticket_id','=',ticket.id)])
            ticket.invoice_count = len(invoice_ids)
        
    def invoice_button(self):
        self.ensure_one()
        return {
            'name': 'Invoice',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'account.move',
            'domain': [('invoice_support_ticket_id', '=', self.id)],
        }
            
    def _active_ticket(self):
        for s_id in self:
            # ticket_ids = self.env['project.task'].search([('ticket_id','=',s_id.id)])
            # count = len(ticket_ids)
            s_id.task_count = 0
        return
        
    def task(self):
        ticket = {}
        # task_obj = self.env['project.task']
        # ticket_ids = task_obj.search([('ticket_id','=',self.id)])
        # ticket1 = []
        # for ticket_id in ticket_ids:
        #     ticket1.append(ticket_id.id)
        # if ticket_ids:
        #     ticket = self.env['ir.actions.act_window'].for_xml_id('project', 'action_view_task')
        #     ticket['domain'] = [('id', 'in', ticket1)]
        return ticket
        
    def _get_avg_ticket_rating(self):
        for review_obj in self:
            avg_ticket_rating = 0.0
            total_messages = len( [x.id for x in review_obj.reviews_ids if x.message_rate > 0] )
            if total_messages > 0:
                total_rate = sum( [x.message_rate for x in review_obj.reviews_ids] )
                avg_ticket_rating = Decimal( total_rate ) / Decimal( total_messages )
            review_obj.avg_ticket_rating = avg_ticket_rating
            
    @api.onchange('support_team_id')
    def onchange_team_id(self):
        res = {}
        if self.support_team_id:
            res = {'user_id': self.support_team_id.user_id,'team_leader_id':self.support_team_id.team_leader,'support_ticket_category':self.support_team_id.support_ticket_category}
        return {'value': res}

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        res = {}
        if self.partner_id:
            res = {'phone': self.partner_id.phone,'email_from':self.partner_id.email,'contact_name':self.partner_id.name}
        return {'value': res}
        
    def change_level(self):
        val = {}
        healdesk_team_id = self.support_team_id
        if healdesk_team_id.parent_team_ids:
            parent_team = healdesk_team_id.parent_team_ids
            val = {
                    'user_id' : parent_team.user_id,
                    'level' : parent_team.level,
                    'parent_team_ids' : [(6,0,parent_team.parent_team_ids.ids)],
                    'team_leader' : parent_team.team_leader
            }
            healdesk_team_id.update(val)
            self.user_id = val['user_id']
            self.team_leader_id = val['team_leader']
        else:
            raise UserError(_('You are already in Parent Team...!!'))
        return 

    def assign_to_manager(self):
        team_manager = self.team_member.filtered(lambda x: x.has_group('bi_website_support_ticket.group_support_manager')
                                                            and not(x.has_group('bi_website_support_ticket.group_support_admin')
                                                                    or x.has_group('bi_website_support_ticket.group_support_event_admin')))

        if len(team_manager)!= 0:
            random_manager = random.choice(team_manager)
            self.write({'user_id': random_manager.id})

        return

    def create_invoice(self):
        account_invoice_obj  = self.env['account.move']
        support_invoice_obj = self.env['support.invoice']
        lines = []

        if not self.invoice_option:
            raise UserError(_('Please select Invoice Policy...'))
        else:

            if self.invoice_option == "manual":
                    for inv_line in self.support_invoice_id:
                        lines.append((0,0,{
                                        'product_uom_id': inv_line.uom_id.id,
                                        'name': inv_line.name,
                                        'partner_id':self.partner_id.id,
                                        'quantity':inv_line.quantity,
                                        'price_unit':inv_line.price_unit,
                                        }))
            elif self.invoice_option == "timesheet": 
                if not self.emp_timesheet_cost:
                    raise UserError(_('Please select Timesheet Cost...'))
                else:
                    if not self.timesheet_ids:
                        raise UserError(_('Please Add Employee Timesheet...'))
                    else:
                        if self.emp_timesheet_cost == "employee_cost":
                            for emp_timesheet_id in self.timesheet_ids:
                                lines.append((0,0,{
                                                'product_uom_id': emp_timesheet_id.product_uom_id.id,
                                                'name': emp_timesheet_id.name,
                                                'partner_id':self.partner_id.id,
                                                'quantity':emp_timesheet_id.unit_amount,
                                                'price_unit':emp_timesheet_id.employee_id.timesheet_cost,
                                                }))
                        else:
                            for emp_timesheet_id in self.timesheet_ids:
                                lines.append((0,0,{
                                                'product_uom_id': emp_timesheet_id.product_uom_id.id,
                                                'name': emp_timesheet_id.name,
                                                'partner_id':self.partner_id.id,
                                                'quantity':emp_timesheet_id.unit_amount,
                                                'price_unit':self.manual_cost,
                                                }))                        
            invoice_vals = {
            'type': 'out_invoice',
            'partner_id': self.partner_id.id,
            'currency_id': self.partner_id.currency_id.id,
            'invoice_support_ticket_id':self.id,
            'invoice_line_ids': lines,
            }

            res = self.env['account.move'].create(invoice_vals)
            if res.invoice_support_ticket_id:
                res.invoice_support_ticket_id.write({'invoice_id': res.id})

        return
    
    def _is_assigned_support_ticket(self):
        for rec in self:
            if rec.stage_id.name == "Assigned":
                rec.is_assigned = True
            else:
                rec.is_assigned = False
            return 
    
    @api.model
    def create(self, vals):
        if vals.get('sequence', _('Ticket')) == _('Ticket'):
            vals['sequence'] = self.env['ir.sequence'].next_by_code('support.ticket') or _('Ticket')
        if str(vals.get('date_close')) < str(vals.get('date_create')):
            raise UserError(_('Close Date must be greater than Create Date'))

        return super(support_ticket, self).create(vals)

    def write(self, vals):
        if 'date_close' in  vals:
            if str(vals.get('date_close')) < str(self.date_create):
                raise UserError(_('Close Date must be greater than Create Date'))
        if 'stage_id' in vals:
            if vals.get('stage_id'):
                stage_id = self.env['support.stage'].browse(vals.get('stage_id'))
                if stage_id.name == "Closed":
                    vals.update({
                        'stage_id' : stage_id.id,
                        'is_ticket_closed':True,
                        'date_close':datetime.now()
                        })
        return super(support_ticket, self.sudo()).write(vals)

    # @api.depends('timesheet_ids')
    # def _calculate_total_hours(self):
    #     for ticket in self:
    #         Hours = 0.0
    #         if ticket.timesheet_ids:
    #             for line in ticket.timesheet_ids:
    #                 Hours += line.unit_amount
    #         ticket.spend_hours = Hours


    ''' Website support ticket Field '''
    partner_id = fields.Many2one('res.partner', string="Contact")
    active = fields.Boolean(string='active',default=True)
    sequence = fields.Char(string='Sequence', readonly=True,copy=False,index=True,default=lambda self: _('Ticket'))
    id = fields.Integer('ID', readonly=True)
    email_from = fields.Char('Email', size=128, help="Destination email for email gateway.")
    phone = fields.Char('Phone')
    contact_name = fields.Char(string='Contact Name')
    category = fields.Many2one('support.ticket.type',string="Category")
    name = fields.Char('Subject', required=True)
    description = fields.Text('Description')
    priority = fields.Selection([('0','Low'), ('1','Normal'), ('2','High')], 'Priority')
    stage_id = fields.Many2one ('support.stage', 'Stage', track_visibility='onchange', index=True)
    team_member = fields.Many2many(related='support_team_id.team_member')
    user_id = fields.Many2one('res.users','Assigned to',index=True)
    # tag_ids = fields.Many2many('project.tags', string='Tags')
    support_team_id = fields.Many2one('support.team',string="Helpdesk Team")
    date_create = fields.Datetime(string="Create Date" , default=lambda self: fields.Datetime.now())
    date_close = fields.Datetime(string="Close Date")
    last_chatter_update = fields.Datetime(string="Last Communication On")
    task_count =  fields.Integer(compute='_active_ticket',string="Tasks")
    # timesheet_ids = fields.One2many('account.analytic.line', 'ticket_id', 'Timesheets')
    # project_id = fields.Many2one('project.project',string="Project")
    # analytic_id = fields.Many2one('account.analytic.account',string="Analytic Account")
    is_ticket_closed = fields.Boolean(string="Is Ticket Closed",readonly=True)
    spend_hours = fields.Monetary("Total Spend Hours", store=True, readonly=True, compute = "_calculate_total_hours")    
    company_id = fields.Many2one('res.company',string="Company" , required = True , default=lambda self: self.env.company)
    support_ticket_category = fields.Many2one('support.ticket.category',string="Event" , default=lambda self: self.env.user.support_ticket_category[0] if len(self.env.user.support_ticket_category)>0 else False)
    attachment_count  =  fields.Integer('Attachments', compute='_get_attachment_count')
    team_leader_id = fields.Many2one('res.users',string="Team Leader" , related = "support_team_id.team_leader")
    customer_rating = fields.Selection([('1','Poor'), ('2','Average'), ('3','Good'),('4','Excellent')], 'Contact Rating')
    comment = fields.Text(string="Comment")
    invoice_option = fields.Selection([('timesheet','Timesheet'),('manual','Manual')],string="Invoice Policy",default="timesheet")
    emp_timesheet_cost = fields.Selection([('employee_cost','Employee Cost'),('manual_cost','Manual Employee Cost')],string="Timesheet Cost",default="employee_cost")
    # support_invoice_id = fields.One2many('support.invoice','support_invoice_id',string="Invoice")
    manual_cost = fields.Monetary(string="Manual Employee Cost",currency_field='currency_id', default=0.0)
    currency_id = fields.Many2one('res.currency', related='company_id.currency_id', readonly=True)
    # invoice_count  =  fields.Integer('Invoices Count', compute='_get_invoice_count')
    is_assigned = fields.Boolean(compute='_is_assigned_support_ticket',string='Is Assigned',default=False)
    # invoice_id = fields.Many2one('account.move', string='Support Invoices', copy=False)
    ticket_source = fields.Selection([('email','Email'),('ivr','IVR'),('tech','Tech Interface')],string='Ticket Source',
                                     default='email', index=True)
    contact_feedback = fields.Selection([('warm','Warm'),
                                      ('luke_warm','Luke Warm'),
                                      ('cold','Cold'),
                                      ('not_interested','Not Interested')],
                                     string="Contact Feedback", copy=False)
    exotel_sid = fields.Char(string='Exotel sid',index=True)
    dial_call_status = fields.Char(string='Call Status')
    call_type = fields.Char(string='Call Type')
    matched_partners = fields.Many2many('res.partner',relation='support_ticket_res_partner_rel',index=True,string='Matched Contacts')

    def get_global_search(self):
        print('called')
        return {
            'name': _('Global Search'),
            'view_mode': 'tree,form',
            'view_id': False,
            'views': [(False, 'tree'),
                      (False, 'form')],

            'res_model': 'res.partner',
            'type': 'ir.actions.act_window',
            'context': {'no_default_search': True},
            # 'help':"<p class='o_view_nocontent_smiling_face'>Please search for Contacts...<\/p>",
            'target': 'current'
        }

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(support_ticket, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False  # to hide in Add Custom filter view
                res[field]['sortable'] = False  # to hide in group by view
                res[field]['exportable'] = False  # to hide in export list
                res[field]['store'] = False  # to hide in 'Select Columns' filter in tree views
        return res

    #DVE FIXME: if partner gets created when sending the message it should be set as partner_id of the ticket.
    def _message_get_suggested_recipients(self):
        recipients = super(support_ticket, self)._message_get_suggested_recipients()
        recipients[self.id] = []
        try:
            for ticket in self:
                if ticket.partner_id and ticket.partner_id.email:
                    ticket._message_add_suggested_recipient(recipients, partner=ticket.partner_id, reason=_('Contact Ticket'))
                elif ticket.email_from:
                    ticket._message_add_suggested_recipient(recipients, email=ticket.email_from, reason=_('Contact Ticket Email'))
        except AccessError:  # no read access rights -> just ignore suggested recipients because this implies modifying followers
            pass
        return recipients

    def _ticket_email_split(self, msg):
        email_list = tools.email_split((msg.get('to') or '') + ',' + (msg.get('cc') or ''))
        # check left-part is not already an alias
        return  [
            x for x in email_list
            if x.split('@')[0] not in self.mapped('support_team_id.alias_name')
        ]

    def get_email(self, email):
        start = '<'
        end = '>'
        return email[email.find(start)+len(start):email.rfind(end)]

    def get_mail_thread_date_formated(self, msg):
        if msg.get('date_hdr', False):
            return 'On ' + msg.get('date_hdr') + ' '
        date_val = msg.get('date',False)
        date_obj = datetime.strptime(date_val, '%Y-%m-%d %H:%M:%S') if date_val else datetime.now()
        return date_obj.strftime("On %a, %b %d, %Y at %-I:%M %p (UTC) ")

    def construct_thread_body(self, msg):
        try:
            thread_date = self.get_mail_thread_date_formated(msg)
            thread_header = '<div dir="ltr">'+thread_date+msg.get('from').replace('<','').replace('>','')+' wrote:</div>'
            thread_body = '<blockquote style="margin:0px 0px 0px 0.8ex; padding-left:1ex" data-o-mail-quote="1">'+msg['body']+'</blockquote>'
            msg['body'] = thread_header+thread_body
        except:
            pass
    @api.model
    def message_new(self, msg, custom_values=None):
        model = self._context.get('thread_model') or self._name
        try:
            self.construct_thread_body(msg)
        except:
            pass
        if not ' ' in msg.get('from'):
            user_name= msg.get('from')
        else:
            try:
                name = msg.get('from').split(" ")
                fname = name[0]
                lname = None
                if len(name) >= 2:
                    lname = name[1]
                user_name = ' '.join(filter(None,[fname,lname])).replace('"','')
            except:
                user_name = msg.get('from').split(" ")[0]
        if not '<' in msg.get('from'):
            user_email = msg.get('from')
        else:
            user_email = self.get_email(msg.get('from'))
        mail_server_email = False
        if self._context.get('default_fetchmail_server_id',False):
            try:
                mail_server_email = self.env['fetchmail.server'].sudo().browse(self._context.get('default_fetchmail_server_id',False)).user
            except Exception as ex:
                _logger.error('Fetch Mail server browser error')
        if not mail_server_email:
            if '<' in msg.get('to'):
                mail_server_email = self.get_email(msg.get('to')).lower()
            else:
                mail_server_email = msg.get('to').split(',')[0].lower().strip()
        if model == 'support.ticket':
            support_team = self.env['support.mail.mapping'].search([('email','=ilike',mail_server_email)])
            if len(support_team)==0:
                support_team = self.env.ref('bi_website_support_ticket.common_support_team')
                team_id = support_team.id
                category_id = support_team.support_ticket_category.id
            else:
                team_id = support_team.team_id.id
                category_id = support_team.category_id.id
            res_partners = self.env['res.partner'].search(['|','|',('email', '=', user_email),('email2', '=', user_email),('email3', '=', user_email)],limit=5)
            if res_partners:
                values = dict(custom_values or {},last_chatter_update=datetime.now(), matched_partners=res_partners, email_from=user_email,contact_name=user_name, support_team_id = team_id, support_ticket_category=category_id)
            else:
                # cc_name = user_email.split('@')
                # partner_id = self.env['res.partner'].create({
                #             'name' : str(user_name),
                #             'email': str(user_email)
                #             })
                values = dict(custom_values or {},last_chatter_update=datetime.now(), contact_name=user_name,email_from=user_email, support_team_id = team_id, support_ticket_category=category_id)
            return super(support_ticket, self.sudo().with_context(incoming_mail_server=True)).message_new(msg, custom_values=values)

        return super(support_ticket, self).message_new(msg, custom_values= custom_values)

    def message_update(self, msg, update_vals=None):
        # partner_ids = [x for x in self._mail_find_partner_from_emails(self._ticket_email_split(msg)) if x]
        # if partner_ids:
        #     self.message_subscribe(partner_ids)
        try:
            self.construct_thread_body(msg)
        except:
            pass
        return super(support_ticket, self).message_update(msg, update_vals=update_vals)

    def _message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None, customer_ids=None):
        return super(support_ticket,self)._message_subscribe(partner_ids, channel_ids, subtype_ids, customer_ids)

    def _message_post_after_hook(self, message, *args, **kwargs):
        # self = self.sudo()
        # for rec in self:
        #     recs = self.env['mail.followers'].sudo().search([
        #         ('res_model', '=', 'support.ticket'),
        #         ('res_id', '=', rec.id)
        #     ])
        #     recs.unlink()

        # if self.email_from and self.partner_id and not self.partner_id.email:
        #     self.partner_id.email = self.email_from
        #
        # if self.email_from and not self.partner_id:
        #     # we consider that posting a message with a specified recipient (not a follower, a specific one)
        #     # on a document without customer means that it was created through the chatter using
        #     # suggested recipients. This heuristic allows to avoid ugly hacks in JS.
        #     new_partner = message.partner_ids.filtered(lambda partner: partner.email == self.email_from)
        #     if new_partner:
        #         self.search([
        #             ('partner_id', '=', False),
        #             ('email_from', '=', new_partner.email),
        #             ('stage_id.fold', '=', False)]).write({'partner_id': new_partner.id})
        try:
            if self._context.get('incoming_mail_server', None):
                update_vals = {'last_chatter_update': datetime.now().strftime('%Y-%m-%d %H:%M:%S')}
                # if ticket is closed then mark it as re-open else mark it as reply received
                if self.stage_id.id == self.env.ref('bi_website_support_ticket.support_stage7').id:
                    update_vals['stage_id'] = self.env.ref('bi_website_support_ticket.support_stage_reopen').id
                elif self.stage_id.id != self.env.ref('bi_website_support_ticket.support_stage').id:
                    update_vals['stage_id'] = self.env.ref('bi_website_support_ticket.support_stage_reply_received').id
                    update_vals['is_ticket_closed'] = False

                self.sudo().write(update_vals)
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)

        # print('post update hook')
        return super(support_ticket, self)._message_post_after_hook(message, *args, **kwargs)


    @api.model
    def get_empty_list_help(self, help):
        help_title, sub_title = "", ""
        if self._context.get('default_type') == 'lead':
            help_title = _('Create a new lead')
        else:
            help_title = _('Create an opportunity in your pipeline')
        alias_record = self.env['mail.alias'].search([
            ('alias_name', '!=', False),
            ('alias_name', '!=', ''),
            ('alias_model_id.model', '=', 'support.ticket'),
            ('alias_parent_model_id.model', '=', 'support.team'),
            ('alias_force_thread_id', '=', False)
        ], limit=1)
        if alias_record and alias_record.alias_domain and alias_record.alias_name:
            email = '%s@%s' % (alias_record.alias_name, alias_record.alias_domain)
            email_link = "<a href='mailto:%s'>%s</a>" % (email, email)
            sub_title = _('or send an email to %s') % (email_link)
        return '<p class="o_view_nocontent_smiling_face">%s</p><p class="oe_view_nocontent_alias">%s</p>' % (help_title, sub_title)


    def _notify_get_reply_to(self, default=None, records=None, company=None, doc_names=None):
        """ Override to set alias of tickets to their team if any. """
        aliases = self.mapped('support_team_id')._notify_get_reply_to(default=default, records=None, company=company, doc_names=None)
        res = {ticket.id: aliases.get(ticket.support_team_id.id) for ticket in self}
        leftover = self.filtered(lambda rec: not rec.support_team_id)
        if leftover:
            res.update(super(support_ticket, leftover)._notify_get_reply_to(default=default, records=None, company=company, doc_names=doc_names))
        return res

    def filter_support_ticket_per_user(self):
        ctx = self.env.context.copy()

        ctx.update({'search_default_email_from': self.email_from,
                    'default_email_from':self.email_from})
        return {
            'name': 'Support Ticket',
            'view_mode': 'tree,form,calendar,pivot,graph,activity',
            'res_model': 'support.ticket',
            'type': 'ir.actions.act_window',
            'context': dict(ctx),
            'search_view_id': self.env.ref('bi_website_support_ticket.view_support_ticket_filter').id
        }

    def _notify_record_by_email(self, message, recipients_data, msg_vals=False,
                                model_description=False, mail_auto_delete=True, check_existing=False,
                                force_send=True, send_after_commit=True,
                                **kwargs):
        """ Method to send email linked to notified messages.

        :param message: mail.message record to notify;
        :param recipients_data: see ``_notify_thread``;
        :param msg_vals: see ``_notify_thread``;

        :param model_description: model description used in email notification process
          (computed if not given);
        :param mail_auto_delete: delete notification emails once sent;
        :param check_existing: check for existing notifications to update based on
          mailed recipient, otherwise create new notifications;

        :param force_send: send emails directly instead of using queue;
        :param send_after_commit: if force_send, tells whether to send emails after
          the transaction has been committed using a post-commit hook;
        """
        partners_data = [r for r in recipients_data['partners'] if r['notif'] == 'email']

        generic_from_email = False
        server_id = None
        if self.support_team_id:
            server_id = self.env['support.mail.mapping'].sudo().search([('team_id', '=', self.support_team_id.id)])
            if not server_id:
                server_id = self.env['support.mail.mapping'].sudo().search(
                    [('category_id', '=', self.support_ticket_category.id)])
        if server_id:
            generic_from_email = server_id[0].reply_email
        if not server_id or not generic_from_email:
            generic_from_email = self.env['ir.config_parameter'].sudo().get_param('bi_website_support_ticket.help_desk_email_id')


        if not self._context.get('incoming_mail_server',None):
            # Sending mail from Portal directly with the email id as we don't create res_partner
            Mail = self.env['mail.mail'].sudo()
            mail_subject = message.subject or (
                        message.record_name and 'Re: %s' % message.record_name)  # in cache, no queries
            # prepare notification mail values
            base_mail_values = {
                'mail_message_id': message.id,
                'mail_server_id': message.mail_server_id.id,
                # 2 query, check acces + read, may be useless, Falsy, when will it be used?
                'auto_delete': mail_auto_delete,
                # due to ir.rule, user have no right to access parent message if message is not published
                'references': message.parent_id.sudo().message_id if message.parent_id else False,
                'subject': mail_subject,
            }

            base_mail_values = self._notify_by_email_add_values(base_mail_values)

            if not self._context.get('forward_mail', None):
                previous_mail = self.env['mail.message'].sudo().search([('id','!=',message.id),('res_id','=',self.id),('model','=',self._name),('subject','not ilike','FWD:%')]).sorted(key=lambda m: m.create_date, reverse=True)
                prev_body = ''
                if len(previous_mail)>=1:
                    prev_body = previous_mail[0].body
                message.body = message.body +'<br/><br/><blockquote style="margin:0px 0px 0px 0.8ex; padding-left:1ex" data-o-mail-quote="1">' + prev_body +'</blockquote>'

                mail_body = message.body
                mail_body = self._replace_local_links(mail_body)

                create_values = {
                    'body_html': mail_body,
                    'subject': mail_subject,
                    'email_from': generic_from_email,
                    'reply_to': generic_from_email
                }
                create_values.update({'email_to': self.email_from})
                create_values.update(base_mail_values)  # mail_message_id, mail_server_id, auto_delete, references, headers
                email = Mail.create(create_values)
                email.send()
            # return True

        model = msg_vals.get('model') if msg_vals else message.model
        model_name = model_description or (self.with_lang().env['ir.model']._get(model).display_name if model else False) # one query for display name
        recipients_groups_data = self._notify_classify_recipients(partners_data, model_name)

        if not recipients_groups_data:
            return True
        force_send = self.env.context.get('mail_notify_force_send', force_send)

        template_values = self._notify_prepare_template_context(message, msg_vals, model_description=model_description) # 10 queries

        email_layout_xmlid = msg_vals.get('email_layout_xmlid') if msg_vals else message.email_layout_xmlid
        template_xmlid = email_layout_xmlid if email_layout_xmlid else 'mail.message_notification_email'
        try:
            base_template = self.env.ref(template_xmlid, raise_if_not_found=True).with_context(lang=template_values['lang']) # 1 query
        except ValueError:
            _logger.warning('QWeb template %s not found when sending notification emails. Sending without layouting.' % (template_xmlid))
            base_template = False

        mail_subject = message.subject or (message.record_name and 'Re: %s' % message.record_name) # in cache, no queries
        # prepare notification mail values
        base_mail_values = {
            'mail_message_id': message.id,
            'mail_server_id': message.mail_server_id.id, # 2 query, check acces + read, may be useless, Falsy, when will it be used?
            'auto_delete': mail_auto_delete,
            # due to ir.rule, user have no right to access parent message if message is not published
            'references': message.parent_id.sudo().message_id if message.parent_id else False,
            'subject': mail_subject,
        }
        base_mail_values = self._notify_by_email_add_values(base_mail_values)

        Mail = self.env['mail.mail'].sudo()
        emails = self.env['mail.mail'].sudo()

        # loop on groups (customer, portal, user,  ... + model specific like group_sale_salesman)
        notif_create_values = []
        recipients_max = 50
        for recipients_group_data in recipients_groups_data:
            # generate notification email content
            recipients_ids = recipients_group_data.pop('recipients')
            render_values = {**template_values, **recipients_group_data}
            # {company, is_discussion, lang, message, model_description, record, record_name, signature, subtype, tracking_values, website_url}
            # {actions, button_access, has_button_access, recipients}

            # if base_template:
            #     mail_body = base_template.render(render_values, engine='ir.qweb', minimal_qcontext=True)
            # else:
            mail_body = message.body
            mail_body = self._replace_local_links(mail_body)

            # create email
            for recipients_ids_chunk in split_every(recipients_max, recipients_ids):
                recipient_values = self._notify_email_recipient_values(recipients_ids_chunk)
                email_to = recipient_values['email_to']
                recipient_ids = recipient_values['recipient_ids']

                create_values = {
                    'body_html': mail_body,
                    'subject': mail_subject,
                    'recipient_ids': [(4, pid) for pid in recipient_ids],
                    'email_from': generic_from_email,
                    'reply_to': generic_from_email
                }
                if email_to:
                    create_values['email_to'] = email_to
                create_values.update(base_mail_values)  # mail_message_id, mail_server_id, auto_delete, references, headers
                email = Mail.create(create_values)

                if email and recipient_ids:
                    tocreate_recipient_ids = list(recipient_ids)
                    if check_existing:
                        existing_notifications = self.env['mail.notification'].sudo().search([
                            ('mail_message_id', '=', message.id),
                            ('notification_type', '=', 'email'),
                            ('res_partner_id', 'in', tocreate_recipient_ids)
                        ])
                        if existing_notifications:
                            tocreate_recipient_ids = [rid for rid in recipient_ids if rid not in existing_notifications.mapped('res_partner_id.id')]
                            existing_notifications.write({
                                'notification_status': 'ready',
                                'mail_id': email.id,
                            })
                    notif_create_values += [{
                        'mail_message_id': message.id,
                        'res_partner_id': recipient_id,
                        'notification_type': 'email',
                        'mail_id': email.id,
                        'is_read': True,  # discard Inbox notification
                        'notification_status': 'ready',
                    } for recipient_id in tocreate_recipient_ids]
                emails |= email

        if notif_create_values:
            self.env['mail.notification'].sudo().create(notif_create_values)

        # NOTE:
        #   1. for more than 50 followers, use the queue system
        #   2. do not send emails immediately if the registry is not loaded,
        #      to prevent sending email during a simple update of the database
        #      using the command-line.
        test_mode = getattr(threading.currentThread(), 'testing', False)
        if force_send and len(emails) < recipients_max and (not self.pool._init or test_mode):
            # unless asked specifically, send emails after the transaction to
            # avoid side effects due to emails being sent while the transaction fails
            if not test_mode and send_after_commit:
                email_ids = emails.ids
                dbname = self.env.cr.dbname
                _context = self._context
                def send_notifications():
                    db_registry = registry(dbname)
                    with api.Environment.manage(), db_registry.cursor() as cr:
                        env = api.Environment(cr, SUPERUSER_ID, _context)
                        env['mail.mail'].browse(email_ids).send()
                self._cr.after('commit', send_notifications)
            else:
                emails.send()

        return True


class MailThread(models.AbstractModel):
    _inherit = 'mail.thread'

    @api.model
    def message_process(self, model, message, custom_values=None, save_original=False, strip_attachments=False, thread_id=None):
        if model == 'support.ticket':
            return super(MailThread, self.sudo().with_context(incoming_mail_server=True)).message_process(model, message, custom_values,
                        save_original, strip_attachments,thread_id)

        return super(MailThread, self).message_process(model, message, custom_values,
                        save_original, strip_attachments,thread_id)

    @api.model
    def message_parse(self, message, save_original=False):
        if self._context.get('incoming_mail_server',False):
            try:
                date_hdr = tools.decode_message_header(message, 'Date')
            except:
                date_hdr = False
            ret_val = super(MailThread, self).message_parse(message, save_original)
            ret_val['date_hdr'] = date_hdr
            return ret_val

        return super(MailThread,self).message_parse(message,save_original)