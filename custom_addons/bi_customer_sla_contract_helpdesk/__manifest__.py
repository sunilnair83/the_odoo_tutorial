# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Customer Sla Contract Helpdesk in Odoo',
    'version': '13.0.0.1',
    'category': 'Website',
    'summary': 'Customer Sla Contract Helpdesk Sla Contract Website helpdesk SLA contract website support SLA contract service level SLA contract support SLA service contract SLA Help desk Service Level Agreement contract customer Service Level Agreement contract SLA',
    'description': '''Customer Sla Contract Helpdesk
	Help Desk Service Level Agreement
service level SLA
helpdesk SLA
SLA helpdesk
support SLA
SLA
Service Level Agreement
Service-Level Agreement
Helpdesk Service Level Agreement
helpdesk Service-Level Agreement
helpdesk SLA
help desk SLA
SLA level
support Service-Level Agreement

customer SLA
client Sla
client Service-Level Agreement

Main Features:
- Allow to you to setup SLA Levels under configuration.

- Allow you to link SLA level on your Customer form.
client contracting in Odoo 
customer contracting 
service contracting 
customer service contract
contracting in Odoo
conctract with client 
helpdesk contract




SLA/Helpdesk SLA Levels
Helpdesk SLA Analysis
Helpdesk SLA Team
	
	''',
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 29,
    'currency': 'EUR',
    'depends':[
		'base','analytic','account','bi_website_support_ticket',
		'bi_helpdesk_service_level_agreement',
		'bi_contract_recurring_invoice_analytic'],
    'data':[
    	'views/helpdesk_agreement_report.xml',
    	'views/helpdesk_agreement_views.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url':'https://youtu.be/lzuLR_V9Nyk',
    'images':['static/description/Banner.png'],
}
