import logging

from datetime import datetime, timedelta
from odoo import models, fields

_logger = logging.getLogger(__name__)



class ProgramTransfers(models.Model):
    _name = "samskriti.program.transfers.history"
    _description = "Samskriti Program Transfer"

    name = fields.Many2one("program.lead")
    record_name = fields.Char("Name")

    current_program = fields.Char("Current Program", readonly=True)
    current_schedule = fields.Char("Current Schedule", readonly=True)
    current_batch = fields.Char("Current Batch", readonly=True)

    current_program_id = fields.Many2one("program.type.master", string="Current Program Id", store=True)
    current_schedule_id = fields.Many2one("program.schedule.master", string="Current Schedule Id",
                                          domain="[('program_type_id','=',current_program_id)]")
    current_batch_id = fields.Many2one("program.schedule.batch.master", string="Current Batch Id",
                                       domain="[('program_schedule_id','=',current_program_id)]")

    target_program = fields.Many2one("program.type.master", string="Target Program", store=True)
    target_schedule = fields.Many2one("program.schedule.master", string="Target Schedule",
                                      domain="[('program_type_id','=',target_program)]")
    target_schedule_date = fields.Char("Target Schedule Start Date", readonly=True)

    target_batch = fields.Many2one("program.schedule.batch.master", string="Target Batch",
                                   domain="[('program_schedule_id','=',target_schedule)]")

    amount = fields.Float("Amount Delta")
    currency = fields.Char()
    reg_link = fields.Char("Registration Link")
    record_email = fields.Char("Email")
    program_reg_id = fields.Char()

    has_expired = fields.Boolean("Expired", default=False)

    def _check_transfer_links(self):
        '''
        the method checks for expired links

        :return: none
        '''

        def update_status(recs):
            for rec in recs:
                rec.name.write({'reg_status': 'Transfer Link Expired'})
                rec.has_expired = True


        # fetching the payment link config
        payment_link_conf = self.env['samaskriti.payment.configuration'].sudo().search(
            [('is_program_transfer', '=', True)], limit=1)

        # if no payment link config found the log the error
        if not payment_link_conf:
            _logger.error("---------No link expiry config defined--------")
            return

        # invalidate all the payment links, if the specified date has reached
        if payment_link_conf.invalidate_all_payment_links_on and payment_link_conf.invalidate_all_payment_links_on < datetime.now().date():
            _logger.info("--------Invalidated all the transfer links-------")
            recs = self.sudo().search([('name.reg_status','=','Program Transfer Requested')])
            # the method updates the status of program.lead record
            update_status(recs)

            return

        # check for all the records before a specified date and mark them as expired
        if not payment_link_conf.payment_link_validity:
            date_to_check = datetime.now() - timedelta(days=payment_link_conf.payment_link_validity)
            recs = self.sudo().search([('name.reg_status','=','Program Transfer Requested'), ('name.create_date','<=',date_to_check)])

            update_status(recs)

            return




