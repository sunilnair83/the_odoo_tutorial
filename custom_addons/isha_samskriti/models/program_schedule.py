import logging
from datetime import datetime
import pytz

from odoo import api, models, fields, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class SamskritiBatchSchedule(models.Model):
    _inherit = "program.schedule.batch.master"

    no_of_seats = fields.Integer("Number of seats")
    seats_left = fields.Integer(
        "Seats left", help="Number of seats left for registrations"
    )
    conflicts = fields.Many2many(
        comodel_name="program.schedule.batch.master",
        relation="batch_conflict_rel",
        column1="base_batch",
        column2="conflicting_batch",
        string="Conflicting Batches",
    )

    def name_get(self):
        result = []
        for batch in self:
            name = f"{batch.program_schedule_id.name}|{batch.display_text}"
            result.append((batch.id, name))
        return result



class ProgramScheduleMaster(models.Model):
    _inherit = "program.schedule.master"

    program_type = fields.Selection(
        [("india", "India"), ("overseas", "Overseas")], string="Type"
    )
    batch_seats = fields.Integer("Default Batch Seats")

    @api.model
    def create(self, vals_list):
        rec = super(ProgramScheduleMaster, self).create(vals_list)

        # putting name against record using seq,
        # used for loop because multiple records can be created at same time
        for record in rec:
            pref = record.program_type_id.sm_seqPref
            seq = self.env["ir.sequence"].next_by_code("program.schedule.seq")
            name = pref and f"{pref}{seq}" or seq
            record.name = name

        return rec
