import datetime
import logging

from odoo import models, fields, _
from odoo.exceptions import ValidationError
import traceback

_logger = logging.getLogger(__name__)


class ProgramLead(models.Model):
    _inherit = "program.lead"

    sm_scheduleId = fields.Many2one(
        "program.schedule.master", string="Program Schedule"
    )
    sm_batchId = fields.Many2one(
        "program.schedule.batch.master", string="Program Schedule Batch"
    )

    sm_answers = fields.One2many(
        "survey.user_input_line", "sm_programLead", string="Answers",copy=True
    )
    is_confirmation_email = fields.Boolean(string="Confirmation Email Sent")
    confirmation_email_send_date = fields.Datetime(string='Confirmation email sent date')

    sm_batchIdText = fields.Char(related="sm_batchId.display_text", store=False,string='Batch Timing')
    sm_schdStartDate = fields.Date(related="sm_scheduleId.start_date", store=False)
    sm_schdEndDate = fields.Date(related="sm_scheduleId.end_date", store=False)

    is_payment_email = fields.Boolean(string="Payment Email Sent")
    payment_email_send_date = fields.Datetime(string='Payment email sent date')

    transfer_local_trans_id = fields.Char("Transferred Local Tnx Id",
                                         help="On creation of a new transfer record this field will get populated with new transaction id, whereas the old field will have the old id, in order to not break the gpms flow")
    transfer_rec = fields.Many2one("program.lead", string="Transferred Record")
    trans_rec_id = fields.Char()
    amount_delta = fields.Float("Amount Delta")
    transfer_currency = fields.Char()

    remarks = fields.Text(string="Remarks")

    move_registration_link = fields.Text(string="Remarks")
    transfer_email = fields.Char()
    transfer_status = fields.Char()
    name_transfer_id = fields.Char()
    # move_registration_link_sent_date = fields.Date()

    def cancel_logic(self):
        view = self.env.ref('isha_samskriti.samskriti_cancel_register_view')
        wiz = self.env['samskriti.register.cancel'].create({
            'register_id': self.id,
        })

        # TDE FIXME: a return in a loop, what a good idea. Really.

        return {
            'name': _('Cancel Register Form '),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'samskriti.register.cancel',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz.id,
            'context': self.env.context,
        }


    def show_checkin_email_template(self):
        self.ensure_one()
        registration_id = self.id
        program_lead_id = self.env['program.lead'].search([('id', '=', registration_id)],
                                                          limit=1)

        try:
            template_id = self.env.ref('isha_samskriti.mail_template_pgm_checkin')
        except ValueError:
            template_id = False
        if template_id.lang:
            lang = template_id._render_template(template_id.lang, 'program.lead',
                                                program_lead_id.id)
        ctx = {
            'default_model': 'program.lead',
            'default_res_id': self.id,
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            "replace_html": True,
            'force_email': True,
            'skip_model_change': True
        }
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(False, 'form')],
            'view_id': False,
            'target': 'new',
            'context': ctx,
        }

    def show_checkin_details_email_template(self):
        self.ensure_one()
        registration_id = self.id
        program_lead_id = self.env['program.lead'].search([('id', '=', registration_id)],
                                                          limit=1)

        try:
            template_id = self.env.ref('isha_samskriti.mail_template_pgm_chk_details')
        except ValueError:
            template_id = False
        if template_id.lang:
            lang = template_id._render_template(template_id.lang, 'program.lead',
                                                program_lead_id.id)
        ctx = {
            'default_model': 'program.lead',
            'default_res_id': self.id,
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            "replace_html": True,
            'force_email': True,
            'skip_model_change': True
        }
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(False, 'form')],
            'view_id': False,
            'target': 'new',
            'context': ctx,
        }

    def show_program_details_template(self):
        self.ensure_one()
        registration_id = self.id
        program_lead_id = self.env['program.lead'].search([('id', '=', registration_id)],
                                                          limit=1)

        try:
            template_id = self.env.ref('isha_samskriti.mail_template_pgm_details')
        except ValueError:
            template_id = False
        if template_id.lang:
            lang = template_id._render_template(template_id.lang, 'program.lead',
                                                program_lead_id.id)
        ctx = {
            'default_model': 'program.lead',
            'default_res_id': self.id,
            'default_use_template': bool(template_id.id),
            'default_template_id': template_id.id,
            'default_composition_mode': 'comment',
            "replace_html": True,
            'force_email': True,
            'skip_model_change': True
        }
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(False, 'form')],
            'view_id': False,
            'target': 'new',
            'context': ctx,
        }



    def contact_population_samskriti(self, limit):
        start = datetime.datetime.now()
        recs = self.env['program.lead'].sudo().search([('contact_id_fkey', '=', 2),
                                                       ('pgm_type_master_id', 'in', [
                                                           self.env.ref("isha_samskriti.kalaripayattu_prog_type").id,
                                                           self.env.ref(
                                                               "isha_samskriti.nirvana_shatakam_prog_type"
                                                           ).id,
                                                           self.env.ref("isha_samskriti.niraive_shakti_prog_type").id,
                                                           self.env.ref("isha_samskriti.nirbhay_nirgun_prog_type").id
                                                       ])], limit=limit)
        for rec in recs:
            try:
                schedule = eval(rec['program_schedule_info'])
                if 'pii' in schedule:
                    pii = schedule['pii']
                    if pii.get('whatsapp_number') and not pii.get('whatsapp_country_code'):
                        pii.pop('whatsapp_number')
                    res_partner = self.env['res.partner'].sudo().create(pii)
                    _logger.info("samskiriti pii cron " + str(res_partner))
                    rec.write({'contact_id_fkey': res_partner.id})
                    self.env.cr.commit()
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error("Error in samskriti contact cron " + tb_ex)

    def payment_resend_logic(self):
        '''
            Re-Send Payment Link to  participant if registration_status is payment_failed
        '''
        active_ids = self._context.get('active_ids')
        registration_id = self.id
        _logger.info("***** Registration ID (Isha Samaskriti) *******" + str(registration_id))
        program_lead_id = self.env['program.lead'].search([('id', '=', registration_id), ('reg_status', '=', 'Failed')],
                                                          limit=1)
        _logger.info("****** Resend Infor Lead Record(Isha Samaskriti) *******" + str(program_lead_id))
        # ('programregistration', '=', registration_id)

        # payment_active_records_rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search([("orderid", "=", program_lead_id.local_trans_id),
        #                                                                                                   ('billing_email','!=',False)],limit=1)
        req_payment_rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search(
            [('orderid', '=', program_lead_id.local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
        if not req_payment_rec:
            req_payment_rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search(
                [('orderid', '=', program_lead_id.local_trans_id), ('billing_email', '!=', False)], limit=1)
        payment_active_records_rec = req_payment_rec
        _logger.info("***** Resend Infor (Isha Samaskriti) *******" + str(payment_active_records_rec))
        if program_lead_id:
            if program_lead_id.sm_batchId.seats_left <= 0:
                raise ValidationError(_('No more seats avaiable for this batch.'))
        for i in payment_active_records_rec:
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            redirect_url = base_url + "/samaskriti/registrationform/paymentpage?registration_id=" + str(
                program_lead_id.id)
            # redirect_url = base_url + "/samaskriti/registrationform/paymentpage?registration_id=" + str(program_lead_id.id)+'&trans_id='+str(i.id)
            mail_template_id = self.env.ref('isha_samskriti.samaskriti_resend_payment_template')
            # reg_status = "In Progress"
            # program_lead_id.write({"reg_status": reg_status})
            if mail_template_id:
                try:
                    _logger.info(
                        "*****Inside mail template ID (Isha Samaskriti) ******" + str(i.id)
                    )
                    i.sudo().write({"payment_link": redirect_url, "payment_link_sent_date": fields.Date.today()})
                    mail_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(i.id,
                                                                                                      force_send=True)
                    if mail_rec:
                        program_lead_id.sudo().write(
                            {"is_payment_email": True, "payment_email_send_date": datetime.datetime.now()})
                    return True
                except Exception as e:
                    print(e)

    def email_resend_logic(self):
        '''
            Re-Send Email Link to  participant if registration_status is Completed
        '''
        registration_id = self.id
        _logger.info("***** Registration ID (Isha Samaskriti) *******" + str(registration_id))
        program_lead_id = self.env['program.lead'].search(
            [('id', '=', registration_id), ('reg_status', '=', 'Confirmed')], limit=1)
        _logger.info("****** Resend Information Lead Record (Isha Samaskriti) *******" + str(program_lead_id))
        payment_active_records_rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search(
            [('orderid', '=', program_lead_id.local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
        _logger.info("***** Resend Information (Isha Samaskriti) *******" + str(payment_active_records_rec))
        for i in payment_active_records_rec:
            # base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            # redirect_url = base_url + "/samaskriti/registrationform/paymentpage?registration_id=" + str(
            #     program_lead_id.id)
            # redirect_url = base_url + "/samaskriti/registrationform/paymentpage?registration_id=" + str(program_lead_id.id)+'&trans_id='+str(i.id)
            program_type = program_lead_id.sm_scheduleId.program_type_id.program_type
            _logger.info("********** payment_transaction.programtype (Isha TP) ************" + str(program_type))
            if program_type == 'Kalaripayattu':
                mail_template_id = self.env.ref('isha_samskriti.mail_template_payment_succes_kalari')
            else:
                mail_template_id = self.env.ref('isha_samskriti.mail_template_payment_succes_chant')

            if mail_template_id:
                try:
                    _logger.info("***** Inside mail template ID (Isha Samaskriti) ******" + str(i.id))
                    mail_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(i.id,
                                                                                                      force_send=True)
                    _logger.info("***** Mail record ID (Isha Samaskriti) ******" + str(mail_rec))
                    if mail_rec:
                        program_lead_id.sudo().write(
                            {"is_confirmation_email": True, "confirmation_email_send_date": datetime.datetime.now()})

                except Exception as e:
                    print(e)

    def open_transfer_wizard(self):
        return {'type': 'ir.actions.act_window',
                'name': _('Transfer Program'),
                'res_model': 'isha_samskriti.program_transfer',
                'target': 'new',
                'view_id': self.env.ref('isha_samskriti.sams_program_transfer_view_form').id,
                'view_mode': 'form',
                }

    def move_registration_link_fun(self):
        '''
        Send Registration Link to  participant Email
        '''
        if self.reg_status == 'Confirmed':
            view = self.env.ref('isha_samskriti.samskriti_move_register_view')
            sch_date = str(self.sm_scheduleId.start_date) + ' - ' + str(self.sm_scheduleId.end_date)
            schedule_date_time = self.sm_batchIdText
            wiz = self.env['samskriti.move.register'].create({
                'register_id': self.id,
                'lead_name': self.record_name,
                'programname':self.sm_scheduleId.program_type_id.program_type,
                'schedule_date':sch_date,
                'schedule_date_time':schedule_date_time,
                'previous_mail':self.record_email,
                # 'residence_country_code': self.country.code if self.country.code == 'IN' and self.nationality_id.code == 'IN' else "NONE"
            })

            # TDE FIXME: a return in a loop, what a good idea. Really.
            return {
                'name': _('Transfer Form '),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'samskriti.move.register',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }
        else:
            raise ValidationError(_("Name transfer process only applicable for Reg status as 'Confirmed'"))

    # Waiting for Approval logic (Program Transfer)
    def approve_action_done(self):
        # creating the re-registration url using the user inputs
        _logger.info("--------Program Transfer History Record -------"+str(self.trans_rec_id))
        trans_rec = self.env['samskriti.program.transfers.history'].sudo().search([('id','=',self.trans_rec_id)],limit=1)
        rec = self.env['isha_samskriti.program_transfer'].sudo().search([('id','=',trans_rec.program_reg_id)],limit=1)
        _logger.info("--------Program Transfer Wizard form Record -------"+str(rec))
        if rec.transfer_type == "IntraProgram":
            _logger.info("--------Program transfer-------")
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            url = f"{base_url}/registrations/samskriti/transfer?programname={rec.target_program.local_program_type_id}&schd={rec.target_schedule.id}&batch={rec.target_batch.id}&istransfer={True}&trans_rec={trans_rec.id}&test_sso_id={self.sso_id}"
            trans_rec.reg_link = url
            _logger.info("********** Trans rec- Email ************" + str(trans_rec))
            _logger.info("********** Trans rec- Email ID************" + str(trans_rec.record_email))
            # fetching the mail template and sending the re-registration email
            mail_template_id = self.env.ref("isha_samskriti.mail_template_pgm_transfer")
            mail_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(trans_rec.id, force_send=True)

            _logger.info("***** Transac Mail record ID ******" + str(mail_rec))
            # updating the status for the current record
            rec.name.reg_status = "Program Transfer Requested"
            rec.name.transfer_status = 'Program Transfer Requested'

    # Waiting for Approval logic (Name Transfer)
    def approve_name_transfer(self):
        move_Reg_rec = self.env['samskriti.move.register'].sudo().search([('id','=',self.name_transfer_id)],limit=1)
        _logger.info("******* Move Registration ID ****" + str(move_Reg_rec))
        if self.reg_status in ["Name Transfer Confirmed"]:
            raise ValidationError(
                _("Name transfer is already done once of this record, you can't request it again."))

        programname = self.sm_scheduleId.program_type_id.program_type

        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        redirect_url = f"{base_url}/registrations/samskriti/transfer?programname={programname}&rec_id={self.id}&is_name_transfer={True}"
        _logger.info("******* Move Registration Link URL ****" + str(redirect_url))

        move_Reg_rec.move_registration_link = redirect_url
        move_Reg_rec.programname = programname

        self.sudo().write({
            "reg_status": "Name Transfer Requested",
            "transfer_status":'Name Transfer Requested',
            "transfer_email": move_Reg_rec.move_email,
            "move_registration_link": redirect_url,
        })
        mail_template_id = self.env.ref('isha_samskriti.move_registration_mail_template')

        try:
            mail_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(move_Reg_rec.id,
                                                                                              force_send=True)
            _logger.info("***** Mail record ID - Move Register ******" + str(mail_rec))

        except Exception as e:
            print(e)

        return True
