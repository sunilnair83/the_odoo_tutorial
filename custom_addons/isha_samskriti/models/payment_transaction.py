from odoo import models, fields, api


class SamskritRegistrationConfiguration(models.Model):
    _name = "samaskriti.payment.configuration"
    _description = "Smaskriti Payment Configuration"

    invalidate_all_payment_links_on = fields.Date(
        string="Invalidate all payment links on "
    )
    payment_link_validity = fields.Integer(
        string="Validity Number of days for individual payment links"
    )

    is_program_transfer = fields.Boolean("Is Program Transfer",
                                         help="The check will acts as identifier to determine if payment config is for program transfer or not")


class SamaskritiPaymentTransaction(models.Model):
    _name = "samaskriti.participant.paymenttransaction"
    _description = "Samskriti Template Program Participant Payment Transaction"

    programregistration = fields.Many2one("program.lead", string="Registration ID")
    first_name = fields.Char(string="first_name")
    last_name = fields.Char(string="last_name")
    email = fields.Char(string="email")
    programtype = fields.Char(string="programtype")
    programname = fields.Char(string="programname")
    # ereceiptgenerated = fields.Boolean(related="programregistration.ereceiptgenerated",store=False)
    # ereceiptreference = fields.Char(related="programregistration.ereceiptreference",store=False)
    # ereceipturl = fields.Char(related="programregistration.ereceipturl",store=False)
    ereceiptgenerated = fields.Boolean(default=False)
    ereceiptreference = fields.Char(string="Receipt No", size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=700)
    ereceiptgenerated_dt = fields.Datetime(string="Receipt No Raised Date")
    orderid = fields.Char(string="Transaction ID", size=150)
    old_local_trans_id = fields.Char(string="Old Local Trans ID", size=150)
    billing_name = fields.Char(string="Participant Name", size=60)
    billing_address = fields.Char(string="Address", size=150)
    billing_city = fields.Char(string="City", size=30)
    billing_state = fields.Char(string="State", size=30)
    billing_zip = fields.Char(string="Zip", size=15)
    billing_country = fields.Char(string="Country", size=50)
    billing_tel = fields.Char(string="Mobile Number", size=20)
    billing_email = fields.Char(string="Email", size=70)
    payment_status = fields.Char(string="Payment Status", size=150)
    amount = fields.Float(string="Amount")
    comments = fields.Char(string="Comments", size=200)
    transactiontype = fields.Char(string="Transaction Type", size=50)
    paymenttrackingid = fields.Char(string="PaymentTracking", size=200, default="0")
    transaction_dt = fields.Datetime(
        string="Transaction Date", compute="_compute_transaction_dt", store=True
    )
    ebook = fields.Char(string="E-Book", compute="_compute_ebook", store=True)
    status = fields.Char(string="Status", compute="_compute_status", store=True)
    # program_purpose = fields.Char(string='Purpose_StatDate_EndDate_ProgramPlace', compute="_compute_programpurpose",
    #                               store=True)
    gateway = fields.Char(string="Gateway", compute="_compute_gateway", store=True)
    billing_tel1 = fields.Char(string="Phone No1", size=20)
    billing_tel2 = fields.Char(string="Phone No2", size=20)
    billing_pan = fields.Char(string="PAN Number", size=20)
    pre_regid = fields.Char(string="Pre Reg Id", size=20)
    raised_date = fields.Char(string="Payment Received Date")
    receipt_number = fields.Char(string="Payment Receipt Number")
    purpose = fields.Char(string="Purpose")

    currency = fields.Char(string="Currency", size=10)
    errorcode = fields.Char(string="Error Code", size=100)
    errorreason = fields.Char(string="Error Reason", size=200)
    errormsg = fields.Char(string="Error Msg", size=400)
    addlinfo = fields.Char(string="Additional Info", size=700)
    paymentmethod = fields.Char(string="Payment Method", size=50)
    paymentdata = fields.Char(string="Payment Data", size=50)
    paymentid = fields.Char(string="Payment Id", size=200)
    invoiceid = fields.Char(string="Invoice Id", size=100)

    payment_link = fields.Char(string="payment link")
    payment_link_sent_date = fields.Date(string="Link Sent Date")

    batch_details = fields.Char(string='Batch')
    schedule_details = fields.Char(string='Schedule')
    gpms_transaction_id = fields.Char(string="GPMS Transaction ID")

    re_registration = fields.Char(string="E-Receipt - Re_registraton")

    # nooftrees = fields.Char(string='noOfTrees', size=20)
    # recurringdonations = fields.Char(string='recurringDonation', size=20)

    @api.depends("payment_status")
    def _compute_transaction_dt(self):
        for rec in self:
            if rec.create_date != False:
                rec.transaction_dt = rec.create_date
            else:
                rec.transaction_dt = False

    @api.depends("ereceiptreference")
    def _compute_ebook(self):
        for rec in self:
            if (
                rec.ereceiptreference != None
                and rec.ereceiptreference != ""
                and rec.ereceiptreference != False
            ):
                rec.ebook = rec.ereceiptreference[5:8]
            else:
                rec.ebook = ""

    @api.depends("payment_status")
    def _compute_status(self):
        for rec in self:
            if (
                rec.payment_status != None
                and rec.payment_status != False
                and rec.payment_status == "Success"
            ):
                rec.status = "Confirmed"
            else:
                rec.status = ""

    # @api.depends('payment_status')
    # def _compute_programpurpose(self):
    #     try:
    #         for rec in self:
    #             if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
    #                 rec.program_purpose = rec.programregistration.programapplied.pgmschedule_programname
    #             else:
    #                 rec.program_purpose = ''
    #     except:
    #         rec.program_purpose = ''

    @api.depends("payment_status")
    def _compute_gateway(self):
        for rec in self:
            if (
                rec.payment_status != None
                and rec.payment_status != False
                and rec.payment_status == "Success"
            ):
                rec.gateway = "ccavenue"
            else:
                rec.gateway = ""

    # @api.model
    # def fields_get(self, fields=None):
    #     fields_notto_show = ['programregistration']
    #     res = super(SamaskritiPgmRegistration, self).fields_get()
    #     for field in res:
    #         if (field in fields_notto_show):
    #             res[field]['selectable'] = False
    #             res[field]['sortable'] = False
    #         else:
    #             res[field]['selectable'] = True
    #             res[field]['sortable'] = True
    #
    #     return res


class SamskritNotifyMe(models.Model):
    _name = "samaskriti.notify.me"
    _description = "Samaskriti Notify Me Configuration"
    _rec_name = 'pgm_name'

    sso_id = fields.Char(string="SSO Id", size=150)
    pgm_name = fields.Char()
    email = fields.Char()
    full_name = fields.Char()
    country = fields.Char()
    timeslot = fields.Char()
