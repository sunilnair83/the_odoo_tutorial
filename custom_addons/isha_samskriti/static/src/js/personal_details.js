
odoo.define('isha_samskriti.personal_details', function (require) {
    'use strict';
    
    var publicWidget = require('web.public.widget');
    
    publicWidget.registry.personalDetails = publicWidget.Widget.extend({
        // defined the selector and the event for the form
        selector: '.personal_details_form',
        events: {
            'change input[name="issame"]': '_onWhatsAppCheckChange',
            'click input': '_onInputClick',
            'click select': '_onSelectionClick',
            'click #person_detial_formsubmit':'_formSubmit',
            'keyup #phone_number':'phone_validation',
            'keyup #email':'email_validation',
        },
        
        // the start function will get called in the very beginning as soon as the page is ready
        start: function(){
            var def = this._super.apply(this, arguments);
            
            return def;
            
        },
        validateEmail:function (email) {
              const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return re.test(email);
        },
        validatephone:function (phone) {
              var regexPattern=new RegExp(/^[0-9-+]+$/);    // regular expression pattern
              return regexPattern.test(phone);
        },
        phone_validation:function(e){
            var  self =this;

            var value = this.$('input:focus').val();

            var keyCode = e.which ? e.which : e.keyCode;
          if(self.validatephone(value)){

            this.$('.phone_err').addClass("oe_hidden");
            this.$('input:focus').removeClass("mandatrory_field");
          }
          else{
                this.$('input:focus').addClass("mandatrory_field");
             this.$('.phone_err').removeClass("oe_hidden");

          }

        },
        email_validation:function(e){
            var  self =this;

            var value = this.$('input:focus').val();

          if(self.validateEmail(value)){

            this.$('.email_err').addClass("oe_hidden");
            this.$('input:focus').removeClass("mandatrory_field");
          }
          else{
           this.$('input:focus').addClass("mandatrory_field");
           this.$('.email_err').removeClass("oe_hidden");

          }
          },

        // the method toggles the view to hide and show whatsapp num block
        _onWhatsAppCheckChange: function(){
            // the checkbox value
            var $isSame = this.$('input[name="issame"]');
            
            // the div for what's app number
            var $whatsAppNumBlock = this.$('div[name="iswhatsappblock"]');
            
            // adding class to hide or show the whatsapp block
            if ($isSame.is(":checked"))
            {
                $whatsAppNumBlock.addClass('oe_hidden');
            }else
            {
                $whatsAppNumBlock.removeClass('oe_hidden');
            }
            
        },
        
        // the method to register the input click event
        _onInputClick: function(){
            // removing the mandatory_field class in order to remove the error outline
            this.$('input:focus').removeClass("mandatrory_field")
            
            // hiding the error texts as soon as the user starts giving inputs
            this.$(".address_error").addClass('oe_hidden');
            this.$(".city_error").addClass('oe_hidden');
            
        },

        // the method to register the input click event
        _onSelectionClick: function(){
            // removing the mandatory_field class in order to remove the error outline
            this.$('select:focus').removeClass("mandatrory_field")
            
        },
        
        // the function to get called when the user submits the form
        _formSubmit:function(e){
            
            var self = this;
            // the variable holds values for the mandatory fields that user has skipped
            var unfilled_mandatory_count=0;
            
            // iterating over each of the fields which are mandatory
            this.$("input[is_input_requred='True']").each(function(index,$input){
                // Extracting the required attrs from the current input field
                
                var val=$(this).val();
                var type=$(this).attr("type");
                var id_attr=$(this).attr('name');
                
                
                // the logic for mandatory field if the type is radio or chechkbox
                if (type=="checkbox" || type=="radio"){
                    
                    // values for checkbox and radio button 
                    var checked = $(this).is(':checked');
                    var is_radio_checked = self.$('input:radio[name='+id_attr+']').is(":checked");
                    
                    // if the class is mandatory and the user hasn't filled it then we'd add error class
                    if (!checked && !$(this).hasClass('uncheckmandatrory_field')){
                        
                        unfilled_mandatory_count = unfilled_mandatory_count+1;
                        $(this).addClass('mandatrory_field');                       
                        
                    }
                    
                    // logic for radio fields
                    if(type=="radio" && is_radio_checked)
                    {
                        $(this).removeClass("mandatrory_field");
                        $(this).addClass("uncheckmandatrory_field");
                    }
                    
                }else{
                    if(val=="")
                    {
                        // incrementing the error count
                        unfilled_mandatory_count=unfilled_mandatory_count+1;
                        
                        // adding the class for mandtory field
                        $(this).addClass('mandatrory_field');
                        
                    }
                }               
                
            });
            
            // checking the values for the selection fields
            this.$("select[is_input_requred='True']").each(function( index ) {
                var val=$(this).val();
                
                if(val=="" ||val==null)
                {
                    unfilled_mandatory_count=unfilled_mandatory_count+1;
                    $(this).addClass('mandatrory_field');
                    
                }
                
            });
            
            // additional validation on Address field to check that it isn't less than 10 letters
            if (this.$("#bill_address").val().length<10){
                this.$("#bill_address").addClass('mandatrory_field');
                this.$(".address_error").removeClass('oe_hidden');
            }
            
            // additional validation on city address field
            if (this.$("#bill_district").val().length<3){
                this.$("#bill_district").addClass('mandatrory_field');
                this.$(".city_error").removeClass('oe_hidden');
            }
            var phone = this.$('#phone_number').val();
            var email = this.$('#email').val();

            if(!self.validatephone(phone))
            { unfilled_mandatory_count=unfilled_mandatory_count+1;
                this.$('#phone_number').addClass("mandatrory_field");
                this.$('.phone_err').removeClass("oe_hidden");


             }
             if(!self.validateEmail(email))
            { unfilled_mandatory_count=unfilled_mandatory_count+1;
             this.$('#email').addClass("mandatrory_field");
                this.$('.email_err').removeClass("oe_hidden");


             }
            // stop the form submit action
            if (unfilled_mandatory_count>0 || this.$("#bill_address").val().length<10 || this.$("#bill_district").val().length<3){
                return false;
            }else{

                $("form.samskriti_reg_form").submit();
            }


            
        }
    });
    
});
