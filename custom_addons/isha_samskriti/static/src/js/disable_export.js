odoo.define('isha_samskriti.disable_export', function (require) {
    "use strict";
    
    var Sidebar = require('web.Sidebar');
    var core = require('web.core');
    var _t = core._t;
    var _lt = core._lt;
    var session = require("web.session");
    var ListView = require('web.ListController')

    var disableExportModel = "samaskriti.participant.paymenttransaction"
    
   
    // console.log(self.controllerParams.modelName)

    
    session.user_has_group('isha_samskriti.group_samaskriti_backoffice_admin').then(function(has_group) {
        
        
        Sidebar.include({
            _addItems: function (sectionCode, items) {

                var self = this;
                var modelName = self.options.env.model;
                
                if (items) {
                    
                    if (!has_group && modelName == disableExportModel){
                        var export_label = _t("Export");
                        if (sectionCode == 'other') {
                            for (var i = 0; i < items.length; i++) {
                                console.log(items[i]['label'])
                                if (items[i]['label'] == export_label) {
                                    items.splice(i, 1);
                                    if ($('a[data-section="other"][data-index="0"]').length > 0)
                                    $('a[data-section="other"][data-index="0"]').parent().remove();
                                }                                
                            }
                        }
                    }
                    
                    this.items[sectionCode].unshift.apply(this.items[sectionCode], items);
                    
                }
            },
        });
        
        ListView.include({
            _onDirectExportData() {
                var modelName = JSON.parse(JSON.parse(self.sessionStorage['current_action']))["res_model"]
                
                if (has_group || modelName != disableExportModel) {
                    this._getExportDialogWidget().export();
                }
            },
        });
        
        
    });
    
    
    
    
    
    
});
