function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}

defer(jsdocumentready);

function getip(url){
    return fetch(url).then(res=>res.text());
}

//https://checkip.amazonaws.com/
//https://www.cloudflare.com/cdn-cgi/trace
//getip('https://www.cloudflare.com/cdn-cgi/trace').then(data=> {
//    ip = data.split("\n")[8].split("=")[1];
//    $('#ipcountry').val(ip);
//})
//
function jsdocumentready() {
$(document).ready(function () {
        $('.fileerror').hide();
        setdefaultprogramapplied();
        pschange();
    });
}


function setdefaultprogramapplied(){
    var programapplied = document.getElementById("programapplied");

    if($('#programapplied').val() == "" && $('#programapplied')[0].options.length > 1){
        $('#programapplied')[0].options.selectedIndex = 1;
        $('#programapplied').trigger('change');
            pschange();
    }
}

function pschange() {

    var programapplied = document.getElementById("programapplied");
    var optselected = programapplied.options[programapplied.selectedIndex].getAttribute('pid');
    var batchapplied = document.getElementById("batchapplied");
    var programapplied = $("#programapplied").val();
       $("#hidebatchapplied option").removeClass("show_value");
        var selected_batch_options = "<option id='' value=''>Select Batch</option>";
        var noselected_batch_options = "<option id='' value=''>No Batch for this Date</option>";
        var show_count=0;
        $("#hidebatchapplied option[pbid="+programapplied+"]").addClass("show_value");
        $('#hidebatchapplied option[class=show_value]').each(function(){
                var batch_value = $(this).val();
                show_count=show_count+1;
                var pstartdate = $(this).attr('pstartdate');
                var id = $(this).attr('pbid');
                var display_text = $(this).attr('display_text');
           selected_batch_options=selected_batch_options + ('<option pbid ='+id+' pstartdate=' + pstartdate + '  value=' + batch_value + '>' + display_text + '</option>');
        });
            $("#batchapplied").empty();
            if (show_count==0)
            {
                 $("#batchapplied").append(noselected_batch_options);

            }
            else{
                $("#batchapplied").append(selected_batch_options);
            }

//    $('#batchapplied').val("")
//    batchapplied.disabled = false;
//    Array.from(batchapplied.options).forEach(item => {
//        var trend = item.getAttribute("pbid");
//        if (trend == optselected)
//            item.style.display = "block";
//        else
//            item.style.display = "none";
//
//    });
//    $('#batchapplied')[0].options.selectedIndex = 1;
}
