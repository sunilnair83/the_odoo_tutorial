// load the state while load the page when pincode is entred
$(window).on('load', function (e) {$('#pincode').trigger("blur");});

$(document).ready(function () {

    function text(url) {
        return fetch(url).then(res => res.text());
    }
    text('https://www.cloudflare.com/cdn-cgi/trace').then(data => {
        latest_val = data.split("\n");
//        console.log(latest_val);
        ipRegex = data.split("\n")[2].split("=")[1];
        $('#amazon_ip').val(ipRegex);
        country_code = latest_val[8].split("=")[1];
        $("#amazon_country").val(country_code);
    });

    $('.kshetragnaregistrationform').submit(function(e) {
        valid = true;
        var nationality_id = $("#nationality_id").val();
        var current_country_id = $("#current_country").val();
        if (nationality_id != 104 || current_country_id != 104) {
            var pass_nno = $("#pas_number").val();
            var pass_cop_file = $("#pass_card_file").val();
            var jsociyes = $(".jsociyes").is(":checked");
            var jsocino = $(".jsocino").is(":checked");
            if (pass_nno.length == 0 || pass_cop_file.length == 0) {
                valid = false;
            }
            if (nationality_id != 104) {
                if (jsociyes == false && jsocino == false) {
                    valid = false;
                    
                }
                if (jsociyes) {
                    var jsociholderfile = $(".oci_file").val();
                    if (jsociholderfile.length == 0) {
                        valid = false
                    }
                }
                if (jsocino){
                    valid = false
                    e.preventDefault();
                    return swal({
                        backdrop: false,
                        title: 'Message',
                        text: "Kshetragna is available only for Indians, NRIs and foreign nationals with OCI Card. If your nationality is not India, it is mandatory that you obtain a OCI Card from the Govt Of India and then apply for Kshetragna",
                        type: 'error'
                    });
                    
                }
            }
        }
        if (valid == false) {
            e.preventDefault();
            return swal({
                backdrop: false,
                title: 'Mandatory Fields Missing',
                text: "Please fill the mandatory details",
                type: 'error'
            });
        }
        validate_dob();
        return valid;
    });
    // the variable helps to keep the state pre-populated on first load of the form
    var fixState = false;
    load_profile();
    //set the min date for booking date
    var todaysDate = new Date();
    var year = todaysDate.getFullYear()-17;
    var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
    var day = ("0" + todaysDate.getDate()).slice(-2);
    var maxDate = (year +"-"+ month +"-"+ day);
    $('#dob').attr('max',maxDate);
    //set the min date for booking date
    var todaysDate = new Date();
    var year = todaysDate.getFullYear();
    var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
    var day = ("0" + todaysDate.getDate()).slice(-2);
    var maxDate = (year +"-"+ month +"-"+ day);
    // $('#approximate_date').attr('min',maxDate);
    var jspgremstatecoll =[];
    var jspgremcountrycoll =[];
    $("#nationality_id").trigger('change');
    $("#countrycode").trigger('click');
    $("#current_country").trigger("click");
    $('#stateid option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "statenameattr":$(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
    $('#nationality_id option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "ccattr":$(jvar).attr("ccatr")
        };
        jspgremcountrycoll.push(objvar);
    });
    $("#nationality_id").change(function(e){
        var id=  $("#nationality_id").val();
        $('.osciradio').attr("required",false);
        $('#is_oci_card').attr("required",false);
        if (id==104){
            $('.kocicardholder_certificatediv').css({"display":"none"});
            $('.jsociholderfile').css({"display":"none"});
        }
        else{
            $('.kocicardholder_certificatediv').css({"display":"block"});
            $('.osciradio').attr("required",true);
            $('#is_oci_card').attr("required",true);

//            $('input:radio[name="jsociyesno"]').filter('[value="yes"]').attr('checked', true);
        }
        checkpassport();
        $('.accomdation-details-wrapper').addClass("hide_view");
    }).change();
     $("#dob").keyup(function() {
                $('.date_err').css({
                    "display": "none"
                });
            });
    function validate_dob(){
        // DOB Validation
        var date_Val = $('#dob').val();
        var todaysDate = new Date();
        var year = todaysDate.getFullYear()-72;
        var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
        var day = ("0" + todaysDate.getDate()).slice(-2);
        var max_date = (year +"-"+ month +"-"+ day);
        console.log(max_date);
        if (max_date > date_Val){
            valid = false;
        }
        if (valid == false) {
         $('.date_err').css({
                    "display": "block"
                });
//            return swal({
//                backdrop: false,
//                title: 'Date Of Birth ',
//                text: "Please enter a valid DOB",
//                type: 'error'
//            });
        }
        return valid;
    }
    function checkpassport(){
        var nationality_id=  $("#nationality_id").val();
        var current_country_id=  $("#current_country").val();
        // for NRI/OCI cad holder make pan as non mandatory
        $('#pan_no').attr("required",true);
        $('#pan_card_file').attr("required",true);
        $('.pan_label').addClass("required");
        
        $('.passportdiv').css({"display":"none"});
        $("#pass_card_file").attr("requried",false);
        $('#pas_number').attr("required",false);
        $('.pass_label').removeClass("required");
        $('.identify_proofdiv input[type=radio]').each(function(){
                    $(this).attr("required",false);
                });
        $('.check_identify_proofdiv input[type=radio]').each(function(){
                    $(this).attr("required",false);
                });
        $('.pan_details_wrapper input[type=radio]').each(function(){
                    $(this).attr("required",false);
                });
        if (nationality_id==104 || current_country_id==104){
                $('.pan_details_wrapper').removeClass("hide_view");
                $('.pan_details_wrapper').addClass("show_view");
                $('.identify_proofdiv').addClass("hide_view");
                $('.pan_details_wrapper input[type=radio]').each(function(){
                    $(this).attr("required",true);
                });
         }
         if (nationality_id!=104){
            $('.check_identify_proofdiv').removeClass("hide_view");
            $('.check_identify_proofdiv').addClass("show_view");
            $('.check_identify_proofdiv input[type=radio]').each(function(){
                    $(this).attr("required",true);
              });

                $('.pan_details_wrapper').removeClass("show_view");
                $('.pan_details_wrapper').addClass("hide_view");
                $('.aadhar_details_wrapper').addClass("hide_view");
                $('.aadhar_details_wrapper').removeClass("show_view");
        }

        if (nationality_id!=104 || current_country_id!=104){
//            $('.identify_proofdiv').addClass("show_view");
            $('#pan_no').attr("required",false);
            $('#pan_card_file').attr("required",false);
            $('.pan_label').removeClass("required");
            $('.passportdiv').css({"display":"block"});
            $("#pass_card_file").attr("requried",true);
            $('#pas_number').attr("required",true);
            $('.pass_label').addClass("required");
        }
        if (nationality_id==104 && current_country_id!=104){
            $('.check_identify_proofdiv').addClass("hide_view");
            $('.check_identify_proofdiv').removeClass("show_view");
            $('.identify_proofdiv').addClass("hide_view");
            $('.identify_proofdiv').removeClass("show_view");
        }
    }
    
//    $( "#pan_no" ).keyup(function() {
//        var pan_vals = $("#pan_no").val();
//        var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
//        if(!regex.test(pan_vals)){
//            console.log('False');
//            $('#pan_no').val($('#pan_no').val().toUpperCase());
//            regex.test(pan_vals);
//            $('.pan_err').css({"display":"block"});
//        }else{
//            $('.pan_err').css({"display":"none"});
//        }
//    });

    // On change mobile country code on - add validation on india 10 for others country max 15
    $('#countrycode').change(function () {
        $('#phone2').val()
        if( $('#countrycode').val() == '91'){
            $('#phone2').attr('maxlength', '10')
            $('#phone2').attr('minlength', '10')
            $('#phone2').attr('pattern', '[0-9]+')
        }
        else{
            $('#phone2').attr('maxlength', '15')
            $('#phone2').attr('pattern', '[1-9]{1}[0-9]+')
        }
    }).change();
    
    $('#current_country').change(function() {
        jspgmregfmselectstates();
        checkpassport();
    }).change();
    
    $('.input_file').bind('change', function() {
        //alert('This file size is: ' + this.files[0].size/1024/1024 + "MiB");
        var file_size = this.files[0].size/1024/1024;
        if (file_size>3){
            ///console.log("File size");
            $(this).val("");
            return swal({
                backdrop: false,
                title: 'File ',
                text: "Kindly Upload the file less than 3MB",
                type: 'error'
            });
        }
    });
    
    function jspgmregfmselectstates() {
        var state = document.getElementById("stateid");
        
        var countryname = document.getElementById("current_country");
        var countryid = countryname.value;
        $('#stateid').attr("required",false);
        $('.state_id').removeClass("required");
        
        var idvar = 0;
        var sd =false;
        var cc=0;
        Array.from(state.options).forEach(item => {
            var trend = item.getAttribute("countryid");
            if (trend == countryid || trend==0) {
                item.style.display = "block";
                item.disabled = false;
                sd =true;
                cc=cc+1;
            } else
            {
                item.style.display = "none";
                item.disabled = true;
            }
        });
        if (idvar != 0) {
            state.options.selectedIndex = idvar;
            state.disabled = false;
            state.height = 35;
        }
        //                 else if (idvar == 0) {
        //                   state.options.selectedIndex = 0;
        //                    state.disabled = true;
        //                }
        
        if (fixState == false){
            $('#stateid').val("");
            $('#state_text_field').val('');
        }
        if(cc>1){
            $('#stateid').attr("required",true);
            $('#state_text_field').attr("required",false);
            $('.state_id').addClass("required");
            $('.state_text').removeClass("required");
            $('.state_dropdown').css({"display":"block"});
            $('.state_txt').css({"display":"none"});
        }else{
            $('#stateid').attr("required",false);
            $('#state_text_field').attr("required",true);
            $('.state_id').removeClass("required");
            $('.state_text').addClass("required");
            $('.state_txt').css({"display":"block"});
            $('.state_dropdown').css({"display":"none"});
        }
        fixState = false;
    }
    $('input[type=radio][name="jsociyesno"]').change(function() {
        var is_ocicard_holder = $(this).val();
        if (is_ocicard_holder=='yes'){
            $('.jsociholderfile').css({"display":"block"});
            $('.oci_file').attr('required', true);
        }
        else{
            $('.jsociholderfile').css({"display":"none"});
            return swal({
                backdrop: false,
                title: 'Message',
                text: "Kshetragna is available only for Indians, NRIs and foreign nationals with OCI Card. If your nationality is not India, it is mandatory that you obtain a OCI Card from the Govt Of India and then apply for Kshetragna",
                type: 'error'
            });
            
        }
    });

    $('input[type=radio][name="ind_bank_ac"]').change(function() {
        var is_indian_bank_ac_holder = $(this).val();
        if (is_indian_bank_ac_holder=='yes'){
        $('.document_details_wrapper').removeClass("hide_view");
        $('.document_details_wrapper').addClass("show_view");
        $('.accomdation-details-wrapper').removeClass("hide_view");
        $('.accomdation-details-wrapper').addClass("show_view");
        }
        if (is_indian_bank_ac_holder=='no'){
        $('.document_details_wrapper').removeClass("show_view");
        $('.document_details_wrapper').addClass("hide_view");
        $('.accomdation-details-wrapper').removeClass("show_view");
        $('.accomdation-details-wrapper').addClass("hide_view");
            return swal({
                backdrop: false,
                title: 'Message',
                text: "Currently we are accepting payment through Indian Bank Accounts only. We request you to revisit this form after opening an Indian Bank Account.",
                type: 'error'
            });
        }
    });

    $('input[type=radio][name="check_identify_proof"]').change(function() {
        var check_identify_proof = $(this).val();
        if (check_identify_proof=='yes'){
            $('.identify_proofdiv').removeClass("hide_view");
            $('.identify_proofdiv').addClass("show_view");
             $('.identify_proofdiv input[type=radio]').each(function(){
                    $(this).attr("required",true);
             });

        }
        if (check_identify_proof=='no'){
            $('.identify_proofdiv').addClass("hide_view");
            $('.identify_proofdiv').removeClass("show_view");
             $('.identify_proofdiv input[type=radio]').each(function(){
                    $(this).attr("required",false);
                });

                return swal({
                backdrop: false,
                title: 'Message',
                text: "PAN Card or Aadhaar Card is mandatory to continue with the payment. PAN Card is the preferred document and you can apply for PAN Card using this link: https://www.onlineservices.nsdl.com/paam/endUserRegisterContact.html",
                type: 'error'
            });
        }
    });

    $('input[type=radio][name="identify_proof"]').change(function() {
        var identify_proof = $(this).val();
        $('.upload_aadhar_details input').each(function(){
                    $(this).attr("required",false);
                });
       $('.upload_pan_details input').each(function(){
                    $(this).attr("required",false);
                });
         $('.pan_ques_div input').each(function(){
                    $(this).attr("required",false);
                });
         $('.aadhar_ques_div input').each(function(){
                    $(this).attr("required",false);
                });
        if (identify_proof=='pan'){
            $('.pan_details_wrapper').removeClass("hide_view");
            $('.pan_details_wrapper').addClass("show_view");
            $('.pan_ques_div').removeClass("show_view");
            $('.pan_ques_div').addClass("hide_view");
            $('.upload_pan_details').addClass("show_view");
            $('.upload_pan_details').removeClass("hide_view");
            $('.aadhar_details_wrapper').addClass("hide_view");
            $('.aadhar_details_wrapper').removeClass("show_view");
            $('.upload_pan_details input').each(function(){
                    $(this).attr("required",true);
                });

        }
        if (identify_proof=='aadhar'){
            $('.aadhar_details_wrapper').removeClass("hide_view");
            $('.aadhar_details_wrapper').addClass("show_view");
            $('.pan_details_wrapper').addClass("hide_view");
            $('.pan_details_wrapper').removeClass("show_view");

             $('.aadhar_ques_div').removeClass("show_view");
            $('.aadhar_ques_div').addClass("hide_view");
            $('.upload_aadhar_details').addClass("show_view");
            $('.upload_aadhar_details').removeClass("hide_view");

            $('.upload_aadhar_details input').each(function(){
                    $(this).attr("required",true);
                });

        }
    });

    $('input[type=radio][name="pan_user"]').change(function() {
        var pan_user_holder = $(this).val();
        if (pan_user_holder=='yes'){
           $('.upload_pan_details').removeClass("hide_view");
            $('.upload_pan_details').addClass("show_view");
             $('.upload_pan_details input').each(function(){
                    $(this).attr("required",true);
                });
             $('.upload_aadhar_details input').each(function(){
                    $(this).attr("required",false);
                });
        }
        else{
            $('.upload_pan_details').addClass("hide_view");
            $('.upload_pan_details').removeClass("show_view");
            $('.upload_pan_details input').each(function(){
                    $(this).attr("required",false);
                });
            $('.upload_aadhar_details input').each(function(){
                    $(this).attr("required",true);
                });
            return swal({
                backdrop: false,
                title: 'Message',
                text: "PAN card is mandatory to continue with the payment. Please revisit this form after getting a PAN card. You can apply for PAN Card using this link: https://www.onlineservices.nsdl.com/paam/endUserRegisterContact.html",
                type: 'error'
            });
        }
    });

    $('input[type=radio][name="aadhar_user"]').change(function() {
        var aadhar_holder = $(this).val();
        if (aadhar_holder=='yes'){
            $('.upload_aadhar_details').removeClass("hide_view");
            $('.upload_aadhar_details').addClass("show_view");
              $('.upload_aadhar_details input').each(function(){
                    $(this).attr("required",true);
                });
                 $('.upload_pan_details input').each(function(){
                    $(this).attr("required",false);
                });
        }
        else{
            $('.upload_aadhar_details').addClass("hide_view");
            $('.upload_aadhar_details').removeClass("show_view");
             $('.upload_aadhar_details input').each(function(){
                    $(this).attr("required",false);
                });
             $('.upload_pan_details input').each(function(){
                    $(this).attr("required",true);
                });
            return swal({
                backdrop: false,
                title: 'Message',
                text: "Aadhar card is mandatory to continue with the payment. Please visit this form again after you have your Aadhar card. You can apply for Aadhar Card using this link.",
                type: 'error'
            });
        }
    });

    $('#current_country').blur(function(){
        var country_id = $('#current_country').val();
        
        if (country_id==104){
            $('#pincode').attr('pattern', '[0-9]*');
            $('#pincode').attr('minlength', '6');
            $('#pincode').attr('maxlength', '6');
            $('#addressline').attr('minlength', '15');
            $('#addressline').attr('maxlength', '15');

        }
        else if(country_id==233) {
            $('#pincode').attr('pattern', '[0-9]*');
            $('#pincode').attr('minlength', '5');
            $('#pincode').attr('maxlength', '5');
            
        }
        else{
            $('#pincode').attr('maxlength', '10');
            $('#pincode').removeAttr('pattern');
            $('#pincode').removeAttr('minlength');
            
        }
        
    });
    $('#pincode').focusout(async () => {
        let selected_country = $('#current_country option:selected').text();
        let selected_country_iso2_code = $('#current_country option:selected').attr("ccode");
        let pincode = $('#pincode').val();
        //                        console.log("pincode");
        //                        console.log(pincode);
        pincode = pincode.trim();
        if( pincode !="")
        {
            let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
            let address_details_json = await address_details.json();
            //console.log("address_details_json");
            //console.log(address_details_json);
            
            if (! _.isEmpty(address_details_json)) {
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0];
                $('#stateid').val(statevar["valueattr"]);
                $('#city').val(address_details_json.defaultcity);
                
                if ($('#state_text_field').attr('required')==true){
                    $('#state_text_field').val(address_details_json.state);
                }
                
                
            }
        }
    });
    
    //$('.otp_submit').click(function(){
    // var entered_text = $('.otp_value').val();
    // var read_otp_txt = sessionStorage.getItem("generated_text");
    // if (read_otp_txt==entered_text)
    //    {
    //          //  console.log("MATCH");
    //            $('.map_otp').css({"display":"none"});
    //            $('.opterror').css({"display":"none"});
    //            $('#is_mobilevalid').val(true);
    //            $('.generate_optdiv').css({"display":"none"});
    //    }
    //    else{
    //      //  console.log("NOT MATCH");
    //        $('.opterror').css({"display":"block"});
    //        //console.log("d NOT MATCH");
    //    }
    //
    //});
    //            async function generate_optFun() {
    //                var  mobi = $('#phone2').val();
    //                var  ccode = $('#countrycode').val();
    //                let queryString = [
    //                            `mobile=${mobi}`,
    //                            `ccode=${ccode}`,
    //                            ].join('&');
    //                              let sre = await fetch(`/generate_otp?${queryString}`)
    //                               let sre_json = await sre.json()
    //                              // console.log(sre_json);
    //                               var generated_text = sre_json["otp"];
    //                               sessionStorage.setItem("generated_text",JSON.stringify(generated_text));
    //                               $('.map_otp').css({"display":"block"});
    //              return 'generated';
    //            }
    //            $('.resendotp').click(function(){
    //
    //            generate_optFun().then(x => { // (B)
    //            })
    //            });
    //            $('.generate_opt').click(function(){
    //            generate_optFun().then(x => { // (B)
    //            });
    //            });
    //            $('#pincode').blur(function(){
    //            load_state_onchangepincode().then(x => { // (B)
    //            });
    //            });
    
    
    
    
    function load_states() {
        var state_dropdown = document.getElementById("stateid");
        var country_dropdown = document.getElementById("current_country");
        var country_id = country_dropdown.value;
        //    if(country_id==104){
        //        $('#stateid').attr("required",true);
        //         $('.state_id').addClass("required");
        //
        //    }
        //    else{
        //    $('#stateid').attr("required",false);
        //      $('.state_id').removeClass("required");
        //    }
        $('#stateid').attr("required",false);
        $('.state_id').removeClass("required");
        var idvar = 0;
        var cc=0;
        Array.from(state_dropdown.options).forEach(item => {
            var id = item.getAttribute("countryid");
            if (id == country_id || id==0) {
                item.style.display = "block";
                item.disabled = false;
                cc =cc+1;
                //            if (idvar == 0)
                //                idvar = item.index;
            } else {
                item.style.display = "none";
                item.disabled = true;
            }
        });
        
        if (fixState==false){
            $('#stateid').val("");
            $('#state_text_field').val('');
        }
        if(cc>1){
            $('#stateid').attr("required",true);
            $('#state_text_field').attr("required",false);
            $('.state_id').addClass("required");
            $('.state_text').removeClass("required");
            $('.state_dropdown').css({"display":"block"});
            $('.state_txt').css({"display":"none"});
        }else{
            $('#stateid').attr("required",false);
            $('#state_text_field').attr("required",true);
            $('.state_id').removeClass("required");
            $('.state_text').addClass("required");
            $('.state_txt').css({"display":"block"});
            $('.state_dropdown').css({"display":"none"});
        }
    }
    function is_not_empty_or_null(objtotestvar){
        var flg = false;
        try{
            if(objtotestvar && objtotestvar !=null && objtotestvar !="") {
                flg = true;
            }
            
        }
        catch(exvar){
        }
        finally{
            return flg;
        }
    }
    function load_profile() {
        var initObjvar;
        var addobjvar;
        
        try {
            initObjvar = $.parseJSON($('#jspgreginputobjid').val());
            console.log(initObjvar)
            
            if ('firstName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['firstName'])
            && $('#first_name')) {
                $('#first_name').val(initObjvar['basicProfile']['firstName']);
                $('#first_name').prop("readonly",true);
            }
            if ('lastName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['lastName'])
            && $('#last_name')) {
                $('#last_name').val(initObjvar['basicProfile']['lastName']);
                $('#last_name').prop("readonly",true);
            }
            if ('email' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['email'])
            && $('#participant_email')) {
                $('#participant_email').val(initObjvar['basicProfile']['email']);
                $('#participant_email').prop("readonly",true);
            }
            
            
            
            //debugger;
            if ('nationality' in initObjvar['extendedProfile'] && is_not_empty_or_null(initObjvar['extendedProfile']['nationality'])
            && $('select#nationality_id')) {
                var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == initObjvar['extendedProfile']['nationality']);
                if(countfiltvar.length > 0) {
                    $('select#nationality_id').val(countfiltvar[0]["valueattr"]);
                }
            }
            if ('phone' in initObjvar["basicProfile"]) {
                if ('countryCode' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['countryCode'])
                && $("select#countrycode") && $("select#countrycode option") && $("select#countrycode option:selected")) {
                    $("select#countrycode").val(initObjvar['basicProfile']['phone']['countryCode']);
                    $("select#countrycode").trigger('change');
                    //                 $("select#countrycode option").prop("disabled", true);
                    //                 $("select#countrycode option:selected").prop("disabled", false);
                }
                if ('number' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['number'])
                && $("input#phone2")) {
                    $("input#phone2").val(initObjvar['basicProfile']['phone']['number']);
                    $("input#phone2").prop("readonly",true);
                }
            }
            addobjvar = initObjvar['addresses'];
            if (addobjvar.length > 0) {
                addobjvar = addobjvar[0];
                //debugger;
                if ('country' in addobjvar && is_not_empty_or_null(addobjvar['country']) && $('#current_country')) {
                    var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == addobjvar['country']);
                    if(countfiltvar.length > 0)
                    {
                        $('#current_country').val(countfiltvar[0]["valueattr"]);
                        load_states();
                    }
                }
                else if($("input[name='country']").val()) {
                    $('#current_country').val($("input[name='country']").val());
                    load_states();
                }
                if ('addressLine1' in addobjvar && is_not_empty_or_null(addobjvar['addressLine1']) && $('#address_line')) {
                    $('#addressline').val(addobjvar['addressLine1']);
                }
                //debugger
                if ('state' in addobjvar && is_not_empty_or_null(addobjvar['state']) && $('#residence_state')) {
                    let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == addobjvar['state'])
                    if(statevar.length > 0){
                        $('#stateid').val(statevar[0]["valueattr"]);
                    }
                }
                
                //debugger;
                if ('townVillageDistrict' in addobjvar && is_not_empty_or_null(addobjvar['townVillageDistrict'])) {
                    $('#city').val(addobjvar['townVillageDistrict']);
                }
                if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
                    $('#pincode').val(addobjvar['pincode']);
                }
                if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
                    $('#pincode').val(addobjvar['pincode']);
                }
                // setting the states and also the fixstate variable to true so that the onchange of country
                // won't lead to removing the populated state on first load
                if ('stateId' in addobjvar && is_not_empty_or_null(addobjvar['stateId'])) {
                    fixState = true
                    $('#stateid').val(addobjvar['stateId']);
                }else {
                    fixState = true
                    $('#state_text_field').val(addobjvar['state']);
                }
                
                
            }
        } catch (exvar) {
            console.log(exvar);
        } finally {
            load_states();
        }
        
    }
});

