odoo.define('isha_crm.HideControlPanelClientAction', function (require) {
"use strict";

var KanbanView = require('web.KanbanView');
var view_registry = require('web.view_registry');

var HideControlPanelClientAction = KanbanView.extend({
    withSearchBar: false
});
view_registry.add('isha_crm_hide_search_panel_kanban', HideControlPanelClientAction);

return HideControlPanelClientAction
});