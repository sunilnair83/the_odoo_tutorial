{
    'name': 'Isha livingspaces',
    'summary': "Isha Mega programs: Yoga is about rekindling the fundamental force of creation within you, to change the very chemistry of who you are. - Sadhguru",
    'description': "Isha yoga Center has been established by Sadhguru to allow an individual to experience inner peace and the joy of a healthy body through a unique and powerful set of offerings. These include residential programs, walk-in medical consultations and Ayurvedic therapies. The Satsang Center is located in the rich, salubrious environs of the Isha Yoga Center at the foothills of the Velliangiri Mountains. These mountains are referred to as the Kailash of the South – they have a powerful spiritual reverberation and also possess a natural beauty that is breathtaking. The Isha Yoga Center is a consecrated space and home to the Dhyanalinga, a unique meditative space that embodies the distilled essence of yogic sciences. The energies of the Dhyanalinga create an atmosphere that supports and catalyzes the frejuvenation process in the human system. Simply being in this space has profound transformative effects.",
    'author': "Isha Foundation",
    'license': "AGPL-3",
    'website': "https://isha.sadhguru.org/",
    'category': 'Yoga & Meditation',
    'version': '0.0.0.1',
    'external_dependencies': {
        'python': [
            'html2text',
        ],
    },
    'depends': ['base', 'website', 'isha_crm', 'web', 'isha_megapgms', 'isha_tp','pdf_binary_file_preview'],
    'data':
        [
            #	'data/rejuvenation_paymentgateway_data.xml',
            'data/mail_template_data.xml',
            'data/ir_config_param.xml',
            'data/cron_us_data_push.xml',
            #	'data/documenttype_data.xml',
            # 'data/sms_data.xml',
            # 'data/terms_and_conditions_data.xml',
            # 'data/megapgm_notificationevent_data.xml',
            # 'data/master_megapgms_data.xml',
            # 'data/megaprogram_type_data.xml',
            #	'data/cron.xml',
            #	'data/sequence_data.xml',
            # 'data/sequence_data.xml',
            # 'views/views.xml',
            'views/assets.xml',
            'views/livingspacestype_view.xml',
            'wizards/allocaterooms.xml',
            'wizards/prgattendance.xml',
            'wizards/blmatches.xml',
            'wizards/prgmatches.xml',
            'wizards/document_submit.xml',
            ##'views/livingspacesschedule_view.xml',
            # 'views/webregistrations.xml',
            # 'views/web/changeofparticipant.xml',
            # 'views/web/packageupgrade.xml',
            # 'views/livingspacesinquiry.xml',
            'views/access_rights_megapgms.xml',
            'wizards/changeprogramdate.xml',
            'wizards/approval.xml',
            'wizards/resendmail.xml',
            'wizards/cancelapproval.xml',
            'wizards/haapproval.xml',
            'wizards/packageupgrade.xml',
            'wizards/confirmation_wizard.xml',
            'views/livingspacesregistration_view.xml',
            # 'views/livingspaces_payment_transaction_view.xml',
            # 'views/livingspaces_payment_transaction_finance_view.xml',
            'security/security.xml',
            'security/ir.model.access.csv',
            'views/livingspaces_common_layout.xml',
            'views/livingspacesreg_form.xml',
            ##'views/volunteer_form.xml',
            ##'views/volunteer_form_page2.xml',
            # 'views/template.xml',
            'views/livingspaces_ssotemp.xml',
            # 'views/livingspaces_form.xml',
            'views/livingspaces_exceptiontemplate.xml',
            # 'views/template_common_layout.xml',
            'views/kshetragna_registration_view.xml',
            'views/kshetragna_prescreenedregistrants.xml',

            'views/online_kshetragna_registration_form.xml',
            'views/kshetragna_payment_view.xml',
            'views/payment_transaction_view.xml',
            'views/kshetragna_config.xml',
            'views/kshetranga_sso_auto_redirect_view.xml',
            'views/kshetragna_user_messages.xml',
            'views/kshetragna_register_inherit_views.xml',
            'views/mail_template.xml',
            'views/ir_ui_view.xml',
            'menus/menus.xml',
            # 'views/settings.xml'
        ],
    'demo': ['demo/demo.xml'],
    'application': True,
    'auto_install': False,
    'installable': True,
}
