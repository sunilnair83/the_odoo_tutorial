import hashlib
import hmac
import json

try:
    from odoo.http import request
except:
    pass
import logging
import traceback
from datetime import timedelta, datetime

import requests
import werkzeug
import werkzeug.utils
import werkzeug.wrappers

from odoo import http
from odoo.http import request
from ..isha_crm_importer import Configuration

#from custom_addons.isha_tp.controllers import livingspaces_gpms
from .livingspaces_gpms import livingspace_validatepayment,livingspace_gpmsstatus,livingspace_paystatus,livingspace_paymentsuccess,livingspace_paymentresponse,livingspace_update_payment_transaction,livingspace_paymentresponse_returntemplate
from .samaskriti_gpms import samaskriti_validatepayment,samaskriti_gpmsstatus,samaskriti_paymentresponse,samaskriti_update_payment_transaction

# from .samaskriti_gpms import samaskriti_validatepayment,samaskriti_gpmsstatus,samaskriti_paystatus,samaskriti_paymentsuccess,samaskriti_paymentresponse,samaskriti_update_payment_transaction,samaskriti_paymentresponse_returntemplate
#from .main import fetch_program_info
_logger = logging.getLogger(__name__)

null = None
false = False
true = True

# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    return requests.post(url, data=dp)

def errorlog(temptext):
    request.env['tp.tempdebuglog'].sudo().create({
        'logtext': temptext
    })

def str_to_int(x):
    try:
        a = int(x)
    except:
        a = 0
    finally:
        return a


# Hash
def fetch_program_inforegform(country_code=None, program_id=None):
    # _logger.info("start executing function fetch_program_inforegform country_code"+country_code)
    # _logger.info("program_id"+program_id)
    res = dict()
    seatsavailable = None
    res['openstatus'] = 'open'
    res['price'] = None
    res['curr'] = None
    try:
        country_data = request.env['res.country'].sudo().search(
            [('code', '=', country_code)])

        region_name = country_data.center_id.region_id.name
        progschedule = request.env['tp.program.schedule'].sudo().search(
            [('id', '=', str_to_int(program_id))])
        serviceinfo = request.env['tp.program.zip.codes'].sudo().search(
            [('notserviceable', '=', 'true')])
        # print('inside function')
        if progschedule:
            progscheduleroom = request.env['tp.program.schedule.roomdata'].sudo().search(
                [('pgmschedule_is_active', '=', 'True'), ('pgmschedule_regularseatsbalance', '>', 0),
                 ('pgmschedule_programname', '=', str_to_int(program_id))])
            for pkg in progscheduleroom:
                # print('Inside progscheduleroom loop')
                for ctry in pkg.pgmschedule_country:
                    if (ctry.name == country_data.name):
                        seatsavailable = True
                        # print('Inside Country Srch')
                        break
                for rgn in pkg.pgmschedule_region:
                    if (rgn.name == region_name):
                        seatsavailable = True
                        # print('Inside Region Srch')
                        break

        # print('seatsavailable')
        if progschedule:
            pricinginfo = progschedule.pgmpricing_data.sudo().search(
                [('pgmpricing_programname', '=', str_to_int(program_id))])
            try:
                for prc in pricinginfo:
                    for cntry in prc.pgmpricing_countryname:
                        if (country_data.name == cntry.name):
                            res['price'] = prc.pgmpricing_countrycost
                            res['curr'] = prc.pgmpricing_countrycur.name
                            raise StopIteration
            except StopIteration:
                pass

        if seatsavailable:
            if progschedule.pgmschedule_registrationopen:
                res['openstatus'] = 'open'
            else:
                res['openstatus'] = 'closed'
            for sinfo in serviceinfo:
                if (country_data.name == sinfo.countryname.name):
                    res['openstatus'] = 'NA'
                    break

        # # print(program_id)
        # pricinginfo = progschedule.pgmpricing_data.sudo().search(
        #     [('pgmpricing_programname', '=', program_id)])
        #
        # #print(pricinginfo)
        # try:
        #     for prc in pricinginfo:
        #         for cntry in prc.pgmpricing_countryname:
        #             if (country_data.name == cntry.name):
        #                 #print(country_data.name)
        #                 #print(cntry.name)
        #                 #print('inside loop')
        #                 res['price'] = prc.pgmpricing_countrycost
        #                 res['curr'] = prc.pgmpricing_countrycur.name
        #                 raise StopIteration
        # except StopIteration:
        #     pass
        else:
            # print('no active program')
            res['openstatus'] = 'closed'
        return res
    except Exception as e:
        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        res['openstatus'] = None
        res['price'] = None
        res['curr'] = None
        return res

def hash_hmac(data,secret):
    data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature


# hmac for urlparams
def hash_url_params(program,country):

    sso_config = Configuration('SSO')
    secret = sso_config['SSO_API_SECRET']
    data_dict = {
        'program': program,
        'country': country
    }
    data_enc = json.dumps(data_dict, sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature

# SSO login trigger
def simulate_sso_login(sso_config,legal_entity):
    try:
        ret = sso_config['SSO_API_SECRET']
        cururls = str(request.httprequest.url).replace("http://", "http://", 1)
        sso_log = {'request_url': str(cururls),
                   "callback_url": str(cururls),
                   "api_key": sso_config['SSO_API_KEY'],
                   "hash_value": "",
                   "action": "0",
                   "legal_entity": legal_entity,
                   "force_consent": "1"
                   }
        ret_list = {'request_url': str(cururls),
                    "callback_url": str(cururls),
                    "api_key": sso_config['SSO_API_KEY'],
                    }
        data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
        data_enc = data_enc.replace("/", "\\/")
        signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                             digestmod=hashlib.sha256).hexdigest()
        sso_log["hash_value"] = signature
        sso_log["sso_logurl"] = sso_config['SSO_LOGIN_URL']
        values = {
            'sso_log': sso_log
        }
        return request.render('isha_tp.auto_redirect_sso', values)
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)
        return request.render('isha_tp.exception')

# IP2Country api
def get_ip_country(ip):
    _logger.info("start executing function get_ip_country ip"+str(ip))
    url = request.env['ir.config_parameter'].sudo().get_param('crm.ip2country_url') + ip
    token = request.env['ir.config_parameter'].sudo().get_param('crm.ip2country_token')
    payload = {}
    headers = {
        'Authorization': 'Bearer '+token,
    }
    response = requests.request("GET", url, headers=headers, data=payload)
    try:
        country_dict = eval(response.text)
        # Defalut it to india
        if country_dict['country_iso2'] in ['-',None]:
            country_dict = {'country': 'India', 'country_iso2': 'IN'}
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)
        country_dict = {'country': 'India', 'country_iso2': 'IN'}
    return country_dict


def get_region_specific_details(region_name):
    _logger.info("starting execution get_region_specific_details region_name"+str(region_name))
    region_exsit = request.env['tp.program.region.contacts'].sudo().search(
        [('region_name', '=', region_name)])
    tnc = ""
    note_txt = " "
    address_text = " "
    acceptance_text = " "
    cancellation_policy = " "
    if region_exsit:
        tnc = str([str(x.value) for x in region_exsit if x.key == "tnc"]).replace("[", "").replace("]", "")
        note_txt = str([str(x.value) for x in region_exsit if x.key == "pbk_note_text"]).replace("[", "").replace("]", "")
        address_text = str([str(x.value) for x in region_exsit if x.key == "res_add_text"]).replace("[", "").replace("]", "")
        acceptance_text = str([str(x.value) for x in region_exsit if x.key == "acct_terms_text"]).replace("[","").replace("]", "")
        # cancellation_policy = str([str(x.value) for x in region_exsit if x.key == "cancel_policy"]).replace("[", "").replace("]", "")

    ret_value = {
        'tnc': str(tnc).replace("'", "") if "'" in tnc else tnc,
        'note_txt': str(note_txt).replace("'", "") if "'" in note_txt else note_txt,
        'address_text': str(address_text).replace("'", "") if "'" in address_text else address_text,
        'acceptance_text': str(acceptance_text).replace("'", "") if "'" in acceptance_text else acceptance_text,
        'cancellation_policy': str(cancellation_policy).replace("'",
                                                                "") if "'" in cancellation_policy else cancellation_policy
    }
    # if contact_info:
    #     for x in contact_info:
    #         if x.key == 'tnc':
    #             ret_value['tnc'] = x.value
            # elif x.key == 'cancel_policy':
            #     ret_value['cancellation_policy'] = x.value
    return ret_value

class TpRegistration(http.Controller):

    def sso_set_entitlement(self, data):
        try:

            parents_dict = {
                "firstName": data.get("firstname"),
                "lastName": data.get("lastname"),
                "email": data.get("email"),
                "password": "",
                "countryOfResidence": data.get("countrycode"),
            }
            sd = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_PMS_ENDPOINT1')
            print("sd:;", sd)
            datavar2 = json.dumps(parents_dict)
            authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
            headers2 = {
                "content-type": "application/json",
                "Authorization": "Bearer " + authreq,
                "X-User-Country": data.get("country_code")
            }
            # requestconsent_url = "https://uat-profile.isha.in/services/pms/api/admin/user-accounts?skipMail=true" + profidvar
            requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_PMS_ENDPOINT1') + "admin/user-accounts?skipMail=true"
            reqvar = requests.post(requestconsent_url, datavar2, headers=headers2)
            reqvar.raise_for_status()
            return reqvar

        except Exception as ex2:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)

    def payment_page(self, data):
        if data:
            recdyn = request.env['tp.program.registration'].sudo().browse(data.get('recdyn'))
            callbackUrl = request.env['ir.config_parameter'].sudo().get_param('tp.gpms_call_back_url')
            gpms_payurl = request.env['ir.config_parameter'].sudo().get_param('tp.gpms_payment_url')
            amounttempvar = data.get('amount')
            amountvariableinpg = int(amounttempvar) if type(amounttempvar) == float else amounttempvar
            values = {

                "firstName": recdyn.first_name,
                "lastName": recdyn.last_name,
                "email": recdyn.participant_email,
                "mobilePhone": str(recdyn.phone),
                "addressLine1": recdyn.shipping_address_line1,  # GPMS accepts max 50 chars
                "addressLine2": recdyn.shipping_address_line2,  # GPMS accepts max 50 chars
                "city": recdyn.shipping_city,
                "state": recdyn.shipping_state.id,
                "postCode": recdyn.shipping_pincode,
                "country": recdyn.country.code if recdyn.country else "IN",
                "amount": amountvariableinpg,
                "currency": data.get('currency'),
                "callbackUrl": callbackUrl,
                "requestId": recdyn.orderid,
                "ssoId": recdyn.sso_id,
                "program_code": recdyn.programapplied.id,
                "program_type": recdyn.programapplied.pgmschedule_programtype.description,
                "gpms_payurl": gpms_payurl
            }
            _logger.info("inside upgrade pay page values")
            _logger.info(str(values))
            return request.render("isha_tp.isha_website_payment", values)

    def checkExistingTxn(self, requestidvar, ssoprofileid, **post):
        _logger.info("starting exection checkExistingTxn requestidvar"+str(requestidvar))
        _logger.info("ssoprofileid"+str(ssoprofileid))
        _logger.info("**post"+str(post))
        ssorecord_exist = request.env['tp.program.registration'].sudo().search([("sso_id", "=", ssoprofileid),("registration_status", "=", 'Paid')])
        record_exist = request.env['tp.program.registration'].sudo().search([("orderid", "=", requestidvar)])
        if len(ssorecord_exist)>0:
            err_msg = "ERR_COMPLETED"
            values = {"support_email":'graceofyoga.online@ishafoundation.org',
                      "status": "ERROR",
                      "program_name":ssorecord_exist[0].programapplied.pgmschedule_programname,
                      "transactionId": "",
                      "region_support_email_id": "",
                      "err_msg": err_msg, }
            return request.render("isha_tp.payumoney_status_template", values)
        if len(record_exist) > 0:
            err_msg = "ERR_VALIDATION"
            if record_exist.registration_status == "Payment Link Sent":
                return False
            if record_exist.registration_status == "Pending":
                return False
            if record_exist.registration_status == "Paid":
                err_msg = "ERR_COMPLETED"
            if record_exist.registration_status == "payment_inprogress":
                err_msg = "ERR_AWAITED"
            if record_exist.registration_status == "payment_pending":
                err_msg = "ERR_AWAITED"
            if record_exist.registration_status == "payment_initiated":
                return False
            if record_exist.registration_status == "payment_failed":
                return False
            values = {"support_email": 'graceofyoga.online@ishafoundation.org',
                      "status": "ERROR",
                      "program_name": record_exist[0].programapplied.pgmschedule_programname,
                      "transactionId": "",
                      "region_support_email_id": "",
                      "err_msg": err_msg, }
            return request.render("isha_tp.payumoney_status_template", values)
        else:
            return False

    @http.route(['/programs/pbk/getuserinfo'], auth="public", type='http', methods=['GET'], csrf=False, website=True, cors='*')
    def get_user_info(self, **post):
        """ API to get tp_program_registration info"""
        varprogramid = None
        varprofileid = None
        varpbkmonthyear = None
        varuserregistrationid = None
        print('In function get_user_info')
        reslist = []
        # if fromdatetime == None:
        #  fromdatetime = datetime.now()
        #  fromdatetime = fromdatetime - timedelta(days=180)
        # req_date = datetime(year=fromdatetime.year, month=fromdatetime.month, day=fromdatetime.day)
        # print(req_date)
        # req_date_utc = req_date.replace(tzinfo=pytz.utc)
        # print(req_date_utc)
        fromdatetime =post.get("date")
        try:
            progreginfo = request.env['tp.program.registration'].sudo().search([("date_paid", ">", fromdatetime)])
            print(str(progreginfo))
            # print(str(fromdatetime))
            if len(progreginfo) > 0:
                for rec in progreginfo:
                    if rec.registration_status == "payment_upgraded" or rec.registration_status == "Paid":
                        # progpaymenttrxinfo = request.env['tp.participant.paymenttransaction'].sudo().search(
                        #  [("orderid", "=", rec.orderid)])
                        # if rec.programapplied.id == 1 :
                        #  varprogramid = 2589
                        # elif rec.programapplied.id == 2 :
                        #  varprogramid = 2590
                        varprogramid = rec.programapplied.id
                        # varpbkmonthyear = fromdatetime.month+fromdatetime.year
                        varprofileid = rec.sso_id
                        varparticipant_email = rec.participant_email
                        result = {'programid': varprogramid,
                                  'profileid': varprofileid,
                                  'emailid': varparticipant_email,
                                  'resgistration_status': rec.registration_status
                                  }
                        reslist.append(result)
                jsnresponse = json.dumps(reslist)
                print('Result : ' + jsnresponse)
                return jsnresponse
            # print('Result : ' + str(reslist))
            else:
                raise
        except Exception as e:
            _logger.info('error in push_registration_info')
            print('in exception')
            result = {'programid': None,
                      'profileid': None,
                      'pbkmonthyear': None,
                      'userregistrationid': None
                      }
            return json.dumps(result)

    @http.route('/programs/pbk/registrationform/', type='http', auth="public", methods=["GET","POST"], website=True, csrf=False)
    def registrationform(self, legal_entity="IFINC", consent_grant_status="1", id_token="", testsso="0",program_id="0", utm_medium="", utm_campaign="", utm_source="", utm_term="", utm_content="", **post):
        _logger.info("starting exection registrationform legal_entity"+ str(legal_entity))
        _logger.info("consent_grant_status"+ str(consent_grant_status))
        _logger.info("id_token"+ str(id_token))
        _logger.info("program_id"+ str(program_id))
        _logger.info("**post"+ str(post))
        # Auto redirect Public users to SSO Login page
        sso_config = Configuration('SSO')
        if post.get("offline_logic") == 'True' or post.get("is_offline_reg") == 'True' or post.get('offline_payment') == 'True':
            programvar = post.get('program')
            ipcountry_id = post.get('country')
            countryres = request.env['res.country'].sudo().search([('id', '=', str_to_int(ipcountry_id))])
            lenofrecs = len(countryres)
            fpires = fetch_program_inforegform(countryres.code, str_to_int(programvar));
            if lenofrecs == 0:
                return request.render("isha_tp.registrationlinkbroken")
            else:
                if (fpires['openstatus'] != 'open'):
                    return request.render("isha_tp.registrationclosedtemplate")
            if request.httprequest.method == "GET" and post.get("offline_logic") == 'True':
                qsurl = ""
                try:
                    qsurl = str(request.httprequest.url).split('?')[1]
                except:
                    pass
                if post.get('checksum', False) != hash_url_params(post.get('program', False),
                                                                  post.get('country', False)):
                    return request.render("isha_tp.registrationlinkbroken")
                offline_req = post.get("offline_logic")
                ret = sso_config['SSO_API_SECRET']
                api_key = sso_config['SSO_API_KEY']
                profileidvar = post.get('profile_id')
                ipcountry_id = post.get('country')
                programvar = post.get('program')
                offlineregid = post.get("reg_program")
                # requestidvar = profileidvar + "-" + programvar
                offlineregidrec = request.env['offline.program.registration'].sudo().search([('id', '=', str_to_int(offlineregid))])
                is_offline_registration = post.get('offline_logic')
                try:
                    regional_info = get_region_specific_details(countryres.center_id.region_id.name)
                except Exception as e:
                    _logger.info("Exception in Region", e)
                    pass
                consentgrantStatusflagvar = request.env['tp.ssoapi'].get_consent_info({"legal_entity": legal_entity,
                                                                                       "profileidvar": profileidvar,
                                                                                       "system_token": sso_config[
                                                                                           'SSO_API_SYSTEM_TOKEN'],
                                                                                       "pms_url": sso_config[
                                                                                           'SSO_PMS_ENDPOINT']})
                fullprofileresponsedatVar = request.env['tp.ssoapi'].get_fullprofileresponse_DTO()
                fullprofileresponsedatVar["basicProfile"]["firstName"] = offlineregidrec.first_name
                fullprofileresponsedatVar["basicProfile"]["lastName"] = offlineregidrec.last_name
                fullprofileresponsedatVar["basicProfile"]["email"] = offlineregidrec.participant_email
                fullprofileresponsedatVar["basicProfile"]["countryOfResidence"] = offlineregidrec.country_id.code
                fullprofileresponsedatVar["profileId"] = profileidvar
                # if consentgrantStatusflagvar == True:
                #     fullprofileresponsedatVar = request.env['tp.ssoapi'].get_fullprofile_info(
                #         {"legalEntity": legal_entity,
                #          "profileidvar": profileidvar,
                #          "system_token": sso_config[
                #              'SSO_API_SYSTEM_TOKEN'],
                #          "pms_url": sso_config[
                #              'SSO_PMS_ENDPOINT']})
                validation_errors = []
                values = {}
                ssoadproofuri2 = ""
                isoidproofuri2 = ""
                # request.env['tp.program.schedule'].sudo().search([('id', '=', programvar)])
                programschedules_info = request.env['tp.program.schedule'].sudo().search(
                    [('id', '=', str_to_int(programvar))])
                programscheduleroomdatas_info = request.env['tp.program.schedule.roomdata'].sudo().search(
                    [('pgmschedule_programname', '=', str_to_int(programvar))])
                programschedulepricingdatas_info = request.env['tp.schedule.pricing'].sudo().search(
                    [('pgmpricing_programname', '=', str_to_int(programvar))])
                try:
                    countries_scl_temp = request.env['res.country'].sudo().search(
                        [('name', 'in', ['India', 'United States',
                                         'United Kingdom',
                                         'Malaysia',
                                         'Singapore', 'Lebanon',
                                         'Russian Federation',
                                         'South Africa'])])
                    countries_scl = request.env['res.country'].sudo()
                    for ctry in ['India', 'United States',
                                 'United Kingdom', 'Malaysia',
                                 'Singapore', 'Lebanon',
                                 'Russian Federation', 'South Africa']:
                        countries_scl |= countries_scl_temp.filtered(lambda x: x.name == ctry)
                    countries = request.env['res.country'].sudo().search([('name', 'not in', ['India', 'United States',
                                                                                              'United Kingdom',
                                                                                              'Malaysia',
                                                                                              'Singapore', 'Lebanon',
                                                                                              'Russian Federation',
                                                                                              'South Africa'])]).sorted(
                        lambda x: x.code)
                    result_countries = countries_scl | countries
                    ordered_countries = countries_scl | countries.sorted(lambda x: x.name)
                    profresponse = json.dumps(fullprofileresponsedatVar)
                    amount = offlineregidrec.registration_amount or 0
                    amount = int(amount) if type(amount) == float else amount
                    currency = fpires['curr'] or 'INR'
                    amountcurrency = currency + " " + str(amount)
                    # FIXME: GPMS should get the exact value given in the backend after conversion
                    isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')

                    try:
                        if isishangamonev == 'ishangamone' and str_to_int(ipcountry_id) == 104:
                            result_countries = countryres
                            ordered_countries = countryres
                    except:
                        pass

                    try:
                        if fullprofileresponsedatVar["basicProfile"]["phone"]["countryCode"] != "91":
                            fullprofileresponsedatVar["basicProfile"]["phone"]["countryCode"] = ""
                            fullprofileresponsedatVar["basicProfile"]["phone"]["number"] = ""
                    except:
                        pass
                    values = {
                        # 'programregn': request.env['tp.program.registration'].search([]),
                        'countries': result_countries,
                        'ordered_countries': ordered_countries,
                        'states': request.env['res.country.state'].sudo().search([]),
                        'prgregnprogramnamecategories': request.env['tp.master.program.category'].sudo().search([]),
                        'programschedules': programschedules_info,
                        'programscheduleroomdatas': programscheduleroomdatas_info,
                        'programschedulepricingdatas': programschedulepricingdatas_info,
                        'isoqs': qsurl,
                        'amountcurrency': amountcurrency,
                        'is_ishangamone': isishangamonev,
                        'isofname': fullprofileresponsedatVar,
                        'ssoadproofuri': ssoadproofuri2,
                        'isoidproofuri': isoidproofuri2,
                        'inputobj': profresponse,
                        'enableishaautofill': consentgrantStatusflagvar,
                        'consent_grant_status': consent_grant_status,
                        'program_id': program_id,
                        'program': int(programvar),
                        'country_id': post.get('country'),
                        'tnc': regional_info['tnc'],
                        'cancellation_policy': regional_info['cancellation_policy'],
                        'note_txt': regional_info['note_txt'],
                        'address_text': regional_info['address_text'],
                        'acceptance_text': regional_info['acceptance_text'],
                        'is_offline_reg': True,
                        'amount':amount,
                        'offline_code_id': offlineregidrec.offline_code_id if offlineregidrec.offline_code_id else "",
                        'offline_code_desc': offlineregidrec.offline_code_desc
                    }
                except Exception as e:
                    tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                    _logger.info("Error during rendering goy form" + tb_ex)
                return request.render('isha_tp.pgmregn', values)
            # redirect to payment page if it offline_payment view
            elif (request.httprequest.method == "GET" and post.get("offline_payment") == 'True'):
                try:
                    registration_id = post.get("registration_id")
                    amount = post.get("amount") or 0
                    if registration_id:
                        recdyn = request.env['tp.program.registration'].sudo().search([("id", "=", registration_id)])
                        responsecheckexisting = self.checkExistingTxn(recdyn.orderid, recdyn.sso_id, **post)
                        if responsecheckexisting:
                            return responsecheckexisting
                        countryres = request.env['res.country'].sudo().search(
                            [("id", "=", recdyn.country.id)])
                        recdyn.write({'registration_status': 'payment_initiated'})
                        currencyvariable = fpires['curr'] or 'INR'
                        payment_values = {"recdyn": recdyn.id,
                                          "currency": currencyvariable,
                                          "countrycode": countryres.code,
                                          "amount": recdyn.registration_amount
                                          }
                    return self.payment_page(payment_values)
                except Exception as e:
                    values = {"status": 'Error', 'message': e, 'email': ''}
                    tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                    _logger.info(tb_ex)
                    return request.render("isha_tp.offline_reg_status_template", values)
            # POST Month of Form submit in offlinereg
            elif (request.httprequest.method == "POST" and post.get("offline_logic") == 'True'):
                _logger.info(str(post))
                offlineregid = post.get("reg_program")
                offlineregidrec = request.env['offline.program.registration'].sudo().search([('id', '=', str_to_int(offlineregid))])
                # Take IP country for payment gateway. If no country take India as default
                try:
                    ip_country = int(post.get('country_id'))
                except:
                    ip_country = 104  # Hardcoded IN
                try:
                    programapplied = post.get('program')
                    countryres = request.env['res.country'].sudo().search([('id', '=', str_to_int(ip_country))])
                    lenofrecs = len(countryres)
                    amount = offlineregidrec.registration_amount or 0
                    amount = int(amount) if type(amount) == float else amount
                    # FIXME: GPMS should get the exact value given in the backend after conversion
                    # TODO: hotfix , revisit this later
                    currency = fpires['curr'] or 'INR'
                    amountcurrency = currency + " " + str(amount)
                    unq_id = post.get('profile_id') + "-" + str(programapplied)
                    responsecheckexisting = self.checkExistingTxn(unq_id, post.get('sso_id'), **post)
                    if responsecheckexisting:
                        return responsecheckexisting
                    # packageselection = post.get('packageselection')
                    first_name = post.get('first_name')
                    last_name = post.get('last_name')
                    name_called = "test"  # post.get('name_called')
                    participant_email = post.get('participant_email')
                    gender = post.get('gender')
                    dob = post.get('dob')
                    marital_status = post.get('marital_status')
                    address_line = post.get('address_line')
                    country = post.get('country')
                    # nationality_id = post.get('nationality_id')
                    rstate = post.get('state', False)
                    if (rstate):
                        state = int(rstate)
                    else:
                        state = ''
                    pincode = post.get('pincode')
                    city = post.get('city')
                    countrycode = post.get('countrycode')
                    phone = post.get('phone')
                    language_preferred = post.get('language_preferred')
                    shipping_address_line1 = post.get('address_line')
                    shipping_address_line2 = post.get('address_line')
                    shipping_country = post.get('country')
                    state = post.get('state')
                    if (state):
                        shipping_state = int(state)
                    else:
                        shipping_state = ''
                    shipping_pincode = post.get('pincode')
                    shipping_city = post.get('city')

                    recdyn = request.env['tp.program.registration'].sudo().search([("orderid", "=", unq_id)])
                    if not recdyn:
                        recdyn = request.env['tp.program.registration'].sudo().create({
                            'programapplied': int(programapplied),
                            # 'packageselection': packageselection,
                            'first_name': first_name,
                            'last_name': last_name,
                            'name_called': name_called,
                            'participant_email': participant_email,
                            'gender': gender,
                            'marital_status': marital_status,
                            'dob': dob,
                            'address_line': address_line,
                            'city': city,
                            'country': int(country),
                            'state': state,
                            'pincode': pincode,
                            'countrycode': countrycode,
                            'phone': phone,
                            'language_preferred': language_preferred,
                            'shipping_address_line1': shipping_address_line1,
                            'shipping_address_line2': shipping_address_line2,
                            'shipping_city': shipping_city,
                            'shipping_country': ip_country,
                            'shipping_state': shipping_state,
                            'shipping_pincode': shipping_pincode,
                            'incompleteregistration_pagescompleted': 1,
                            'incompleteregistration_email_send': False,
                            'incompleteregistration_sms_send': False,
                            'registration_email_send': True,
                            'registration_sms_send': True,
                            'registration_status': 'payment_initiated',
                            'orderid': unq_id,
                            'utm_term': utm_term,
                            'utm_medium': utm_medium,
                            'utm_source': utm_source,
                            'utm_content': utm_content,
                            'utm_campaign': utm_campaign,
                            'ip_country': ip_country,
                            'sso_id': post.get('sso_id'),
                            'is_offline_reg': True,
                        })
                    # for x in range(len(attached_file)):.sudo().create
                    offline_code = False
                    offline_code_id = request.env['tp.program.registration.offline.code'].sudo().search(
                        [("id", "=", offlineregidrec.offline_code_id.id)])
                    if offline_code_id:
                        offline_code = offline_code_id.id
                    headers2 = {
                        "content-type": "application/json",
                    }
                    if amount == 0:
                        regstatus = "Paid"
                    else:
                        regstatus = "Pending"
                    recdyn.sudo().write(
                        {"registration_status": regstatus, 'utm_term': utm_term, 'utm_medium': utm_medium,
                         'utm_source': utm_source, 'utm_content': utm_content, 'utm_campaign': utm_campaign,
                         "offline_code_id": offline_code,
                         "registration_amount": amount,
                         "registration_currency":currency,
                         "offline_code_desc": offlineregidrec.offline_code_desc,
                         'language_preferred': language_preferred})
                    baserequrl = request.env['ir.config_parameter'].sudo().get_param('tp.isoapiurlpath')
                    request_url = baserequrl + countryres.code
                    try:
                        req = requests.get(request_url, headers=headers2)
                        req.raise_for_status()
                        respone = req.json()
                        goy_idvar = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 3-Day Online Program')])[0]
                        pbk_idvar = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 1-Day Online Program')])[0]
                        selectidvar = recdyn.programapplied.id
                        reg_url = ""
                        if selectidvar == pbk_idvar.id:
                            reg_url = respone.get("pbk_reg_url")
                        if selectidvar == goy_idvar.id:
                            reg_url = respone.get("goy_reg_url")
                        # reg_url = respone.get("pbk_reg_url")
                        redirect_url = reg_url + "&registration_id=" + str(
                            recdyn.id) + "&offline_payment=True"
                        mail_template_id = request.env.ref('isha_tp.tp_mail_template_program_registration')
                        if recdyn.registration_amount == 0:
                            try:
                                self.put_user_sso_info(unq_id)
                            except:
                                pass
                        if recdyn.registration_amount>0:
                            mail_template_id = request.env.ref('isha_tp.tp_mail_offline_payment_template')
                            recdyn.sudo().write({"created_by": redirect_url})
                        try:
                            request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(recdyn.id,
                                                                                                      force_send=True)
                            values = {"status": "COMPLETED", 'email': recdyn.participant_email}
                            return request.render("isha_tp.offline_reg_status_template", values)
                        except Exception as e:
                            values = {"status": 'Error', 'message': "There is error in sending mail",
                                      'email': recdyn.participant_email}
                            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                            _logger.info(tb_ex)
                            return request.render("isha_tp.offline_reg_status_template", values)
                    except Exception as e:
                        values = {"status": 'Error', 'message': e, 'email': recdyn.participant_email}
                        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                        _logger.info(tb_ex)
                    return request.render("isha_tp.offline_reg_status_template", values)
                except Exception as e:
                    values = {"status": 'Error', 'message': e, 'email': ''}
                    tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                    _logger.info(tb_ex)
                    return request.render("isha_tp.offline_reg_status_template", values)



            else:
                return request.render("isha_tp.registrationlinkbroken")


        elif request.httprequest.method == "GET":
            schedule_id = post.get('program')
            ipcountry_id = post.get('country')
            countryres = request.env['res.country'].sudo().search([('id', '=', ipcountry_id)])
            _logger.info(str(countryres))
            _logger.info(str(ipcountry_id))
            lenofrecs = len(countryres)
            if lenofrecs == 0:
                return request.render("isha_tp.registrationlinkbroken")
                pass
            else:
                fpires = fetch_program_inforegform(countryres.code,str_to_int(schedule_id))
                if (fpires['openstatus'] != 'open'):
                    return request.render("isha_tp.registrationclosedtemplate")
                pass
            return simulate_sso_login(sso_config,legal_entity)

        # Successful login from SSO
        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            _logger.info("successful login from sso")
            qsurl = ""
            try:
                qsurl = str(request.httprequest.url).split('?')[1]
            except:
                pass
            # validate the url params
            if post.get('checksum', False) != hash_url_params(post.get('program', False), post.get('country', False)):
                return request.render("isha_tp.registrationlinkbroken")

            ret = sso_config['SSO_API_SECRET']
            api_key = sso_config['SSO_API_KEY']
            session_id = request.httprequest.form['session_id']
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            sso_user_profile = eval(sso_get_user_profile(sso_config['SSO_LOGIN_INFO_ENDPOINT'], data).text)
            profileidvar = sso_user_profile['autologin_profile_id']
            ipcountry_id = post.get('country')
            programvar = post.get('program')
            requestidvar = profileidvar + "-" + programvar
            countryres = request.env['res.country'].sudo().search([('id', '=', str_to_int(ipcountry_id))])
            lenofrecs = len(countryres)
            is_offline_registration = post.get('offline')
            registration_id = post.get('registration_id')
            is_upgrade_logic = post.get('upgrade_logic')
            # if registration_id and is_upgrade_logic:
            #     recdyn = request.env['tp.program.registration'].sudo().search([("id", "=", registration_id)])
            #     upgrade_amount = 0
            #     program_details = request.env['tp.program.registration'].fetch_program_inforegform(countryres.code,
            #                                                                                        recdyn.programapplied.id)
            #     upgrade_amount = post.get('amount')
            #     recdyn.write({'orderid': requestidvar, 'registration_status': 'payment_initiated'})
            #     payment_values = {"recdyn": recdyn.id,
            #                       "currency": countryres.currency_id.name,
            #                       "countrycode": countryres.code,
            #                       "amount": upgrade_amount
            #                       }
            #     return self.payment_page(payment_values)

            countryres = request.env['res.country'].sudo().search([('id', '=', str_to_int(ipcountry_id))])
            lenofrecs = len(countryres)
            if lenofrecs == 0:
                return request.render("isha_tp.registrationlinkbroken")
            else:
                fpires = fetch_program_inforegform(countryres.code, programvar)
                _logger.info("price in goy submit"+str(fpires))
                if registration_id and is_upgrade_logic:
                    recdyn = request.env['tp.program.registration'].sudo().search([("id", "=", registration_id)])
                    newprogamount = fpires['price'] or 0
                    currencyvariableupgrade = fpires['curr'] or 'INR'
                    _logger.info("inside if registration_id and is_upgrade_logic")
                    _logger.info(str(registration_id))
                    _logger.info(str(countryres.code))
                    _logger.info(str(programvar))
                    _logger.info(str(newprogamount))
                    fpiresold = fetch_program_inforegform(countryres.code, str(recdyn.previous_programapplied.id))
                    _logger.info("old price in goy upgrade" + str(fpiresold))
                    _logger.info(str(recdyn.previous_programapplied.id))
                    oldprogamount = fpiresold['price'] or 0
                    _logger.info(str(oldprogamount))
                    upgrade_amount = newprogamount - oldprogamount
                    _logger.info(str(upgrade_amount))
                    #program_details = request.env['tp.program.registration'].fetch_program_inforegform(countryres.code,
                    #                                                                                   recdyn.programapplied.id)
                    #upgrade_amount = post.get('amount')
                    recdyn.write({'orderid': requestidvar, 'registration_status': 'payment_initiated'})
                    payment_values = {"recdyn": recdyn.id,
                                      "currency": currencyvariableupgrade,
                                      "countrycode": countryres.code,
                                      "amount": upgrade_amount
                                      }
                    return self.payment_page(payment_values)
                if (fpires['openstatus'] != 'open'):
                    pass#return request.render("isha_tp.registrationclosedtemplate")

            response = self.checkExistingTxn(requestidvar, profileidvar, **post)
            if response:
                return response
            try:
                regional_info = get_region_specific_details(countryres.center_id.region_id.name)
            except Exception as e:
                _logger.info("Exception in Region", e)
                pass
            consentgrantStatusflagvar = request.env['tp.ssoapi'].get_consent_info({"legal_entity": legal_entity,
                                                                                   "profileidvar": profileidvar,
                                                                                   "system_token":sso_config['SSO_API_SYSTEM_TOKEN'],
                                                                                   "pms_url":sso_config['SSO_PMS_ENDPOINT']})
            fullprofileresponsedatVar = request.env['tp.ssoapi'].get_fullprofileresponse_DTO()
            if consentgrantStatusflagvar == True:
                fullprofileresponsedatVar = request.env['tp.ssoapi'].get_fullprofile_info({"legalEntity": legal_entity,
                                                                                           "profileidvar": profileidvar,
                                                                                           "system_token":sso_config['SSO_API_SYSTEM_TOKEN'],
                                                                                           "pms_url":sso_config['SSO_PMS_ENDPOINT']})
            validation_errors = []
            values ={}
            ssoadproofuri2 = ""
            isoidproofuri2 = ""
            #request.env['tp.program.schedule'].sudo().search([('id', '=', programvar)])
            programschedules_info = request.env['tp.program.schedule'].sudo().search([('id', '=', str_to_int(programvar))])
            programscheduleroomdatas_info = request.env['tp.program.schedule.roomdata'].sudo().search([('pgmschedule_programname', '=', str_to_int(programvar))])
            programschedulepricingdatas_info = request.env['tp.schedule.pricing'].sudo().search([('pgmpricing_programname', '=', str_to_int(programvar))])
            try:
                countries_scl_temp = request.env['res.country'].sudo().search([('name','in',['India','United States',
                                                                                        'United Kingdom','Malaysia',
                                                                                        'Singapore','Lebanon',
                                                                                        'Russian Federation','South Africa'])])
                countries_scl = request.env['res.country'].sudo()
                for ctry in ['India','United States',
                        'United Kingdom','Malaysia',
                        'Singapore','Lebanon',
                        'Russian Federation','South Africa']:
                    countries_scl |= countries_scl_temp.filtered(lambda x: x.name == ctry)
                countryres = request.env['res.country'].sudo().search([('id', '=', str_to_int(ipcountry_id))])
                lenofrecs = len(countryres)
                if lenofrecs == 0:
                    return request.render("isha_tp.registrationlinkbroken")
                else:
                    fpires = fetch_program_inforegform(countryres.code, str_to_int(programvar));
                    if (fpires['openstatus'] != 'open'):
                        pass#return request.render("isha_tp.registrationclosedtemplate")
                countries =  request.env['res.country'].sudo().search([('name','not in',['India','United States',
                                                                                        'United Kingdom','Malaysia',
                                                                                        'Singapore','Lebanon',
                                                                                        'Russian Federation','South Africa'])]).sorted(lambda x: x.code)
                result_countries = countries_scl | countries
                ordered_countries =  countries_scl | countries.sorted(lambda x:x.name)
                profresponse = json.dumps(fullprofileresponsedatVar)
                amount = fpires['price'] or 0
                # fetch the amount field from registration record if it is offline mode
                if is_offline_registration:
                    registration_id_model = request.env['tp.program.registration'].sudo().browse(registration_id)
                    amount = registration_id_model.amount
                # FIXME: GPMS should get the exact value given in the backend after conversion
                amount = int(amount) if type(amount) == float else amount
                currency = fpires['curr'] or 'INR'
                amountcurrency = currency + " " + str(amount)
                isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')

                try:
                    if isishangamonev == 'ishangamone' and str_to_int(ipcountry_id) == 104:
                        result_countries = countryres
                        ordered_countries = countryres
                except:
                    pass

                try:
                    if fullprofileresponsedatVar["basicProfile"]["phone"]["countryCode"] != "91":
                        fullprofileresponsedatVar["basicProfile"]["phone"]["countryCode"] = ""
                        fullprofileresponsedatVar["basicProfile"]["phone"]["number"] = ""
                except:
                    pass
                values = {
                    # 'programregn': request.env['tp.program.registration'].search([]),
                    'countries': result_countries,
                    'ordered_countries': ordered_countries,
                    'states': request.env['res.country.state'].sudo().search([]),
                    'prgregnprogramnamecategories': request.env['tp.master.program.category'].sudo().search([]),
                    'programschedules': programschedules_info,
                    'programscheduleroomdatas': programscheduleroomdatas_info,
                    'programschedulepricingdatas': programschedulepricingdatas_info,
                    'isoqs': qsurl,
                    'amount':amount,
                    'amountcurrency': amountcurrency,
                    'is_ishangamone': isishangamonev,
                    'isofname': fullprofileresponsedatVar,
                    'ssoadproofuri': ssoadproofuri2,
                    'isoidproofuri': isoidproofuri2,
                    'inputobj': profresponse,
                    'enableishaautofill': consentgrantStatusflagvar,
                    'consent_grant_status': consent_grant_status,
                    'program_id': program_id,
                    'program': int(programvar),
                    'country_id': post.get('country'),
                    'tnc': regional_info['tnc'],
                    'cancellation_policy': regional_info['cancellation_policy'],
                    'note_txt': regional_info['note_txt'],
                    'address_text': regional_info['address_text'],
                    'acceptance_text': regional_info['acceptance_text'],
                    'is_offline_reg': False,
                    'offline_code_id': "",
                    'offline_code_desc': "",
                }
            except Exception as e:
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info("Error during rendering goy form" + tb_ex)
            return request.render('isha_tp.pgmregn', values)
        elif request.httprequest.method == "POST":
            _logger.info(str(post))
            # Take IP country for payment gateway. If no country take India as default
            try:
                ip_country = int(post.get('country_id'))
            except:
                ip_country = 104  # Hardcoded IN

            validation_errors = []
            values = {}
            first_data = []
            pgmrecs = []
            try:

                response = self.checkExistingTxn(post.get('sso_id') + "-" + str(post.get('program')),
                                                 post.get('sso_id'), **post)
                programapplied = post.get('program')
                if response:
                    return response
                countryres = request.env['res.country'].sudo().search([('id', '=', str_to_int(ip_country))])
                lenofrecs = len(countryres)
                if lenofrecs == 0:
                    return request.render("isha_tp.registrationlinkbroken")
                else:
                    fpires = fetch_program_inforegform(countryres.code, str_to_int(programapplied))
                    if (fpires['openstatus'] != 'open'):
                        pass#return request.render("isha_tp.registrationclosedtemplate")

                amount = fpires['price'] or 0
                # FIXME: GPMS should get the exact value given in the backend after conversion
                amount = int(amount) if type(amount) == float else amount
                # TODO: hotfix , revisit this later
                try:
                    if amount == 0:
                        values = {"support_email": 'graceofyoga.online@ishafoundation.org',
                                  "status": "ERROR",
                                  "program_name": "",
                                  "transactionId": "",
                                  "region_support_email_id": "",
                                  "err_msg": "", }
                        _logger.info("Amount is null")
                        return request.render("isha_tp.payumoney_status_template", values)
                except:
                    pass
                currency = fpires['curr'] or 'INR'
                unq_id = post.get('sso_id') + "-" + str(programapplied)
                # packageselection = post.get('packageselection')
                first_name = post.get('first_name')
                last_name = post.get('last_name')
                name_called = "" #post.get('name_called')
                participant_email = post.get('participant_email')
                gender = post.get('gender')
                dob = post.get('dob')
                marital_status = post.get('marital_status')
                address_line = post.get('address_line')
                country = post.get('country')
                # nationality_id = post.get('nationality_id')
                rstate = post.get('state', False)
                if (rstate):
                    state = int(rstate)
                else:
                    state = ''
                pincode = post.get('pincode')
                city = post.get('city')
                # utm_medium = post.get('utm_medium') or ""
                # utm_campaign = post.get('utm_campaign') or ""
                # utm_source = post.get('utm_source') or ""
                # utm_term = post.get('utm_term') or ""
                # utm_content = post.get('utm_content') or ""
                countrycode = post.get('countrycode')
                phone = post.get('phone')
                language_preferred=post.get('language_preferred')
                shipping_address_line1 = post.get('address_line')
                shipping_address_line2 = post.get('address_line')
                shipping_country = post.get('country')
                state = post.get('state')
                if (state):
                    shipping_state = int(state)
                else:
                    shipping_state = ''
                shipping_pincode = post.get('pincode')
                shipping_city = post.get('city')
                recdyn = request.env['tp.program.registration'].sudo().search([("orderid", "=", unq_id)])
                if not recdyn:
                    recdyn = request.env['tp.program.registration'].sudo().create({
                        'programapplied': int(programapplied),
                        # 'packageselection': packageselection,
                        'first_name': first_name,
                        'last_name': last_name,
                        'name_called': name_called,
                        'participant_email': participant_email,
                        'gender': gender,
                        'marital_status': marital_status,
                        'dob': dob,
                        'address_line': address_line,
                        'city': city,
                         'country': int(country),
                         'state': state,
                         'pincode': pincode,
                        'countrycode': countrycode,
                         'phone': phone,
                        'language_preferred': language_preferred,
                        'shipping_address_line1': shipping_address_line1,
                        'shipping_address_line2': shipping_address_line2,
                        'shipping_city': shipping_city,
                        'shipping_country': ip_country,
                        'shipping_state': shipping_state,
                        'shipping_pincode': shipping_pincode,
                        'incompleteregistration_pagescompleted': 1,
                        'incompleteregistration_email_send': False,
                        'incompleteregistration_sms_send': False,
                        'registration_email_send': True,
                        'registration_sms_send': True,
                        'registration_status':'payment_initiated',
                        'orderid':unq_id,
                        'utm_term': utm_term,
                        'utm_medium': utm_medium,
                        'utm_source': utm_source,
                        'utm_content': utm_content,
                        'utm_campaign': utm_campaign,
                        'ip_country':ip_country,
                        'sso_id':post.get('sso_id')
                    })
                # for x in range(len(attached_file)):.sudo().create
                if recdyn:
                    recdyn.sudo().write({"registration_status": "payment_initiated",'utm_term': utm_term,'utm_medium': utm_medium,'utm_source': utm_source,'utm_content': utm_content,'utm_campaign': utm_campaign,'language_preferred': language_preferred})
                first_data = request.env['tp.program.registration'].sudo().search([('access_token', '=', recdyn.access_token)])
                progtypevar = first_data.programapplied.pgmschedule_programtype
                df = datetime.now()
                callbackUrl = request.env['ir.config_parameter'].sudo().get_param('tp.gpms_call_back_url')
                gpms_payurl = request.env['ir.config_parameter'].sudo().get_param('tp.gpms_payment_url')
                values = {
                    "firstName": recdyn.first_name,
                    "lastName": recdyn.last_name,
                    "email": recdyn.participant_email,
                    "mobilePhone": str(recdyn.phone),
                    "addressLine1": shipping_address_line1[:50], # GPMS accepts max 50 chars
                    "addressLine2": shipping_address_line1[:50], # GPMS accepts max 50 chars
                    "city": shipping_city,
                    "state": shipping_state,
                    "postCode": shipping_pincode,
                    "country": recdyn.country.code if recdyn.country else "IN",
                    "amount": amount,
                    "currency": currency,
                    "callbackUrl": callbackUrl,
                    "requestId": str(unq_id),
                    "ssoId": post.get('sso_id'),
                    "program_code":post.get('program'),
                    "program_type":recdyn.programapplied.pgmschedule_programtype.description,
                    "gpms_payurl":gpms_payurl
                }
                _logger.info(str(values))
                return request.render("isha_tp.isha_website_payment", values)
            except Exception as e:
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.error("Error caught during request creation" + tb_ex )


    @http.route('/templeprograms/get_center', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
    def scope_location_data(self, country_id=None, state_id=None, **post):
        _logger.info("starting execution scope_location_data country_id"+country_id)
        _logger.info("state_id"+state_id)
        _logger.info("post"+str(post))
        state_ids = {}
        center_ids = {}
        if country_id:
            # For india return the possible states
            if int(country_id) == 104:
                state_data = request.env['res.country.state'].search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['id','name']
                )
                for state in state_data:
                    state_ids[str(state['id'])] = state['name']

            else:
                # For non india get the list of centers from i_p_c and r_c
                center_data = request.env['isha.pincode.center'].search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['center_id']
                )

                center_data += request.env['res.country'].search_read(
                        domain=[('id', '=', int(country_id))],
                        fields=['center_id']
                    )

                for center in center_data:
                    if center['center_id']:
                        center_ids[str(center['center_id'][0])] = True
        elif state_id:
            # For direct state selection get the center from r_c_s directly
            center_data = request.env['res.country.state'].search_read(
                domain=[('id', '=', int(state_id))],
                fields=['center_ids']
            )
            for state in center_data:
                for center in state['center_ids']:
                    center_ids[str(center)] = True

        result = {'state_ids': state_ids, 'center_ids': center_ids}
        return json.dumps(result)

    @http.route('/programs/pbk/validatepayment', type='json', methods=['GET', 'POST'],
                auth='none', csrf=False)
    def validatepayment(self, **post):
        _logger.info('************ Validate payment ***************')
        req_obj = eval(request.httprequest.data)
        requestId = req_obj.get('requestId', '')
        if 'SKT_MUSIC' in requestId or 'SKT_KALARI' in requestId  or 'SKT_' in requestId:
            _logger.info(str('*******Samaskriti********'))
            return samaskriti_validatepayment(self, **post)
        elif 'KTG' in requestId:
            _logger.info(str('******* Kshetragna ********'))
            return livingspace_validatepayment(self, **post)
        else:
            _logger.info(str('******* Others ********'))
            _logger.info('inside validatepayment post' + str(post))
            resvar = {
                "allow": False,
                "amount": 0,
                'error': {
                    "code": 0,
                    "message": "validate payment failed"
                }
            }
            try:
                _logger.info(str(eval(request.httprequest.data)))
                req_obj = eval(request.httprequest.data)
                requestId = req_obj.get('requestId','')
                amountvar = req_obj.get('amount')
                time_ts = datetime.now() - timedelta(seconds=120)
                valid_txn = request.env['tp.program.registration'].sudo().search([('orderid', '=', requestId),
                                                                                  ('registration_status', '=',
                                                                                   'payment_initiated')
                                                                                  ])
                _logger.info(requestId)
                if len(valid_txn) > 0:
                    res = {
                        "allow": True,
                        "amount":amountvar,
                    }
                    _logger.info('inside true validate' + str(res))
                else:
                    resvar = {
                        "allow": False,
                        "amount": 0,
                        'error': {
                            "code": 0,
                            "message": "validate payment failed"
                        }
                    }
                    # _logger.info('inside false validate' + str(res))
                return res
            except Exception as e:
                _logger.info('error in validatepayment')
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                return resvar
            # return request.render("isha_tp.pgmregn")

    @http.route('/programs/pbk/gpmsstatus', type='http', methods=['GET', 'POST'],
                auth='public', csrf=False)
    def gpmsstatus(self, **post):
        request_id = str(post.get('request_id'))
        _logger.info('************ GPMS Status ***************')
        _logger.info("Request ID"+ str(request_id))
        req_obj = eval(request.httprequest.data)
        requestId = req_obj.get('requestId', '')
        _logger.info("RequestID from http request"+ str(requestId))
        if 'SKT_MUSIC' in requestId or 'SKT_KALARI' in requestId or 'SKT_' in requestId:
            return samaskriti_gpmsstatus(self, **post)
        elif 'KTG' in requestId:
            return livingspace_gpmsstatus(self, **post)
        else:
            _logger.info('inside gmpsstatus post'+str(post))
            errorlog('inside gmpsstatus post'+str(post))
            try:
                # raise Exception
                _logger.info(str(eval(request.httprequest.data)))
                # _logger.info(str(post))
                posteddata = eval(request.httprequest.data)
                try:
                    _logger.info('inside gmpsstatus post' + str(posteddata))
                except:
                    pass

                # c_value_gpms = dict()

                c_value_gpms = {
                              'paymentid' : posteddata['paymentId'],
                              'invoiceid' : posteddata['invoiceId'],
                              'addlinfo': posteddata['addlInfo'],
                              'paymentmethod': posteddata['paymentMethod'],
                              'paymentdata': posteddata['paymentData'],
                              'ereceipturl': posteddata['receiptUrl'],
                              'payment_status': posteddata['paymentStatus'],
                              'paymenttrackingid': posteddata['transactionId'],
                              'orderid': posteddata['requestId']
                              }

                self.update_payment_transaction(c_value_gpms)
                try:
                    self.put_user_sso_info(posteddata['requestId'])
                except:
                    pass
                return json.dumps({'message': 'Success'})
            except Exception as e:
                _logger.info('error in gmpsstatus')
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                errorlog('error in gmpsstatus')
                errorlog(tb_ex)
                datavar = {}
                datavar['requestId'] = 1000;
                datavar['error'] = tb_ex
                return json.dumps({'message': 'Fail'})
                return werkzeug.wrappers.Response(datavar, content_type='application/json;charset=utf-8', status=400)

    @http.route('/programs/pbk/paystatus', type='http', methods=['GET', 'POST'],
                auth='public', csrf=False)
    def paystatus(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                        errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
        c_value_pr = {'payment_status': status,
                      'amount': amount,
                      'paymenttrackingid': transactionId,
                      'orderid': requestId,
                      'currency': currency,
                      'errorcode': errCode,
                      'errorreason': errReason,
                      'errormsg': errMsg,
                      'errMsg': errMsg
                      }
        return self.paymentresponse_returntemplate(c_value_pr)

    @http.route('/programs/pbk/paymentsuccess', type='http', methods=['GET', 'POST'],
                auth='public', csrf=False)
    def paymentsuccess(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                       errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
        c_value_pr = {'payment_status': status,
                      'amount': amount,
                      'paymenttrackingid': transactionId,
                      'orderid': requestId,
                      'currency': currency,
                      'errorcode': errCode,
                      'errorreason': errReason,
                      'errormsg': errMsg
                      }
        return self.paymentresponse_returntemplate(c_value_pr)

    @http.route('/programs/pbk/paymentresponse', type='http', methods=['GET', 'POST'],
                auth='public', csrf=False)
    def paymentresponse(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                        errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
        _logger.info('************ Payment Reponse Status ***************')
        request_id = requestId
        _logger.info("Request ID" + str(request_id))
        if 'SKT_MUSIC' in request_id or 'SKT_KALARI' in request_id or 'SKT_' in request_id:
            _logger.info('Samaskriti-- Payment Response ')
            return samaskriti_paymentresponse(self, status, amount, transactionId, requestId, currency, errorCode,
                        errorReason, errorMsg, errCode, errReason, errMsg, **post)
        elif 'KTG' in request_id:
            _logger.info('LIVING SPACES-- Payment Response ')
            return livingspace_paymentresponse(self, status, amount, transactionId, requestId, currency, errorCode,
                        errorReason, errorMsg, errCode, errReason, errMsg, **post)
        else:
            _logger.info('----------------->    inside paymentresponse')
            _logger.info('status' + str(status))
            _logger.info('amount' + str(amount))
            _logger.info('transactionId' + str(transactionId))
            _logger.info('requestId' + str(requestId))
            _logger.info('post' + str(post))
            errorlog('paymentresponse post' + str(post))

            try:
                # c_value_pr = dict()

                c_value_pr = {'payment_status': status,
                              'amount': amount,
                              'paymenttrackingid': transactionId,
                              'orderid': requestId,
                              'currency': currency,
                              'errorcode': errCode,
                              'errorreason': errReason,
                              'errormsg': errMsg
                              }
                urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse", "/programs/pbk/paystatus")
                if status == "COMPLETED":
                    urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse",
                                                             "/programs/pbk/paymentsuccess")
                    return werkzeug.utils.redirect(urlvar)
                if status == "ERROR":
                    urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse",
                                                             "/programs/pbk/paystatus")
                    return werkzeug.utils.redirect(urlvar)

                return werkzeug.utils.redirect(urlvar)  # INPROGRESSself.paymentresponse_returntemplate(c_value_pr)
            except Exception as e:
                _logger.info('error in gmpsstatus')
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                errorlog('paymentresponse error' + str(tb_ex))
                datavar = {}
                datavar['requestId'] = 1000
                datavar['error'] = tb_ex
                return json.dumps({'message': 'Fail'})
            # return werkzeug.wrappers.Response(datavar, content_type='application/json;charset=utf-8', status=400)

    def update_payment_transaction(self, c_value):
        """ Update the Payment Transaction on every hits
        :param:@array of values
        @:return: display the msg in HTML template"""
        _logger.info("startinng execution update_payment_transation c_value" + str(c_value['orderid']))
        errorlog("startinng execution update_payment_transation c_value" + str(c_value['orderid']))
        rec_id = str(c_value['orderid'])
        err_msg = ""
        person_name = ""
        support_email = 'graceofyoga.online@ishafoundation.org'
        p_registration_rec = request.env['tp.program.registration'].sudo().search([("orderid", "=", rec_id)])
        msg = "Transaction_Failure"
        mail_template_id = None
        if c_value.get("payment_status") == "ERROR":
            err_msg = c_value.get("errMsg")
        # if p_registration_rec.previous_programapplied:
        #     prev_registration_rec = request.env['tp.program.registration'].sudo().search(
        #         [("id", "=", p_registration_rec.previous_programapplied)])
        #     prev_registration_rec.write({"registration_status": "payment_upgraded"})
        c_value['transactiontype'] = 'asyncresponse'
        try:
            if p_registration_rec and p_registration_rec.previous_programapplied and c_value['payment_status'] == "COMPLETED":
                prev_registration_rec = request.env['tp.program.registration'].sudo().search([("id", "=", p_registration_rec.previous_upgraded_fromprogreg)])
                prev_registration_rec.write({"registration_status": "payment_upgraded"})
        except:
            pass

        payment_transaction = request.env['tp.participant.paymenttransaction'].sudo().create(c_value)
        if payment_transaction:
            state = "payment_failed"
            if c_value['payment_status'] == "COMPLETED":
                msg = "Transaction_Success"
                state = "Paid"
                mail_template_id = request.env.ref('isha_tp.tp_mail_template_program_registration')

            elif c_value['payment_status'] == "INPROGRESS":
                msg = "Transaction_InProgress"
                state = "payment_inprogress"
            elif c_value['payment_status'] == "ERROR":
                msg = "Transaction_failed"
                state = "payment_failed"
                mail_template_id = request.env.ref('isha_tp.tp_mail_template_program_payment_failed')

            if p_registration_rec:
                support_email = p_registration_rec.support_email_id if p_registration_rec.support_email_id else support_email
                c_value['programregistration'] = int(p_registration_rec.id)
                p_registration_rec.sudo().write({"registration_status": state,'online_transaction_id':c_value.get("paymenttrackingid"),'date_paid': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
                person_name = p_registration_rec.first_name
                p_registration_rec._compute_seatallocation()
                if mail_template_id and \
                        p_registration_rec.registration_status != p_registration_rec.last_email_sent_status:
                    request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(p_registration_rec.id,
                                                                                              force_send=True)
                    p_registration_rec.sudo().write({
                        "last_email_sent_status": p_registration_rec.registration_status,
                        "last_email_sent_ts": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    })

    def paymentresponse_returntemplate(self, c_value):
        """ Update the Payment Transaction on every hits
        :param:@array of values
        @:return: display the msg in HTML template"""
        _logger.info("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
        errorlog("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
        rec_id = str(c_value['orderid'])
        # below line need to removed - enable only for testing
        # self.put_user_sso_info(rec_id)
        err_msg = ""
        person_name = ""
        support_email = 'graceofyoga.online@ishafoundation.org'
        p_registration_rec = request.env['tp.program.registration'].sudo().search([("orderid", "=", rec_id)])
        if p_registration_rec:
            person_name = p_registration_rec.first_name
        msg = "Transaction_Failure"
        mail_template_id = None
        if c_value.get("payment_status") == "ERROR":
            err_msg = c_value.get("errMsg")
            # Error validation does not have order id. so no need to create the record in the payment txn table.
            if err_msg == 'ERR_VALIDATION':
                return request.render("isha_tp.payumoney_status_template", {"support_email": support_email,
                  "status": c_value.get("payment_status"),
                  "transactionId": c_value.get("paymenttrackingid"),
                  "err_msg": c_value.get("errormsg") if c_value.get("errormsg") else err_msg,
                  "order_id": c_value.get("orderid") if c_value.get("orderid") else "",
                  "error_code": c_value.get("errorcode") if c_value.get("errorcode") else "",
                  "person_name": "",
                  "object": p_registration_rec,
                  "region_support_email_id": p_registration_rec.support_email_id,
                  "is_ishangamone": request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                  })

        c_value['transactiontype'] = 'synccallback'
        progreg = request.env['tp.program.registration'].sudo().search([("orderid", "=", rec_id)])

        if len(progreg) > 0:
            try:
                c_value['programname'] = progreg.programapplied.pgmschedule_programname
                c_value['first_name'] = progreg.first_name
                c_value['last_name'] = progreg.last_name
                c_value['email'] = progreg.participant_email
                c_value['programtype'] = progreg.programapplied.pgmschedule_programtype.programtype
            except:
                pass

        payment_transaction = request.env['tp.participant.paymenttransaction'].sudo().create(c_value)

        values = {"support_email": support_email,
                  "status": c_value.get("payment_status"),
                  "transactionId": c_value.get("paymenttrackingid"),
                  "err_msg": c_value.get("errormsg") if c_value.get("errormsg") else err_msg,
                  "order_id": c_value.get("orderid") if c_value.get("orderid") else "",
                  "error_code": c_value.get("errorcode") if c_value.get("errorcode") else "",
                  "person_name": "   " + person_name,
                  "object": p_registration_rec,
                  "region_support_email_id": p_registration_rec.support_email_id,
                  "is_ishangamone": request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                  }
        return request.render("isha_tp.payumoney_status_template", values)

    # def inserttemplog(self, logtext):
    #     self.env['tp.tempdebuglog'].create({
    #         'logtext': logtext
    #     })

    def put_user_sso_info(self,rec_id):
        """ API to get tp_program_registration info spefici to new SSO application"""
        varprogramid = None
        _logger.info('Inside function put_user_sso_info '+ str(rec_id))
        reslist = {"participants":[]}
        arrobj = []
        progreginfo = request.env['tp.program.registration'].sudo().search([("orderid", "=", rec_id)])
        try:
            progreginfo = request.env['tp.program.registration'].sudo().search([("orderid", "=", rec_id)])
            goy_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 3-Day Online Program')])[0]
            pbk_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 1-Day Online Program')])[0]
            _logger.info(str(progreginfo))
            if len(progreginfo) > 0:
                for rec in progreginfo:
                    _logger.info('fetching program registration info')
                    if rec.registration_status == "Paid":
                        res_countryinfo = request.env['res.country'].sudo().search(
                            [("id", "=", rec.country.id)])
                        res_stateinfo = request.env['res.country.state'].sudo().search(
                            [("id", "=", rec.state.id)])
                        res_langinfo = request.env['res.lang'].sudo().search(
                            [("id", "=", rec.language_preferred.id)])
                        if len(res_countryinfo) > 0:
                            countryname = res_countryinfo.name
                        else:
                            countryname = 'NULL'
                        if len(res_stateinfo) > 0:
                            statename = res_stateinfo.name
                        else:
                            statename = 'NULL'
                        if len(res_langinfo) > 0:
                            langname = res_langinfo.name
                        else:
                            langname = 'English'
                        if rec.programapplied.id == goy_id.id:
                            varprogram = 'THREE_DAY'
                        elif rec.programapplied.id == pbk_id.id:
                            varprogram = 'ONE_DAY'
                        varcity = rec.city
                        varname = rec.first_name + ' ' + rec.last_name
                        varprofileid = rec.sso_id
                        _logger.info(varprofileid)
                        _logger.info(varname)
                        _logger.info(varcity)
                        # varparticipant_email = rec.participant_email
                        result = {'ssoId': varprofileid,
                                  'name': varname,
                                  'streamLanguage': langname,
                                  'city': varcity,
                                  'state': statename,
                                  'country': countryname,
                                  'programType': varprogram
                                  }
                        # reslist.participants.append(result)
                        if progreginfo.previous_programapplied:
                            result['upgrade'] = True
                        arrobj.append(result)
                jsnresponse = json.dumps({"participants":arrobj})
                _logger.info('Result : ' + jsnresponse)
            else:
                _logger.info('No data for orderid - '+rec_id)
                raise
            try:
                headersinfo = {
                    "content-type": "application/json"
                }
                # requestconsent_url = "https://www.goy.wardrobefactor.com/admin/genStudentSessions?key=D%238tgJ@x4G@sxz9D7qJQ"
                requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
                    'tp.goyonlinessoapiurl') + "?key=" + request.env['ir.config_parameter'].sudo().get_param(
                    'tp.goyonlinessoapiurlkey')
                reqvar = requests.post(requestconsent_url, jsnresponse, headers=headersinfo)
                reqvar.raise_for_status()
                progreginfo.sudo().write( {"pushedtosaso": 'True'})
                _logger.info('data pushed to saso')
                _logger.info(reqvar.text)
            except:
                _logger.info('In exception')
                progreginfo.sudo().write({"pushedtosaso": 'Error'})
                pass
        except Exception as ex2:
            _logger.info('error in put_user_sso_info')
            try:
                progreginfo.sudo().write({"pushedtosaso": 'Error'})
            except:
                pass
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)
            print(tb_ex)
