from odoo.addons.portal.controllers.portal import CustomerPortal

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR


class GOYPortalController(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(GOYPortalController, self)._prepare_portal_layout_values()
        return values

    @http.route(['/onlinemegaprograms/goy'], type='http', auth="user", website=True)
    def query_goy_data(self, page=1, date_begin=None, date_end=None, sortby=None, search_in='name', search='', **kw):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        values = self._prepare_portal_layout_values()
        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'create_date'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'create_date desc'},
            'write_date_asc': {'label': _('Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Updated On (Desc)'), 'order': 'write_date desc'},
        }
        searchbar_inputs = {
            'name': {'input': 'name', 'label': _('Search in Contact Name')},
            'email': {'input': 'email', 'label': _('Search in Email')},
            'phone': {'input': 'phone', 'label': _('Search in Phone')},
        }

        domain = []
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('email', 'all'):
                search_domain = OR([search_domain, [('participant_email', 'ilike', search)]])
            if search_in in ('phone', 'all'):
                search_domain = OR([search_domain, [('phone', 'ilike', search)]])
            if search_in in ('name', 'all'):
                search_domain = OR([search_domain, ['|',('first_name', 'ilike', search),('last_name','ilike',search)]])

            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']
        goy_model_sudo = request.env['tp.program.registration'].sudo()
        goy_recs = goy_model_sudo.search(domain, order=order, limit=5) if search else goy_model_sudo

        values.update({
            'goy_recs': goy_recs,
            'page_name': 'goy',
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            'default_url': '/onlinemegaprograms/goy',
        })
        return request.render("isha_tp.portal_goy_view", values)
