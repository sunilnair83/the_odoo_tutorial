from odoo import models, fields, _

class MessageWizard(models.TransientModel):
    _name="message.wizard"

    message = fields.Text()

    def action_ok(self):
        return {'type': 'ir.actions.act_window_close'}



class ResendMailToParticipant(models.TransientModel):
    _name = 'tp.program.registration.searchview'

    first_name = fields.Char(string="First Name")
    last_name = fields.Char(string="Last Name")
    mobile = fields.Char(string="Mobile")
    email = fields.Char(string="Email")

    def search_record(self):
        """ Read records """
        domain = []
        if self.first_name:
            domain.append(("first_name","ilike",self.first_name))
        if self.last_name:
            domain.append(("last_name", "ilike", self.last_name))
        if self.mobile:
            domain.append(("phone", "ilike", self.mobile))
        if self.email:
           domain.append(("participant_email", "ilike", self.email))
        if len(domain)>0:
            records = self.env["tp.program.registration"].search(domain,limit=5)
            return {
                'name': _('Registration'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'tree,form',
                'res_model': 'tp.program.registration',
                'domain': [('id', 'in', records.ids)],
            }

