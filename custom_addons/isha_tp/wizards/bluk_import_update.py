import base64
import csv
import logging

from odoo import fields, models, _

_logger = logging.getLogger(__name__)

class BlukUpdateWizard(models.TransientModel):
    _name = 'bluk.update.wizard'

    file = fields.Binary(string="File")


    def upload_action(self):
        """ """
        sql = ''
        sso_dict = {}
        if self.file:
            ieo_reader = csv.DictReader(base64.b64decode(self.file).decode('utf-8').split('\n'))
            for row in list(ieo_reader):
                if row.get("External ID")!="":
                    id_rec = row.get("External ID").replace("__export__.tp_program_registration_","")
                    id_rec = id_rec.split("_")
                    sql += "update tp_program_registration set courier_tracking_no = \'%s\' where id = \'%s\';" % (
                        row.get("Courier Tracking No"), id_rec[0])
                    continue

            _logger.info('Update registration start')

            try:
                self.env.cr.execute(sql)
                self.env.cr.commit()
                message_id = self.env['message.wizard'].create(
                    {'message': _(
                        "Update Functionality is done.")})
                return {
                    'name': _('Warning'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    'res_id': message_id.id,
                    'target': 'new'
                }
                _logger.info('Update registration end')
            except e:
                message_id = self.env['message.wizard'].create(
                    {'message': _(
                        e)})
                return {
                    'name': _('Error'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }
                _logger.info(e)

    def update_check_in_action(self):
        sql = ''
        if self.file:
            try:
                goy_reader = csv.DictReader(base64.b64decode(self.file).decode('utf-8').split('\n'))
                for row in goy_reader:
                    sql += "update tp_program_registration set is_checkin_completed = true,time_zone = \'%s\',stream_lang = \'%s\' where sso_id = \'%s\';"  % (row['timeZone'],row['streamLanguage'],row['_id'])
                _logger.info('update the check-in start')
                self.env.cr.execute(sql)
                self.env.cr.commit()
                _logger.info('update the check-in end')

                message_id = self.env['message.wizard'].create(
                    {'message': _(
                        "Update Functionality is done.")})
                return {
                    'name': _('Warning'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    'res_id': message_id.id,
                    'target': 'new'
                }
            except Exception as e:
                message_id = self.env['message.wizard'].create(
                    {'message': _(
                        e)})
                return {
                    'name': _('Error'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }