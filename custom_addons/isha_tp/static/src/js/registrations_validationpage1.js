function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
var jspgremstatecoll=[];
var jspgremcountrycoll=[];
defer(document_ready);

function document_ready() {
    $(document).ready(function () {
        get_country_and_program();
        check_program_availability_and_fee();
        load_profile();
        set_country_pin_code_rule();
        set_country_rule();
        // set event functions
        $('#residence_country').change(function () {
        	set_country_pin_code_rule();
         	set_country_rule();
         });
        $('#countrycode').change(function () { set_country_phone_code_rule(); });
        $('#pincode').focusout(async () => {
            let selected_country = $('#residence_country option:selected').text();
            let selected_country_iso2_code = $('#residence_country option:selected').attr("ccode");
            let pincode = $('#pincode').val();
            pincode = pincode.trim();
            if( pincode !="")
            {
                let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
                let address_details_json = await address_details.json();

                if (! _.isEmpty(address_details_json)) {
                    let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0];
                    $('#residence_state').val(statevar["valueattr"]);
                    $('#city').val(address_details_json.defaultcity);
                    $('#district').val(address_details_json.defaultcity);
                }
            }
        });
    });
}

function get_country_and_program() {
    $('#program_country').val($("input[name='country_id']").val());
    //$('#program_country option:not(:selected)').remove();
    $('#program_list').val($("input[name='program']").val());
}

function check_program_availability_and_fee() {
    var selected_program_id = $("input[name='program']").val();
    var selected_country_id = $("input[name='country_id']").val();
    if(true) {
        //should be a separate function
        //$('.registration-form-title').html('Registration form  -  ' + $('#program_list option:selected').attr('program-type'));
        if(selected_country_id==104){
            $('#residence_country').empty().html('<option value="104" ccatr="IN" ccode="IN">India</option>');
            $('#pincode').attr('pattern', '[0-9]*');
            $('#pincode').attr('minlength', '6');
            $('#pincode').attr('maxlength', '6');
            $('#form_message2').removeClass('d-none');
        } else if(selected_country_id==233) {
            $('#form_message').html('<span>Ages 14+ are eligible to participate in this program.'
             + 'However, if you are under the age of 18, please have your parent/guardian contact '
             + '<a href="mailto:info@ishausa.org" target="_blank" style="color: #cf462c;">'
             + 'info@ishausa.org</a> to complete the registration process for you.</span>').removeClass('d-none');
            $('#dob').parent().addClass('d-none');
            $('#dob').attr('value', '1970-01-01');
            $('#dob').removeAttr('required');
            $('#age_checkbox').attr('required', 'required');
            $('#age_checkbox').parents('.form-row').removeClass('d-none');
            $('#pincode').attr('pattern', '[0-9]*');
            $('#pincode').attr('minlength', '5');
            $('#pincode').attr('maxlength', '5');
        }
        $('.program-and-category-type-wrapper, .basic-profile-wrapper, .residential-address-wrapper, .terms-wrapper, .button-wrapper').removeClass('d-none');
    }
    $('#content').height()+5 < screen.height ? $('.footer-wrapper').addClass('fixed-bottom')
                                                              : $('.footer-wrapper').removeClass('fixed-bottom');

}

function load_profile() {
    var initObjvar;
    var addobjvar;

    $('#residence_state option').each(function(ivar,jvar){
       var objvar ={
        "valueattr":$(jvar).attr("value"), // state id
        "statenameattr":$(jvar).attr("statename") // state name
        };

        jspgremstatecoll.push(objvar);

    });
    $('#residence_country option').each(function(ivar,jvar){
       var objvar ={
        "valueattr":$(jvar).attr("value"), // country id
        "ccattr":$(jvar).attr("ccatr") // country code
        };
        jspgremcountrycoll.push(objvar);
    });
    try {
        initObjvar = $.parseJSON($('#jspgreginputobjid').val());

        if ('firstName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['firstName'])
            && $('#first_name')) {
            $('#first_name').val(initObjvar['basicProfile']['firstName']);
            $('#first_name').prop("readonly",true);
        }
        if ('lastName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['lastName'])
            && $('#last_name')) {
            $('#last_name').val(initObjvar['basicProfile']['lastName']);
             $('#last_name').prop("readonly",true);
        }
        if ('email' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['email'])
            && $('#participant_email')) {
            $('#participant_email').val(initObjvar['basicProfile']['email']);
             $('#participant_email').prop("readonly",true);
        }
        if ('preferredName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['preferredName'])
            && $('#name_called')) {
            $('#name_called').val(initObjvar['basicProfile']['preferredName']);
            $('#name_called').prop("readonly",true);
        }
        if ('company' in initObjvar['extendedProfile'] && is_not_empty_or_null(initObjvar['extendedProfile']['company'])
            && $('#occupation')) {
            $('#occupation').val(initObjvar['extendedProfile']['company']);
        }
        if ('profession' in initObjvar['extendedProfile'] && is_not_empty_or_null(initObjvar['extendedProfile']['profession'])
            && $('#occupation')) {
            $('#occupation').val($('#occupation').val() + " " + initObjvar['extendedProfile']['profession']);
        }
        //debugger;
        if ('nationality' in initObjvar['extendedProfile'] && is_not_empty_or_null(initObjvar['extendedProfile']['nationality'])
            && $('select#nationality_id')) {
            var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == initObjvar['extendedProfile']['nationality']);
                if(countfiltvar.length > 0) {
                    $('select#nationality_id').val(countfiltvar[0]["valueattr"]);
                }
        }
        if ('phone' in initObjvar["basicProfile"]) {
            if ('countryCode' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['countryCode'])
                && $("select#countrycode") && $("select#countrycode option") && $("select#countrycode option:selected")) {
                $("select#countrycode").val(initObjvar['basicProfile']['phone']['countryCode']);
                $("select#countrycode").trigger('change');
//                 $("select#countrycode option").prop("disabled", true);
//                 $("select#countrycode option:selected").prop("disabled", false);
            }
            if ('number' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['number'])
                && $("input#phone")) {
                $("input#phone").val(initObjvar['basicProfile']['phone']['number']);
//                $("input#phone").prop("readonly",true);
            }
        }
        addobjvar = initObjvar['addresses'];
        if (addobjvar.length > 0) {
            addobjvar = addobjvar[0];
            //debugger;
            if ('country' in addobjvar && is_not_empty_or_null(addobjvar['country']) && $('#country')) {
                var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == addobjvar['country']);
                if(countfiltvar.length > 0)
                {
                $('#residence_country').val(countfiltvar[0]["valueattr"]);
                load_states();
                }
            }
            else if($("input[name='country_id']").val()) {
                $('#residence_country').val($("input[name='country_id']").val());
                load_states();
            }
            if ('addressLine1' in addobjvar && is_not_empty_or_null(addobjvar['addressLine1']) && $('#address_line')) {
                $('#address_line').val(addobjvar['addressLine1']);
            }
            //debugger
            if ('state' in addobjvar && is_not_empty_or_null(addobjvar['state']) && $('#residence_state')) {
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == addobjvar['state'])
                if(statevar.length > 0){
                    $('#residence_state').val(statevar[0]["valueattr"]);
                }
            }


            if ('addressLine2' in addobjvar && is_not_empty_or_null(addobjvar['addressLine2'])) {
                $('#address_line').val($('#address_line').val() + "  " + addobjvar['addressLine2']);
            }
            //debugger;
            if ('townVillageDistrict' in addobjvar && is_not_empty_or_null(addobjvar['townVillageDistrict'])) {
                $('#city').val(addobjvar['townVillageDistrict']);
            }
            if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
                $('#pincode').val(addobjvar['pincode']);
            }

        }
    } catch (exvar) {
        console.log(exvar);
    } finally {
        load_states();
    }

}

function load_states() {
    var state_dropdown = document.getElementById("residence_state");
    var country_dropdown = document.getElementById("residence_country");
    var country_id = country_dropdown.value;

    var idvar = 0;
    Array.from(state_dropdown.options).forEach(item => {
        var id = item.getAttribute("countryid");
        if (id == country_id) {
            item.style.display = "block";
            item.disabled = false;
            if (idvar == 0)
                idvar = item.index;
        } else {
            item.style.display = "none";
            item.disabled = true;
        }
    });
//    if (idvar != 0) {
//        state_dropdown.options.selectedIndex = idvar;
//        state_dropdown.disabled = false;
//        state_dropdown.height = 35;
//    }
//     else if (idvar == 0) {
//       state_dropdown.options.selectedIndex = 0;
//        state_dropdown.disabled = true;
//    }
}

function validate_form() {
    //set_updated_form_values();
//    if(validateemptystring()){
//         alert("Please enter valid characters for first name(without numbers or special characters)")
//         return false;
//    }
    retval = validate_gender();
    retval = retval && validate_age();
    retval = retval && validate_address_length();
    return retval;
}

function set_updated_form_values(){
    var initObjvar;
    var addobjvar;
    try {
        initObjvar = $.parseJSON($('#jspgreginputobjid').val());
        //debugger;
        //if ('preferredName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['preferredName'])) {
            initObjvar['basicProfile']['preferredName'] = $('#name_called').val();
        //}
         initObjvar['basicProfile']['gender'] = $('#gender').val().toUpperCase();
         initObjvar['basicProfile']['firstName'] = $('#first_name').val();
         initObjvar['basicProfile']['lastName'] = $('#last_name').val();
        if ('company' in initObjvar['extendedProfile']) {
            initObjvar['extendedProfile']['company'] = $('#occupation').val();
        }
        if ('profession' in initObjvar['extendedProfile']) {
            //$('#occupation').val($('#occupation').val() + " " + initObjvar['extendedProfile']['profession']);
        }
        //debugger;
        if ('nationality' in initObjvar['extendedProfile']) {
            initObjvar['extendedProfile']['nationality'] = $('select#nationality_id option:selected').attr('ccatr');
        }
        if ('phone' in initObjvar["basicProfile"]) {
            if ('countryCode' in initObjvar["basicProfile"]["phone"]) {
                initObjvar['basicProfile']['phone']['countryCode'] = $("select#countrycode").val();
            }
            if ('number' in initObjvar["basicProfile"]["phone"]) {
                initObjvar['basicProfile']['phone']['number'] = $("input#phone").val();
            }
        }
        addobjvar = initObjvar['addresses'];
        if (addobjvar.length > 0) {
            addobjvar = addobjvar[0];
            //debugger;
            if ('country' in addobjvar && is_not_empty_or_null(addobjvar['country'])) {
                 addobjvar['country'] = $('#country option:selected').attr('ccatr')
            }
            if ('addressLine1' in addobjvar && is_not_empty_or_null(addobjvar['addressLine1'])) {
                addobjvar['addressLine1'] = $('#address_line').val();
            }
            if ('state' in addobjvar && is_not_empty_or_null(addobjvar['state'])) {
                    addobjvar['state'] = $('#stateid option:selected').text();
            }


            if ('addressLine2' in addobjvar && is_not_empty_or_null(addobjvar['addressLine2'])) {
                //$('#address_line').val($('#address_line').val() + "  " + addobjvar['addressLine2']);
            }
            //debugger;
            if ('townVillageDistrict' in addobjvar && is_not_empty_or_null(addobjvar['townVillageDistrict'])) {
                addobjvar['townVillageDistrict'] = $('#city').val();
            }
            if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
                addobjvar['pincode'] = $('#pincode').val();
            }

        }
        $('#jspgregoutputobjid').val(JSON.stringify(initObjvar));
    } catch (exvar) {
    //console.log(exvar);
    }
}

function validate_empty_string() { return ($.trim($("#first_name").val()) == ""); }

function validate_address_length(){
    if($('#address_line').val().length < 10){
		event.preventDefault();
		return  swal({
		   backdrop:false,
		   title: 'Address Line is Invalid',
		   text: 'Please enter minimum 10 characters for address',
		   type: 'error'
		});
    }

    if($('#city').val().length < 2){
		event.preventDefault();
		return  swal({
		   backdrop:false,
		   title: 'City is Invalid',
		   text: 'Please enter minimum characters for city',
		   type: 'error'
		});
    }
    return true;
}

function validate_age() {
    var dob = document.getElementById("dob");
    if (dob.value == "") {
		event.preventDefault();
		return swal({
		   backdrop:false,
		   title: 'DOB is Invalid',
		   text: "Please enter a valid date of birth",
		   type: 'error'
		});
    }
    var today = new Date();
    var birthDate = new Date(dob.value);
    var age = ((Date.now() - birthDate) / (31557600000));
    var program_list = document.getElementById("program_list");
    var minage = program_list.options[program_list.selectedIndex].getAttribute("minage");
    if (age < minage) {
		event.preventDefault();
		return  swal({
		   backdrop:false,
		   title: 'Age is Invalid',
		   text: "Minimum age to undergo this program is "+minage+". Sorry, you are not allowed to undergo this program",
		   type: 'error'
		});
    } else if(age > 100)
    {
		event.preventDefault();
	    return swal({
		   backdrop:false,
		   title: 'Age is Invalid',
		   text: "Please enter a valid date of birth",
		   type: 'error'
		});
    }
    else
        return true;
}

function validate_gender() {
    var gender = document.getElementById("gender");
    var gendervalue = gender.options[gender.selectedIndex].text;
    var program_list = document.getElementById("program_list");
    var gender = program_list.options[program_list.selectedIndex].getAttribute("gender");
    if (gender != gendervalue) {
        if (gender != "All"){
			event.preventDefault();
			return  swal({
			   backdrop:false,
			   title: 'Gender is Invalid',
			   text: "Selected gender is not allowed",
			   type: 'error'
			});
        } else
            return true;
    }
    else
        return true;
}

function validate_terms() {}

function is_not_empty_or_null(objtotestvar){
var flg = false;
 try{
  if(objtotestvar && objtotestvar !=null && objtotestvar !="") {
        flg = true;
  }

 }
catch(exvar){
}
finally{
return flg;
}
}

function set_country_pin_code_rule(){

  if(  $('#residence_country').val() == '104' ){
        $('#pincode').attr('maxlength', '6');
        $('#pincode').attr('minlength', '6');
        $('#pincode').attr('pattern', '[1-9]{1}[0-9]+');
    }
    else if(  $('#residence_country').val() == '233' ){
        $('#pincode').attr('maxlength', '5');
        $('#pincode').attr('minlength', '5');
        $('#pincode').attr('pattern', '[1-9]{1}[0-9]+');
    }
    else{
    $('#pincode').attr('maxlength', '10');
    $('#pincode').removeAttr('pattern');
    $('#pincode').removeAttr('minlength');
    }
}

function set_country_rule(){
$('#address_line').attr('minlength', '10');
  if(  $('#residence_country').val() == '104' || $('#residence_country').val() == '233' ){
        $('#residence_state').attr('required', true);
        $('#state_label').attr('class', 'required');
        $('#city').attr('pattern', '[A-Za-z\\s]*');
        if($('#residence_country').val() == '104')
        { $('#address_line').attr('minlength', '15'); }
    }
    else{
    $('#residence_state').attr('required', false);
    $('#state_label').attr('class', '');
	$('#city').attr('pattern', '*');

    }
}

function set_country_phone_code_rule(){

     if( $('#countrycode').val() == '91'){
        $('#phone').attr('maxlength', '10')
        $('#phone').attr('minlength', '10')
        $('#phone').attr('pattern', '[0-9]+')
    }
    else{
        $('#phone').attr('maxlength', '15')
        $('#phone').attr('pattern', '[1-9]{1}[0-9]+')
    }
}
