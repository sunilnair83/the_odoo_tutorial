{
    'name': 'Isha Celebrations',
    'summary': "Join us for powerful meditations, musical performances and Satsangs with Sadhguru.",
    'description': "Join us for powerful meditations, musical performances and Satsangs with Sadhguru.",
    'author': "Isha Foundation",
    'license': "AGPL-3",
    'website': "https://isha.sadhguru.org/",
    'category': 'Isha Celebrations',
    'version': '20.0.0.1',
    'external_dependencies': {
        'python': [
            'html2text',
        ],
    },
    'depends': ['mail', 'sms', 'auth_signup', 'http_routing', 'base', 'website', 'isha_crm'],
    'data':
        [

            'security/security.xml',
            'views/tp_groups.xml',
            'data/mail_template_data.xml',
            # 'data/documenttype_data.xml',
            #'data/sms_data.xml',
            # not required any more 'data/tp_globalnotificationevent_data.xml',
            # 'data/terms_and_conditions_data.xml',
            #'data/tp_paymentgateway_data.xml',
            #'data/tp_notificationevent_data.xml',
            #'data/master_tp_data.xml',
            # 'data/program_type_data.xml',
            # 'data/cron.xml',
            #'data/sequence_data.xml',
	        'data/url_details.xml',
	        'data/registration_template.xml',
	        'views/goy_portal_template.xml',
            'views/views.xml',
            'views/assets.xml',
            'views/programtype_view.xml',
            'wizards/prgattendance.xml',
            'wizards/blmatches.xml',
            'wizards/prgmatches.xml',
            'views/programschedule_view.xml',
            'views/webregistrations.xml',
            'views/program_zip_codes_view.xml',
            'views/program_region_contacts_view.xml',
            # not required any more 'views/web/changeofparticipant.xml',
            # not required any more 'views/web/packageupgrade.xml',
            'views/inquiry.xml',
            'views/access_rights_tp.xml',
            'wizards/changeprogramdate.xml',
            'wizards/resendmail.xml',
            'wizards/registration_search_view.xml',
            'wizards/confirmation_wizard.xml',
            'wizards/bluk_import_update.xml',
            # not required any more 'wizards/packageupgrade.xml',
            'views/programregistration_view.xml',
            'views/payment_transaction_view.xml',
            'views/payment_transaction_finance_view.xml',
            'security/ir.model.access.csv',
            'views/template_common_layout.xml',
            'views/template.xml',
            'views/offline_registration_view.xml',
            'menus/menus.xml',
            'views/settings.xml',
            'views/payment_view.xml',
            'views/auto_redirect_sso.xml',
        ],
    'demo': ['demo/demo.xml'],
    'application': True,
    'auto_install': False,
    'installable': True,
}
