# from odoo import models, fields
# from odoo import api
#
# class IshaPersonType(models.Model):
#     _name = 'isha.person.type'
#     _description = 'Isha Person Type'
#     _rec_name = 'person_type'
#     _sql_constraints = [('name_uniq', 'UNIQUE (person_type)', 'You can not have two person types with the same name !')]
#
#     person_type = fields.Char(string='Person Type')
#     active = fields.Boolean(default=True,
#                             help="The active field allows you to hide the Person Type without removing it.")
#
#     # person_type_department = fields.Many2many(comodel_name="hr.department",
#     #                                  relation="m2m_isha_person_type_hr_dept",
#     #                                  column1="person_id", column2="hr_department",
#     #                                  string="Person_department", required=True)