odoo.define('isha_crm.right_click_copy_blocked', function(require) {
    'use strict';
    var ListView = require('web.ListRenderer');

    ListView.include({
        on_attach_callback: function() {
            this._super();
            var model = this.state.model;
            console.log(model);
            if (model == 'res.partner') {
                console.log("Right click and Copy disabled");
                $(document).ready(function() {
                    //Disable cut copy paste
                    $('body').bind('cut copy paste', function(e) {
                        e.preventDefault();
                    });
                    //Disable mouse right click
//                        $("body").on("contextmenu mousedown onselectstart",function(e){
                    $("body").on("contextmenu", function(e) {
                        return false;
                    });
                });
                $(document).mousedown(function(e) {
                    if (e.which === 2 || e.which === 3) {
                        return false; // Or e.preventDefault()
                    }
                });
            }
        },

    });
});