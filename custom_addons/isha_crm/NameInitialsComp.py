
NO_MATCH = 0
LOW_MATCH = 1
HIGH_MATCH = 2
EXACT_MATCH = 4
NA = -1

class Name:
	name = ''
	tokens = []
	initials = None
	firstCharsOfWords = None

	def __init__(self, name):
		self.name = name
		self.initials= {}
		self.firstCharsOfWords = {}
		self.initialize()

	def initialize(self):
		self.tokens = self.name.split()
		for x in self.tokens:
			if len(x) == 1:
				count = self.initials[x] if x in self.initials.keys() else 0
				self.initials[x] = count + 1
			else:
				count = self.firstCharsOfWords[x[0]] if x[0] in self.firstCharsOfWords.keys() else 0
				self.firstCharsOfWords[x[0]] = count + 1
	


class NameInitialsComp:
	name1 = ''
	name2 = ''
	n1 = None
	n2 = None

	initialMatchStatus = NO_MATCH
	initialsExpanded = False
	allWordsExhausted = False
	containsInitials = False
	hasCommonWords = True

	def __init__(self, name1, name2):
		self.name1 = name1
		self.name2 = name2

		self.n1 = Name(name1)
		self.n2 = Name(name2)

		n1WordCount = len(self.n1.firstCharsOfWords)
		n2WordCount = len(self.n2.firstCharsOfWords)

		n1StartLetters = self.getStartingLettersIncludingInitials(name1)
		n2StartLetters = self.getStartingLettersIncludingInitials(name2)
		self.allWordsExhausted = (n1StartLetters == n2StartLetters)
		self.removeCommon(self.n1.firstCharsOfWords , self.n2.firstCharsOfWords)

		self.hasCommonWords = not (n1WordCount == len(self.n1.firstCharsOfWords)) or not (n2WordCount == len(self.n2.firstCharsOfWords))

		n1InitialCount = len(self.n1.initials)
		n2InitialCount = len(self.n2.initials)

		self.containsInitials = n1InitialCount > 0 or n2InitialCount > 0
		self.removeCommon(self.n1.initials,self.n2.initials)
		intersectionSize = n1InitialCount - len(self.n1.initials)

		self.removeCommon(self.n1.initials, self.n2.firstCharsOfWords)
		self.removeCommon(self.n2.initials, self.n1.firstCharsOfWords)

		n1InitialCountAfterExpansion = len(self.n1.initials) + intersectionSize
		n2InitialCountAfterExpansion = len(self.n2.initials) + intersectionSize

		if (n1InitialCount > n1InitialCountAfterExpansion) or (n2InitialCount > n2InitialCountAfterExpansion):
			self.initialsExpanded = True

		if n1InitialCountAfterExpansion == intersectionSize and n1InitialCountAfterExpansion == n2InitialCountAfterExpansion:
			self.initialMatchStatus = HIGH_MATCH
		elif intersectionSize == 0 and (n1InitialCountAfterExpansion ==0 or n2InitialCountAfterExpansion == 0):
			self.initialMatchStatus = HIGH_MATCH
		elif intersectionSize > 0:
			self.initialMatchStatus = LOW_MATCH
		elif intersectionSize == 0:
			self.initialMatchStatus = NO_MATCH

	def removeCommon(self,i1,i2):
		ks = set(i1.keys())
		for s in ks:
			if s in i2.keys():
				self.removeCommonKey(s,i1,i2)

	def removeCommonKey(self, s, i1, i2):
		ci1 = i1[s] if s in i1 else 0
		ci2 = i2[s] if s in i2 else 0

		c = min(ci1,ci2)
		self.updateValue(s, i1,ci1,c)
		self.updateValue(s, i2, ci2, c)

	def updateValue(self,s, i1,ci1,c):
		if c >= ci1:
			i1.pop(s,None)
		else:
			i1[s] = ci1 - c


	def getStartingLettersIncludingInitials(self, str):
		return set(x[0] for x in str.split())



