import datetime
import json
import logging
import traceback

import requests
import werkzeug
import werkzeug.wrappers
from odoo.addons.muk_rest import tools

import odoo
from odoo.http import request, Response

_logger = logging.getLogger(__name__)
null = None


def converter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


class MsrOfferingsApi(odoo.http.Controller):

    def construct_api_response(self, params):
        _logger.info(str(params))
        return Response(json.dumps(params, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    @odoo.http.route([
        '/api/rudraksha_deeksha'
    ], auth="none", type='json', methods=['POST'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def search_registrations(self, **kw):
        _logger.info("Rudraksha Deeksha Search api [POST]"+json.dumps(request.params))

        phone = request.params.get('phone', None)
        wa = request.params.get('whatsapp_number', None)
        delivery_status_check = request.params.get('delivery_status_check', None)

        existing_phone_rec = request.env['rudraksha.deeksha']
        existing_wa_rec = request.env['rudraksha.deeksha']

        if phone:
            existing_phone_rec = request.env['rudraksha.deeksha'].sudo().search(['|', ('phone', '=', phone), ('whatsapp_number', '=', phone)])
        if len(existing_phone_rec) == 0 and wa:
            existing_wa_rec = request.env['rudraksha.deeksha'].sudo().search(['|', ('phone', '=', wa), ('whatsapp_number', '=', wa)])

        existing_rec = existing_phone_rec | existing_wa_rec
        delivery_status_arr = []

        for rec in existing_rec:
            if rec.delivery_status:
                delivery_status_arr.append((rec.id, rec.delivery_status))

        if len(existing_rec) > 0:
            if delivery_status_check == 1:
                response_dict = {'delivery_status': delivery_status_arr, 'already_registered': True, "ishangam_ids": existing_rec.ids}
                return response_dict
            else:
                response_dict = {'already_registered': True, "ishangam_ids": existing_rec.ids}
                return response_dict

        if delivery_status_check == 1:
            response_dict = {'delivery_status': [], 'already_registered': False, "ishangam_ids": []}
            return response_dict
        else:
            response_dict = {'already_registered':False,"ishangam_ids":[]}
            return response_dict
