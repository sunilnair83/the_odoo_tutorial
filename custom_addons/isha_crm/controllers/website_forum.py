# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
import werkzeug.exceptions
import werkzeug.urls
import werkzeug.wrappers
from odoo.addons.website_forum.controllers.main import WebsiteForum
from odoo import http, tools, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.http import request
from . import website_forum_tasks

_logger = logging.getLogger(__name__)

class IshaWebsiteForum(WebsiteForum):

    def is_access_authorized(self, partner):
        if len(partner.starmark_category_id) == 0 and \
                partner.is_meditator is True and (not partner.country or partner.country == 'IN'):
            return True
        else:
            return False

    def validate_forum_group_access(self, user_groups, forum_groups):
        return any(group in user_groups for group in forum_groups)

    def check_forum_restrain(self, forum):
        # Allow internal users to all forums
        if request.env.user.has_group('base.group_user'):
            return False

        # Auto redirect Public users to SSO login page
        if request.env.user.has_group('base.group_public'):
            return request.render('isha_crm.sso_auto_redirect', {})

        # Portal unverified user
        if not request.env.user.partner_id.portal_verified:
            url = '/my/account?redirect=' + request.httprequest.url
            return request.redirect(url, code=302)

        # Check if restrain is applied for this forum
        if not forum.check_forum_restrain:
            return False

        # Forum access check
        if not self.is_access_authorized(request.env.user.partner_id):
            return werkzeug.utils.redirect('/my/verify?login='+request.env.user.login, code=302)

        # Forum Group Check
        if not self.validate_forum_group_access(request.env.user.groups_id.ids,forum.groups_id.ids):
            return request.render("isha_crm.mv_unauthozized_user",{'user':request.env.user})

        # Forum eligible user
        return False

    @http.route(['/forum'], type='http', auth="public", website=True)
    def forum(self, **kwargs):
        # Auto redirect Public users to SSO Login page
        if request.env.user.has_group('base.group_public'):
            return request.render('isha_crm.sso_auto_redirect', {})

        # Portal Unverified user
        if not request.env.user.partner_id.portal_verified:
            url = '/my/account?redirect=' + request.httprequest.url
            return werkzeug.utils.redirect(url, code=302)

        domain = request.website.website_domain()

        # Forum access check
        if not request.env.user.has_group('base.group_user') and \
                not self.is_access_authorized(request.env.user.partner_id):
            domain.append(('check_forum_restrain','=',False))

        domain.append(('groups_id','in',request.env.user.groups_id.ids))
        forums = request.env['forum.forum'].search(domain)
        if len(forums) == 1:
            return werkzeug.utils.redirect('/forum/%s' % slug(forums[0]), code=302)
        return request.render("website_forum.forum_all", {'forums': forums})

    @http.route(['/forum/<model("forum.forum"):forum>',
                 '/forum/<model("forum.forum"):forum>/page/<int:page>',
                 '''/forum/<model("forum.forum"):forum>/tag/<model("forum.tag"):tag>/questions''',
                 '''/forum/<model("forum.forum"):forum>/tag/<model("forum.tag"):tag>/questions/page/<int:page>''',
                 ], type='http', auth="public", website=True, sitemap=WebsiteForum.sitemap_forum)
    def questions(self, forum, tag=None, page=1, filters='all', my=None, sorting=None, search='', **post):
        response = self.check_forum_restrain(forum)
        if response:
            return response
        if forum.mode == 'vtasks':
            return website_forum_tasks.questions(self, forum, tag, page, filters, my, sorting, search, **post)

        return super(IshaWebsiteForum, self).questions(forum, tag, page, filters, my, sorting, search, **post)

    @http.route(['''/forum/<model("forum.forum", "[('website_id', 'in', (False, current_website_id))]"):forum>/faq'''], type='http',
                auth="public", website=True)
    def forum_faq(self, forum, **post):
        response = self.check_forum_restrain(forum)
        if response:
            return response

        return super(IshaWebsiteForum, self).forum_faq(forum, **post)

    @http.route('/forum/get_tags', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
    def tag_read(self, query='', limit=25, **post):
        return super(IshaWebsiteForum, self).tag_read(query, limit, **post)

    @http.route(['/forum/<model("forum.forum"):forum>/tag', '/forum/<model("forum.forum"):forum>/tag/<string:tag_char>'],
                type='http', auth="public", website=True, sitemap=False)
    def tags(self, forum, tag_char=None, **post):
        response = self.check_forum_restrain(forum)
        if response:
            return response
        if forum.mode == 'vtasks':
            return website_forum_tasks.tags(self, forum, tag_char=None, **post)

        return super(IshaWebsiteForum, self).tags(forum, tag_char, **post)

    @http.route(['''/forum/<model("forum.forum", "[('website_id', 'in', (False, current_website_id))]"):forum>/question/<model("forum.post", "[('forum_id','=',forum[0]),('parent_id','=',False),('can_view', '=', True)]"):question>'''],
                type='http', auth="public", website=True)
    def question(self, forum, question, **post):
        response = self.check_forum_restrain(forum)
        if response:
            return response
        if forum.mode == 'vtasks':
            return website_forum_tasks.question(self, forum, question, **post)

        return super(IshaWebsiteForum, self).question(forum, question, **post)

    @http.route(['/forum/<model("forum.forum"):forum>/partner/<int:partner_id>'], type='http', auth="public", website=True)
    def open_partner(self, forum, partner_id=0, **post):
        response = self.check_forum_restrain(forum)
        if response:
            return response

        return super(IshaWebsiteForum, self).open_partner(forum, partner_id, **post)

    @http.route(['/forum/<model("forum.forum"):forum>/user/<int:user_id>'], type='http', auth="public", website=True)
    def view_user_forum_profile(self, forum, user_id, forum_origin, **post):
        response = self.check_forum_restrain(forum)
        if response:
            return response

        return super(IshaWebsiteForum, self).view_user_forum_profile(forum, user_id, forum_origin, **post)

    @http.route('/forum/<model("forum.forum"):forum>/post/<model("forum.post"):post>/comment/<model("mail.message"):comment>/flag_comment', type='http', auth="user", methods=['POST'], website=True)
    def flag_comment(self, forum, post, comment, **kwarg):
        response = self.check_forum_restrain(forum)
        if response:
            return response

        post = request.env['forum.post'].convert_comment_to_answer_and_flag(comment['id'])
        if not post:
            return werkzeug.utils.redirect("/forum/%s" % slug(forum))
        question = post.parent_id if post.parent_id else post
        return werkzeug.utils.redirect("/forum/%s/question/%s" % (slug(forum), slug(question)))

    @http.route('/forum/<model("forum.forum"):forum>/post/<model("forum.post"):post>/comment/<model("mail.message"):comment>/delete', type='json', auth="user", website=True)
    def delete_comment(self, forum, post, comment, **kwarg):
        response = self.check_forum_restrain(forum)
        if response:
            return response

        if not request.session.uid:
            return {'error': True,'msg': 'Please Login to Continue!!'}
        try:
            ret_rec =  post.unlink_comment(comment.id)[0]
            return {'error': False}
        except Exception as ex:
            print('exception'+str(ex))
            return {'error': True,'msg':ex.args[0]}


    # Plz check ISH-447 there is a auth issue in redirect so calling the direct render functions instead of redirect
    @http.route('/forum/<model("forum.forum"):forum>/post/<model("forum.post"):post>/validate', type='http', auth="user", website=True)
    def post_accept(self, forum, post, **kwargs):
        response = self.check_forum_restrain(forum)
        if response:
            return response

        url = 'validation_queue'
        if post.state == 'flagged':
            url = 'flagged_queue'
        elif post.state == 'offensive':
            url = 'offensive_posts'
        post.validate()
        if url == 'validation_queue':
            return self.validation_queue(forum,**kwargs)
        elif url == 'flagged_queue':
            return self.flagged_queue(forum, **kwargs)
        elif url == 'offensive_posts':
            return self.offensive_posts(forum, **kwargs)

        return werkzeug.utils.redirect(url)

    @http.route(['/forum/<model("forum.forum"):forum>/new',
                 '/forum/<model("forum.forum"):forum>/<model("forum.post"):post_parent>/reply'],
                type='http', auth="user", methods=['POST'], website=True)
    def post_create(self, forum, post_parent=None, **post):
        if forum.mode == 'vtasks':
            return website_forum_tasks.post_create(self, forum, post_parent, **post)
        return super(IshaWebsiteForum,self).post_create(forum, post_parent, **post)


    @http.route(['/forum/<model("forum.forum"):forum>/<model("forum.post"):post_parent>/vtasksapply'],
                type='http', auth="user", methods=['POST'], website=True)
    def vtasks_apply(self, forum, post_parent=None, **post):

        vtask_rec = request.env['forum.task.applications'].create({
            'forum_id': forum.id,
            'forum_post_id': post_parent and post_parent.id or False,
            'task_id': post_parent.task_id and post_parent.task_id.id or False,
            'applied_user':request.env.uid,
            'availability':post.get('availability'),
            'task_eta':post.get('task_eta')
        })
        return super(IshaWebsiteForum,self).post_create(forum,post_parent,**post)

    @http.route('/forum/<model("forum.forum"):forum>/question/<model("forum.post"):question>/ask_for_close', type='http', auth="user", methods=['POST'], website=True)
    def question_ask_for_close(self, forum, question, **post):
        if forum.mode == 'vtasks':
            return website_forum_tasks.question_ask_for_close(self, forum, question, **post)
        return super(IshaWebsiteForum,self).question_ask_for_close(forum, question, **post)

    @http.route(['/forum/<model("forum.forum"):forum>/ask'], type='http', auth="user", website=True)
    def forum_post(self, forum, **post):
        if forum.mode == 'vtasks':
            return website_forum_tasks.forum_post(self, forum, **post)
        return super(IshaWebsiteForum,self).forum_post(forum, **post)

    @http.route('/forum/<model("forum.forum"):forum>/post/<model("forum.post"):post>/edit', type='http', auth="user", website=True)
    def post_edit(self, forum, post, **kwargs):
        if forum.mode == 'vtasks':
            return website_forum_tasks.post_edit(self, forum, post, **kwargs)
        return super(IshaWebsiteForum,self).post_edit(forum, post, **kwargs)

    @http.route('/forum/<model("forum.forum"):forum>/flagged_queue', type='http', auth="user", website=True)
    def flagged_queue(self, forum, **kwargs):
        if forum.mode == 'vtasks':
            return website_forum_tasks.flagged_queue(self, forum, **kwargs)
        return super(IshaWebsiteForum,self).flagged_queue( forum, **kwargs)

    @http.route('/forum/<model("forum.forum"):forum>/offensive_posts', type='http', auth="user", website=True)
    def offensive_posts(self, forum, **kwargs):
        if forum.mode == 'vtasks':
            return website_forum_tasks.offensive_posts(self, forum, **kwargs)
        return super(IshaWebsiteForum,self).offensive_posts(forum, **kwargs)

    @http.route('/forum/<model("forum.forum"):forum>/post/<model("forum.post"):post>/ask_for_mark_as_offensive', type='http', auth="user", methods=['GET'], website=True)
    def post_ask_for_mark_as_offensive(self, forum, post, **kwargs):
        if forum.mode == 'vtasks':
            return website_forum_tasks.post_ask_for_mark_as_offensive(self, forum, post, **kwargs)
        return super(IshaWebsiteForum,self).post_ask_for_mark_as_offensive(forum, post, **kwargs)


    def _prepare_open_forum_user(self, user, forums, **kwargs):
        Post = request.env['forum.post']
        Vote = request.env['forum.post.vote']
        Activity = request.env['mail.message']
        Followers = request.env['mail.followers']
        Data = request.env["ir.model.data"]
        forum_vtasks = forums.filtered(lambda x: x.mode == 'vtasks')
        forum_forum = forums.filtered(lambda x: x.mode != 'vtasks')

        # questions and answers by user
        user_question_ids = Post.search([
            ('parent_id', '=', False),
            ('forum_id', 'in', forum_forum.ids), ('create_uid', '=', user.id)],
            order='create_date desc')
        count_user_questions = len(user_question_ids)
        min_karma_unlink = min(forums.mapped('karma_unlink_all'))

        # limit length of visible posts by default for performance reasons, except for the high
        # karma users (not many of them, and they need it to properly moderate the forum)
        post_display_limit = None
        if request.env.user.karma < min_karma_unlink:
            post_display_limit = 20

        user_questions = user_question_ids[:post_display_limit]
        user_answer_ids = Post.search([
            ('parent_id', '!=', False),
            ('forum_id', 'in', forum_forum.ids), ('create_uid', '=', user.id)],
            order='create_date desc')
        count_user_answers = len(user_answer_ids)
        user_answers = user_answer_ids[:post_display_limit]

        user_application_ids = request.env['forum.task.applications'].sudo().search([('create_uid', '=', user.id),
                                                                                ('forum_id', 'in', forum_vtasks.ids)],
                                                                               order='create_date desc')
        count_user_applications = len(user_application_ids)
        user_applications = user_application_ids[:post_display_limit]

        # showing questions which user following
        post_ids = [follower.res_id for follower in Followers.sudo().search(
            [('res_model', '=', 'forum.post'), ('partner_id', '=', user.partner_id.id)])]
        followed = Post.search([('id', 'in', post_ids), ('forum_id', 'in', forums.ids), ('parent_id', '=', False)])

        # showing Favourite questions of user.
        favourite = Post.search(
            [('favourite_ids', '=', user.id), ('forum_id', 'in', forums.ids), ('parent_id', '=', False)])

        # votes which given on users questions and answers.
        data = Vote.read_group([('forum_id', 'in', forums.ids), ('recipient_id', '=', user.id)], ["vote"],
                               groupby=["vote"])
        up_votes, down_votes = 0, 0
        for rec in data:
            if rec['vote'] == '1':
                up_votes = rec['vote_count']
            elif rec['vote'] == '-1':
                down_votes = rec['vote_count']

        # Votes which given by users on others questions and answers.
        vote_ids = Vote.search([('user_id', '=', user.id)])

        # activity by user.
        model, comment = Data.get_object_reference('mail', 'mt_comment')
        activities = Activity.search(
            [('res_id', 'in', (user_question_ids + user_answer_ids).ids), ('model', '=', 'forum.post'),
             ('subtype_id', '!=', comment)],
            order='date DESC', limit=100)

        posts = {}
        for act in activities:
            posts[act.res_id] = True
        posts_ids = Post.search([('id', 'in', list(posts))])
        posts = {x.id: (x.parent_id or x, x.parent_id and x or False) for x in posts_ids}

        # TDE CLEANME MASTER: couldn't it be rewritten using a 'menu' key instead of one key for each menu ?
        if user == request.env.user:
            kwargs['my_profile'] = True
        else:
            kwargs['users'] = True

        values = {
            'uid': request.env.user.id,
            'user': user,
            'main_object': user,
            'searches': kwargs,
            'questions': user_questions,
            'count_questions': count_user_questions,
            'answers': user_answers,
            'count_answers': count_user_answers,
            'applications': user_applications,
            'count_applications': count_user_applications,
            'followed': followed,
            'favourite': favourite,
            'up_votes': up_votes,
            'down_votes': down_votes,
            'activities': activities,
            'posts': posts,
            'vote_post': vote_ids,
            'is_profile_page': True,
            'badge_category': 'forum',
        }

        return values