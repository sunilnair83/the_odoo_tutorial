import traceback

import odoo
import uuid
import Levenshtein as lev
from fuzzywuzzy import fuzz
from .. import NameMatchEngine
from datetime import datetime, tzinfo

null = None
NO_MATCH = 0
LOW_MATCH = 1
HIGH_MATCH = 2
EXACT_MATCH = 4
NA = -1


class ContactService:

    mapModel = None
    contactModel = None
    countryModel = None
    stateModel = None
    lowMatchModel = None
    pincodeModel = None
    ishaCenterModel = None
    low_match = []
    CR = None
    env = None

    def setEnvValues(self, ENV):
        self.contactModel = ENV['res.partner']
        self.mapModel = ENV['contact.map']
        self.lowMatchModel = ENV['low.match.pairs']
        self.pincodeModel = ENV['isha.pincode.center']
        self.ishaCenterModel = ENV['isha.center']
        self.countryModel = ENV['res.country']
        self.stateModel = ENV['res.country.state']
        self.low_match=[]
        self.CR = ENV.cr
        self.env = ENV

    def getApiSearchList(self, env, name, phone, email):
        self.setEnvValues(env)
        domain_list = []
        if phone:
            for x in ['|','|','|',('phone','=',phone),('phone2','=',phone),('phone3','=',phone),('phone4','=',phone)]:
                domain_list.append(x)
        if email:
            for x in ['|','|',('email','=ilike',email),('email2','=ilike',email),('email3','=ilike',email)]:
                domain_list.append(x)
        if (phone or email):
            rec_list = self.contactModel.search(domain_list)
            if name:
                high_match = []
                low_match = []
                self.nameMatchList(rec_list,name,high_match,low_match)
                return high_match
            elif not name:
                return rec_list
        if name:
            rec_list = self.getNameMatch(name)
            return rec_list
        else:
            return []

    def nameMatchList(self, rec_list, name, high_match, low_match):
        if len(rec_list) > 0:
            matchEngine = NameMatchEngine.NameMatchEngine()
            for rec in rec_list:
                match_status = matchEngine.compareNamesWithVariations(name, rec['name'])
                if match_status == EXACT_MATCH or match_status == HIGH_MATCH:
                    high_match.append(rec)
                elif match_status == LOW_MATCH:
                    low_match.append(rec)
        return

    def getNameMatch(self, name):
        rec = self.getMatchRecordList('name', '=ilike', name)
        return rec

    def getMatchRecordList(self, field, condition, value):
        match = self.contactModel.sudo().search([(field, condition, value)])
        return match
