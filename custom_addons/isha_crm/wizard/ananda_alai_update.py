import base64
import csv
import json
import logging

from odoo import models, fields
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class AnandaAlaiUpdate(models.TransientModel):
	_name = 'ananda.alai.update'
	_description = 'Ananda Alai Update'

	upd_file = fields.Binary(string='Update File')

	def update_ananda_alai_data(self):
		self.update_data('Online Ananda Alai', 'sso_id')

	def update_uyir_nokkam_data(self):
		self.update_data('Uyir Nokkam Online', 'local_trans_id')

	def update_data(self, program_name, key_field):
		file_reader = csv.DictReader(base64.b64decode(self.upd_file).decode('utf-8').split('\n'))
		for row in file_reader:
			read_sql = '''
				select * from program_lead where %s = '%s'
				and active = true and program_name = '%s';\n
			''' % (key_field, row[key_field], program_name)
			self.env.cr.execute(read_sql)
			recs = self.env.cr.dictfetchall()
			if recs and recs[0]:

				upd_sql = '''
					update program_lead set pgm_remark = '%s', bay_name = '%s',
					 reg_number = '%s', meeting_id = '%s',
					 meeting_number = '%s', meeting_url = '%s', check_in_status = '%s', day_1_attendance = '%s',
					  day_1_duration_missed = %s, day_2_attendance = '%s', day_2_duration_missed = %s,
					   day_3_attendance = '%s', day_3_duration_missed = %s, day_4_attendance = '%s',
					    day_4_duration_missed = %s, day_5_attendance = '%s', day_5_duration_missed = %s,
					     whats_app_group = '%s', meeting_registrant_id = '%s', additional_remarks = '%s' where %s = '%s'
					and active = true and program_name = '%s';\n
				''' % (
					   row['pgm_remark'].strip() if 'pgm_remark' in row and row['pgm_remark'].strip() else recs[0]['pgm_remark'] or 'null',
					   row['bay_name'].strip() if 'bay_name' in row and row['bay_name'].strip() else recs[0]['bay_name'] or 'null',
					   row['reg_number'].strip() if 'reg_number' in row and row['reg_number'].strip() else recs[0]['reg_number'] or 'null',
					   row['meeting_id'].strip() if 'meeting_id' in row and row['meeting_id'].strip() else recs[0]['meeting_id'] or 'null',
					   row['meeting_number'].strip() if 'meeting_number' in row and row['meeting_number'].strip() else recs[0]['meeting_number'] or 'null',
					   row['meeting_url'].strip() if 'meeting_url' in row and row['meeting_url'].strip() else recs[0]['meeting_url'] or 'null',
					   row['check_in_status'].strip() if 'check_in_status' in row and row['check_in_status'].strip() else recs[0]['check_in_status'] or 'null',
					   row['day_1_attendance'].strip() if 'day_1_attendance' in row and row['day_1_attendance'].strip() else recs[0]['day_1_attendance'] or 'null',
					   row['day_1_duration_missed'].strip() if 'day_1_duration_missed' in row and row['day_1_duration_missed'].strip() else recs[0]['day_1_duration_missed'] or 'null',
					   row['day_2_attendance'].strip() if 'day_2_attendance' in row and row['day_2_attendance'].strip() else recs[0]['day_2_attendance'] or 'null',
					   row['day_2_duration_missed'].strip() if 'day_2_duration_missed' in row and row['day_2_duration_missed'].strip() else recs[0]['day_2_duration_missed'] or 'null',
					   row['day_3_attendance'].strip() if 'day_3_attendance' in row and row['day_3_attendance'].strip() else recs[0]['day_3_attendance'] or 'null',
					   row['day_3_duration_missed'].strip() if 'day_3_duration_missed' in row and row['day_3_duration_missed'].strip() else recs[0]['day_3_duration_missed'] or 'null',
					   row['day_4_attendance'].strip() if 'day_4_attendance' in row and row['day_4_attendance'].strip() else recs[0]['day_4_attendance'] or 'null',
					   row['day_4_duration_missed'].strip() if 'day_4_duration_missed' in row and row['day_4_duration_missed'].strip() else recs[0]['day_4_duration_missed'] or 'null',
					   row['day_5_attendance'].strip() if 'day_5_attendance' in row and row['day_5_attendance'].strip() else recs[0]['day_5_attendance'] or 'null',
					   row['day_5_duration_missed'].strip() if 'day_5_duration_missed' in row and row['day_5_duration_missed'].strip() else recs[0]['day_5_duration_missed'] or 'null',
					   row['whatsapp_group'].strip() if 'whatsapp_group' in row and row['whatsapp_group'].strip() else recs[0]['whats_app_group'] or 'null',
					   row['meeting_registrant_id'].strip() if 'meeting_registrant_id' in row and row['meeting_registrant_id'].strip() else recs[0]['meeting_registrant_id'] or 'null',
					   row['additional_remarks'].strip() if 'additional_remarks' in row and row['additional_remarks'].strip() else recs[0]['additional_remarks'] or 'null',
					   key_field, row[key_field], program_name)
				self.env.cr.execute(upd_sql)

		cleanup_sql = '''
			update program_lead set pgm_remark = null where pgm_remark = 'null';
			update program_lead set bay_name = null where bay_name = 'null';
			update program_lead set reg_number = null where reg_number = 'null';
			update program_lead set meeting_id = null where meeting_id = 'null';
			update program_lead set meeting_number = null where meeting_number = 'null';
			update program_lead set meeting_url = null where meeting_url = 'null';
			update program_lead set check_in_status = null where check_in_status = 'null';
			update program_lead set day_1_attendance = null where day_1_attendance = 'null';
			update program_lead set day_2_attendance = null where day_2_attendance = 'null';
			update program_lead set day_3_attendance = null where day_3_attendance = 'null';
			update program_lead set day_4_attendance = null where day_4_attendance = 'null';
			update program_lead set day_5_attendance = null where day_5_attendance = 'null';
			update program_lead set whats_app_group = null where whats_app_group = 'null';
			update program_lead set meeting_registrant_id = null where meeting_registrant_id = 'null';
			update program_lead set additional_remarks = null where additional_remarks = 'null';
		'''
		self.env.cr.execute(cleanup_sql)
		self.env.cr.commit()

