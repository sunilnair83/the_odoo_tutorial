import json
import logging
import sys
import traceback
from _datetime import datetime
from datetime import date, datetime
from datetime import timezone

import requests

from odoo import api, fields, models, SUPERUSER_ID
from odoo.exceptions import UserError, AccessError, ValidationError

_logger = logging.getLogger('ResPartner')
_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
_logger.addHandler(ch)

class IshaOnlineSathsangApiWiazrd(models.TransientModel):
	_name = 'isha.online.sathsang.api.wizard'
	_description = 'Wizard to add verify contact and invoke PRS api'

	selected_count = fields.Integer(string='Selected Contacts')
	program_name = fields.Selection([('IE - Manually Added','IE'),('SCK - Manually Added','Shoonya'),('Uyir Nokkam','Uyir Nokkam - Unknown')],string='Verification For')
	start_date = fields.Date(string='Start Date')
	teacher_name = fields.Char(string='Teacher Name')
	center_name = fields.Char(string='Center Name')

	@api.model
	def default_get(self, fields):
		res = super(IshaOnlineSathsangApiWiazrd, self).default_get(fields)

		res['selected_count'] = len(self._context.get('active_ids'))
		return res

	def send_request(self, prs_api_url, params):
		payload = {'result': json.dumps(params)}
		response = requests.request("POST", prs_api_url, headers={}, data=payload, files=[],verify=False)

	def sso_set_entitlement(self, entitlementid, profidvar):
		retflg = False
		try:
			startDate = datetime.now(timezone.utc)
			endDate = date(startDate.year + 4, startDate.month, startDate.day)
			endDate = startDate.replace(startDate.year + 1)
			parents_dict = {
				"entitlements": [
					{
						"id": entitlementid,
						"validity": {
							"endTime": int(endDate.timestamp() * 1000),
							"startTime": int(startDate.timestamp() * 1000)
						}
					}
				]
			}
			datavar2 = json.dumps(parents_dict)
			authreq = self.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
			headers2 = {
				"content-type": "application/json",
				"x-user": profidvar,
				"X-legal-entity": "IF",
				"Authorization": "Bearer " + authreq
			}
			# requestconsent_url = "https://uat-profile.isha.in/services/pms/api/entitlements/" + profidvar
			requestconsent_url = self.env['ir.config_parameter'].sudo().get_param('satsang.SSO_PMS_ENDPOINT1') + "entitlements/" + profidvar
			reqvar = requests.post(requestconsent_url, datavar2, headers=headers2)
			if reqvar.status_code == 200:
				retflg = True
		except (UserError, AccessError, ValidationError) as exc:
			_logger.error("Error caught during request sso update evergreen flag", exc, exc_info=True)
		return retflg

	def call_prs_api(self,recs=None):
			if not recs:
				recs = self.env[self._context.get('active_model')].browse(self._context.get('active_ids'))
			#sso api call
			for rec in recs:
				try:
					profidvar = rec.sso_id
					evergreenentitlment = ""
					event_type = rec.program_name
					if event_type == "Online SCK Satsang":
						evergreenentitlment = "evergreen_shoonya_2020"
					elif event_type == "Online Satsang":
						evergreenentitlment = "evergreen_satsang_2020"
					elif event_type == "Online IEO Satsang":
						evergreenentitlment = "evergreen_ieo_2020"
					elif event_type == "Online Samyama Satsang":
						evergreenentitlment = "evergreen_samyama_2020"

					#evergreenflag= self.sso_set_entitlement("evergreen_satsang_2020", profidvar)
					if evergreenentitlment:
						evergreenflag = self.sso_set_entitlement(evergreenentitlment, profidvar)
					#evergreenflag = True
					# if 'entitlements' in consentreq_dict['entitlements']:
					# 	for objvar in consentreq_dict['entitlements']:
					# 		if objvar["id"] == "satsang_evergreen_2020":
					# 			evergreenflag = True
					# 			break;
					if event_type=="Online Ananda Alai":
						rec.sudo().write({'last_api_called': datetime.now(), 'reg_status': 'CONFIRMED'})
						self.env['res.partner'].browse(rec.contact_id_fkey.id).sudo().write({
						    'category_id': [(4, self.env['res.partner.category'].search([('name', '=', 'Verified - Manually')])[0].id)],
						})
						transprogramlead_rec = self.env['transprogramlead'].sudo().create({
						    'record_name': rec.name,
						    'record_phone': rec.phone,
						    'record_email': rec.email,
						    'trans_lang': rec.trans_lang,
						    'program_name': event_type,
							'tzlangregion': rec.bay_name
						})
						self.env['satsang.notification'].sudo().sendRegistrationEmailSMSCore(transprogramlead_rec)
					if evergreenflag:
						rec.sudo().write({'last_api_called': datetime.now(), 'reg_status': 'CONFIRMED'})
						self.env['res.partner'].browse(rec.contact_id_fkey.id).sudo().write({
										'category_id': [(4,self.env['res.partner.category'].search([('name','=','Verified - Manually')])[0].id)],
										})
						transprogramlead_rec = self.env['transprogramlead'].sudo().create({
							'record_name': rec.name,
							'record_phone': rec.phone,
							'record_email': rec.email,
							'trans_lang': rec.trans_lang,
							'program_name': event_type
						})
						self.env['satsang.notification'].sudo().sendRegistrationEmailSMSCore(transprogramlead_rec)
				except Exception as ex:
					tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
					self.env['satsang.tempdebuglog'].sudo().create({
						'logtext': "satsang mail notif"
					})
					self.env['satsang.tempdebuglog'].sudo().create({
						'logtext': tb_ex
					})

	def send_email_notif(self, rec):
		template = self.env.ref('isha_crm.pgm_verification_mail_template')
		values = template.sudo().generate_email(rec.id, fields=None)
		mail_mail_obj = self.env['mail.mail']
		msg_id = mail_mail_obj.sudo().create(values)
		return msg_id.send(False)

	def mark_meditator(self, rec):
		try:
			url = self.env['ir.config_parameter'].sudo().get_param('isha_crm.global_mark_meditator_api')
			authreq = self.env['ir.config_parameter'].sudo().get_param('isha_crm.global_meditator_api_token')
			payload = json.dumps({
				"isMeditator": True,
				"ssoId": rec.sso_id,
				"key": self.env['ir.config_parameter'].sudo().get_param('isha_crm.global_mark_meditator_secret')
			})
			headers = {
				'Authorization': 'Bearer '+authreq,
				'Content-Type': 'application/json'
			}
			response = requests.request("PUT", url, headers=headers, data=payload)
			_logger.info("Global api mark_meditator for "+str(rec.sso_id)+' -> '+response.text)
		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			_logger.error(tb_ex)

	def create_pgm_att(self):
		rec = self.env['program.lead'].browse(self._context.get('active_id'))

		pgm_master = None
		if self.program_name == 'IE - Manually Added':
			pgm_master = self.env.ref('isha_crm.ie_manually_added_master').id
		if self.program_name == 'SCK - Manually Added':
			pgm_master = self.env.ref('isha_crm.sck_manually_added_master').id
		if self.program_name == 'Uyir Nokkam':
			pgm_master = self.env.ref('isha_crm.uyir_nokkam_manually_added_master').id

		if self.env['program.attendance'].search([('contact_id_fkey','=',rec.contact_id_fkey.id),
													 ('program_name','=',self.program_name)],count=True) > 0:
			return self.env['partner.import.temp.wizard'].get_popup('Attendance record is already added for '+str(rec.contact_id_fkey.name))
		pgm_att_dict = {
				'system_id': 0,
				'local_trans_id':rec.id,
				'active': True,
				'reg_status': 'COMPLETED',
				'local_program_type_id': self.program_name,
				'program_name': self.program_name,
				'center_name':self.center_name,
				'pgm_type_master_id':pgm_master,
				'contact_id_fkey': rec.contact_id_fkey.id,
				'teacher_name': self.teacher_name,
				'start_date': self.start_date.strftime('%Y-%m-%d') if self.start_date else None,
				'reg_date_time': str(self.start_date) + ' 00:00:00' if self.start_date else datetime.now().strftime(
				'%Y-%m-%d %H:%M:%S')
			 }
		pgm_arr_rec = self.env['program.attendance'].create([pgm_att_dict])
		rec.write({'reg_status':'Manually Verified'})

		# should remove the unable to verify tag once manually verified
		not_verified_id = self.env['res.partner.category'].search([('name', '=', 'Unable to Verify')])[0].id
		rec.contact_id_fkey.write({'category_id': [(3, not_verified_id)]})

		# Mandala verification email to be sent only when IE rec is added for 'IE mandala' verification txns
		if rec.program_name == 'IE mandala' and self.program_name == 'IE - Manually Added':
			self.send_email_notif(rec)
			self.mark_meditator(rec)
		# For satsang manual verification set the entitlement and send out an email
		if 'satsang' in rec.program_name.lower() or 'Online Ananda Alai' == rec.program_name:
			self.call_prs_api()
		return self.env['partner.import.temp.wizard'].get_popup('Successfully created attendance record for '+str(rec.contact_id_fkey.name))
