from odoo import models,fields,api
from odoo.exceptions import UserError

class M2MTagManipulationTemp(models.TransientModel):
	_name = 'm2m.tag.manipulation.temp'
	_description = 'Tag Manipulation Temp'

	name = fields.Char(string='name')
	tag_id = fields.Integer(string='Tag Id')
	field_id = fields.Many2one('ir.model.fields', string='Field')


class M2MTagManipulation(models.TransientModel):
	_name = 'm2m.tag.manipulation'
	_description = 'Tag Manipulation'


	model_id = fields.Many2one('ir.model', string='Model')
	field_id = fields.Many2one('ir.model.fields', string='Field')
	show_values = fields.Boolean(default=False)
	restrict_fields = fields.Boolean(string='Restrict Fields')
	value_id = fields.Many2many('m2m.tag.manipulation.temp', string='Tag Value')

	@api.model
	def default_get(self, fields):
		if len(self.env.context.get('active_ids')) > 200:
			raise UserError('Sorry you cannot modify more than 200 records !!!.')

		res = super(M2MTagManipulation, self).default_get(fields)
		if self.env.context.get('mapped_model'):
			model = self.env['ir.model'].search([('model', '=', self.env.context.get('mapped_model'))])
		elif self.env.context.get('active_model'):
			model = self.env['ir.model'].search([('model','=',self.env.context.get('active_model'))])
		if self._context.get('restrict_fields'):
			res['field_id'] = self.env['ir.model.fields'].search([('name', '=', self._context.get('restrict_fields')),
					('model', '=', model.model)]).id
			res['restrict_fields']=True
		res['model_id'] = model.id
		return res

	@api.onchange('model_id')
	def scope_fields(self):
		if self.model_id:
			return {'domain':{'field_id':[('model_id','=',self.model_id.id), ('ttype', 'in', ['many2many', 'one2many'])]}}

	@api.onchange('field_id')
	def scope_values(self):
		if self.field_id:
			return {'domain':{'value_id':[('field_id','=',self.field_id.id)]}}

	def generate_m2m_tags(self):
		co_model = self.field_id.relation
		tag_list = []
		self.env['m2m.tag.manipulation.temp'].search([('field_id','=', self.field_id.id)]).unlink()
		if self._context.get('restrict_values'):
			recs = self.env[co_model].search(self._context.get('restrict_values'))
		else:
			recs = self.env[co_model].search([])
		for x in recs:
			tag_list.append({'field_id': self.field_id.id, 'name': x.name, 'tag_id': x.id})
		self.env['m2m.tag.manipulation.temp'].create(tag_list)
		self.show_values = True
		return {
				"context":{'default_show_values': True},
				"type": "set_scrollTop",
			}

	def append_tag(self):
		active_ids = self.env.context.get('active_ids')
		active_recs  = self.env[self.env.context.get('active_model')].browse(active_ids)
		if self.env.context.get('mapped_model'):
			active_recs = active_recs.mapped(self.env.context.get('mapped_field'))
		active_recs.sudo().write({self.field_id.name : [(4,x) for x in self.value_id.mapped('tag_id')]})
		self.clear_temp()

	def replace_tags(self):
		active_ids = self.env.context.get('active_ids')
		active_recs  = self.env[self.env.context.get('active_model')].browse(active_ids)
		if self.env.context.get('mapped_model'):
			active_recs = active_recs.mapped(self.env.context.get('mapped_field'))
		active_recs.sudo().write({self.field_id.name : [(6,0,self.value_id.mapped('tag_id'))]})
		self.clear_temp()

	def remove_specific_tags(self):
		active_ids = self.env.context.get('active_ids')
		active_recs  = self.env[self.env.context.get('active_model')].browse(active_ids)
		if self.env.context.get('mapped_model'):
			active_recs = active_recs.mapped(self.env.context.get('mapped_field'))
		active_recs.sudo().write({self.field_id.name : [(3,x) for x in self.value_id.mapped('tag_id')]})
		self.clear_temp()


	def remove_n_delete_tags(self):
		active_ids = self.env.context.get('active_ids')
		active_recs  = self.env[self.env.context.get('active_model')].browse(active_ids)
		if self.env.context.get('mapped_model'):
			active_recs = active_recs.mapped(self.env.context.get('mapped_field'))
		active_recs.sudo().write({self.field_id.name : [(2,x) for x in self.value_id.mapped('tag_id')]})
		self.clear_temp()

	def remove_all_tags(self):
		active_ids = self.env.context.get('active_ids')
		active_recs  = self.env[self.env.context.get('active_model')].browse(active_ids)
		if self.env.context.get('mapped_model'):
			active_recs = active_recs.mapped(self.env.context.get('mapped_field'))
		active_recs.sudo().write({self.field_id.name : [(5,0,0)]})
		self.clear_temp()

	def clear_temp(self):
		if self.field_id:
			self.env['m2m.tag.manipulation.temp'].search([('field_id', '=', self.field_id.id)]).unlink()