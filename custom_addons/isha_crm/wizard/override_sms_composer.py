from odoo import _, api, fields, models


class SendSMSInherit(models.TransientModel):
	_inherit = 'sms.composer'
	template_id = fields.Many2one('sms.template', string='Use Template', domain="[('model', '=', res_model)]")


	@api.model_create_multi
	def create(self, vals_list):
		for val in vals_list:
			if 'body' not in val:
				val['body'] = self.env['sms.template'].browse(val['template_id'])[0].body
		return super(SendSMSInherit,self).create(vals_list)