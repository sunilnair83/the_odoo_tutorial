import odoo

class ErrorUpdater:

    def logError(self, registry, msg):
        UID = odoo.SUPERUSER_ID
        CR = registry.cursor()
        with odoo.api.Environment.manage():
            ENV = odoo.api.Environment(CR, UID, {})
            errorModel = ENV['kafka.errors']
            rec = errorModel.create(msg)
            if rec:
                flag_dict =  {'flag':True, 'rec':rec}
            else:
                flag_dict = {'flag':False, 'rec':None}
            CR.commit()
            CR.close()
            return flag_dict

    def logErrorEnv(self, env, msg):
        errorModel = env['kafka.errors']
        rec = errorModel.create(msg)
        if rec:
            flag_dict = {'flag': True, 'rec': rec}
        else:
            flag_dict = {'flag': False, 'rec': None}
        return flag_dict