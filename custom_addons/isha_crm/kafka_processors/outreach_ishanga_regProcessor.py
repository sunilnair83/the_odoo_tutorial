from .. import ContactProcessor
from ..controllers import sso_auth_endpoint
from ..isha_base_importer import Configuration
from datetime import date, timedelta, datetime
import logging
_logger = logging.getLogger(__name__)

null = None
false = False
true = True
class outreach_ishanga_regProcessor:

    system_id = 44

    def getGoldenFormat(self, msg, iso2, correction):
        sso_id = msg['profileId']
        basic_profile = ContactProcessor.replaceEmptyString(msg['basicProfile'])
        phone_dict = ContactProcessor.replaceEmptyString(msg['basicProfile']['phone'])
        extended_profile = ContactProcessor.replaceEmptyString(msg['extendedProfile'])

        name = basic_profile['firstName']
        if basic_profile['lastName'] and len(basic_profile['lastName']) > 0:
            name = name + " " + basic_profile['lastName']
        phone = None
        if phone_dict['number']:
            phone = phone_dict['countryCode'] if phone_dict['countryCode'] else ''
            phone += phone_dict['number']
        email = basic_profile['email']
        gender = ContactProcessor.getgender(basic_profile['gender'])
        dob = basic_profile['dob'] if basic_profile['dob'] else None
        nationality = extended_profile['nationality']
        passport = extended_profile['passportNum'] if extended_profile['passportNum'] else None
        pan = extended_profile['pan'] if extended_profile['pan'] else None
        street = None
        street2 = None
        state = None
        city = None
        country = None
        zip = None
        if len(msg['addresses']) > 0:
            address_dict = ContactProcessor.replaceEmptyString(msg['addresses'][0])
            street = address_dict['addressLine1']
            street2 = address_dict['addressLine2']
            if address_dict['townVillageDistrict']:
                if street2 and type(street2) == str:
                    street2 = street2 + ', ' + address_dict['townVillageDistrict']
                else:
                    street2 = address_dict['townVillageDistrict']
            city = address_dict['city']
            state = address_dict['state']
            country = address_dict['country']
            zip = address_dict['pincode']
        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': sso_id,
            'active': True,
            'name': name, 'display_name': name, 'prof_dr': None,
            'phone': phone, 'phone2': None,
            'phone3': None, 'phone4': None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email, 'email2': None, 'email3': None,
            'street': street, 'street2': street2, 'city': city, 'state': state, 'country': country,
            'zip': zip,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': gender, 'occupation': None, 'marital_status': None, 'dob': dob,
            'nationality': nationality, 'deceased': None, 'contact_type': None, 'companies': None,
            'aadhaar_no': None, 'pan_no': pan, 'passport_no': passport,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': sso_id

        }

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatTxn(self, msg):
        msg_dict = {
            'request_id': msg.get('request_id', None),
            'donation_id': msg.get('donation_id', None),
            'status': msg.get('status', None),
            'is_passport_verified':msg.get('is_passport_verified',None),
            'transaction_date': msg.get('transaction_date', None),
            'donate_purpose_id': msg.get('donate_purpose_id', None),
            'gateway_type': msg.get('gateway_type', None),
            'amount': msg.get('amount', None),
            'currency': msg.get('currency', None),
            'country': msg.get('country', None),
            'u_date': msg.get('u_date', None),
            'gateway_status': msg.get('gateway_status', None),
        }
        return msg_dict

    def update_no_marketing_flag(self, odoo_rec, txn_dict, profile_data, env):
        url = env['ir.config_parameter'].sudo().get_param('web.base.url')
        # check if it is s1
        if 'ishangam.isha.in' in url and profile_data['basicProfile']['countryOfResidence'] != 'IN':
            odoo_rec['no_marketing'] = True
        # check if it is s2
        elif 'ishangam.isha.us' in url and profile_data['basicProfile']['countryOfResidence'] == 'IN':
            odoo_rec['no_marketing'] = True

    def process_message(self, src_msg, env, metadata):
        src_msg_dict = src_msg

        txn_dict = self.getGoldenFormatTxn(src_msg_dict)
        try:
            flag_dict = self.createPgmInternal(env, txn_dict)
            env.cr.commit()
            return flag_dict
        finally:
            pass

    def createPgmInternal(self, ENV, src_rec):
        # TO DO add outreach table here and outreach specific business logic
        txnModel = ENV['ishanga.nu.payment.transaction']
        donationModel = ENV['ishanga.donation.registration']
        _logger.info("inndide Kaffka")

        recs = txnModel.search([('id', '=', int(src_rec['request_id']))])
        if recs:

            vals = {"sso_id": recs.sso_id,
                    "type": "kafka_response",
                    "donation_type": recs.donation_type,
                    "contribute_amount": src_rec['amount'],
                    "converted_amount": src_rec['amount'],
                    "donation_currency": recs.donation_currency,
                    "currency": "INR",
                    "status": src_rec['status'],
                    "message": "Response from KAFKA to MOKSHA",
                    "session_id": recs.session_id,
                    "tracking_id":recs.id,
                    "donation_reg_id": recs.donation_reg_id.id,
                    "date": date.today()}
            crete_new_entry = txnModel.create(vals)
            _logger.info("Kaffak result")
            _logger.info(src_rec)
            status =src_rec['status']
            if src_rec['status']=="pending activation":
                status="pending_activation"
            if src_rec['status']=="activation failed":
                status="activation_failed"
            if src_rec['status']=="Invalid":
                status = "invalid"
            if src_rec['status'] == "Awaited":
                status = "awaited"
            if src_rec['status'] == "Success":
                status = "success"
            if src_rec['status'] == "Active":
                status = "active"
            if src_rec['status'] == "Timeout":
                status = "timeout"
            if src_rec['status'] == "Initiated":
                status = "initiated"


            crete_new_entry.write({"donation_status":status})
            _logger.info("donation_status UDATE KAFFA--before")
            _logger.info(status)
            _logger.info("donation_status UDATE KAFFA--after")
            _logger.info(crete_new_entry.donation_status)
            if recs.donation_reg_id:
                d_rec = donationModel.search([("id","=",recs.donation_reg_id.id)])
                if d_rec:
                    if status in ("Success","Awaited","active","verificationPending","success","Verification Pending","Active"):
                        d_rec.write({"is_ishaga_active":True,"donation_type":recs.donation_type})
                        _logger.info("Update isgnga - Kaffka")


        flag_dict = {'flag': True, 'rec': recs}
        return flag_dict
