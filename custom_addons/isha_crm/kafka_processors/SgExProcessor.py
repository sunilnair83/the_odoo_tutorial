from datetime import datetime

from .. import ContactProcessor


class SgExProcessor:
    system_id = 33

    def removePlus(self, code):
        if code and type(code) == str and len(code)>1 and code[0]=='+':
            return code[1:]
        else:
            return code

    def getDateParsed(self, timestamp):
        try:
            date_parsed = datetime.fromtimestamp(timestamp/1000)
            date_parsed = str(date_parsed.strftime('%Y-%m-%d %H:%M:%S')) if date_parsed else None
        except:
            date_parsed = None
        return date_parsed

    def getLastTxnDate(self, datetime):
        try:
            date_parsed = datetime.split(' ')[0]
        except:
            date_parsed = None
        return date_parsed


    def currencyTransform(self, code):
        if code:
            if code == 'AUD':
                return 'AP'
            if code == 'CAD':
                return 'US'
            if code == 'EUR':
                return 'EU'
            if code == 'GBP':
                return 'EU'
            if code == 'HKD':
                return 'AP'
            if code == 'IDR':
                return 'AP'
            if code == 'INR':
                return 'IN'
            if code == 'MYR':
                return 'AP'
            if code == 'NZD':
                return 'AP'
            if code == 'PHP':
                return 'AP'
            if code == 'SGD':
                return 'AP'
            if code == 'THB':
                return 'AP'
            if code == 'USD':
                return 'US'
        return code


    def getGoldenFormat(self, src_msg, iso2, correction):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        name = src_msg['first_name']
        name += ' ' + src_msg['last_name'] if src_msg['last_name'] else ''
        email = src_msg['email']
        phone = src_msg['mobile_phone'] if 'mobile_phone' in src_msg else ''
        if phone == '':
            phone = None

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['id'],
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': phone, 'phone2': None,'phone3': None, 'phone4': None, 'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email if email else None, 'email2': None,'email3': None,
            'street': src_msg['address_line_1'] if 'address_line_1' in src_msg else None,  'street2':src_msg['address_line_2'] if 'address_line_2' in src_msg else None,
            'city': src_msg['city'], 'state': src_msg['state'], 'country': src_msg['country'], 'zip': src_msg['post_code'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': src_msg['sso_id'],
            'has_charitable_txn': True
        }

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatForTxn(self, sgex_txn):
        sgex_txn_dict = {
                'local_trans_id': sgex_txn['id'],
                'active':True,
                'email':sgex_txn.get('email', None),
                'current':True,
                'reg_status':sgex_txn['status'],
                'subscription_date':self.getDateParsed(sgex_txn['created_ts']),
                'subscription_type':sgex_txn['type'],
                'amount':sgex_txn['amount'],
                'currency':sgex_txn['currency'],
                'currency_region':self.currencyTransform(sgex_txn['currency']),
                'country':sgex_txn['country'],
                'cancellation_date':self.getDateParsed(sgex_txn['cancel_ts']),
                'cancellation_reason':sgex_txn['cancel_reason'],
                'modified_ts':self.getDateParsed(sgex_txn['modified_ts']),
                'sso_id':sgex_txn['sso_id'],
                'subscription_expiry_date':self.getDateParsed(sgex_txn['renewal_ts']),
                'renewal_type': sgex_txn['renewal_type'],
                'trial_days':sgex_txn['trial_days'] if 'trial_days' in sgex_txn else 0
        }
        if sgex_txn_dict['reg_status'] == 'ACTIVE' and sgex_txn_dict['trial_days'] > 0:
            sgex_txn_dict['trial_conversion'] = True

        # To Avoid resetting the value
        if not sgex_txn_dict.get('subscription_expiry_date'):
            sgex_txn_dict.pop('subscription_expiry_date')


        return sgex_txn_dict

    def process_message(self, src_msg, env, metadata):
        # logger.debug(msg)
        src_msg_dict = src_msg

        iso2 = metadata['iso2']
        correction = metadata['correction']

        msg_dict = self.getGoldenFormat(src_msg_dict, iso2, correction)
        txn_dict = self.getGoldenFormatForTxn(src_msg_dict)

        last_txn_date = self.getLastTxnDate(txn_dict['subscription_date'])
        if last_txn_date:
            msg_dict['last_txn_date'] = self.getLastTxnDate(txn_dict['subscription_date'])
            msg_dict['charitable_last_txn_date'] = msg_dict['last_txn_date']

        try:
            contact_flag = env['res.partner'].sudo().create_internal_flag(msg_dict)
            contact = contact_flag['rec']
            txn_dict['contact_id_fkey'] = contact[0].id
            # Check if the contact has already inquired for the Sadhguru Exclusive
            if env.ref('isha_crm.sadhguru_exclusive_inq').id in contact.category_id.ids:
                txn_dict['sgx_inquired'] = True
            else:
                txn_dict['sgx_inquired'] = False
            # Check if it is a new contact which got created
            if contact_flag['phase'] == 'phase4':
                txn_dict['new_contact_created'] = True
            else:
                txn_dict['new_contact_created'] = False

            flag_dict = self.createPgmInternal(env, txn_dict)
            txn = flag_dict['rec']
            env.cr.commit()
            return flag_dict
        finally:
            pass
    def createPgmInternal(self, ENV, src_rec):
        txnModel = ENV['sadhguru.exclusive']

        rec = txnModel.search([('local_trans_id', '=', src_rec['local_trans_id'])])
        if len(rec) != 0:
            if src_rec['reg_status'] == 'CANCELLED':
                src_rec['cancellation_count'] = rec.cancellation_count + 1 if rec.reg_status == 'ACTIVE' else rec.cancellation_count
                existing_rec = txnModel.search([('contact_id_fkey', '=', src_rec['contact_id_fkey']),
                                                        ('reg_status','=','ACTIVE'),('local_trans_id', '!=', src_rec['local_trans_id'])])
                if len(existing_rec) > 0:
                    src_rec['current'] = False
                    existing_rec.write({'last_cancellation_date':src_rec['cancellation_date'],'cancellation_count':src_rec['cancellation_count']})

            rec.write(src_rec)
        elif len(rec) == 0:
            existing_rec = txnModel.search([('contact_id_fkey', '=', src_rec['contact_id_fkey'])])
            current_active_rec = txnModel
            for x in existing_rec:
                if x.current:
                    current_active_rec = x
                    break
            if current_active_rec:
                src_rec['inquiry_date'] = current_active_rec['inquiry_date']
                src_rec['last_cancellation_date'] = current_active_rec['cancellation_date'] if current_active_rec['cancellation_date'] else current_active_rec['last_cancellation_date']
                src_rec['cancellation_count'] = current_active_rec['cancellation_count']
                if src_rec['reg_status'] == 'CANCELLED':
                    src_rec['cancellation_count'] += 1
            if src_rec['reg_status'] == 'ACTIVE':
                non_active = existing_rec.filtered(lambda txn: txn.reg_status != 'ACTIVE' and (txn.current is True or txn.active is True))
                non_active.write({'current': False})
            rec = txnModel.create(src_rec)
        flag_dict = {'flag': True, 'rec': rec}
        return flag_dict
