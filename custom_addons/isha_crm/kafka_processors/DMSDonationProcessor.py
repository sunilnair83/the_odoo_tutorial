from .. import TxnUpdater, TxnProcessor


class DMSDonationProcessor:

    def process_message(self, msg, env, metadata):
        try:
            donationUpdater = TxnUpdater.DonationsUpdater()
            flag_dict = donationUpdater.createDonationInternal(env, TxnProcessor.DMSTxn().getOdooFormat(msg))
            env.cr.commit()
            return flag_dict
        finally:
            pass