from datetime import datetime

from .. import ContactProcessor


class FeedbackProcessor:

    def getDateParsed(self, str_date):
        try:
            date_parsed = datetime.strptime(str_date, '%Y/%m/%d %H:%M:%S') if str_date else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            date_parsed = None
        return date_parsed


    def getGoldenFormat(self, src_msg, iso2, correction, ENV):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        name = src_msg.get('First Name') if 'First Name' in src_msg else ''
        name += ' ' + src_msg.get('Last Name') if 'Last Name' in src_msg else ''
        if name == '':
            name = src_msg.get('Name')
        submitted_email = src_msg.get('Email')

        if 'Education Level' in src_msg and src_msg['Education Level']:
            if src_msg.get('Education Level').lower() == 'high school':
                src_msg['Education Level'] = 'Diploma'
            elif src_msg.get('Education Level').lower() == 'undergraduate':
                src_msg['Education Level'] = 'Bachelor - Other'
            elif src_msg.get('Education Level').lower() == 'postgraduate':
                src_msg['Education Level'] = 'Master - Other'
            else:
                src_msg['Education Level'] = 'Other'

        skill_tags = ENV['isha.skill.tag']
        ids_skill_tags = []
        otherskills_options = src_msg.get('Professional/ Other skills')
        if otherskills_options:
            otherskills_array = otherskills_options.split("\n")
            for x in otherskills_array:
                rec = skill_tags.search([('name', '=', x)], limit=1)
                if rec:
                    ids_skill_tags.append((4, rec.id))
        advertisingskills_options = src_msg.get('Advertising Skills')
        if advertisingskills_options:
            advertisingskills_array = advertisingskills_options.split("\n")
            for x in advertisingskills_array:
                rec = skill_tags.search([('name', '=', x)], limit=1)
                if rec:
                    ids_skill_tags.append((4, rec.id))
        itskills_options = src_msg.get('IT Skills')
        if itskills_options:
            itskills_array = itskills_options.split("\n")
            for x in itskills_array:
                rec = skill_tags.search([('name', '=', x)], limit=1)
                if rec:
                    ids_skill_tags.append((4, rec.id))
        multimediaskills_options = src_msg.get('Multimedia Skills')
        if multimediaskills_options:
            multimediaskills_array = multimediaskills_options.split("\n")
            for x in multimediaskills_array:
                rec = skill_tags.search([('name', '=', x)], limit=1)
                if rec:
                    ids_skill_tags.append((4, rec.id))
        languageskills_options = src_msg.get('Language Skills')
        if languageskills_options:
            languageskills_array = languageskills_options.split("\n")
            for x in languageskills_array:
                rec = skill_tags.search([('name', '=', x)], limit=1)
                if rec:
                    ids_skill_tags.append((4, rec.id))

        marketingskills_options = src_msg.get('Marketing Skills')
        if marketingskills_options:
            marketingskills_array = marketingskills_options.split("\n")
            for x in marketingskills_array:
                rec = skill_tags.search([('name', '=', x)], limit=1)
                if rec:
                    ids_skill_tags.append((4, rec.id))

        languagesknown = ENV['isha.language.tag']
        ids_languagesknown = []
        languagesknown_options = src_msg.get('Languages Known')
        if languagesknown_options:
            languagesknown_array = languagesknown_options.split("\n")
            for x in languagesknown_array:
                rec = languagesknown.search([('name', '=', x)], limit=1)
                if rec:
                    ids_languagesknown.append((4, rec.id))

        dob = datetime.strptime(src_msg.get('Date of Birth'), '%d-%m-%Y').date() if src_msg.get('Date of Birth') else None
        gender = src_msg.get('Gender')
        if gender:
            if gender == 'Male':
                gender = 'M'
            elif gender == 'Female':
                gender = 'F'
            else:
                gender = None

        odooRec = {
            'system_id': src_msg.get('system_id'),
            'local_contact_id': src_msg.get('local_contact_id'),
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None, 'phone_country_code': src_msg.get('phone_country_code'),
            'phone': src_msg.get('Phone Number'), 'phone2': None,'phone3': None, 'phone4': None, 'whatsapp_number': src_msg.get('WhatsApp Number'),
            'whatsapp_country_code': src_msg.get('whatsapp_country_code'),
            'email': submitted_email if submitted_email else None, 'email2': None,'email3': None,
            'street': None,  'street2': None, 'work_company_name': src_msg.get('Company/Organization'),
            'city': src_msg.get('City'), 'state': None, 'country': src_msg.get('Country'), 'zip': src_msg.get('Zip Code'),
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': gender, 'occupation': src_msg.get('Occupation'), 'marital_status': None, 'dob': dob,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None, 'education': src_msg.get('Education Level'),
            'nationality': src_msg.get('Nationality'), 'deceased': None, 'contact_type': 'INDIVIDUAL', 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'work_designation': src_msg.get('Designation'),
            'sso_id': src_msg.get('SSO id') if 'SSO id' in src_msg else None,
            'skill_tag_ids': ids_skill_tags,
            'vol_duration': src_msg.get('Volunteering Availability'),
            'lang_ids': ids_languagesknown,
        }

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatForTxn(self, ENV, feedback_txn):

        hearaboutsadhguru = ENV['hear.about.sadhguru.tags']
        hearaboutprogram = ENV['hear.about.program.tags']
        motivationtosignup = ENV['motivation.to.signup']
        vitalaspectofpgm = ENV['vital.aspect.pgm']
        supportinitiatives = ENV['support.initiatives']
        howtostayconnected = ENV['how.to.stay.connected']
        programlist = ENV['program.list']
        activitiestosupport = ENV['isha.project.support']

        ids_hear_about_sadhguru = []
        ids_hear_about_program = []
        ids_motivation_to_sign_up = []
        ids_vital_aspect_of_pgm = []
        ids_support_initiatives = []
        ids_how_to_stay_connected = []
        ids_programlist = []
        ids_activitiestosupport = []

        other_comments_hear_about_sadhguru = None
        other_comments_hear_about_program = None
        other_comments_vital_aspect_of_pgm = None
        other_comments_motivation_to_sign_up = None

        submitted_date = self.getDateParsed(feedback_txn.get('Submission Date'))

        hear_about_sadhguru_options = feedback_txn.get('How did you hear about Sadhguru?')
        if hear_about_sadhguru_options:
            hear_about_sadhguru_array = hear_about_sadhguru_options.split("; ")
            for x in hear_about_sadhguru_array:
                rec = hearaboutsadhguru.search([('name', '=', x)], limit=1)
                if rec:
                    ids_hear_about_sadhguru.append(rec.id)
                else:
                    rec = hearaboutsadhguru.search([('name', '=', 'Other')], limit=1)
                    ids_hear_about_sadhguru.append(rec.id)
                    other_comments_hear_about_sadhguru = x
        hear_about_program_options = feedback_txn.get('How did you hear about Inner Engineering')
        if hear_about_program_options:
            hear_about_program_array = hear_about_program_options.split("; ")
            for x in hear_about_program_array:
                rec = hearaboutprogram.search([('name', '=', x)], limit=1)
                if rec:
                    ids_hear_about_program.append(rec.id)
                else:
                    rec = hearaboutprogram.search([('name', '=', 'Other')], limit=1)
                    ids_hear_about_program.append(rec.id)
                    other_comments_hear_about_program = x
        motivation_to_sign_up_options = feedback_txn.get('What motivated you to sign up for Inner Engineering?')
        if motivation_to_sign_up_options:
            motivation_to_sign_up_array = motivation_to_sign_up_options.split("; ")
            for x in motivation_to_sign_up_array:
                rec = motivationtosignup.search([('name', '=', x)], limit=1)
                if rec:
                    ids_motivation_to_sign_up.append(rec.id)
                else:
                    rec = motivationtosignup.search([('name', '=', 'Other')], limit=1)
                    ids_motivation_to_sign_up.append(rec.id)
                    other_comments_motivation_to_sign_up = x
        vital_aspect_of_pgm_options = feedback_txn.get('What do you feel is the most vital aspect of the program?')
        if vital_aspect_of_pgm_options:
            vital_aspect_of_pgm_array = vital_aspect_of_pgm_options.split("; ")
            for x in vital_aspect_of_pgm_array:
                rec = vitalaspectofpgm.search([('name', '=', x)], limit=1)
                if rec:
                    ids_vital_aspect_of_pgm.append(rec.id)
                else:
                    rec = vitalaspectofpgm.search([('name', '=', 'Other')], limit=1)
                    ids_vital_aspect_of_pgm.append(rec.id)
                    other_comments_vital_aspect_of_pgm = x
        support_initiatives_options = feedback_txn.get("Would you like to support Isha's Initiatives?")
        if support_initiatives_options:
            support_initiatives_array = support_initiatives_options.split("; ")
            for x in support_initiatives_array:
                rec = supportinitiatives.search([('name', '=', x)], limit=1)
                if rec:
                    ids_support_initiatives.append(rec.id)
        how_to_stay_connected_options = feedback_txn.get('(For participants within North America): How would you like to stay connected with local Isha meditators, programs and events? (Check all that apply)')
        if how_to_stay_connected_options:
            how_to_stay_connected_array = how_to_stay_connected_options.split("; ")
            for x in how_to_stay_connected_array:
                rec = howtostayconnected.search([('name', '=', x)], limit=1)
                if rec:
                    ids_how_to_stay_connected.append(rec.id)

        programlist_options = feedback_txn.get('Programs Interested In')
        if programlist_options:
            programlist_array = programlist_options.split("\n")
            for x in programlist_array:
                rec = programlist.search([('name', '=', x)], limit=1)
                if rec:
                    ids_programlist.append(rec.id)
        activitiestosupport_options = feedback_txn.get('Activities Interested In')
        if activitiestosupport_options:
            activitiestosupport_array = activitiestosupport_options.split("\n")
            for x in activitiestosupport_array:
                rec = activitiestosupport.search([('name', '=', x)], limit=1)
                if rec:
                    ids_activitiestosupport.append(rec.id)

        if feedback_txn.get('How easy was the login process to the system?'):
            ease_of_login = feedback_txn.get('How easy was the login process to the system?') + "/5"
        else:
            ease_of_login = feedback_txn.get('How easy was the login process to the system?')
        if feedback_txn.get('How was the video stream? Were you able to go through session smoothly?'):
            vedio_streaming_quality = feedback_txn.get('How was the video stream? Were you able to go through session smoothly?') + "/5"
        else:
            vedio_streaming_quality = feedback_txn.get('How was the video stream? Were you able to go through session smoothly?')

        return {
            'submitted_date': submitted_date,
            'telegram_number': feedback_txn.get('Telegram Number'),
            'active': True,
            'system_id': feedback_txn.get('system_id'),
            'local_trans_id': feedback_txn.get('local_contact_id'),
            'submitted_email': feedback_txn.get('Email'),
            'hear_about_sadhguru': [(6, 0, ids_hear_about_sadhguru)],
            'hear_about_program': [(6, 0, ids_hear_about_program)],
            'motivation_to_sign_up': [(6, 0, ids_motivation_to_sign_up)],
            'vital_aspect_of_pgm': [(6, 0, ids_vital_aspect_of_pgm)],
            'support_initiatives': [(6, 0, ids_support_initiatives)],
            'how_to_stay_connected': [(6, 0, ids_how_to_stay_connected)],
            'feedback_for': feedback_txn.get('event_name'),
            'event_date': feedback_txn.get('event_date'),
            'derive_from_pgm': feedback_txn.get('What did you wish to derive from this program?'),
            'benefits_since_pgm': feedback_txn.get('What specific benefits have you noticed since the program started?'),
            'say_about_pgm': feedback_txn.get('Wish to say about pgm'),
            'would_recommend_pgm': feedback_txn.get('Would you recommend the program to your friends and family?'),
            'interested_in_advanced_pgms': feedback_txn.get('Interested in advanced pgms') == 'Yes',
            'other_way_to_support': feedback_txn.get("Is there any other way you would like to support Isha Foundation's activities?"),
            'add_to_mandala_wa_group': feedback_txn.get('(For participants outside the Americas): Would you like to be added to a Mandala WhatsApp group to support your 40 days mandala of Shambhavi Mahamudra? Trained Ishangas will be part of this group to clarify your queries and to support your practices.'),
            'ease_of_login': ease_of_login,
            'video_stream_quality': vedio_streaming_quality,
            'technical_support': feedback_txn.get('If and when you had technical difficulties, did you find the necessary support to resolve them?'),
            'technical_requirements_clear': feedback_txn.get('Were the technical requirements to go through the program clear enough?'),
            'other_technical_feedback': feedback_txn.get('Any other feedback on the technical aspects'),
            'other_comments_hear_about_sadhguru': other_comments_hear_about_sadhguru,
            'other_comments_hear_about_program': other_comments_hear_about_program,
            'other_comments_vital_aspect_of_pgm': other_comments_vital_aspect_of_pgm,
            'other_comments_motivation_to_sign_up': other_comments_motivation_to_sign_up,
            'programs_interested_in': [(6, 0, ids_programlist)],
            'activities_to_support': [(6, 0, ids_activitiestosupport)],
            'interested_in_ftv': feedback_txn.get('FTV Interest') == 'Yes',
            'occupation_feedback': feedback_txn.get('Feedback Occupation'),
            'interested_in_volunteering': feedback_txn.get('Volunteering Interest') == 'Yes'
        }

    def process_message(self, src_msg, env, metadata):
        # logger.debug(msg)

        iso2 = metadata['iso2']
        correction = metadata['correction']

        msg_dict = self.getGoldenFormat(src_msg, iso2, correction, env)
        txn_dict = self.getGoldenFormatForTxn(env, src_msg)

        try:
            contact = env['res.partner'].sudo().create_internal(msg_dict)
            txn_dict['contact_id_fkey'] = contact[0].id
            flag_dict = self.create_feedback_internal(env, txn_dict)
            env.cr.commit()
            return flag_dict
        finally:
            pass

    def create_feedback_internal(self, ENV, src_rec):
        feedbackModel = ENV['completion.feedback']

        rec = feedbackModel.search([('feedback_for', '=', 'IECO'), ('submitted_date', '=', src_rec['submitted_date']),
                                    ('contact_id_fkey', '=', src_rec['contact_id_fkey'])])
        if src_rec['feedback_for'] == 'IECO':
            if len(rec) != 0:
                src_rec['active'] = False
                rec = feedbackModel.create(src_rec)
            elif len(rec) == 0:
                rec = feedbackModel.create(src_rec)
        elif src_rec['feedback_for'] == 'Stay Connected':
            rec = feedbackModel.search([('feedback_for','=','Stay Connected'),('contact_id_fkey','=',src_rec['contact_id_fkey'])])
            if len(rec) != 0:
                rec.write(src_rec)
            elif len(rec) == 0:
                rec = feedbackModel.create(src_rec)
        elif src_rec['feedback_for'] == 'Online Ananda Alai':
            rec = feedbackModel.search([('feedback_for','=','Online Ananda Alai'),('contact_id_fkey','=',src_rec['contact_id_fkey'])])
            if len(rec) != 0:
                rec.write(src_rec)
            elif len(rec) == 0:
                rec = feedbackModel.create(src_rec)

        flag_dict = {'flag': True, 'rec': rec}
        return flag_dict
