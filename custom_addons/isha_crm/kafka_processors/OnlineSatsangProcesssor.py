from datetime import datetime, timedelta

from .. import ContactProcessor, TxnUpdater


class OnlineSatsangProcessor:
    system_id = 13

    def getDateFromDateTime(self, date_time):
        try:
            return datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        except:
            return None

    def getEventAttDatetime(self, src_dt):
        try:
            return str(datetime.strptime(src_dt, '%d-%m-%Y %H:%M:%S') - timedelta(hours=5, minutes=30))
        except:
            return None

    def getGoldenFormat(self, src_msg, iso2, correction):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        name = src_msg['name']
        email = ContactProcessor.getTokenizedVersion(src_msg['email'], ',')
        sso_id = src_msg['sso_id']
        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': sso_id,
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': None, 'phone2': None,'phone3': None, 'phone4': None, 'whatsapp_number':None, 'whatsapp_country_code':None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': None,  'street2':None,'city': None, 'state': None, 'country': None, 'zip': None,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': sso_id,
            'has_charitable_txn': True
        }
        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatForTxn(self, satsang_txn):
        if satsang_txn['program_name'] == 'Online Monthly Satsang Confirmation':
            prog_name = 'Online Satsang'
        else:
            prog_name = satsang_txn['program_name']
        email = ContactProcessor.getTokenizedVersion(satsang_txn['email'], ',')
        event_att_date = self.getEventAttDatetime(satsang_txn['event_attendance_date'])
        start_date = self.getDateFromDateTime(event_att_date)
        odooRec = {
            'local_trans_id': satsang_txn['sso_id'],
            'system_id': self.system_id,
            'reg_status': 'ATTENDED',
            'program_name': prog_name,
            'local_program_type_id': prog_name,
            'event_attendance_date': event_att_date,
            'start_date': start_date,
            'reg_date_time': str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
            'pgm_remark': 'RSVP',
            'trans_lang': satsang_txn['trans_lang'],
            'record_email': email['1'] if '1' in email else None
        }
        return odooRec

    def process_message(self, src_msg, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        try:
            pgmUpdater = TxnUpdater.ProgramAttUpdater()

            contactMsgDict = self.getGoldenFormat(src_msg, iso2, correction)
            txn_dict = self.getGoldenFormatForTxn(src_msg)

            last_txn_date = txn_dict['start_date']
            if last_txn_date:
                contactMsgDict['last_txn_date'] = last_txn_date
                contactMsgDict['charitable_last_txn_date'] = last_txn_date

            contactModel = env['res.partner']
            contact = contactModel.sudo().create_internal(contactMsgDict)
            txn_dict['contact_id_fkey'] = contact[0].id
            txn_dict['guid'] = contact[0].guid
            # logger.debug('-----------------'+str(txn_dict['contact_id_fkey'])+'contacts done-----------')

            flag_dict = pgmUpdater.createPgmInternal(env, txn_dict)
            env.cr.commit()
            return flag_dict
        finally:
            pass



