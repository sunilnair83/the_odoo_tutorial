from .EreceiptsProcessor import EreceiptsProcessor
from .. import ContactProcessor


class EreceiptsReligiousProcessor(EreceiptsProcessor):

    def getGoldenFormatForContact(self, msg, metadata):
        odooRec = super(EreceiptsReligiousProcessor, self).getGoldenFormatForContact(msg, metadata)
        odooRec['has_charitable_txn'] = None
        odooRec['has_religious_txn'] = True
        return odooRec

    def getGoldenFormatForTxn(self, src_msg, env, metadata):
        odooRec = super(EreceiptsReligiousProcessor, self).getGoldenFormatForTxn(src_msg, env, metadata)
        src_msg = ContactProcessor.replaceEmptyString(src_msg)

        odooRec['ebook_number'] = src_msg['ebook_number']
        odooRec['books_of_account'] = src_msg['books_of_account'] if 'books_of_account' in src_msg else None

        return odooRec
