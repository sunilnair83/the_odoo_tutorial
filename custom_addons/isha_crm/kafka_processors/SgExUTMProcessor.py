from odoo.exceptions import UserError


class SgExUTMProcessor:
    system_id = 33


    def getGoldenFormatForTxn(self, sgex_utm_txn):
        trans_id = sgex_utm_txn['subscription_request_id']
        return {
                'local_trans_id':trans_id.lower(),
                'utm_campaign':sgex_utm_txn['utm_campaign'],
                'utm_content':sgex_utm_txn['utm_content'],
                'utm_medium':sgex_utm_txn['utm_medium'],
                'utm_source':sgex_utm_txn['utm_source'],
                'utm_term':sgex_utm_txn['utm_term'] if 'utm_term' in sgex_utm_txn else None
        }

    def process_message(self, src_msg, env, metadata):
        # logger.debug(msg)
        src_msg_dict = src_msg
        txn_dict = self.getGoldenFormatForTxn(src_msg_dict)
        try:
            flag_dict = self.createPgmInternal(env, txn_dict)
            txn = flag_dict['rec']
            env.cr.commit()
            return flag_dict
        finally:
            pass

    def createPgmInternal(self, ENV, src_rec):
        txnModel = ENV['sadhguru.exclusive']
        local_trans_id = src_rec.pop('local_trans_id',None)
        rec = txnModel.search([('local_trans_id', 'ilike', '%'+local_trans_id)])
        if len(rec) != 0:
            rec.write(src_rec)
        elif len(rec) == 0:
            raise UserError('Out Of Order Processing.')
        flag_dict = {'flag': True, 'rec': rec}
        return flag_dict
