import json

from confluent_kafka import Producer

from .. import ContactProcessor
from ...isha_base.configuration import Configuration
from datetime import datetime


class SulabaContactsProcessor:

    def process_message(self, src_msg_dict, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        contactMsgDict = self.getContactFormat(src_msg_dict, env, iso2, correction)
        contact_flag = env['res.partner'].sudo().create_internal_flag(contactMsgDict)
        rec = contact_flag['rec']
        maps = env['contact.map'].sudo().search_read(
            domain=[('system_id', '=', 61), ('contact_id_fkey', '=', rec['id'])],
            fields=['id', 'local_contact_id', 'system_id'], order='id ASC')
        dob = str(rec['dob']) if rec['dob'] else False
        msg = {
            'name': rec.name, 'phone': rec.phone,
            'phone_country_code': rec.phone_country_code,
            'phone2': rec.phone2,
            'phone2_country_code': rec.phone2_country_code,
            'phone3': rec.phone3,
            'phone3_country_code': rec.phone3_country_code,
            'phone4': rec.phone4,
            'phone4_country_code': rec.phone4_country_code,
            'email': rec.email, 'email2': rec.email2, 'email3': rec.email3,
            'is_meditator': True if rec.is_meditator else False,
            'program_tags': ",".join([record.name for record in rec.pgm_tag_ids]),
            'is_starmarked': True if len(rec.approved_incident_by_partner_ids) > 0 else False,
            'is_new_contact': True if contact_flag['phase'] == 'phase4' else False,
            'contact_map': maps, # get the list of local_contact_id values from the maps list
            'local_id': src_msg_dict['local_id'],
            'starmark_category': rec['starmark_category_id'].name,
            'incident_date_closed': str(rec['incident_date_closed']) if rec['incident_date_closed'] else False,
            'incident_date_assigned': str(rec['incident_date_assigned']) if rec['incident_date_assigned'] else False,
            'partner_severity': rec['partner_severity'],
            'message_type': src_msg_dict['message_type'],
            'gender': rec['gender'], 'dob': dob
        }

        crm_config = Configuration('CRM')
        kafka_config = Configuration('KAFKA')
        p = Producer({
            'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
            'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
            'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
            'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH']
        })

        sulaba_topic = crm_config['CRM_KAFKA_PUSH_SULABA_MERGE_RESPONSE']
        record = json.dumps(msg)
        p.produce(sulaba_topic, record.encode('utf-8'))
        p.flush(2)

    def getContactFormat(self, src_msg, env, iso2, correction):
        dob = datetime.strptime(src_msg['dob'], '%Y-%m-%d') if src_msg['dob'] else None
        contact = ContactProcessor.get_empty_contact_object()
        contact['system_id'] = src_msg['system_id']
        contact['local_contact_id'] = src_msg['local_id']
        contact['name'] = src_msg['name']
        contact['phone_country_code'] = src_msg['phone_country_code']
        contact['phone'] = src_msg['phone']
        contact['email'] = src_msg['email']
        contact['gender'] = src_msg['gender']
        contact['dob'] = dob

        return ContactProcessor.validateValues(contact, iso2, correction)
