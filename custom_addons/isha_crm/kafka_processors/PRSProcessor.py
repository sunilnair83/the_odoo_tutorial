from .. import TxnUpdater, TxnProcessor, ContactProcessor
from datetime import datetime, timedelta


class PRSProcessor:

    def process_message(self, src_msg_dict, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        pgmUpdater = TxnUpdater.ProgramAttUpdater()
        # logger.debug(msg)
        contactMsgDict = ContactProcessor.PRSContacts().getOdooFormat(src_msg_dict, iso2, correction)
        pgm_mst = env['program.type.master'].search([('local_program_type_id', '=', src_msg_dict['program_name']),
                                                     ('system_id', '=', 13)])
        if pgm_mst and pgm_mst[0].is_religious:
            contactMsgDict['has_religious_txn'] = True
        else:
            contactMsgDict['has_charitable_txn'] = True

        if pgm_mst and pgm_mst[0].is_covid_support:
            contact = env['res.partner'].sudo().create_internal(contactMsgDict)
            self.create_covid_support_txn(src_msg_dict, env, contact[0].id)
        elif src_msg_dict['program_name'] == 'Stay Connected with Isha':
            contact = env['res.partner'].sudo().create_internal(contactMsgDict)
            self.create_feedback_txn(src_msg_dict, env, contact[0].id)
        else:
            txn_dict = TxnProcessor.PRSProgramTxn().getOdooFormat(src_msg_dict)

            # Ignoring shivanga sadhana if it is not confirmed
            if 'shivanga sadhana' in txn_dict['local_program_type_id'].lower():
                if txn_dict['reg_status'].lower() != 'confirmed':
                    return False

            # Ignoring msr passes if it is not confirmed or cancelled
            if 'msr 2021 seating passes' in txn_dict['local_program_type_id'].lower():
                if txn_dict['reg_status'].lower() not in ['confirmed','cancelled','registered']:
                    return False

            last_txn_date = TxnProcessor.PRSProgramTxn().getLastTxnDate( txn_dict['event_attendance_date'])
            last_txn_date = TxnProcessor.PRSProgramTxn().getLastTxnDate( txn_dict['reg_date_time']) if not last_txn_date else last_txn_date
            if last_txn_date:
                contactMsgDict['last_txn_date'] = last_txn_date
                if pgm_mst and pgm_mst[0].is_religious:
                    contactMsgDict['religious_last_txn_date'] = last_txn_date
                else:
                    contactMsgDict['charitable_last_txn_date'] = last_txn_date

            contactModel = env['res.partner']
            contact = contactModel.sudo().create_internal(contactMsgDict)
            txn_dict['contact_id_fkey'] = contact[0].id
            txn_dict['guid'] = contact[0].guid
            # logger.debug('-----------------'+str(txn_dict['contact_id_fkey'])+'contacts done-----------')

            pgmUpdater.createPgmInternal(env, txn_dict)
        env.cr.commit()

    def create_covid_support_txn(self, src_rec, ENV, contact_id):
        rec = ENV['covid.support'].search([('local_trans_id', '=', src_rec['_id'])])
        if not rec:
            txn_dict = {
                'support_options': self.get_covid_support_tags(ENV, src_rec['covid_support']) if 'covid_support' in src_rec else None,
                'willing_whatsapp_group': (src_rec['willing_whatsapp_group'] == 'Yes') if 'willing_whatsapp_group' in src_rec else None,
                'contact_id_fkey': contact_id,
                'email': src_rec['email'],
                'reg_date_time': (datetime.strptime(src_rec['created_date'], '%Y-%m-%d %H:%M:%S') -
                                  timedelta(hours=5, minutes=30)).strftime('%Y-%m-%d %H:%M:%S'),
                'local_trans_id': src_rec['_id'],
                'system_id': 13
            }
            ENV['covid.support'].create(txn_dict)

    def create_feedback_txn(self, src_rec, ENV, contact_id):
        rec = ENV['completion.feedback'].search([('contact_id_fkey', '=', contact_id), ('feedback_for', '=', 'Stay Connected')])
        if rec:
            rec[0].write({
                'submitted_date': (datetime.strptime(src_rec['created_date'], '%Y-%m-%d %H:%M:%S') -
                                   timedelta(hours=5, minutes=30)).strftime('%Y-%m-%d %H:%M:%S'),
                'local_trans_id': src_rec['_id'],
            })
        else:
            txn_dict = {
                'contact_id_fkey': contact_id,
                'system_id': 13,
                'local_trans_id': src_rec['_id'],
                'submitted_email': src_rec['email'],
                'submitted_date': (datetime.strptime(src_rec['created_date'], '%Y-%m-%d %H:%M:%S') -
                                   timedelta(hours=5, minutes=30)).strftime('%Y-%m-%d %H:%M:%S'),
                'feedback_for': 'Stay Connected',
            }
            ENV['completion.feedback'].create(txn_dict)

    def get_covid_support_tags(self, ENV, support):
        if support:
            tag_set = set()
            options = support.split(',')
            for x in options:
                if 'to make calls' in x.lower():
                    tag_set.add(ENV.ref('isha_crm.covid_support_option_1').id)
                if 'social media awareness' in x.lower():
                    tag_set.add(ENV.ref('isha_crm.covid_support_option_2').id)
                if 'medical professional' in x.lower() or 'tele consultation' in x.lower():
                    tag_set.add(ENV.ref('isha_crm.covid_support_option_3').id)
                if 'to donate' in x.lower():
                    tag_set.add(ENV.ref('isha_crm.covid_support_option_4').id)
                if 'conduct simha kriya sessions online' in x.lower():
                    tag_set.add(ENV.ref('isha_crm.covid_support_option_5').id)

            return [(4, x) for x in tag_set]
        else:
            return None
