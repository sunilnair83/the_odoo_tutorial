import fiscalyear


def get_donor_category(ENV, isha_campaign_id, donor_name, contact_id, donation_date, inr_amount, transfer_reference,
                       receipt_number):
    campaign_name = None
    if isha_campaign_id:
        campaign_map = ENV['iv.campaign.map'].search([('isha_campaign_id', '=', isha_campaign_id)])
        if campaign_map:
            campaign_name = campaign_map[0].campaign_name
        else:
            raise Exception('No mapping is present for the isha_campaign_id: ' + str(isha_campaign_id))

    if campaign_name:
        donor_category = campaign_name
    else:
        donor_orgs = ENV['iv.donor.org'].search([('name', '=ilike', donor_name)])
        if donor_orgs:
            donor_category = donor_orgs[0].category
        else:
            fy = get_financial_year(donation_date)
            cur_fy_donations = None
            if contact_id:
                domain = [('contact_id_fkey', '=', contact_id),
                         ('donation_date', '>=', fy['from_date']),
                         ('donation_date', '<=', fy['to_date']),
                         ('iv_donor_category', 'in', ['Individual', 'HNIs']),
                         ('consolidated_receipt_type', '!=', 'parent')]
                if transfer_reference:
                    domain += [('transfer_reference', '!=', transfer_reference)]
                if receipt_number:
                    domain += [('receipt_number', '!=', receipt_number)]
                cur_fy_donations = ENV['donation.isha.education'].search(domain)
            total_amount = 0
            if cur_fy_donations:
                for donation in cur_fy_donations:
                    total_amount += donation.inr_amount
            total_amount += inr_amount
            if total_amount >= 50000:
                donor_category = 'HNIs'
                for donation in cur_fy_donations:
                    donation.iv_donor_category = 'HNIs'
            else:
                donor_category = 'Individual'
    return donor_category


def get_new_renewal_info(ENV, donation_date, contact_id, fes_qty, scho_qty, transfer_reference, receipt_number):
    prev_fes_qty = 0
    prev_scho_qty = 0
    cur_fes_qty = 0
    cur_scho_qty = 0
    if contact_id:
        prev_fy = get_previous_financial_year(donation_date)
        prev_fy_donations = None
        if prev_fy:
            prev_fy_donations = ENV['donation.isha.education'].search([('contact_id_fkey', '=', contact_id),
                                                                       ('donation_date', '>=', prev_fy['from_date']),
                                                                       ('donation_date', '<=', prev_fy['to_date']),
                                                                       ('consolidated_receipt_type', '!=', 'parent')])
        if prev_fy_donations:
            for donation in prev_fy_donations:
                prev_fes_qty += donation.full_educational_support_qty
                prev_scho_qty += donation.scholarship_qty

        cur_fy = get_financial_year(donation_date)
        if cur_fy:
            domain = [('contact_id_fkey', '=', contact_id),
                     ('donation_date', '>=', cur_fy['from_date']),
                     ('donation_date', '<=', cur_fy['to_date']),
                     ('consolidated_receipt_type', '!=', 'parent')]
            if transfer_reference:
                domain += [('transfer_reference', '!=', transfer_reference)]
            if receipt_number:
                domain += [('receipt_number', '!=', receipt_number)]

            cur_fy_donations = ENV['donation.isha.education'].search(domain)

            if cur_fy_donations:
                for donation in cur_fy_donations:
                    cur_fes_qty += donation.full_educational_support_qty
                    cur_scho_qty += donation.scholarship_qty

    prev_fes_qty = max(prev_fes_qty - cur_fes_qty, 0)
    prev_scho_qty = max(prev_scho_qty - cur_scho_qty, 0)

    fes_renewal = min(prev_fes_qty, fes_qty)
    fes_new = fes_qty - fes_renewal

    scho_renewal = min(prev_scho_qty, scho_qty)
    scho_new = scho_qty - scho_renewal

    return {
        'fes_new_qty': fes_new,
        'fes_renewal_qty': fes_renewal,
        'scho_new_qty': scho_new,
        'scho_renewal_qty': scho_renewal
    }

def get_new_renew(prev_fy_qty, cur_fy_qty, cur_qty):
    prev = prev_fy_qty
    cur = cur_fy_qty + cur_qty
    renew_qty = min(prev, cur)
    new_qty = cur - renew_qty
    return {'new_qty': new_qty,
            'renew_qty': renew_qty}

def get_purpose_breakup(src_msg, ENV, donation_date, currency, purpose, contact_id, short_year, campaign_name):
    purpose_breakup = {}
    purpose_breakup['subsidy_breakup_modified_by'] = ''

    # determine the subsidy breakup
    purpose_to_subsidy_amount_field_map = {
        'Govt School Adoption Project': 'govt_school_adoption_prgm_amount',
        'Govt School Adoption Project FCRA': 'govt_school_adoption_prgm_amount',
        'Scholarship': 'scholarship_amount',
        'Full Education': 'full_educational_support_amount',
        'Infrastructure': 'infrastructure_requirements_amount',
        'Noonmeal': 'noon_meal_subsidy_amount',
        'Donation': 'general_donation_amount',
        'Transport': 'transport_subsidy_amount'
    }
    amount_field_to_qty_field_map = {
        'full_educational_support_amount': 'full_educational_support_qty',
        'scholarship_amount': 'scholarship_qty',
        'transport_subsidy_amount': 'transport_subsidy_qty',
        'noon_meal_subsidy_amount': 'noon_meal_subsidy_qty',
    }

    currency_to_field_name_map = {
        ENV.ref('base.INR').id: 'inr_cost',
        ENV.ref('base.USD').id: 'usd_cost',
        ENV.ref('base.GBP').id: 'gbp_cost',
    }

    subsidy_amount_field_names = ['general_donation_amount', 'govt_school_adoption_prgm_amount',
                                  'infrastructure_requirements_amount',
                                  'full_educational_support_amount', 'scholarship_amount',
                                  'transport_subsidy_amount', 'noon_meal_subsidy_amount'
                                  ]

    if 'center' in src_msg and src_msg['center'] == 'IVMS Admin Office - CBE':
        amount_field_name = purpose_to_subsidy_amount_field_map[purpose]
        if purpose in ['Full Education', 'Scholarship', 'Noonmeal', 'Transport']:
            # requirement is to assign the amount directly without checking if it matches the cost
            purpose_breakup[amount_field_name] = float(src_msg['amount'])
            # calculate the quantity
            results = ENV['iv.purpose.cost'].search([('purpose', '=', purpose),
                                                          ('financial_year', '=', short_year)])
            if results:
                currency_field_name = currency_to_field_name_map[currency]
                if (src_msg['amount'] % results[0][currency_field_name]) == 0:
                    qty = src_msg['amount'] / results[0][currency_field_name]
                    qty_field_name = amount_field_to_qty_field_map[amount_field_name]
                    purpose_breakup[qty_field_name] = qty
                    purpose_breakup['subsidy_breakup_modified_by'] = 'System'
                else:
                    purpose_breakup['subsidy_breakup_modified_by'] = 'System-For Review: Amount mismatch'
            else:
                purpose_breakup['subsidy_breakup_modified_by'] = 'System-For Review: No Matching Purpose'
        else:
            # no quantity field for these purposes, just set the amount.
            purpose_breakup[amount_field_name] = float(src_msg['amount'])
            purpose_breakup['subsidy_breakup_modified_by'] = 'System'

    else:
        short_year = get_short_financial_year(donation_date)
        purpose_cost_maps = ENV['iv.purpose.cost'].search([('financial_year', '=', short_year)], order='priority')
        amount_match_found = False
        if purpose_cost_maps:
            currency_field_name = currency_to_field_name_map[currency]

            # search for a single quantity match
            for purpose_cost_map in purpose_cost_maps:
                if src_msg['amount'] == purpose_cost_map[currency_field_name]:
                    amount_match_found = True
                    amount_field_name = purpose_to_subsidy_amount_field_map[purpose_cost_map.purpose]
                    purpose_breakup[amount_field_name] = src_msg['amount']
                    qty_field_name = amount_field_to_qty_field_map[amount_field_name] if amount_field_name in amount_field_to_qty_field_map else None
                    if qty_field_name:
                        purpose_breakup[qty_field_name] = 1
                    purpose_breakup['subsidy_breakup_modified_by'] = 'System'
                    break
                else:
                    continue

            # search for multiple quantity match
            if not amount_match_found:
                for purpose_cost_map in purpose_cost_maps:
                    if purpose_cost_map[currency_field_name] and (src_msg['amount'] % purpose_cost_map[currency_field_name]) == 0:
                        amount_match_found = True
                        amount_field_name = purpose_to_subsidy_amount_field_map[purpose_cost_map.purpose]
                        purpose_breakup[amount_field_name] = src_msg['amount']
                        qty_field_name = amount_field_to_qty_field_map[amount_field_name] if amount_field_name in amount_field_to_qty_field_map else None
                        if qty_field_name:
                            purpose_breakup[qty_field_name] = src_msg['amount'] / purpose_cost_map[currency_field_name]
                        purpose_breakup['subsidy_breakup_modified_by'] = 'System'
                        break
                    else:
                        continue
            # check if it matches FES + Noonmeal or Scholarship + Noonmeal
            if not amount_match_found:
                fes_cost = 0
                scholarship_cost = 0
                noon_meal_cost = 0
                for purpose_cost_map in purpose_cost_maps:
                    if purpose_cost_map.purpose == 'Full Education':
                        fes_cost = purpose_cost_map[currency_field_name]
                    elif purpose_cost_map.purpose == 'Scholarship':
                        scholarship_cost = purpose_cost_map[currency_field_name]
                    elif purpose_cost_map.purpose == 'Noonmeal':
                        noon_meal_cost = purpose_cost_map[currency_field_name]
                if src_msg['amount'] == (fes_cost + noon_meal_cost):
                    purpose_breakup['full_educational_support_amount'] = fes_cost
                    purpose_breakup['full_educational_support_qty'] = 1
                    purpose_breakup['noon_meal_subsidy_amount'] = noon_meal_cost
                    purpose_breakup['noon_meal_subsidy_qty'] = 1
                if src_msg['amount'] == (scholarship_cost + noon_meal_cost):
                    purpose_breakup['scholarship_amount'] = scholarship_cost
                    purpose_breakup['scholarship_qty'] = 1
                    purpose_breakup['noon_meal_subsidy_amount'] = noon_meal_cost
                    purpose_breakup['noon_meal_subsidy_qty'] = 1

        else:
            raise Exception('No purpose-cost entry found for FY: ' + short_year)
        if not amount_match_found:
            # check if there are any donations by the user in the previous financial year.
            # if there is a single donation, get the purpose from that and assign the current donation against
            # that purpose/subsidy type.
            prev_fy_donations = None
            if contact_id:
                prev_fy = get_previous_financial_year(donation_date)
                prev_fy_donations = ENV['donation.isha.education'].search([('contact_id_fkey', '=', contact_id),
                                                                           ('donation_date', '>=', prev_fy['from_date']),
                                                                           ('donation_date', '<=', prev_fy['to_date']),
                                                                           ('consolidated_receipt_type', '!=', 'parent')])
            if prev_fy_donations:
                if len(prev_fy_donations) > 1:
                    purpose_breakup['general_donation_amount'] = src_msg['amount']
                    purpose_breakup['subsidy_breakup_modified_by'] = 'System-For Review: Multiple Previous FY donations'
                else:
                    for amount_field_name in subsidy_amount_field_names:
                        if prev_fy_donations[0][amount_field_name] and prev_fy_donations[0][amount_field_name] != 0:
                            if amount_field_name in amount_field_to_qty_field_map:
                                purpose = get_key(purpose_to_subsidy_amount_field_map, amount_field_name)
                                purpose_cost_map = ENV['iv.purpose.cost'].search([('purpose', '=', purpose),
                                                                              ('financial_year', '=', short_year)])
                                if purpose_cost_map:
                                    currency_field_name = currency_to_field_name_map[currency]
                                    if src_msg['amount'] < purpose_cost_map[0][currency_field_name]:
                                        purpose_breakup['general_donation_amount'] = src_msg['amount']
                                        purpose_breakup['subsidy_breakup_modified_by'] = 'System-For Review: Amount less than cost'
                                    elif src_msg['amount'] % purpose_cost_map[0][currency_field_name] != 0:
                                        purpose_breakup['general_donation_amount'] = src_msg['amount'] % purpose_cost_map[0][currency_field_name]
                                        purpose_breakup[amount_field_name] = src_msg['amount'] - purpose_breakup['general_donation_amount']
                                        qty_field_name = amount_field_to_qty_field_map[amount_field_name]
                                        purpose_breakup[qty_field_name] = src_msg['amount'] / purpose_cost_map[0][currency_field_name]
                                        purpose_breakup['subsidy_breakup_modified_by'] = 'System'
                                else:
                                    raise Exception('No entry found for cost of the purpose: ' + purpose +
                                                    ' for FY: ' + short_year)
                            else:
                                purpose_breakup[amount_field_name] = src_msg['amount']
                                purpose_breakup['subsidy_breakup_modified_by'] = 'System'

            elif campaign_name and 'isha vidhya valasaravakkam' in campaign_name.lower():
                purpose_breakup['scholarship_amount'] = src_msg['amount']
                purpose_breakup['subsidy_breakup_modified_by'] = 'System'
            else:
                purpose_breakup['general_donation_amount'] = src_msg['amount']
                purpose_breakup['subsidy_breakup_modified_by'] = 'System-For Review: Previous FY donations not found'

    crm_breakup_fields = ['general_donation_amount', 'govt_school_adoption_prgm_amount',
                          'infrastructure_requirements_amount', 'full_educational_support_qty',
                          'full_educational_support_amount', 'scholarship_qty', 'scholarship_amount',
                          'transport_subsidy_qty', 'transport_subsidy_amount', 'noon_meal_subsidy_qty',
                          'noon_meal_subsidy_amount']
    remaining_breakup_fields = crm_breakup_fields - purpose_breakup.keys()
    for key in remaining_breakup_fields:
        purpose_breakup[key] = None

    if 'subsidy_breakup_modified_by' in purpose_breakup and not purpose_breakup['subsidy_breakup_modified_by'].startswith('System-For Review'):
        amount_present = False
        multiple_purposes_allocated = False
        for amount_field_name in subsidy_amount_field_names:
            if purpose_breakup[amount_field_name]:
                if amount_present:
                    multiple_purposes_allocated = True
                    break
                else:
                    amount_present = True

        if (multiple_purposes_allocated or
                purpose_breakup.get('full_educational_support_qty') and purpose_breakup['full_educational_support_qty'] > 1 or
                purpose_breakup.get('scholarship_qty') and purpose_breakup['scholarship_qty'] > 1 or
                purpose_breakup.get('transport_subsidy_qty') and purpose_breakup['transport_subsidy_qty'] > 1 or
                purpose_breakup.get('noon_meal_subsidy_qty') and purpose_breakup['noon_meal_subsidy_qty'] > 1):
                    purpose_breakup['subsidy_breakup_modified_by'] = 'System-For Review: Multiple allocations'


    return purpose_breakup


def is_consolidated_parent_receipt(donor_name, comments):
    if donor_name.lower() == 'Isha Foundation Inc'.lower() or (comments and comments.lower() == 'consolidated receipts'):
        return True
    return False


def get_short_financial_year(cur_date):
    short_year = None
    if cur_date:
        fiscalyear.START_MONTH = 4
        fiscal_date = fiscalyear.FiscalDate(cur_date.year, cur_date.month, cur_date.day)
        fy = fiscalyear.FiscalYear(fiscal_date.fiscal_year)
        short_year = str(fy.start.year % 100) + str(fy.end.year % 100)
    return short_year


def get_financial_year(donation_date):
    if donation_date:
        return _get_financial_year(donation_date.year, donation_date.month)
    else:
        return None


def get_previous_financial_year(donation_date):
    if donation_date:
        return _get_financial_year(donation_date.year - 1, donation_date.month)
    else:
        return None


def _get_financial_year(year, month):
    fiscalyear.START_MONTH = 4
    fiscal_date = fiscalyear.FiscalDate(year, month, 1)
    fy = fiscalyear.FiscalYear(fiscal_date.fiscal_year)
    return {
        'from_date': fy.start.date(),
        'to_date': fy.end.date()
    }


# function to return key for any value
def get_key(my_dict, val):
    for key, value in my_dict.items():
        if val == value:
            return key
    return None
