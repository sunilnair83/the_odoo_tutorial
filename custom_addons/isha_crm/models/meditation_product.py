from odoo import models, fields, api, exceptions, modules
from odoo.exceptions import ValidationError, UserError
from odoo import SUPERUSER_ID, _
import logging


class MeditationProduct (models.Model):
	_name = 'meditation.product'
	_description = 'Yantra and Sannidhi'
	_order = "write_date desc"
	_rec_name = 'med_prod_name'

	contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', delegate=True, auto_join=True, index=True)
	med_sl_no = fields.Integer(string='Prod SL No')
	med_prod_name = fields.Char(string='Prod Tag')
	med_product_date = fields.Date(string='Prod Date')

	@api.model_create_multi
	def create(self, vals_list):
		return super(MeditationProduct, self.with_context(model='meditation.product')).create(vals_list)

