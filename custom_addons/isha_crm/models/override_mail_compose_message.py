import logging
import traceback
from datetime import datetime

from odoo import _, models, fields
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)


class MailComposer(models.TransientModel):
	_inherit = 'mail.compose.message'

	add_sign = fields.Boolean(default=False)

	def action_send_mail(self):
		try:
			if self.body == '' and self.template_id is not None:
				self.update(self.onchange_template_id(self.template_id.id, self.composition_mode, self.model, self.res_id)['value'])
			before = datetime.now()
			self.send_mail()
			after = datetime.now()
			_logger.info("Before send: " + str(before))
			_logger.info("After send: " + str(after))
			_logger.info("Time to overall send email: " + str(after - before))

		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			print(tb_ex)
		return {'type': 'ir.actions.act_window_close', 'infos': 'mail_sent'}


	# def send_mail(self, auto_commit=False):
	# 	""" Process the wizard content and proceed with sending the related
	# 		email(s), rendering any template patterns on the fly if needed. """
	# 	notif_layout = self._context.get('custom_layout')
	# 	# Several custom layouts make use of the model description at rendering, e.g. in the
	# 	# 'View <document>' button. Some models are used for different business concepts, such as
	# 	# 'purchase.order' which is used for a RFQ and and PO. To avoid confusion, we must use a
	# 	# different wording depending on the state of the object.
	# 	# Therefore, we can set the description in the context from the beginning to avoid falling
	# 	# back on the regular display_name retrieved in '_notify_prepare_template_context'.
	# 	model_description = self._context.get('model_description')
	# 	for wizard in self:
	# 		# Duplicate attachments linked to the email.template.
	# 		# Indeed, basic mail.compose.message wizard duplicates attachments in mass
	# 		# mailing mode. But in 'single post' mode, attachments of an email template
	# 		# also have to be duplicated to avoid changing their ownership.
	# 		if wizard.attachment_ids and wizard.composition_mode != 'mass_mail' and wizard.template_id:
	# 			new_attachment_ids = []
	# 			for attachment in wizard.attachment_ids:
	# 				if attachment in wizard.template_id.attachment_ids:
	# 					new_attachment_ids.append \
	# 						(attachment.copy({'res_model': 'mail.compose.message', 'res_id': wizard.id}).id)
	# 				else:
	# 					new_attachment_ids.append(attachment.id)
	# 			new_attachment_ids.reverse()
	# 			wizard.write({'attachment_ids': [(6, 0, new_attachment_ids)]})
    #
	# 		# Mass Mailing
	# 		mass_mode = wizard.composition_mode in ('mass_mail', 'mass_post')
    #
	# 		Mail = self.env['mail.mail']
	# 		ActiveModel = self.env[wizard.model] if wizard.model and hasattr(self.env[wizard.model], 'message_post') else self.env['mail.thread']
	# 		if wizard.composition_mode == 'mass_post':
	# 			# do not send emails directly but use the queue instead
	# 			# add context key to avoid subscribing the author
	# 			ActiveModel = ActiveModel.with_context(mail_notify_force_send=False, mail_create_nosubscribe=True)
	# 		# wizard works in batch mode: [res_id] or active_ids or active_domain
	# 		if mass_mode and wizard.use_active_domain and wizard.model:
	# 			res_ids = self.env[wizard.model].search(safe_eval(wizard.active_domain)).ids
	# 		elif not self._context.get('skip_model_change',False) :
	# 			if self._context.get('active_model') == 'program.lead':
	# 				res_ids = []
	# 				for x in self.env['program.lead'].browse(self._context.get('active_ids')):
	# 					if x.contact_id_fkey.id not in res_ids:
	# 						res_ids.append(x.contact_id_fkey.id)
	# 			elif self._context.get('active_model') == 'program.attendance':
	# 				res_ids = []
	# 				for x in self.env['program.attendance'].browse(self._context.get('active_ids')):
	# 					if x.contact_id_fkey.id not in res_ids:
	# 						res_ids.append(x.contact_id_fkey.id)
	# 			elif self._context.get('active_model') == 'ieo.record':
	# 				res_ids = []
	# 				for x in self.env['ieo.record'].browse(self._context.get('active_ids')):
	# 					if x.contact_id_fkey.id not in res_ids:
	# 						res_ids.append(x.contact_id_fkey.id)
	# 			elif self._context.get('active_model') == 'sadhguru.exclusive':
	# 				res_ids = []
	# 				for x in self.env['sadhguru.exclusive'].browse(self._context.get('active_ids')):
	# 					if x.contact_id_fkey.id not in res_ids:
	# 						res_ids.append(x.contact_id_fkey.id)
	# 			elif self._context.get('active_model') == 'programs.view.orm':
	# 				res_ids = []
	# 				for x in self.env['programs.view.orm'].browse(self._context.get('active_ids')):
	# 					if x.contact_id_fkey.id not in res_ids:
	# 						res_ids.append(x.contact_id_fkey.id)
	# 			elif self._context.get('active_model') == 'rudraksha.deeksha':
	# 				res_ids = []
	# 				for x in self.env['rudraksha.deeksha'].browse(self._context.get('active_ids')):
	# 					if x.contact_id_fkey.id not in res_ids:
	# 						res_ids.append(x.contact_id_fkey.id)
	# 			elif self._context.get('active_model') == 'res.users':
	# 				res_ids = []
	# 				for x in self.env['res.users'].browse(self._context.get('active_ids')):
	# 					if x.partner_id.id not in res_ids:
	# 						res_ids.append(x.partner_id.id)
	# 			elif self._context.get('active_model') == 'iec.mega.pgm':
	# 				res_ids = []
	# 				for x in self.env['iec.mega.pgm'].browse(self._context.get('active_ids')):
	# 					if x.partner_id.id not in res_ids:
	# 						res_ids.append(x.partner_id.id)
	# 			before = datetime.now()
	# 			contacts = self.env['res.partner'].search([('id', 'in', res_ids)])
	# 			after = datetime.now()
	# 			_logger.info("####### ComposeSend SEARCH time: " + str(after - before))
    #
	# 			before = datetime.now()
	# 			if contacts:
	# 				for contact in contacts:
	# 					# if contact.email_is_bounced:
	# 					if contact.email_validity == 'bounced':
	# 						res_ids.remove(contact.id)
	# 			after = datetime.now()
	# 			_logger.info("####### ComposeSend REMOVE_BOUNCED time: " + str(after - before))
	# 			_logger.info("####### ComposeSend IDS count: " + str(len(res_ids)))
	# 		elif mass_mode and wizard.model and self._context.get('active_ids'):
	# 			res_ids = self._context['active_ids']
	# 		else:
	# 			res_ids = [wizard.res_id]
    #
    #
	# 		batch_size = int(self.env['ir.config_parameter'].sudo().get_param('mail.batch_size')) or self._batch_size
	# 		sliced_res_ids = [res_ids[i:i + batch_size] for i in range(0, len(res_ids), batch_size)]
	# 		_logger.info("####### ComposeSend BATCH size: " + str(batch_size))
    #
	# 		if wizard.composition_mode == 'mass_mail' or wizard.is_log or \
	# 				(wizard.composition_mode == 'mass_post' and not wizard.notify):  # log a note: subtype is False
	# 			subtype_id = False
	# 		elif wizard.subtype_id:
	# 			subtype_id = wizard.subtype_id.id
	# 		else:
	# 			subtype_id = self.env['ir.model.data'].xmlid_to_res_id('mail.mt_comment')
    #
	# 		for res_ids in sliced_res_ids:
	# 			batch_mails = Mail
	# 			all_mail_values = wizard.get_mail_values(res_ids)
    #
	# 			before = datetime.now()
	# 			for res_id, mail_values in all_mail_values.items():
    #
	# 				if wizard.composition_mode == 'mass_mail':
	# 					batch_mails |= Mail.create(mail_values)
	# 				else:
	# 					post_params = dict(
	# 						message_type=wizard.message_type,
	# 						subtype_id=subtype_id,
	# 						email_layout_xmlid=notif_layout,
	# 						add_sign=not bool(wizard.template_id),
	# 						mail_auto_delete=wizard.template_id.auto_delete if wizard.template_id else False,
	# 						model_description=model_description)
	# 					post_params.update(mail_values)
	# 					if ActiveModel._name == 'mail.thread':
	# 						if wizard.model:
	# 							post_params['model'] = wizard.model
	# 							post_params['res_id'] = res_id
	# 						if not ActiveModel.message_notify(**post_params):
	# 							# if message_notify returns an empty record set, no recipients where found.
	# 							raise UserError(_("No recipient found."))
	# 					else:
	# 						ActiveModel.browse(res_id).message_post(**post_params)
	# 			after = datetime.now()
	# 			_logger.info("####### ComposeSend EACH BATCH time: " + str(after - before))
	# 			# self.env.cr.commit()
    #
	# 			if wizard.composition_mode == 'mass_mail':
	# 				before = datetime.now()
	# 				batch_mails.send(auto_commit=auto_commit)
	# 				after = datetime.now()
	# 				_logger.info("####### ComposeSend SEND time: " + str(after - before))
    #
	# def render_message(self, res_ids):
	# 	if not self._context.get('skip_model_change',False) and (self.model == 'program.lead' or self.model == 'ieo.record' or self.model == 'res.users' or self.model == 'sadhguru.exclusive'\
	# 			or self.model == 'iec.mega.pgm' or self.model == 'program.attendance' or self.model == 'programs.view.orm'\
	# 			or self.model=='rudraksha.deeksha'):
	# 		self.model = 'res.partner'
	# 	return super(MailComposer, self).render_message(res_ids)

	def onchange_template_id(self, template_id, composition_mode, model, res_id):
		is_override_template_view = self.env.context.get("override_template")
		replace_html = self.env.context.get("replace_html")
		ret_value = super(MailComposer,self).onchange_template_id(template_id,composition_mode,model,res_id)
		template = self.env['mail.template'].browse(template_id)
		if not is_override_template_view and not replace_html:
			if ret_value['value']['body'] != template.body_html:
				ret_value['value']['body'] = template.body_html
		if self._context.get('default_subject',False):
			ret_value['value']['subject'] = self._context.get('default_subject')
		return ret_value

