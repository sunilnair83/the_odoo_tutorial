from odoo import models, fields, api, exceptions, modules
from odoo.exceptions import ValidationError, UserError
from odoo import SUPERUSER_ID, _


class SkillTags (models.Model):
	_name = 'isha.skill.tag'
	_description = 'Skill tags'
	_order = 'sequence'
	_parent_store = True

	name = fields.Char(string='Tag Name', required=True, translate=True)
	color = fields.Integer(string='Color Index')
	parent_id = fields.Many2one('isha.skill.tag', string='Parent Skill tags', index=True, ondelete='cascade')
	child_ids = fields.One2many('isha.skill.tag', 'parent_id', string='Child Skill tags')
	active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
	parent_path = fields.Char(index=True)
	sequence = fields.Integer('Sequence', default=10)
	partner_ids = fields.Many2many('res.partner', column1='skill_tag_ids', column2='partner_id', string='Partners')

	@api.constrains('parent_id')
	def _check_parent_id(self):
		if not self._check_recursion():
			raise ValidationError(_('You can not create recursive tags.'))


	def name_get(self):
		""" Return the categories' display name, including their direct
			parent by default.

			If ``context['partner_category_display']`` is ``'short'``, the short
			version of the category name (without the direct parent) is used.
			The default is the long version.
		"""
		if self._context.get('partner_category_display') == 'short':
			return super(SkillTags, self).name_get()

		res = []
		for program in self:
			names = []
			current = program
			while current:
				names.append(current.name)
				current = current.parent_id
			res.append((program.id, ' / '.join(reversed(names))))
		return res

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		if name:
			# Be sure name_search is symetric to name_get
			name = name.split(' / ')[-1]
			args = [('name', operator, name)] + args
		partner_pgm_tag_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
		return self.browse(partner_pgm_tag_ids).name_get()
