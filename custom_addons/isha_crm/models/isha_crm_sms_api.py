# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging

import requests

from odoo import api, models
from odoo.exceptions import UserError

logger = logging.getLogger('SMS Service')

DEFAULT_ENDPOINT = 'http://api.smscountry.com'

class IshaCrmSmsSms(models.Model):
    _inherit = 'sms.sms'

    def send(self, delete_all=False, auto_commit=False, raise_exception=False):
        return super(IshaCrmSmsSms,self).send(delete_all,auto_commit,True)

class IshaCrmSmsApi(models.AbstractModel):
    _inherit = 'sms.api'
    _description = 'Isha SMS API'

    @api.model
    def _send_sms_batch(self, messages):
        for x in messages:
            self._send_sms(x['number'],x['content'])
        return []

    @api.model
    def _send_sms(self, number, message):
        params = {
            'number': number,
            'message': message,
        }
        return self._send_smcountry_sms(params)

    @api.model
    def _send_smcountry_sms(self, data):
        url = None
        user = None
        passwd = None
        sid = None
        if len(self.env.user.centers.ids) > 0:
            region = self.env.user.centers[0].region_id
            if region.sms_provider_base_url:
                url = region.sms_provider_base_url
                user = region.sms_provider_username
                passwd = region.sms_provider_password
                sid = region.sms_provider_sender_id
                fl = region.sms_provider_fl
                gwid = region.sms_provider_gwid
            if not url:
                raise UserError(region.name + ' is not Configured to send SMS !!!')
        if url and 'nettyfish' in url:
            params = {
                "apikey": passwd,
                "clientid": user,
                "msisdn": data['number'][-10:],
                "sid": sid,
                "msg": data['message'],
                "fl": str(fl)
            }
            if gwid and gwid != 0:
                params.update({'gwid':str(gwid)})
        elif url:
            params = { 'User': user,
                      'passwd': passwd,
                      'mobilenumber': data['number'],
                      'message':data['message'],
                      'sid': sid,
                       # LNG option is needed for regional languages. It's also working for English.
                      'mtype':'LNG','DR':'Y'}
        try:
            if url:
                result = requests.get(url, params)
                logger.info('SMS result: ' + str(result.content))
        except Exception as e:
            self._raise_query_error(e)
        if 'Invalid Password' in result.text:
            self._raise_query_error('Unexpected issue. Please contact Administrator.')
        if 'ERROR:' in result.text:
            self._raise_query_error(result.text)
        return True

    def _raise_query_error(self, error):
        raise ValueError('Error with sending sms: %s' % error)