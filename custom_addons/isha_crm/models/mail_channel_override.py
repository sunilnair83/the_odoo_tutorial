from odoo import _, api, fields, models, modules, tools


class MailChannelOverride(models.Model):
    _inherit = 'mail.channel'

    # ISH-1556
    # This is causing perf issues on the res_user group assignment.
    # Commenting this function till the above jira issue is solved.
    def _subscribe_users(self):
        return []
        # for mail_channel in self:
        #     mail_channel.write({'channel_partner_ids': [(4, pid) for pid in mail_channel.mapped('group_ids').mapped('users').mapped('partner_id').ids]})
