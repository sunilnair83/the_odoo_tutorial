# -*- coding: utf-8 -*-
import csv
import json
import logging
import os
import sys
import traceback
from confluent_kafka import Producer
import sys
from odoo import models, fields, api, SUPERUSER_ID
from .. import ContactProcessor
from .. import TxnProcessor
from .. import TxnUpdater
from .. import isha_base_importer
from ..kafka_processors import IEOProcessor, EreceiptsProcessor, PRSProcessor, SystemContactProcessor, ProgramProcessor, \
    DMSDonationProcessor, SgExProcessor, SgExUTMProcessor, OnlineSatsangProcesssor, IECMegaPgmProcessor, \
    RudrakshaDeekshaProcessor, IshaVidhyaOutreachProcessor, IshaVidhyaImportProcessor, outreach_ishanga_regProcessor,AthithiContactsProcessor, \
    SPProcessor, FeedbackProcessor, PR_Events_AttendeesProcessor, SulabaContactsProcessor
from ..kafka_processors.EreceiptsEducationProcessor import EreceiptsEducationProcessor
from ..kafka_processors.EreceiptsReligiousProcessor import EreceiptsReligiousProcessor

logger = logging.getLogger('Kafka-Consumer-Logger')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setFormatter(logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(threadName)s:%(message)s"))
logger.addHandler(ch)
null = None
true = True
false = False


crm_config = isha_base_importer.Configuration('CRM')
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'country-nationality-iso2-map.csv')) as f:
    iso2 = dict(filter(None, csv.reader(f)))
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'sp_interviewer.csv')) as users:
    sp_users = dict(filter(None, csv.reader(users)))

with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'eReceipts-country-nationality-corrections.csv')) as f:
    csvReader = []
    for x in csv.reader(f):
        csvReader.append([x[0].upper(), x[1].upper()])
    correction = dict(filter(None, csvReader))



mapper_dict = {
    'CRM_KAFKA_DMS_TOPIC': ('contact', ContactProcessor.DMSContacts(), iso2, correction),
    'CRM_KAFKA_HYS_TOPIC': ('contact', ContactProcessor.HYSContacts(), iso2, correction),
    'CRM_KAFKA_PRS_TOPIC': ('contact', ContactProcessor.PRSContacts(), iso2, correction),
    'CRM_KAFKA_AL_TOPIC': ('contact', ContactProcessor.ALContact(), iso2, correction),
    'CRM_KAFKA_PGM_TOPIC': ('pgm', TxnProcessor.Program()),
    'CRM_KAFKA_PGMADV_TOPIC': ('pgm', TxnProcessor.ProgramAdv()),
    'CRM_KAFKA_PRS_TXN_TOPIC': ('prs', ContactProcessor.PRSContacts(), iso2, correction, TxnProcessor.PRSProgramTxn()),
    'CRM_KAFKA_DMS_TXN_TOPIC': ('don', TxnProcessor.DMSTxn()),
    # 'CRM_KAFKA_ER_TOPIC': ('erc', ContactProcessor.EReceipts(), iso2, correction, TxnProcessor.EreciptsTxn()),
    'CRM_KAFKA_IEO_TOPIC' : ('ieo', iso2, correction)
}


def kafka_delivery_callback(err, msg):
    if err:
        logger.error('kafka message push failed '+str(msg))
        return False
    else:
        return msg.offset()




class KafkaTopicConfig(models.Model):
    _name = 'kafka.config'
    _description = 'Kafka Configuration'

    kafka_topic = fields.Char(string='Topic', required=True)
    kafka_group = fields.Char(string='Topic Group', required=False)
    kafka_type = fields.Selection([('C','Consumer'),('P','Producer')], string='Topic Type', required=True)
    kafka_threads = fields.Integer(string='Thread Count', required=False)
    kafka_pid = fields.Char(string='Topic PIDs', required=False)
    kafka_topic_key = fields.Char(string='Topic Key', required=False)
    active = fields.Boolean(string='active', default=True)


class KafkaErrors (models.Model):

    _name = 'kafka.errors'
    _description = 'Kafka Failed Messages'

    src_msg = fields.Text(string= 'Source Message')
    error = fields.Text(string= 'Error Message')
    topic = fields.Char(string='Topic')
    active = fields.Boolean(string='active', default=True)
    processor = fields.Char(string='Processor')
    process_msg = fields.Boolean(store=False, default=False)

    @api.model
    def create(self, src):
        if src['process_msg']:
            try:
                # logger.info("Inside  Kaffka.py --CREATE$$")
                # logger.info(str(src))
                self.process_kafka_internal(src)
            except Exception as ex:
                self.env.cr.rollback()
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                src['process_msg'] = False
                src['error'] = tb_ex
                self.create(src)
            # Odoo is expecting some record to be returned in the create method.
            # so returning some random record.
            return self.env.ref('isha_crm.donation_tag_cauvery_upasaka')
        else:
            return super(KafkaErrors,self).create(src)

    # @api.model_create_multi
    # def create(self, vals_list):
    #     vals = super(KafkaErrors, self).create(vals_list)
    #
    #     subject = "Emergency : Kafka Error In Ishangam"
    #     body = 'Total Kafka Errors => %d' %(len(vals_list))
    #     self.send_error_log_mail(subject, body)
    #
    #     return vals

    # def unlink(self):
    #     self.write({'active': False})
    def single_mode(self):
        logger.debug("Kafka errors retry Single Mode......")
        self.retry_kafka_msg()

    def retry_kafka_msg(self):
        try:
            self.process_kafka_internal(self)
        except Exception as ex:
            self.env.cr.rollback()
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            self.write({'error': tb_ex})
        else:
            self.unlink()
            self.env.cr.commit()

    def bulk_mode(self):
        logger.debug("Kafka errors retry Bulk Mode......")
        kafka_errors_list = self.search([('active', '=', True)])
        for msg_dict in kafka_errors_list:
            msg_dict.retry_kafka_msg()

    def process_kafka(self, msg):
        self.process_kafka_internal(json.loads(msg))

    def process_kafka_internal(self, req):
        msg = json.loads(req['src_msg']) if type(req['src_msg']) == str else req['src_msg']
        processor = req['processor']
        if processor == 'dms-contacts':
            dms_contacts_metadata = {
                'iso2': iso2,
                'correction': correction,
                'contactFormatConverter': ContactProcessor.DMSContacts()
            }
            SystemContactProcessor.SystemContactProcessor().process_message(msg, self.sudo().env, dms_contacts_metadata)
        elif processor == 'sulaba-contact-search':
            al_pgm_metadata = {
                 'iso2': iso2,
                'correction': correction,
            }
            SulabaContactsProcessor.SulabaContactsProcessor().process_message(msg, self.sudo().env, al_pgm_metadata)
        elif processor == 'hys-contacts':
            hys_contacts_metadata = {
                'iso2': iso2,
                'correction': correction,
                'contactFormatConverter': ContactProcessor.HYSContacts()
            }
            SystemContactProcessor.SystemContactProcessor().process_message(msg, self.sudo().env, hys_contacts_metadata)
        elif processor == 'al-contacts':
            al_contacts_metadata = {
                'iso2': iso2,
                'correction': correction,
                'contactFormatConverter': ContactProcessor.ALContact()
            }
            SystemContactProcessor.SystemContactProcessor().process_message(msg, self.sudo().env, al_contacts_metadata)
        elif processor == 'al-prog':
            al_pgm_metadata = {
                'programFormatConverter': TxnProcessor.Program()
            }
            ProgramProcessor.ProgramProcessor().process_message(msg, self.sudo().env, al_pgm_metadata)
        elif processor == 'al-adv-prog':
            al_adv_pgm_metadata = {
                'programFormatConverter': TxnProcessor.ProgramAdv()
            }
            ProgramProcessor.ProgramProcessor().process_message(msg, self.sudo().env, al_adv_pgm_metadata)
        elif processor == 'prs':
            prs_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            PRSProcessor.PRSProcessor().process_message(msg, self.sudo().env, prs_metadata)
        elif processor == 'dms-donation':
            DMSDonationProcessor.DMSDonationProcessor().process_message(msg, self.sudo().env, None)
        elif processor == 'ereceipts':
            erc_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            EreceiptsProcessor.EreceiptsProcessor().process_message(msg, self.sudo().env, erc_metadata)
        elif processor == 'ereceipts-edu':
            erc_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            EreceiptsEducationProcessor().process_message(msg, self.sudo().env, erc_metadata)
        elif processor == 'ereceipts-religious':
            erc_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            EreceiptsReligiousProcessor().process_message(msg, self.sudo().env, erc_metadata)
        elif processor == 'ieo':
            ieo_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            IEOProcessor.IEOProcessor().process_message(msg, self.sudo().env, ieo_metadata)
        elif processor == 'sgex':
            sgex_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            SgExProcessor.SgExProcessor().process_message(msg, self.sudo().env, sgex_metadata)
        elif processor == 'sgex_utm':
            SgExUTMProcessor.SgExUTMProcessor().process_message(msg,self.sudo().env,{})
        elif processor == 'satsang':
            satsang_metadata = {
                'iso2': iso2,
                'correction': correction
            }
            OnlineSatsangProcesssor.OnlineSatsangProcessor().process_message(msg,self.sudo().env,satsang_metadata)
        elif processor == 'iecmegapgm':
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            IECMegaPgmProcessor.IECMegaPgmProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'rudraksha':
            # print(msg)
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            RudrakshaDeekshaProcessor.RudrakshaDeekshaProcessor().process_message(msg,self.sudo().env,metadata)
        elif processor == 'isha-vidhya':
            # print(msg)
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            IshaVidhyaOutreachProcessor.IshaVidhyaOutreachProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'isha-vidhya-import':
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            IshaVidhyaImportProcessor.IshaVidhyaImportProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'sp-registration':
            metadata = {
                'iso2': iso2,
                'correction': correction,
                'sp_users': sp_users
            }
            SPProcessor.SPProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'outreach_ishanga_reg':
            metadata = {
                'iso2': iso2,
                'correction': correction,
            }
            outreach_ishanga_regProcessor.outreach_ishanga_regProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'athithi-contacts':
            # print(msg)
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            AthithiContactsProcessor.AthithiContactsProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'feedback-contacts':
            # print(msg)
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            FeedbackProcessor.FeedbackProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'rd-delivery-updates':
            RudrakshaDeekshaProcessor.RudrakshaDeekshaDeliveryUpdates().process_message(msg, self.sudo().env, {})
        elif processor == 'event-attendees':
            metadata = {
                'iso2': iso2,
                'correction': correction
            }
            PR_Events_AttendeesProcessor.AttendeesProcessor().process_message(msg, self.sudo().env, metadata)
        elif processor == 'email_marketing_sqs':
            from .mail_activity_override import update_server
            update_server(isha_base_importer.Configuration('EMAIL_MARKETING')['EVENTS_URL'],req['src_msg'].encode('UTF-8'))
        tree_id = self.env.ref("isha_crm.kafka_errors_tree_view")
        form_id = self.env.ref("isha_crm.kafka_error_form_view")
        return {
            'type': 'ir.actions.act_window',
            'name': 'All',
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'kafka.errors',
            # if you don't want to specify form for example
            # (False, 'form') just pass False
            'views': [(tree_id.id, 'tree'), (form_id.id, 'form'), (False, 'pivot')],
            'target': 'current'
        }

    def send_error_log_mail(self, subject, body):
        su_id = self.env['res.partner'].browse(SUPERUSER_ID)

        # post the message
        mail_values = {
            'email_from': su_id.email,
            'model': None,
            'res_id': None,
            'subject': subject,
            'body_html': body,
            'attachment_ids': [],
            'auto_delete': True,
        }
        mail_values['recipient_ids'] = [(4, 3)]
        mail = self.env['mail.mail'].sudo().create(mail_values)
        return mail.send(False)


    def contact_processor(self, transformer_list):
        src_msg_dict = eval(self.src_msg)
        msgDict = transformer_list[1].getOdooFormat(src_msg_dict, transformer_list[2], transformer_list[3])
        flag_dict = self.env['res.partner'].sudo().create_internal(msgDict)
        if flag_dict:
            return True
        else:
            return False

    def pgm_processor(self, transformer_list):
        src_msg_dict = eval(self.src_msg)
        msgDict = transformer_list[1].getOdooFormat(src_msg_dict)
        pgmUpdater = TxnUpdater.ProgramAttUpdater()
        flag_dict = pgmUpdater.createPgmInternal(self.env, msgDict)
        return flag_dict['flag']

    def prs_processor(self, transformer_list):
        src_msg_dict = eval(self.src_msg)
        contactMsgDict = transformer_list[1].getOdooFormat(src_msg_dict, transformer_list[2], transformer_list[3])
        prs_dict = transformer_list[4].getOdooFormat(src_msg_dict)
        pgmUpdater = TxnUpdater.ProgramAttUpdater()

        contact = self.env['res.partner'].sudo().create_internal(contactMsgDict)
        prs_dict['contact_id_fkey'] = contact[0].id
        prs_dict['guid'] = contact[0].guid

        flag_dict = pgmUpdater.createPgmInternal(self.env, prs_dict)
        return flag_dict['flag']

    def dms_procssor(self, transformer_list):
        src_msg_dict = eval(self.src_msg)
        msgDict = transformer_list[1].getOdooFormat(src_msg_dict)
        donationUpdater = TxnUpdater.DonationsUpdater()
        flag_dict = donationUpdater.createDonationInternal(self.env, msgDict)
        return flag_dict['flag']

    # 'CRM_KAFKA_ER_TOPIC': ('erc', ContactProcessor.EReceipts(), iso2, correction, TxnProcessor.EreciptsTxn()),

    def erc_processor(self, transformer_list):
        src_msg_dict = eval(self.src_msg)
        contactMsgDict = transformer_list[1].getOdooFormat(src_msg_dict['payload'], transformer_list[2], transformer_list[3])
        erc_dict = transformer_list[4].getOdooFormat(src_msg_dict)
        ercUpdater = TxnUpdater.DonationsUpdater()
        contact_flag = ercUpdater.contactCreateFlag(self.env, erc_dict, contactMsgDict)

        if contact_flag:
            contact = self.env['res.partner'].sudo().create_internal(contactMsgDict)
            erc_dict['contact_id_fkey'] = contact[0].id
            erc_dict['guid'] = contact[0].guid

        flag_dict = ercUpdater.createDonationInternal(self.env, erc_dict)
        return flag_dict['flag']

    def ioe_processor(self, transformer_list):
        src_msg_dict = eval(self.src_msg)
        src_msg_dict = src_msg_dict['payload']
        ieo_processor = IEOProcessor.IEOProcessor()
        msg_dict = ieo_processor.getGoldenFormat(src_msg_dict, transformer_list[1], transformer_list[2])
        txn_dict = ieo_processor.getGoldenFormatForTxn(src_msg_dict)
        contact = self.env['res.partner'].sudo().create_internal(msg_dict)
        txn_dict['contact_id_fkey'] = contact[0].id
        flag_dict = ieo_processor.createPgmInternal(self.env, txn_dict)
        contact._compute_ieo_date()
        return flag_dict['flag']

    def assign_tag(self):
        topic = 'ishangam_async_tags'
        tag_id = 70
        msg = {}
        if self._context.get('active_model') == 'res.partner':
            msg = {'res_partner_id': self._context.get('active_id'), 'category_id': [(4, tag_id)]}
        if self._context.get('active_model') == 'program.lead':
            msg = {'pgm_lead_id': self._context.get('active_id'), 'category_id': [(4, tag_id)]}

        status = self.kafka_producer(topic, str(msg))
        if status:
            return self.env["sh.message.wizard"].get_popup('Tag assignment Successfully Pushed to queue')
        else:
            return self.env["sh.message.wizard"].get_popup('Failed to push Tag assignment to queue. Plz retry after some time')


    def kafka_producer(self, topic, msg):
        """
        Method to publish a message to given topic
        :param topic: topic name
        :param msg: actual string message to be published
        :return: True if successful else False
        """
        kafka_config = isha_base_importer.Configuration('KAFKA')
        p = Producer({
            'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
            'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
            'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
            'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
        })

        try:
            p.produce(topic, msg, callback=kafka_delivery_callback)
        except BufferError:
            logger.info("Kafka message producing failed -- "+str(topic)+ ' -- '+ str(msg))
            return False
        p.poll(0)
        p.flush()
        return True
