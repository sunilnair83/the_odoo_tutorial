from odoo import models, fields, api, exceptions, _, SUPERUSER_ID
from odoo.exceptions import UserError, ValidationError
from odoo.addons.portal.controllers import portal
from odoo.http import content_disposition, Controller, request, route
import json
from .. import GoldenContactAdv

class PortalInherit(portal.CustomerPortal):

	MANDATORY_BILLING_FIELDS = ["phone_country_code", "phone","dob", "gender", "country_id","nationality_id", "zipcode","qualification_ids","skill_tag_ids"]
	OPTIONAL_BILLING_FIELDS = ["name", "email", "state_id", "vat", "company_name","vol_duration",'prof_status','companies',
							   "ashram_vol_days","portfolio_link","practice_support","selectable_fields",
							   "occupation","center_id","rm_vol_ids","proj_supp_ids","lang_ids","work_experience",
							   "work_company_name","work_designation","twitter_handle","linkedin_id","instagram_id","facebook_id","website_published","website_description",
							   "whatsapp_number","whatsapp_country_code","shoonya_willingness","bsp_willingness"] #"pgm_tag_ids",
	MANY2MANY_FIELDS = ["rm_vol_ids","proj_supp_ids","skill_tag_ids","lang_ids","qualification_ids"] # "pgm_tag_ids",

	@route(['/my/account'], type='http', auth='user', website=True)
	def account(self, redirect=None, **post):
		# result = super(PortalInherit,self).account(redirect,**post)
		# return result
		values = self._prepare_portal_layout_values()
		partner = request.env.user.partner_id.sudo()
		values.update({
			'error': {},
			'error_message': [],
		})

		if post and request.httprequest.method == 'POST':
			if 'image_1920' in post or 'clear_avatar' in post:
				self.update_profile_image(**post)
				post.pop('image_1920',None)
				post.pop('clear_avatar',None)
			error, error_message = self.details_form_validate(post)
			values.update({'error': error, 'error_message': error_message})
			values.update(post)
			if not error:
				values = {key: post[key] for key in self.MANDATORY_BILLING_FIELDS}
				values.update({key: post[key] for key in self.OPTIONAL_BILLING_FIELDS if key in post})
				values.update({key:[(6,0, [int(y)for y in post[key].split(',') if y.isdigit()])] for key in self.MANY2MANY_FIELDS if key in post})
				values.update({'country_id': int(values.pop('country_id', 0))})
				values.update({'zip': values.pop('zipcode', '')})
				if values.get('state_id') == '':
					values.update({'state_id': False})
				if values.get('center_id'):
					values.update({'center_id': int(values.get('center_id'))})
				if partner.is_meditator:
					values.update({'forum_status': 'verified'})
				else:
					values.update({'forum_status': 'unverified'})
				values.update({'portal_verified': True})
				goldenContact = GoldenContactAdv.GoldenContactAdv()
				# Phone
				values.update({'phone_is_valid':True,'phone_is_verified':True})
				phone_dict = goldenContact.getPhonesDict(partner, values)
				values.update(phone_dict)
				# Email
				email_dict = goldenContact.getEmailsDict(partner, values)
				values.update(email_dict)
				partner.write(values)
				if values.get('website_published'):
					request.env.user.sudo().write({'website_published':True})
				else:
					request.env.user.sudo().write({'website_published': False})
				if redirect:
					return request.redirect(redirect)
				return request.redirect('/my/home')
		website_description = partner.website_description
		if website_description:
			website_description = website_description.replace('<p>','')
			website_description = website_description.replace('</p>','')
			if len(website_description)== 0:
				website_description = False
		countries = request.env['res.country'].sudo().search([('id','=',104)])|request.env['res.country'].sudo().search([('id','!=',104)])
		states = request.env['res.country.state'].sudo().search([])
		centers = request.env['isha.center'].sudo().search([])
		skills = [dict(id=tag.id, name=tag.name) for tag in partner.skill_tag_ids]
		skills = json.dumps(skills)
		vols= [dict(id=tag.id, name=tag.name) for tag in partner.rm_vol_ids]
		vols = json.dumps(vols)
		projs= [dict(id=tag.id, name=tag.name) for tag in partner.proj_supp_ids]
		projs=json.dumps(projs)
		pgms = [dict(id=tag.id, name=tag.name) for tag in partner.pgm_tag_ids]
		pgms = json.dumps(pgms)
		langs = [dict(id=tag.id, name=tag.name) for tag in partner.lang_ids]
		langs = json.dumps(langs)
		quals = [dict(id=tag.id, name=tag.name) for tag in partner.qualification_ids]
		quals = json.dumps(quals)

		values.update({
			'website_description':website_description,
			'website_published':request.env.user.website_published,
			'quals':quals,
			'portal_verified':partner.portal_verified,
			'partner': partner,
			'countries': countries,
			'states': states,
			'centers':centers,
			'skills':skills,
			'pgms':pgms,
			'vols':vols,
			'projs':projs,
			'langs':langs,
			'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),
			'redirect': redirect,
			'page_name': 'my_details',
		})

		response = request.render("portal.portal_my_details", values)
		response.headers['X-Frame-Options'] = 'DENY'
		return response


class VolunteerRemote (models.Model):
	_name = 'isha.remote.volunteer'
	_description = 'Remote Volunteering Options'
	_order = 'sequence'

	name = fields.Char(string='Volunteer options', required=True, translate=True)
	color = fields.Integer(string='Color Index')
	active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
	sequence = fields.Integer('Sequence', default=10)
	partner_ids = fields.Many2many('res.partner', column1='rm_vol_id', column2='partner_id', string='Partners')

class ProjectSupport (models.Model):
	_name = 'isha.project.support'
	_description = 'Support for Projects'
	_order = 'sequence'

	name = fields.Char(string='Projects', required=True, translate=True)
	color = fields.Integer(string='Color Index')
	active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
	sequence = fields.Integer('Sequence', default=10)
	partner_ids = fields.Many2many('res.partner', column1='project_id', column2='partner_id', string='Partners')

class LanguageTag (models.Model):
	_name = 'isha.language.tag'
	_description = 'Language tags'
	_order = 'sequence'
	_parent_store = True

	name = fields.Char(string='Language Name', required=True, translate=True)
	color = fields.Integer(string='Color Index')
	parent_id = fields.Many2one('isha.language.tag', string='Parent Lang tags', index=True, ondelete='cascade')
	child_ids = fields.One2many('isha.language.tag', 'parent_id', string='Child Lang tags')
	active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
	parent_path = fields.Char(index=True)
	sequence = fields.Integer('Sequence', default=10)
	partner_ids = fields.Many2many('res.partner', column1='lang_id', column2='partner_id', string='Partners')

	@api.constrains('parent_id')
	def _check_parent_id(self):
		if not self._check_recursion():
			raise ValidationError(_('You can not create recursive tags.'))


	def name_get(self):
		""" Return the categories' display name, including their direct
			parent by default.

			If ``context['partner_category_display']`` is ``'short'``, the short
			version of the category name (without the direct parent) is used.
			The default is the long version.
		"""
		if self._context.get('partner_category_display') == 'short':
			return super(LanguageTag, self).name_get()

		res = []
		for lang in self:
			names = []
			current = lang
			while current:
				names.append(current.name)
				current = current.parent_id
			res.append((lang.id, ' / '.join(reversed(names))))
		return res

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		if name:
			# Be sure name_search is symetric to name_get
			name = name.split(' / ')[-1]
			args = [('name', operator, name)] + args
		partner_lang_tag_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
		return self.browse(partner_lang_tag_ids).name_get()


class QualificationTag (models.Model):
	_name = 'isha.qualification.tag'
	_description = 'Qualification tags'
	_order = 'sequence'
	_parent_store = True

	name = fields.Char(string='Qualification', required=True, translate=True)
	color = fields.Integer(string='Color Index')
	parent_id = fields.Many2one('isha.qualification.tag', string='Parent Qualification tags', index=True, ondelete='cascade')
	child_ids = fields.One2many('isha.qualification.tag', 'parent_id', string='Child Qualification tags')
	active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
	parent_path = fields.Char(index=True)
	sequence = fields.Integer('Sequence', default=10)
	partner_ids = fields.Many2many('res.partner', column1='qual_id', column2='partner_id', string='Partners')

	@api.constrains('parent_id')
	def _check_parent_id(self):
		if not self._check_recursion():
			raise ValidationError(_('You can not create recursive tags.'))


	def name_get(self):
		""" Return the categories' display name, including their direct
			parent by default.

			If ``context['partner_category_display']`` is ``'short'``, the short
			version of the category name (without the direct parent) is used.
			The default is the long version.
		"""
		if self._context.get('partner_category_display') == 'short':
			return super(QualificationTag, self).name_get()

		res = []
		for lang in self:
			names = []
			current = lang
			while current:
				names.append(current.name)
				current = current.parent_id
			res.append((lang.id, ' / '.join(reversed(names))))
		return res

	@api.model
	def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
		args = args or []
		if name:
			# Be sure name_search is symetric to name_get
			name = name.split(' / ')[-1]
			args = [('name', operator, name)] + args
		partner_qual_tag_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
		return self.browse(partner_qual_tag_ids).name_get()
