import logging
import uuid
import json

import phonenumbers
from werkzeug import urls

from .constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from odoo import models, api, fields, tools
from odoo.exceptions import UserError
from odoo.http import request

_logger = logging.getLogger(__name__)


class WhatsAppCampaign(models.Model):
    _name = 'whatsapp.campaign'
    _description = 'Campaign for Whats app'

    def _get_default_access_token(self):
        return str(uuid.uuid4())

    def _read_group_states(self, values, domain, order):
        selection = self.env['whatsapp.campaign'].fields_get(allfields=['state'])['state']['selection']
        return [s[0] for s in selection]

    name = fields.Char(string='Campaign Name', required=True)
    active = fields.Boolean(string="Active", default=True)
    access_token = fields.Char('Access Token', default=lambda self: self._get_default_access_token(), copy=False)
    public_url = fields.Char("Unique link", copy=False)
    analyser_url = fields.Char("Analyser link", copy=False)
    campaign_outdated = fields.Boolean(string='Outdated Campaign',default=False)

    res_partner_ids = fields.Text(string='Partner ids')
    res_partner_phones = fields.Text(string='Phone CSV')

    message_mode = fields.Selection([('static','Static Message'),('dynamic','Personalized Message')],
                                    string='Message Mode', default='static')
    model_id = fields.Many2one('ir.model', string='Applies For')
    dynamic_fields = fields.Many2many('ir.model.fields', 'whatsapp_campaign_dynamic_fields', string='Dynamic Fields')

    # add image field for the campaign.
    image = fields.Image(string='Campaign Image', max_width=256)

    @api.onchange('model_id')
    def get_model_fields_domain(self):
        self.dynamic_fields = False
        self.place_holder = ''
        if self.model_id:
            self.res_partner_ids = ''
            self.res_partner_phones = ''
            self.total_partner_ids = 0
            self.valid_phones = 0
            self.invalid_phones = 0

            return {'domain':{'dynamic_fields':[('model_id','=',self.model_id.id)]}}

    @api.onchange('dynamic_fields')
    def generate_placeholder(self):
        if self.dynamic_fields:
            text_to_display = []
            for x in self.dynamic_fields:
                text_to_display.append(x.name + " -> " + "${object." + x.name+"}")
            self.place_holder = '<br/>'.join(text_to_display)
        else:
            self.place_holder = ''

    place_holder = fields.Text(string='Dynamic Place Holders')
    whatsapp_message = fields.Text(string='WhatsApp Message', required=True)

    total_partner_ids = fields.Integer(string='Total contacts selected')
    valid_phones = fields.Integer(sting='Valid Phones', copy=False)
    invalid_phones = fields.Integer(sting='InValid Phones', copy=False)

    # Split campaign
    parent_id = fields.Many2one('whatsapp.campaign',string='Source Campaign')
    child_ids = fields.One2many('whatsapp.campaign','parent_id',string='Split campaigns')

    campaign_mode = fields.Selection([('full', 'Whole List'),
                                     ('split', 'Split List by count'),
                                     ('split_center','Split List by Center')],string='Campaign Mode', default='full')
    split_count = fields.Integer(string='Split Count')
    split_done = fields.Boolean(string='Split Done')


    def _get_center_domain(self):
        return [('id','in',self.env.user.centers.ids)]
    center_id = fields.Many2one('isha.center', string='Center',domain=_get_center_domain,
                                default=lambda self: self.env.user.centers.ids[0] if self.env.user.centers else False, index=True)
    region = fields.Many2one('isha.region', string='Region', related='center_id.region_id', store=True)

    state = fields.Selection(
        string="Campaign Stage",
        selection=[
            ('draft', 'Draft'),
            ('open', 'In Progress'),
            ('split', 'Split-Up'),
            ('closed', 'Completed'),
            ('analysed', 'Analysed'),
        ], default='draft', required=True, copy=False,
        group_expand='_read_group_states'
    )

    messages_sent = fields.Integer(string='Messages successfully Sent', copy=False)
    messages_errored = fields.Integer(string='Messages Errored Out', copy=False)
    error_phone_list = fields.Text(string='Error List', copy=False)
    parse_failed_list = fields.Text(string='Parse Failed List', copy=False)
    phone_statuses_updated = fields.Boolean()

    analyse_list = fields.One2many('whatsapp.campaign.analyser', 'campaign_id', string='Analyser Data')


    def whatsapp_update_validity(self):
        campaigns = self.search([('state', 'in', ['closed', 'analysed']), ('phone_statuses_updated', '=', False)])
        if campaigns:
            for campaign in campaigns:
                error_phone_nums = []
                if campaign.error_phone_list:
                    errors_list = json.loads(campaign.error_phone_list)
                    if errors_list:
                        for error in errors_list:
                            error_phone_nums += list(error.keys())

                all_phones = []
                if campaign.res_partner_phones:
                    all_phones_list = json.loads(campaign.res_partner_phones)
                    if all_phones_list:
                        for phone in all_phones_list:
                            all_phones.append(phone.get('wa_number'))

                valid_phone_nums = set(all_phones) - set(error_phone_nums)
                valid_national_numbers = self.get_national_numbers(valid_phone_nums)['national_numbers']
                if valid_national_numbers:
                    contacts = self.env['res.partner'].search([('whatsapp_number', 'in', valid_national_numbers)])
                    contacts.whatsapp_validity = 'valid'

                error_phone_result = self.get_national_numbers(error_phone_nums)
                invalid_national_numbers = error_phone_result['national_numbers']
                if invalid_national_numbers:
                    contacts = self.env['res.partner'].search([('whatsapp_number', 'in', invalid_national_numbers)])
                    contacts.whatsapp_validity = 'invalid'
                campaign.write({
                    'parse_failed_list': json.dumps(error_phone_result['parse_failed_list']),
                    'phone_statuses_updated': True
                })
                self.env.cr.commit()

    def get_national_numbers(self, phone_nums):
        national_numbers = []
        parse_failed_list = []
        if phone_nums:
            for phone in phone_nums:
                if not phone.startswith('+'):
                    phone = '+' + phone
                try:
                    parsed_phone = phonenumbers.parse(phone)
                except Exception:
                    parse_failed_list.append(phone)
                    continue
                national_numbers.append(parsed_phone.national_number)
        return {
            'national_numbers': national_numbers,
            'parse_failed_list': parse_failed_list
        }

    def action_allocate_contacts(self):
        if self.env.user.has_group('isha_crm.group_isha_vidhya') or self.env.user.has_group('isha_crm.group_isha_vidhya_read_only'):
            action = self.env.ref('isha_crm.isha_vidhya_donor_view_action').read()[0]
        else:
            action = self.env.ref('isha_crm.all_center_action_server').read()[0]
        request.session['whatsapp_campaign_id'] = self.id
        return action

    def split_campaign_trigger(self):
        res_partner_dict = eval(self.res_partner_ids)
        if self.campaign_mode == 'split':
            res_partner_ids = res_partner_dict.keys()
            campaigns_list = []
            for batch in tools.split_every(self.split_count,res_partner_ids):
                temp_dict = dict()
                for x in batch:
                    temp_dict[int(x)] = res_partner_dict[int(x)]
                campaigns_list.append({
                    'name': self.name + ' split ' + str(len(campaigns_list) + 1),
                    'active': True,
                    'res_partner_ids': str(temp_dict),
                    'whatsapp_message': self.whatsapp_message,
                    'total_partner_ids': len(batch),
                    'center_id':self.center_id.id,
                    'parent_id':self.id,
                    'message_mode':self.message_mode,
                    'model_id':self.model_id.id,
                    'dynamic_fields': [(6,0,[x for x in self.dynamic_fields.ids])],
                    'place_holder':self.place_holder
                })
            campaigns = self.create(campaigns_list)
            self.env.cr.commit()
            campaigns.in_progress_trigger()
        elif self.campaign_mode == 'split_center':
            res_partner_ids = eval(self.res_partner_ids)
            res_partner_ids = res_partner_ids.keys()
            ids_str = ','.join([str(x) for x in res_partner_ids])
            center_ids = ','.join([str(x) for x in self.env.user.centers.ids])
            campaigns_list = []
            base_query = """select string_agg(id::text,',') as res_ids,center_id 
                            from res_partner where center_id in (%s) and id in (%s) 
                            group by center_id""" % (center_ids,ids_str)
            self.env.cr.execute(base_query)
            for row in self.env.cr.dictfetchall():
                batch = row['res_ids'].split(',')
                temp_dict = dict()
                for x in batch:
                    temp_dict[int(x)] = res_partner_dict[int(x)]
                campaigns_list.append({
                    'name': self.name + ' split ' + self.env['isha.center'].browse(int(row['center_id'])).name,
                    'active': True,
                    'res_partner_ids': str(temp_dict),
                    'whatsapp_message': self.whatsapp_message,
                    'total_partner_ids': len(batch),
                    'center_id':int(row['center_id']),
                    'parent_id':self.id,
                    'message_mode': self.message_mode,
                    'model_id': self.model_id.id,
                    'dynamic_fields': [(6,0,[x for x in self.dynamic_fields.ids])],
                    'place_holder': self.place_holder
                })
            campaigns = self.create(campaigns_list)
            self.env.cr.commit()

        self.write({'state':'split','split_done':True})

    def in_progress_trigger(self):
        """ Computes a public URL for the campaign """
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        # Do dedup across the campaigns specifically for the split campaigns
        dedup_set = set()
        for campaign in self:
            if not campaign.res_partner_ids:
                raise UserError('No contacts added to the campaign. Plz add contacts and then generate the token...')
            vals = dict()

            valid_partners = eval(campaign.res_partner_ids)
            dynamic_content = {}
            if campaign.message_mode == 'dynamic':
                txn_list = self.env[campaign.model_id.model].sudo().search_read([('id','in',list(valid_partners.values()))],
                                                                                fields=[x.name for x in campaign.dynamic_fields])
                for x in txn_list:
                    dynamic_content[x['id']] = x

            base_query = "select id,whatsapp_country_code,whatsapp_number,dnd_wa_ext from res_partner where id in (%s)"% ','.join([str(x) for x in valid_partners])
            self.env.cr.execute(base_query)
            partner_recs = self.env.cr.dictfetchall()
            phone_list = list()
            for rec in partner_recs:
                if rec['whatsapp_country_code'] and rec['whatsapp_number'] and rec['dnd_wa_ext'] is not True and \
                        rec['whatsapp_country_code'] + rec['whatsapp_number'] not in dedup_set:
                    wa_number = rec['whatsapp_country_code'] + rec['whatsapp_number']
                    temp_dict = {'wa_number': wa_number}
                    if campaign.message_mode == 'dynamic':
                        temp_dict.update(dynamic_content[valid_partners[rec['id']]])
                    phone_list.append(temp_dict)
                    dedup_set.add(wa_number)
            vals['res_partner_phones'] = json.dumps(phone_list)
            vals['valid_phones'] = len(phone_list)
            vals['public_url'] = urls.url_join(base_url,
                                               "whatsapp_campaign?caller_token=%s" % (campaign.access_token)).replace(
                'http://', 'https://')
            vals['state'] = 'open'
            campaign.write(vals)

    def generate_analyser_token(self):
        """ Computes a public URL for the campaign """
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for campaign in self:
            vals = dict()
            request_token = str(int(campaign.create_date.timestamp()))
            vals['analyser_url'] = urls.url_join(base_url,
                                                 "whatsapp_campaign/analyse?caller_token=%s&request_token=%s" %
                                                 (request_token + '-' + campaign.access_token, request_token)
                                                 ).replace('http://', 'https://')
            campaign.write(vals)


class WhatsAppCampaignAnalyser(models.Model):
    _name = 'whatsapp.campaign.analyser'
    _description = 'Analyser for Whats app campaign'
    _order = 'create_date desc'
    _rec_name = 'wa_number'

    campaign_id = fields.Many2one('whatsapp.campaign', string='Campaign')
    dnd_prediction = fields.Char(string='DND Prediction')
    wa_number = fields.Char(string='Whatsapp Number')
    wa_number_parsed = fields.Char(string='Number parsed')
    replies = fields.Text(string='Replies')

    def mark_dnd(self):
        dnd_wa_numbers = []
        for rec in self:
            try:
                wa_number = str(phonenumbers.parse('+' + rec.wa_number).national_number)
            except:
                wa_number = rec.wa_number[2:]
            _logger.info("Whatsapp extn dnd trigger for -> " + wa_number)
            contact_recs = self.env['res.partner'].sudo().search(['|', '|', '|', '|',
                                                                  ('phone', '=', wa_number),
                                                                  ('phone2', '=', wa_number),
                                                                  ('phone3', '=', wa_number),
                                                                  ('phone4', '=', wa_number),
                                                                  ('whatsapp_number', '=', wa_number)])
            contact_recs.sudo().write({
                'dnd_wa_ext': True
            })
            rec.sudo().write({'wa_number_parsed': wa_number})
            dnd_wa_numbers.append(rec.wa_number)

        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = 'Successfully Flagged DND for ' + ', '.join(dnd_wa_numbers)
        return {
            'name': 'Success',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }


class WhatsappCampaignContactAllocation(models.TransientModel):
    """ wizard to assign contacts to whatsapp campaign """

    _name = 'whatsapp.campaign.contact.allocation'
    _description = 'Contacts for whatsapp campaign'

    campaign_id = fields.Many2one('whatsapp.campaign', string='Campaign', required=True)
    selected_count = fields.Integer(string='Selected count', readonly=True)
    selection_mode = fields.Selection([('selected_contact','Only Selected Contacts'),('selected_filter', 'All Filtered Contacts')],
                                      string='Selection Mode')

    @api.onchange('selection_mode')
    def get_ids_for_selection(self):
        for x in self:
            if self.selection_mode:
                x.selected_count = len(x.get_valid_partners().keys())

    def get_valid_partners(self):
        """
        This method gets the valid partners after excluding the influencers, dnd contacts,
        restricted contacts and star marked contacts
        :return: dict res_partner to txn_id mapping
        """
        partner_recs = []
        if not self.selection_mode or self.selection_mode == 'selected_contact':
            base_ids = ','.join([str(x) for x in self._context.get('active_ids')])
            where_clause_params = []
        else:
            table_name = self.env[self.env.context['active_model']]._table
            from_clause, where_clause, where_clause_params = self.env[self.env.context['active_model']].get_query_params(self.env.context['active_domain'])
            where_str = where_clause and (" WHERE %s" % where_clause) or ''
            base_query = 'SELECT "%s".id FROM %s %s'
            base_ids = base_query % (table_name,from_clause, where_str)

        center_ids = ','.join([str(x) for x in self.env.user.centers.ids])
        charitable = self.user_has_groups(CHARITABLE_GROUPS)
        religious = self.user_has_groups(RELIGIOUS_GROUPS)
        charitable_clause = ' (rp.has_charitable_txn = true) '
        religious_clause = ' (rp.has_religious_txn = true) '

        suffix_array = []
        if charitable:
            suffix_array.append(charitable_clause)
        if religious:
            suffix_array.append(religious_clause)
        suffix_clause = ''
        if suffix_array:
            suffix_clause = ' and (' + ' or '.join(suffix_array) + ')'

        suffix_clause += "and (rp.whatsapp_validity is null or rp.whatsapp_validity != 'invalid')"

        suffix_clause = "and (rp.whatsapp_validity is null or rp.whatsapp_validity != 'invalid')"
        if self._context.get('active_model') == 'call_campaign.user_input':
            base_query = '''SELECT ccui.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,
            rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM res_partner rp, call_campaign_user_input ccui 
            where ccui.partner_id = rp.id and ccui.id in (%s) /* and (rp.no_marketing is false or rp.no_marketing is null) */ 
            and rp.center_id in (%s) %s''' % (base_ids,center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'call_campaign.user_input_line':
             base_query = '''SELECT ccuil.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,
             rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM res_partner rp, call_campaign_user_input ccui, call_campaign_user_input_line ccuil  
             where ccui.partner_id = rp.id and ccuil.user_input_id = ccui.id and ccuil.id in (%s) 
             /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
             self.env.cr.execute(base_query, where_clause_params)
             partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'program.lead':
            base_query = '''SELECT pl.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                                 rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM 
                                 res_partner rp, program_lead pl where rp.id = pl.contact_id_fkey and pl.id in (%s) and 
                                 (rp.no_marketing is false or rp.no_marketing is null) and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
#suri: check
        #
        elif self._context.get('active_model') == 'program.attendance':
            base_query = '''SELECT pa.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, program_attendance pa where pa.contact_id_fkey = rp.id and pa.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'ieo.record':
            base_query = '''SELECT ir.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, ieo_record ir where ir.contact_id_fkey = rp.id and ir.contact_id_fkey is not null 
            and ir.id in (%s) /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'event.registration':
            base_query = '''SELECT er.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, event_registration er where er.partner_id = rp.id and er.id in (%s) and 
            (rp.no_marketing is false or rp.no_marketing is null) and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'res.users':
            base_query = '''SELECT ru.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,
            rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, res_users ru where ru.partner_id = rp.id and ru.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'iec.mega.pgm':
            base_query = '''SELECT imp.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, iec_mega_pgm imp where imp.partner_id = rp.id and imp.partner_id is not null and imp.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'res.partner':
            base_query = '''SELECT rp.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM res_partner rp where rp.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'partner.import.temp.wizard':
            base_query = '''SELECT pit.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                        rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id
                 FROM res_partner rp, partner_import_temp pit
                 where pit.partner_id = rp.id and pit.id in (%s) 
                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'sadhguru.exclusive':
            base_query = '''SELECT se.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                             rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                FROM res_partner rp, sadhguru_exclusive se where se.contact_id_fkey = rp.id and se.id in (%s) 
                                /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'programs.view.orm':
            base_query = '''SELECT pvo.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                FROM res_partner rp, programs_view_orm pvo where pvo.contact_id_fkey = rp.id and pvo.id in (%s) 
                                /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'rudraksha.deeksha':
            base_query = '''SELECT rd.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                   FROM res_partner rp, rudraksha_deeksha rd where rd.contact_id_fkey = rp.id and rd.id in (%s) 
                                   /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'covid.support':
            base_query = '''SELECT cs.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                 FROM res_partner rp, covid_support cs where cs.contact_id_fkey = rp.id and cs.id in (%s) 
                                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'completion.feedback':
            base_query = '''SELECT cf.id as txn_id, rp.id, rp.whatsapp_country_code, rp.dnd_wa_ext,rp.deceased,rp.whatsapp_number,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                 FROM res_partner rp, completion_feedback cf where cf.contact_id_fkey = rp.id and cf.id in (%s) 
                                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()

        # valid_partners is a dict with txn_id to res_partner_id mapping.
        # {'txn_id':'res_partner_id', ...}
        valid_partners = dict()
        # DND check on res_partner
        for x in partner_recs:
            if x['dnd_wa_ext'] is not True and x['whatsapp_number'] and x['whatsapp_country_code'] and x['deceased'] is not True and x['starmark_category_id'] is None:
                # if self.env.user.has_group('isha_crm.group_sangam'):
                #     if x.is_caca_donor is not True or (
                #             x.is_caca_donor is True and str(x.last_txn_date) != '1900-01-01'):
                #             valid_partners.append(x.id)
                #suri: check
                #
                if (not x['influencer_type']) and (x['is_caca_donor'] is not True or (
                        x['is_caca_donor'] is True and str(x['last_txn_date']) != '1900-01-01')):
                    valid_partners[x['id']] = x['txn_id']
        return valid_partners

    @api.model
    def default_get(self, fields):
        result = super(WhatsappCampaignContactAllocation, self).default_get(fields)

        campaign_id = request.session.pop('whatsapp_campaign_id', None)
        if campaign_id:
            result['campaign_id'] = campaign_id
        return result

    def action_assign_contacts(self):
        if self.campaign_id.model_id.model and self.campaign_id.model_id.model != self._context.get('active_model'):
            raise UserError('The campaign is configured with Personalized message(Dynamic Placeholder). '
                            ' And the current data Source is Incompatible with the configuration.'
                            ' Plz choose records only from ' + self.campaign_id.model_id.name)

        valid_partners = self.get_valid_partners()
        if valid_partners:
            self.env['whatsapp.campaign'].sudo().browse(self.campaign_id.id).write(
                {
                    'total_partner_ids': len(valid_partners.keys()),
                    'res_partner_ids': str(valid_partners)
                }
            )
            return {
                "type": "ir.actions.act_window",
                "res_model": "whatsapp.campaign",
                "views": [[False, "form"]],
                "res_id": self.campaign_id.id
            }
        else:
            return self.env["sh.message.wizard"].get_popup(
                'No valid records found for the selection. Plz check the validity of the selected records')

    def action_append_contacts(self):
        if self.campaign_id.model_id.model and self.campaign_id.model_id.model != self._context.get('active_model'):
            raise UserError('The campaign is configured with Personalized message(Dynamic Placeholder). '
                            ' And the current data Source is Incompatible with the configuration.'
                            ' Plz choose records only from ' + self.campaign_id.model_id.name)
        valid_partner = self.get_valid_partners()
        if valid_partner:
            existing_list = eval(self.campaign_id.res_partner_ids) if self.campaign_id.res_partner_ids else dict()
            total_list = valid_partner
            total_list.update(existing_list)
            self.env['whatsapp.campaign'].sudo().browse(self.campaign_id.id).write(
                {
                    'total_partner_ids': len(total_list.keys()),
                    'res_partner_ids': str(total_list)
                }
            )
            return {
                "type": "ir.actions.act_window",
                "res_model": "whatsapp.campaign",
                "views": [[False, "form"]],
                "res_id": self.campaign_id.id
            }
        else:
            return self.env["sh.message.wizard"].get_popup(
                'No valid records found for the selection. Plz check the validity of the selected records')
