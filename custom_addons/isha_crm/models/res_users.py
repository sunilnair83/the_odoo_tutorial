from lxml import etree
from lxml.builder import E

from odoo import models, fields, api, _
from odoo.addons.base.models.res_users import name_boolean_group, name_selection_groups
from odoo.exceptions import UserError


class ResUsers(models.Model):
	_inherit = 'res.users'

	centers = fields.Many2many('isha.center', column1='user_id', column2='center_id', string='Centers Admin Of')
	regions = fields.Many2many('isha.region', column1='user_id', column2='region_id', string='Regions Admin Of')
	task_rating_aggregate = fields.Float(string='Tasks Rating')
	special_portal_access = fields.Boolean(string='Special Portal Access')
	yoga_veera_type = fields.Selection([('potential', 'Potential'), ('suport', 'Support'), ('conducting', 'Conducting')], string='Yoga Veera Type')
	nda_signed_off = fields.Boolean(string="NDA signed off")
	mailing_teams = fields.Many2many('isha.mailing.team', column1='user_id', column2='team_id', string='Mailing Teams')

	@api.model_create_multi
	def create(self, vals_list):
		rec_list = super(ResUsers, self).create(vals_list)
		for val in rec_list:
			self.send_mail_from_template(val)
		return rec_list

	@api.model
	def create(self, values):
		return_value = super(ResUsers, self).create(values)
		return_value.send_mail_from_template(values)
		return return_value


	def write(self, values):
		return_value = super(ResUsers, self).write(values)
		self.send_mail_from_template(values)
		return return_value

	def send_mail_from_template(self, values):
		template_browse = self.env.ref('isha_crm.group_access_granted_mail_template')
		if template_browse:
			group_names = []
			for g in self.groups_id:
				if values.get('in_group_%d' % g.id) == True:
					if self.env['res.groups'].sudo().browse(g.id).send_notification_email:
						group_names.append(g.full_name)

			if group_names != []:
				self.env.context = dict(self.env.context)
				self.env.context.update({
					'user_new_groups': group_names,
				})

				template_browse.send_mail(self.id, force_send=True)
				return True


	def unable_to_verify(self):
		if self.is_meditator == True or self.forum_status == 'verified':
			raise UserError('Sorry you cannot change status once it is verified!.')
		self.write({'forum_status' : 'unable_to_verify'})

	def cancel_registration(self):
		if self.is_meditator == True or self.forum_status == 'verified':
			raise UserError('Sorry you cannot change status once it is verified!.')
		self.write({'forum_status' : 'cancelled'})

	@api.model
	def fields_get(self, allfields=None, attributes=None):
		res = super(ResUsers, self).fields_get(allfields, attributes=attributes)
		if self._context.get('restrict_fields_get_list'):
			not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
			for field in not_selectable_fields:
				res[field]['selectable'] = False # to hide in Add Custom filter view
				res[field]['sortable'] = False # to hide in group by view
				res[field]['exportable'] = False # to hide in export list
				res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
		return res

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		res = super(ResUsers, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
														submenu=submenu)
		if view_type =='form' and self._context.get('custom_access_rights'):
			self.env['res.groups']._custom_access_rights(res)
		return res

	@api.depends('name', 'phone')
	def name_get(self):
		result = []
		if self.env.user.has_group('base.group_portal'):
			# For forums app we are not supposed to reveal the phone numbers
			for rec in self:
				name = rec.name
				name += ' ('+rec.center_id.name+')' if rec.center_id else ''
				result.append((rec.id, name))
		else:
			for rec in self:
				name = ', '.join(filter(None, [rec.name, rec.phone]))
				result.append((rec.id, name))
		return result

	@api.onchange('regions')
	def _onchange_regions(self):
		centers = []
		for rec in self.regions:
			centers += rec.centers.ids
		self['centers'] = [(6, 0, centers)]

	@api.model
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		context = self._context or {}
		if context.get('center_scope'):
			domain += [('center_id','in', self.env.user.centers.ids)]
		elif context.get('centers_scope'):
			domain += [('centers', 'in', self.env.user.centers.ids)]
		return super(ResUsers, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
													  orderby=orderby, lazy=lazy)

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if context.get('center_scope'):
			args += [('center_id','in', self.env.user.centers.ids)]
		elif context.get('centers_scope'):
			args += [('centers', 'in', self.env.user.centers.ids)]
		# default - do not show any results
		if len(args) == 0:
			return []
		return super(ResUsers, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)

	def _rank_changed(self):
		"""
			override the res.user method, so that no action is taken
		"""
		nothing = ''
		return nothing

class ResGroupsInherited(models.Model):
	_inherit = "res.groups"
	_order = 'category_id,name'
	send_notification_email = fields.Boolean(string='Send Notification Email', readonly=True, default=False)


	@api.model
	def _custom_access_rights(self, res):
		""" show only the groups from allowed_access in context
		"""
		allowed_access = self._context.get('allowed_access')
		domain=['|']
		domain.append(('category_id','in',[self.env.ref(y).id for y in allowed_access['category_id']]))
		domain.append(('id','in',[self.env.ref(y).id for y in allowed_access['id']]))

		permitted_access_groups = self.env['res.groups'].search(domain)
		set_permitted_access_groups = set(permitted_access_groups)
		view = res
		if view:
			group_no_one = self.env.ref('base.group_no_one')
			group_employee = self.env.ref('base.group_user')
			xml1, xml2, xml3 = [], [], []
			xml_by_category = {}


			user_type_field_name = ''
			user_type_readonly = str({})
			sorted_tuples = sorted(self.get_groups_by_application(),
								   key=lambda t: t[0].xml_id != 'base.module_category_user_type')
			for app, kind, gs, category_name in sorted_tuples:  # we process the user type first
				set_gs = set(gs)
				attrs = {}
				# hide groups in categories 'Hidden' and 'Extra' (except for group_no_one)
				if app.xml_id in self._get_hidden_extra_categories():
					attrs['groups'] = 'base.group_no_one'

				# User type (employee, portal or public) is a separated group. This is the only 'selection'
				# group of res.groups without implied groups (with each other).
				if app.xml_id == 'base.module_category_user_type' and 'base.module_category_user_type' in allowed_access['category_id']:
					# application name with a selection field
					field_name = name_selection_groups(gs.ids)
					user_type_field_name = field_name
					user_type_readonly = str({'readonly': [(user_type_field_name, '!=', group_employee.id)]})
					attrs['widget'] = 'radio'
					attrs['groups'] = 'base.group_no_one'
					xml1.append(E.separator(string='User Type', colspan="2", groups='base.group_no_one'))
					xml1.append(E.field(name=field_name, **attrs))
					xml1.append(E.newline())

				elif kind == 'selection':
					if (set_gs.issubset(set_permitted_access_groups)):
						# application name with a selection field
						field_name = name_selection_groups(gs.ids)
						attrs['attrs'] = user_type_readonly
						if category_name not in xml_by_category:
							xml_by_category[category_name] = []
							xml_by_category[category_name].append(E.newline())
						xml_by_category[category_name].append(E.field(name=field_name, **attrs))
						xml_by_category[category_name].append(E.newline())

				else:
					# application separator with boolean fields
					if len(set_gs.intersection(set_permitted_access_groups))>0:
						app_name = app.name or 'Other'
						xml3.append(E.separator(string=app_name, colspan="4", **attrs))
						attrs['attrs'] = user_type_readonly

						for g in gs:
							field_name = name_boolean_group(g.id)
							if g.id in permitted_access_groups.ids:
								xml3.append(E.field(name=field_name, **attrs))

			xml3.append({'class': "o_label_nowrap"})
			if user_type_field_name:
				user_type_attrs = {'invisible': [(user_type_field_name, '!=', group_employee.id)]}
			else:
				user_type_attrs = {}

			for xml_cat in sorted(xml_by_category.keys(), key=lambda it: it[0]):
				xml_cat_name = xml_cat[1]
				master_category_name = (_(xml_cat_name))
				xml2.append(E.group(*(xml_by_category[xml_cat]), col="2", string=master_category_name))

			xml = E.page(
				E.group(*(xml1), col="2"),
				E.group(*(xml2), col="2", attrs=str(user_type_attrs)),
				E.group(*(xml3), col="4", attrs=str(user_type_attrs)), name="access_rights", string='Access Rights')
			xml.addprevious(etree.Comment("GENERATED AUTOMATICALLY BY GROUPS"))

			form_arch = etree.fromstring(view['arch'])
			notebook_element = form_arch.xpath("//notebook")[0]
			page_element = notebook_element.xpath("//page[@name=\'access_rights\']")[0]
			notebook_element.replace(page_element, xml)

			res['arch'] = etree.tostring(form_arch, pretty_print=True, encoding="unicode")
			return
