from odoo import models,fields


class IrAttachment(models.Model):
	_inherit = 'ir.attachment'

	access_token = fields.Char('Access Token', groups="base.group_user,base.group_portal")

