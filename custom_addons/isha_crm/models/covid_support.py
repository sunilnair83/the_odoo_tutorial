import logging

from .constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from odoo import models, fields, api
_logger = logging.getLogger(__name__)


class CovidSupport (models.Model):
    _name = "covid.support"
    _description = 'Covid Support'
    _order = "write_date desc"

    contact_id_fkey = fields.Many2one('res.partner', string='Contact', delegate=True, auto_join=True,index=True)
    local_trans_id = fields.Char(string='Local Trans Id')
    system_id = fields.Integer(string='System Id')
    email = fields.Char(string='Registered Email')
    reg_date_time = fields.Datetime(string='Reg Date')
    support_options = fields.Many2many("covid.support.tags",string='Opted Support')
    willing_whatsapp_group = fields.Boolean(string='Willing Whatsapp Group')
    remarks = fields.Text(string='Remarks')
    nurturing_volunteer = fields.Many2one('res.partner', string='Nurturing Volunteer',index=True)


    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id', 'in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            domain = rel_char_domain + domain
        return super(CovidSupport, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)


    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(CovidSupport, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False  # to hide in Add Custom filter view
                res[field]['sortable'] = False  # to hide in group by view
                # res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False  # to hide in 'Select Columns' filter in tree views
        return res


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('center_scope'):
            args += [('center_id', 'in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            args = rel_char_domain + args

        # default - do not show any results
        if len(args) == 0:
            return []
        return super(CovidSupport, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)

class CovidSupportOptions(models.Model):
    _name = "covid.support.tags"
    _description = 'Covid Support Options'

    name = fields.Char(string='Name')
    color = fields.Integer(string='Color')
    active = fields.Boolean(string='Active')