import imaplib
import email
import logging
import json
from odoo.http import request




# username = "sns.notices@ishafoundation.org"
# password = "icyxibtnjxifrnkz"
# imap_host = "imap.gmail.com"

class MailUtil:
    """
    Contains methods for interacting with a mail server over IMAP.

    """
    def __init__(self, imap_host, mail_user_name, mail_password):
        self.imap_host = imap_host
        self.mail_user_name = mail_user_name
        self.mail_password = mail_password
        self.imap_connection = self.__connect_to_mail_server()


    def __connect_to_mail_server(self):
        """
        this method is supposed to be called at the time of instantion of the class.
        Creates an IMAP connection to server with SSL.


        :return: imaplib.IMAP4_SSL -> connection to host
        """
        login_status = ""

        try:
            imap_connection = imaplib.IMAP4_SSL(self.imap_host)
            # authenticate
            login_status = imap_connection.login(self.mail_user_name, self.mail_password)
        except:
            raise Exception("Login Failed with status message: {}".format(login_status))
        return imap_connection

    def get_mails_by_search_criteria(self, search_criteria: str, mail_box_folder: str = "INBOX"):
        """

        :param search_criteria:
        :param mail_box_folder:
        :return: byte string containing mail_ids that satisfies search_criteria
        """
        self.imap_connection.select(mail_box_folder, readonly=True)
        response, mail_ids = self.imap_connection.search(None, search_criteria)

        if response != "OK":
            raise Exception("Searching failed for criteria: {}".format(search_criteria))
        else:
            return [int(mail_id) for mail_id in mail_ids[0].split(b" ")]
            #return mail_ids[0]


    def fetch_mail_body_by_ids(self, mail_ids: [int]):
        """

        :param mail_ids: byte string containing mail_ids to be fetched
        :return:
        """
        mail_ids = ','.join([str(mail_id) for mail_id in mail_ids])
        status, mail_bodies = self.imap_connection.fetch(mail_ids, '(RFC822.HEADER BODY.PEEK[1])')
        print("Status: {} \n mail_bodies: {}".format(status, mail_bodies))

    def search_mail_with_criteria(self, search_criteria, mail_box_folder: str = "INBOX"):
        self.imap_connection.select(mail_box_folder, readonly=True)
        status, mail_uids = self.imap_connection.uid("search", None, search_criteria)
        if status != "OK":
            raise Exception(f"Searching failed for criteria: {search_criteria}")
        else:
            return mail_uids


    def fetch_mail_bodies(self, mail_uids):
        """

        :param mail_uids: list containing batches of mail_uids as byte string
        :return: list containing mail bodies as strings
        """
        if mail_uids == [b'']:
            return []
        mail_bodies = []
        # todo : limit the number of mails to be fetched per call
        for mail_uid_batch in mail_uids:
            # mail_uid_batch_ints = [int(mail_id) for mail_id in mail_uid_batch.split(b" ")]
            # mail_uid_batch_str = ','.join([str(mail_id) for mail_id in mail_uid_batch_ints])
            mail_uid_batch_str = self.__get_mail_uid_batch_str(mail_uid_batch)
            try:
                status, response = self.imap_connection.uid("fetch", mail_uid_batch_str, '(BODY[1])')
                logging.info(f"Fetched mail_ids {mail_uid_batch_str} successfully.")
                for entry in response:
                    if (isinstance(entry, tuple)):
                        mail_body = email.message_from_bytes(entry[1])
                        mail_bodies.append(str(mail_body))
            except:
                raise Exception(f"Error while fetching mail body for mail_uid_batch : {mail_uid_batch_str}")
        return mail_bodies

    def mark_seen(self, mail_uids):
        if mail_uids == [b'']:
            return "NO_UNSEEN_LEFT"
        for mail_uid_batch in mail_uids:
            mail_uid_batch_str = self.__get_mail_uid_batch_str(mail_uid_batch)
            try:
                self.imap_connection.select("INBOX",readonly=False)
                self.imap_connection.uid("STORE", mail_uid_batch_str, "+FLAGS",'\Seen')
                logging.info(f"Successfully marked mail_uids : {mail_uid_batch_str} as SEEN.")
            except:

                raise Exception(f"Error while setting SEEN flag for  for mail_uid_batch : {mail_uid_batch_str}")
        return "OK"



    def __get_mail_uid_batch_str(self,mail_uid_batch: bytes):
        mail_uid_batch_ints = [int(mail_id) for mail_id in mail_uid_batch.split(b" ")]
        mail_uid_batch_str = ','.join([str(mail_id) for mail_id in mail_uid_batch_ints])
        return mail_uid_batch_str



def mail_poller():
    """
    Polls the mail server repeatedly till there are no more email ids left which satisfies the search criteria

    """
    username = 'username'
    password = 'password'
    imap_host = "imap.gmail.com"


    mail_uids = []

    while mail_uids != [b'']:

        search_criteria: str = '(UNSEEN SUBJECT "Amazon SES Email Event Notification" FROM "Ishangam-test")'
        mail_util = MailUtil(imap_host, username, password)
        mail_uids = mail_util.search_mail_with_criteria(search_criteria)
        mail_bodies = mail_util.fetch_mail_bodies(mail_uids)
        process_mail_notifications(mail_bodies)
        mail_util.mark_seen(mail_uids)

    print("Processed all messages")



# def process_mail_notifications(mail_bodies: []):
#     for mail_body in mail_bodies:
#         mail_body_json = json.loads(mail_body)
#         message_node = json.loads(mail_body_json["Message"])
#         notification_type = message_node['eventType']
#         if(notification_type.upper() == "OPEN"):
#             print("It's OPEN")
#             messageId = message_node["mail"]["commonHeaders"]["messageId"]
#             recipient = message_node["mail"]["commonHeaders"]["to"]
#             open_ts = message_node["open"]["timestamp"]
#             open_ts_without_tz = open_ts.split(".")[0].replace("T", " ")
#
#             print(f"messageId : {messageId}, recipient : {recipient}, open_ts : {open_ts},open_ts_without_tz : {open_ts_without_tz} ")
#
#         elif(notification_type.upper() == "CLICK"):
#             print("It's CLICK")
#             messageId = message_node["mail"]["commonHeaders"]["messageId"]
#             recipient = message_node["mail"]["commonHeaders"]["to"]
#             click_ts = message_node["click"]["timestamp"]
#             click_ts_without_tz = click_ts.split(".")[0].replace("T"," ")
#
#             print(f"messageId : {messageId}, recipient : {recipient}, click_ts : {click_ts}, click_ts_without_tz: {click_ts_without_tz}")
#
#         else:
#             logging.error(f"Getting non - OPEN / CLICK notifications of type: {notification_type} \n Mail Body : {mail_body}")
#             raise Exception(f"Getting non - OPEN / CLICK notifications of type: {notification_type}")

def process_mail_notifications(mail_bodies: []):
    for mail_body in mail_bodies:
        mail_body_json = json.loads(mail_body)
        message_node = json.loads(mail_body_json["Message"])
        notification_type = message_node['eventType']
        if(notification_type.upper() == "OPEN"):
            track_mail_opened_event(mail_body_json)
        elif(notification_type.upper() == "CLICK"):
            print("It's CLICK")
            track_mail_link_clicked_event(mail_body_json)
        else:
            logging.error(f"Getting non - OPEN / CLICK notifications of type: {notification_type} \n Mail Body : {mail_body}")
            raise Exception(f"Getting non - OPEN / CLICK notifications of type: {notification_type}")



def track_mail_opened_event(mail_body_json: {}):
    message_node = json.loads(mail_body_json["Message"])
    messageId = message_node["mail"]["commonHeaders"]["messageId"]
    recipient = message_node["mail"]["commonHeaders"]["to"]
    open_ts = message_node["open"]["timestamp"]
    open_ts_without_tz = open_ts.split(".")[0].replace("T", " ")
    mailingTrace = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId), ('email', '=', recipient)])

    print(mailingTrace)
    if mailingTrace:
        mailingTrace.write(
            {'opened': open_ts_without_tz})


def track_mail_link_clicked_event(mail_body_json: {}):
    message_node = json.loads(mail_body_json["Message"])
    messageId = message_node["mail"]["commonHeaders"]["messageId"]
    recipient = message_node["mail"]["commonHeaders"]["to"]
    click_ts = message_node["click"]["timestamp"]
    click_ts_without_tz = click_ts.split(".")[0].replace("T", " ")
    mailingTrace = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])

    print(mailingTrace)
    if mailingTrace:
        mailingTrace.write(
            {' clicked': click_ts_without_tz})







