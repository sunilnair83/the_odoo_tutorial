import json
from datetime import datetime, date, timedelta

import ContactProcessor


class EUAdvProg2ProgAttProcessor:
    system_id = 31

    def removePlus(self, code):
        if code and type(code) == str and len(code)>1 and code[0]=='+':
            return code[1:]
        else:
            return code

    def getDateParsed(self, str_date):
        try:
            date_parsed = datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S') if str_date else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            date_parsed = None
        return date_parsed

    def getContactFormat(self, env, src_msg, iso2, correction):
        gc = env['gc']
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        name = src_msg['Name']
        gender = ContactProcessor.getgender(src_msg['Gender'])
        phone = src_msg['Phone Country Code'] if src_msg['Phone Country Code'] else ''
        phone += src_msg['Phone'] if src_msg['Phone'] else ''
        if phone == '':
            phone = None

        phone2 = src_msg['Phone2 Country Code'] if src_msg['Phone2 Country Code'] else ''
        phone2 += src_msg['Phone2'] if src_msg['Phone2'] else ''
        if phone2 == '':
            phone2 = None

        whatsapp_country_code = src_msg['WhatsApp Country Code'] if src_msg['WhatsApp Country Code'] else None
        whatsapp_number = src_msg['WhatsApp Number'] if src_msg['WhatsApp Number'] else None

        occuption = src_msg['Occupation'] if src_msg['Occupation'] else None

        country_id = None
        if src_msg['Country ISO2'] in gc.countryModel:
            country_id = gc.countryModel[src_msg['Country ISO2']]['id']

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['local_trans_id'],
            'street': src_msg['Street'],
            'active': True,
            'name': name, 'display_name': name,
            'city': src_msg['City'], 'state': src_msg['State Text'], 'country': src_msg['Country ISO2'], 'zip': src_msg['Zip'],
            'country_id': country_id,
            'center_id': src_msg['Center'],
            'prof_dr': None,
            'phone': phone, 'phone2': phone2, 'phone3': None, 'phone4': None, 'whatsapp_number': whatsapp_number,
            'whatsapp_country_code': whatsapp_country_code,
            'email': src_msg['Email'],
            'email2': None, 'email3': None,
            'street2': None,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': gender, 'occupation': occuption,
            'marital_status': None, 'dob': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None
        }

        registration_date = None
        if src_msg['Registration date']:
            registration_date = datetime.strptime(src_msg['Registration date'], '%d/%m/%Y').strftime("%Y-%m-%d") + ' 00:00:00'
        elif src_msg['Year']:
            registration_date = src_msg['Year'] + '-01-01 00:00:00'


        odooRec['local_modified_date'] = registration_date

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getTxnFormat(self,src_msg):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        try:
            sd = self.getFirstSunday(src_msg['rsvp_month_year']) if 'rsvp_month_year' in src_msg and src_msg['rsvp_month_year'] else None
            if not sd:
                sd = datetime.strptime(src_msg['start_date'], '%d-%m-%Y') if src_msg['start_date'] else None
                sd = sd.strftime('%Y-%m-%d') if sd else None
        except:
            sd = None

        try:
            ar = self.parseDateTime(src_msg['arrival_date'])
        except:
            ar = None

        try:
            dp = self.parseDateTime(src_msg['departure_date'])
        except:
            dp = None

        start_date = None
        if src_msg['Start Date']:
            start_date = datetime.strptime(src_msg['Start Date'], '%d/%m/%Y').strftime("%Y-%m-%d")
        elif src_msg['Year']:
            start_date = src_msg['Year'] + '-01-01 00:00:00'

        end_date = None
        if src_msg['End Date']:
            end_date = datetime.strptime(src_msg['End Date'], '%d/%m/%Y').strftime("%Y-%m-%d")

        pgm_schedule_info = {
            'schedule_name': src_msg['Program Name'],
            'center_name': src_msg['Program Center Name'],
            'country': src_msg['Program Country ISO2'],
            'start_date': start_date,
            'end_date': end_date,
            'teacher_name': src_msg['Teacher Name'],
            'teacher_id': None,
            'center_id': None,
            'location': None,
            'remarks': None,
            'program_id': None,
            'arrival_date': None,
            'departure_date': None,
            'program_schedule_guid': None,
            'localScheduleId': None
        }

        registration_date = None
        if src_msg['Registration date']:
            registration_date = datetime.strptime(src_msg['Registration date'], '%d/%m/%Y').strftime(
                "%Y-%m-%d") + ' 00:00:00'
        elif src_msg['Year']:
            registration_date = src_msg['Year'] + '-01-01 00:00:00'

        odooRec = {
            'system_id': self.system_id,
            'active': True,
            'local_program_type_id': src_msg['Local Program Type Id'],
            'center_name': src_msg['Program Center Name'],
            'reg_date_time': registration_date,
            'reg_status': src_msg['Registration Status'],
            'start_date': start_date,
            'local_trans_id': src_msg['local_trans_id'],
            'local_contact_id': src_msg['local_trans_id'],
            'program_schedule_info': pgm_schedule_info,
            'program_name': src_msg['Program Name'],
            'bay_name': None,
            'arrival_date': None,
            'departure_date': None,
            'teacher_name': src_msg['Teacher Name'],
        }

        return odooRec

    def process_message(self, src_msg_dict, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        try:
            # logger.debug(msg)
            print('src: ' + json.dumps(src_msg_dict))
            contactMsgDict = self.getContactFormat(env, src_msg_dict, iso2, correction)
            txn_dict = self.getTxnFormat(src_msg_dict)

            last_txn_date = self.getLastTxnDate(txn_dict['reg_date_time'])
            if last_txn_date:
                contactMsgDict['last_txn_date'] = last_txn_date

            gc = env['gc']
            contact_flag = gc.createOrUpdate({}, contactMsgDict)
            contact = contact_flag['rec']
            txn_dict['contact_id_fkey'] = contact['id']

            txn_dict['guid'] = contact['guid']

            self.createPgmInternal(env, txn_dict)
            return True
        finally:
            pass

    def createPgmInternal(self, ENV, src_rec):
        gc = ENV['gc']
        pgm_mst = gc.ishaPgmTypeMaster.get(src_rec['local_program_type_id'] + str(src_rec['system_id']), None)
        # pgm_mst = self.get_mapping_pgm_master(ENV,src_rec)
        if pgm_mst:
            src_rec.update({'pgm_type_master_id': pgm_mst})
        else:
            dummy = gc.ishaPgmTypeMaster['Placeholder']
            src_rec.update({'pgm_type_master_id': dummy})
        if src_rec['reg_status']:
            status = gc.regStatusMap.get(src_rec['reg_status'].lower() + str(src_rec['system_id']), None)
            if not status:
                status = src_rec['reg_status']
            src_rec.update({'reg_status': status})

        self.createOrUpdate(src_rec, ENV)

    def createOrUpdate(self, src_rec, env_model):
        gc = env_model['gc']
        existing_rec = None
        query = 'select * from program_attendance where local_trans_id = \'' + src_rec[
            'local_trans_id'] + '\' and system_id = \'' + str(src_rec['system_id']) + '\';'
        gc.db_cursor.execute(query)
        temp = gc.db_cursor.fetchall()
        for row in temp:
            temp_dict = dict()
            for x in row:
                temp_dict[x] = row[x]
            existing_rec = temp_dict
        if existing_rec:
            existing_rec.update(src_rec)
            env_model['txn_updater_class'].write(existing_rec)
            return True

        record = env_model['txn_updater_class'].create(src_rec)
        return record

    def parseDateTime(self, date_str):
        try:
            date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M') if date_str else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            try:
                date_parsed = datetime.strptime(date_str, '%Y-%m-%d') if date_str else None
                date_parsed = str(date_parsed) if date_parsed else None
            except:
                date_parsed = None

        return date_parsed

    def getDateTime(self, date_str):
        try:
            date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S') - timedelta(hours=5, minutes=30) if date_str else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            try:
                date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M') - timedelta(hours=5, minutes=30) if date_str else None
                date_parsed = str(date_parsed) if date_parsed else None
            except:
                date_parsed = None

        return date_parsed

    def getEventAttDatetime(self, src_dt):
        try:
            return str(datetime.strptime(src_dt, '%d-%m-%Y %H:%M:%S') - timedelta(hours=5, minutes=30))
        except:
            return None
    def getDateFromDateTime(self, date_time):
        try:
            return datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        except:
            return None

    def getLastTxnDate(self, datetime):
        last_txn_date = None
        try:
            last_txn_date =  datetime.split(' ')[0]
        except:
            pass
        return last_txn_date

    def getFirstSunday(self, rsvp_month_year):
        if rsvp_month_year and type(rsvp_month_year)==str:
            # rsvp_month_year comes as mm-yy we need to return the first sunday for the corresponding month year
            try:
                dt_split = rsvp_month_year.split('-')
                dt = date(int('20' + dt_split[1]), int(dt_split[0]), 1)
                dt += timedelta(days=6 - dt.weekday())
                return str(dt)
            except Exception as ex:
                return False

