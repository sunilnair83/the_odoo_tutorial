
import csv

# email
# webinar name
# start_date date value only

# find attendance for a registration, mark status as ATTENDED and remove the attendance record

fields = ['System ID', 'local_trans_id', 'Name', 'Gender', 'Date of Birth', 'Email', 'Alt Email', 'Phone Country Code',
'Phone', 'Phone2 Country Code', 'Phone2', 'Phone3 Country Code', 'Phone3', 'WhatsApp Country Code', 'WhatsApp Number',
'Occupation', 'Street', 'City', 'State Text', 'Zip', 'Country ISO2', 'Center', 'program_name', 'webinar_event_name',
'webinar_event_date', 'reg_date_time', 'reg_status', 'event_attendance_date', 'Language']

atts_map = {}
with open('eu_webinar_att.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        key = row['Email']+row['webinar_event_name'] + row['webinar_event_date']
        if key in atts_map:
            continue
        atts_map[key] = {'Name': row['Name'], 'Email': row['Email'], 'Phone Country Code': row['Phone Country Code'], 'Phone': row['Phone'], 'City': row['City'], 'Zip': row['Zip'], 'Country ISO2': row['Country ISO2'], 'webinar_event_name': row['webinar_event_name'], 'webinar_event_date': row['webinar_event_date'], 'Language': row['Language']}

write_obj = open('eu_webinar_merged_file.csv', 'w', newline='')
output_file = csv.DictWriter(write_obj, fieldnames=fields)
output_file.writeheader()
regs_map = {}
with open('eu_webinar_reg.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        key = row['Email']+row['webinar_event_name'] + row['webinar_event_date']
        if key in regs_map:
            continue
        else:
            regs_map[key] = {'exists': 1}
        if key in atts_map:
            row['reg_status'] = 'ATTENDED'
            row['event_attendance_date'] = atts_map[key]['webinar_event_date']
            row['Language'] = atts_map[key]['Language']
            atts_map.pop(key, None)
        output_file.writerow(row)


att_fields = ['Name', 'Email', 'Phone Country Code', 'Phone', 'City', 'Zip', 'Country ISO2', 'webinar_event_name',
'webinar_event_date', 'Language']

write_obj = open('eu_webinar_atts_without_regs.csv', 'w', newline='')
output_file = csv.DictWriter(write_obj, fieldnames=att_fields)
output_file.writeheader()

for rec in atts_map.values():
    output_file.writerow(rec)
