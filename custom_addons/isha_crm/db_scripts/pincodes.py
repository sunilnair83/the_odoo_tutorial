import json

import requests

pincodes_file = open('pincode-list.txt', 'r')
output_file = open('output.txt', 'w')
output_file.writelines(['Pincode, City, District, State\n'])

pincodes = pincodes_file.readlines()

for pincode in pincodes:

        print('pin: ' + pincode)
        url = 'https://1.rome.api.flipkart.com/api/1/address/pincode?pincode=' + pincode.rstrip()
        res = requests.get(url, headers = {
                "Connection": 'keep-alive',
                'X-User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36 FKUA/website/42/website/Desktop',
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Origin': 'https://www.flipkart.com',
                'Sec-Fetch-Site': 'same-site',
                'Sec-Fetch-Mode': 'cors',
                'Sec-Fetch-Dest': 'empty',
                'Referer': 'https://www.flipkart.com/',
                'Accept-Language': 'en-US,en;q=0.9,fr;q=0.8,ru;q=0.7',
                'Cookie': 'T=TI160075231934500203792336673291533706431226250400852672407364687054; AMCVS_17EB401053DAF4840A490D4C%40AdobeOrg=1; AMCV_17EB401053DAF4840A490D4C%40AdobeOrg=-227196251%7CMCIDTS%7C18640%7CMCMID%7C49179095296105233173466742281592986221%7CMCAAMLH-1611053515%7C12%7CMCAAMB-1611053515%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1610455915s%7CNONE%7CMCAID%7CNONE; s_cc=true; SN=VIB9CC72A24C904653AB88DD377D5DFFA2.TOKB54816D3C5C54DA1B7CD704C447E71DC.1610454238.LI; gpv_pn=My%20Addresses; gpv_pn_t=My%20Addresses; s_sq=flipkart-prd%3D%2526pid%253DMy%252520Addresses%2526pidt%253D1%2526oid%253Dfunction%252528%252529%25257B%25257D%2526oidt%253D2%2526ot%253DSUBMIT; S=d1t13Pwk/PhQ6P3snP3J0GGlOP1Rfx4NBCOCpdYV+AZU25kcJDtaR3DsPFe6TjP/dYvHiFzGT1/SD6FBcYd4XFF0Ikw=='
        })

        print(res.text)
        pins = json.loads(res.text)
        try:
                pin = pins['RESPONSE']['addressComponents'][0]['pincode']
                city = pins['RESPONSE']['addressComponents'][0]['city']
                district = pins['RESPONSE']['addressComponents'][0]['district']
                state = pins['RESPONSE']['addressComponents'][0]['state']
                print(pins)
                print(pin)
                print(city)
                print(district)
                print(state)
                print()
                output_file.writelines([str(pin) + ', ' + str(city) + ', ' + str(district) + ', ' + str(state) + '\n'])
                output_file.flush()
        except:
                continue


pincodes_file.close()
output_file.close()
