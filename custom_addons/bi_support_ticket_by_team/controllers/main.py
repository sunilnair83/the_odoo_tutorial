# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
##############################################################################

from odoo import SUPERUSER_ID, http, tools, _
from odoo.http import request
from datetime import datetime
import base64
import io

class HelpdeskTeams(http.Controller):

    @http.route(['/helpdesk_team'], type='http', auth="public", website=True)
    def teams(self, page=0, category=None, search='', **post):
        support_team_ids = request.env['support.team'].search([])
        values = {'category_id': support_team_ids}
        
        return request.render("bi_support_ticket_by_team.bi_helpdesk_teams_team_website",values)


    @http.route(['/helpdesk/team/view/<model("support.team"):team>'],type='http',auth="public",website=True)
    def project_category_view(self, team, category='', search='', **post):
        context = dict(request.env.context or {})
        team_obj = request.env['support.team']
        context.update(active_id=team.id)
        team_data_list = []
        team_data = team_obj.sudo().search([('id', '=', team.id)])
        team = request.env['support.team'].search([])

        for items in team_data:
            team_data_list.append(items)
            
        return http.request.render('bi_support_ticket_by_team.team_main_view',{
            'team_data_list': team_data_list,'teams':team
        })