# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

class AutoResponse(models.TransientModel):
    _name = "maintanance.ticketrequest"

    user_id = fields.Many2one('res.users','Responsible User',required=True)
    maintanance_team_id = fields.Many2one('maintenance.team','Maintenance Team',required=True)
    maintanance_equipment_id = fields.Many2one('maintenance.equipment','Maintenance Equipment')
    maintanance_employee_id = fields.Many2one('hr.employee','Employee',required=True)

    def create_maintanance_ticket(self):
        maintenance_obj = self.env['maintenance.request']
        ticket = self.env['support.ticket'].browse(self.env.context.get('active_id'))
        for maintenance in self:
            vals = {
            'name' : ticket.name,
            'user_id' : maintenance.user_id.id,
            'maintenance_team_id' : maintenance.maintanance_team_id.id,
            'equipment_id' : maintenance.maintanance_equipment_id.id,
            'employee_id' : maintenance.maintanance_employee_id.id,
            'helpdesk_ticket_id' : ticket.id,
            }
        maintenance_obj.create(vals)
        return {
            'name': 'Ticket Maintenance Request',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'res_model': 'maintenance.request',
            'domain': [('helpdesk_ticket_id', '=', ticket.id)],
        }


class HelpdeskSupportInherit(models.Model):
    _inherit = 'support.ticket'

    def _maintenance_count(self):
        for ticket in self:
            maintenance_ids = self.env['maintenance.request'].search([('helpdesk_ticket_id','=',ticket.id)])
            ticket.maintenance_count = len(maintenance_ids)

    maintenance_count = fields.Integer('Maintenance Count',compute="_maintenance_count",default=0.0)

    def maintanance_view(self):
        self.ensure_one()
        return {
            'name': 'Ticket Maintenance Request',
            'type': 'ir.actions.act_window',
            'view_mode': 'kanban,form',
            'res_model': 'maintenance.request',
            'domain': [('helpdesk_ticket_id', '=', self.id)],
        }

class HelpdeskMaintanance(models.Model):
    _inherit = 'maintenance.request'

    helpdesk_ticket_id = fields.Many2one('support.ticket',string="Helpdesk Ticket")