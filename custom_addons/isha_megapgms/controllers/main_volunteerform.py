import hashlib
import hmac
import json
import logging
import traceback

import requests

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)

# Api to get full profile data
def sso_get_full_data(sso_id):
    #request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + sso_id
    request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_PMS_ENDPOINT1') + "full-profiles/" + sso_id
    authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
    headers = {
        "Authorization": "Bearer " + authreq,
        'X-legal-entity': 'IF',
        'x-user': sso_id
    }
    profile = requests.get(request_url, headers=headers)
    return profile.json()

def get_fullprofileresponse_DTO():
    return {
        "profileId": "",
        "basicProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "profileId": "",
            "email": "",
            "firstName": "",
            "lastName": "",
            "phone": {
                "countryCode": "",
                "number": ""
            },
            "gender": "",
            "dob": "",
            "countryOfResidence": ""
        },
        "extendedProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "nationality": "",
            "profileId": ""
        },
        "documents": [],
        "attendedPrograms": [],
        "profileSettingsConfig": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "nameLocked": False,
            "isPhoneVerified": False,
            "phoneVerificationDate": None,
            "profileId": ""
        },
        "addresses": [
            {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "townVillageDistrict": "",
                "city": "",
                "state": "",
                "country": "",
                "pincode": "",
                "profileId": ""
            }
        ]
    }

# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
    # request.env['satsang.tempdebuglog'].sudo().create({
    #     'logtext': "sso_get_user_profile config " + url
    # })
    return requests.post(url, data=dp)

def hash_hmac(data,secret):
    data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature

def get_is_meditator_or_ieo(name=None, phone=None, email=None, check_meditator=False, check_ieo=False,
                            check_shoonya=False, check_samyama=False):
    try:
        if check_meditator:
            check_meditator = eval(check_meditator)
        if check_ieo:
            check_ieo = eval(check_ieo)
        if check_shoonya:
            check_shoonya = eval(check_shoonya)
        if check_samyama:
            check_samyama = eval(check_samyama)
    except:
        pass
    high_match = request.env['res.partner'].getHighMatch(name, phone, email)
    ishangam_id = None
    is_meditator = False
    shoonya_completed = False
    samyama_completed = False
    ieo_completed = False
    rec_id = None

    _logger.info('check Shoonya ' + str(high_match))
    for rec in high_match:
        if rec.shoonya_date or rec.samyama_date or rec.is_meditator :
            is_meditator = True
            rec_id = rec.id
            break
    #return {"ishangam_id": rec_id, "shoonya_completed": shoonya_completed}

    # if check_samyama:
    #     _logger.info('check Samyama ' + str(high_match))
    #     for rec in high_match:
    #         if rec.samyama_date:
    #             samyama_completed = True
    #             rec_id = rec.id
    #             break
    #     return {"ishangam_id": rec_id, "samyama_completed": samyama_completed}
    #
    # if check_meditator:
    #     _logger.info('check meditator ' + str(high_match))
    #     for rec in high_match:
    #         if rec.is_meditator:
    #             is_meditator = True
    #             ieo_completed = 'NA'
    #             rec_id = rec.id
    #             break

    if check_ieo and not is_meditator:
        _logger.info('check ieo ' + str(high_match))
        for rec in high_match:
            if rec.ieo_date:
                ieo_completed = True
                is_meditator = rec.is_meditator
                rec_id = rec.id
                break

    if len(high_match) > 0 and not rec_id:
        recent_contact = high_match[0]
        for rec in high_match:
            if rec.write_date > recent_contact.write_date:
                recent_contact = rec
        rec_id = recent_contact.id

    res = {
        'ishangam_id': rec_id,
        'is_meditator': is_meditator,
        'ieo_completed': ieo_completed,
        'matched_id': rec_id,
        'matched_rec': request.env['res.partner'].sudo().search([('id', '=', rec_id)])
    }
    return res

class ProctorRegistration(http.Controller):
    # @http.route(['/payment/authorize/return/', ], type='http', auth='public', csrf=False)
    #   def authorize_form_feedback(self, **post):
    @http.route('/onlinemegaprograms/proctorform/', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def registrationform(self, legal_entity="", consent_grant_status="",eventregid=0, id_token="", testsso="0", program_id="0", language="english", **post):
        fullprofileresponsedatVar = request.env['megapgms.ssoapi'].get_fullprofileresponse_DTO()
        consentgrantStatusflagvar = True
        if request.httprequest.method=="GET":
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                       "callback_url": str(cururls),
                       "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                       "hash_value": "",
                       "action": "0",
                       "legal_entity": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_ENTITY1'),
                       "force_consent": "1"
                       }
                ret_list = {'request_url': str(cururls),
                        "callback_url": str(cururls),
                        "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                        }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                 digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_megapgms.megapgmsso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                values = {'language': language}
                return request.render("isha_megapgms.exception")
        # if request.httprequest.method == "GET":
        #     validation_errors = []
        #     values = {}
        #     try:
        #         result_countries = request.env['res.country'].sudo().search(
        #             [ ('name', '!=', ' ')])
        #
        #         profresponse = json.dumps(fullprofileresponsedatVar)
        #         values = {
        #             'countries': result_countries,
        #             'doctypes': request.env['documenttype'].sudo().search([]),
        #             'states': request.env['res.country.state'].sudo().search([]),
        #             'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
        #                 [('keyprogram', '=', True)]),
        #             'programschedules': request.env['megapgms.program.schedule'].sudo().search([]),
        #             'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].sudo().search([]),
        #             'submitted': post.get('submitted', False),
        #             'current_page': 1,
        #             'filled_pages': 1,
        #             'isofname': fullprofileresponsedatVar,
        #             'ischangeofparticipant': post.get('iscop', False),
        #             'changeofparticipant_ref': post.get('cop', ''),
        #             'ssoadproofuri': '',
        #             'isoidproofuri': '',
        #             'inputobj': profresponse,
        #             'enableishaautofill': consentgrantStatusflagvar,
        #             'consent_grant_status': '1', #consent_grant_status,
        #             'program_id': program_id
        #         }
        #     except (UserError, AccessError, ValidationError) as exc:
        #         validation_errors.append(ustr(exc))
        #     except Exception:
        #         _logger.error(
        #             "Error caught during request creation", exc_info=True)
        #
        #     values['validation_errors'] = validation_errors
        #     if (language == 'english'):
        #         return request.render('isha_megapgms.volunteer_form', values)
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            validation_errors = []
            try:
                result_countries = request.env['res.country'].sudo().search(
                    [('name', '!=', ' ')])

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = "https://uat-sso.isha.in/getlogininfo"
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                if consent_grant_status == "1":
                    fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id']);

                profresponse = json.dumps(fullprofileresponsedatVar)
                values = {
                    'countries': result_countries,
                    'doctypes': request.env['documenttype'].sudo().search([]),
                    'states': request.env['res.country.state'].sudo().search([]),
                    'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                        [('keyprogram', '=', True)]),
                    'programschedules': request.env['megapgms.program.schedule'].sudo().search([]),
                    'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].sudo().search([]),
                    'submitted': post.get('submitted', False),
                    'current_page': 1,
                    'filled_pages': 1,
                    'isofname': fullprofileresponsedatVar,
                    'ischangeofparticipant': '',#post.get('iscop', False),
                    'changeofparticipant_ref': '',#post.get('cop', ''),
                    'ssoadproofuri': '',
                    'isoidproofuri': '',
                    'inputobj': profresponse,
                    'enableishaautofill': consentgrantStatusflagvar,
                    'consent_grant_status': '1',  # consent_grant_status,
                    'program_id': program_id,
                 }

                values['validation_errors'] = validation_errors
                if (language == 'english'):
                    return request.render('isha_megapgms.volunteer_form', values)

            except Exception as ex:
                _logger.error("Error caught during request creation", exc_info=True)
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                request.env['megapgm.tempdebuglog'].sudo().create({
                    'logtext': tb_ex
                })
        elif request.httprequest.method == "POST":
            validation_errors = []
            try:
                first_name = post.get('first_name')
                last_name = post.get('last_name')
                participant_email = post.get('participant_email')
                #gender = post.get('gender')
                #dob = post.get('dob')
                countrycode = post.get('countrycode')
                phone = post.get('phone')
                wacountrycode=post.get('wacountrycode')
                waphone=post.get('waphone')
                if waphone:
                    pass
                else:
                    waphone = phone
                    wacountrycode = countrycode
                if consentgrantStatusflagvar == True:
                    fullprofileresponsedatVar['basicProfile']['firstName'] = first_name
                    fullprofileresponsedatVar['basicProfile']['lastName'] = last_name
                    #fullprofileresponsedatVar['basicProfile']['gender'] = gender.upper()
                    #fullprofileresponsedatVar['basicProfile']['email'] = participant_email

                event_rec = request.env['event.registration'].sudo().search([('id', '=', eventregid)], limit=1)
                event_rec.write({'state': 'open'})

                # if event_type == "shoonya":
                #     resofismed = get_is_meditator_or_ieo(first_name,phone,participant_email,False,False,True,False)
                #     is_meditator = resofismed['shoonya_completed']
                # elif event_type == "shambhavi":
                resofismed = get_is_meditator_or_ieo(first_name,phone,participant_email,True,False,False,False)
                is_meditator = resofismed['is_meditator']
                # elif event_type == "ieo":
                #     resofismed = get_is_meditator_or_ieo(name,phone,participantemail,False,True,False,False)
                #     is_meditator = resofismed['ieo_completed']
                # elif event_type == "samyama":
                #     resofismed = get_is_meditator_or_ieo(name,phone,participantemail,False,False,False,True)
                #     is_meditator = resofismed['samyama_completed']

                rec_id = resofismed['ishangam_id']

                if (is_meditator == True and rec_id != None):
                    print('record found ', rec_id)
                    # update respartner record
                    rec = request.env['res.partner'].sudo().create({
                        'name': first_name,
                        'phone_country_code': countrycode,
                        'phone': phone,
                        'email': participant_email,
                        'whatsapp_country_code': wacountrycode,
                        'whatsapp_number': waphone,
                        #'sso_id': profileid
                    })
                if (is_meditator == False):
                    respartrec=request.env['res.partner'].sudo().create({
                        'name': first_name,
                        'phone_country_code': countrycode,
                        'phone': phone,
                        'email': participant_email,
                        'whatsapp_country_code': wacountrycode,
                        'whatsapp_number': waphone,
                        #'sso_id': profileid,
                        'is_meditator': is_meditator
                    })
                return request.render("isha_megapgms.registrationsuccess2")
            # except (UserError, AccessError, ValidationError) as exc:
            #     validation_errors.append(ustr(exc))
            except Exception as ex:
                stacktrace = traceback.print_exc()
                request.env['megapgms.tempdebuglog'].sudo().create({
                    'logtext': ex
                })
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)

            # if not validation_errors:
            #     if (True):#progtype == 'Medical Consultation Review' or progtype == 'IECO - Preliminary class'
            #         first_data.incompleteregistration_pagescompleted = 5
            #         first_data.registration_status = "Incomplete"#Payment Link Sent
            #         first_data.registration_email_send = False
            #         first_data.registration_sms_send = False
            #         values = {'object': first_data}
            #         return request.render("isha_megapgms.volunteer_form_page2", values)
            #         #return request.render("isha_megapgms.pgmregn2", values)
            #
            #     else:
                    # state_onlyindia = request.env['res.country.state'].search([('country_id', '=', 104)])
                    # values = {

                        #'pgmrecs': pgmrecs,
                        #'programregn': first_data,
                        #'countries': request.env['res.country'].search([]),
                        #'states': state_onlyindia,
                        #'prgregncenters': request.env['isha.center'].search([]),
                        #'prgregnprogramnamecategories': request.env['master.program.category'].search(
                        #    [('keyprogram', '=', True)]),
                        #'programschedules': request.env['megapgms.program.schedule'].search([]),
                        #'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].search([]),
                    #     'state': 'Completed',
                    #     'submitted': post.get('submitted', False),
                    #     'current_page': 2,
                    #     'filled_pages': 2,
                    #     'firstnamevar': first_data.first_name,
                    # }
                    #
                    # values['validation_errors'] = validation_errors
                    #return request.render("isha_megapgms.pgmregn2", values)
            # else:
            #     _logger.log(25, validation_errors)
            #     logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
            #         [str(elem) for elem in validation_errors])
            #     request.env['megapgms.tempdebuglog'].sudo().create({
            #         'logtext': logtext
            #     })
            #     return request.render('isha_megapgms.exception')
