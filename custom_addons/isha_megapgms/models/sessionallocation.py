from odoo import models, fields, api


class MegaProgramType(models.Model):
    _name = 'session.allocation'
    _description = 'Mega Program Type'
    _rec_name = 'pgm_session'

    pgm_schedule = fields.Many2one('megapgms.program.schedule',string='Schedule Name')
    pgm_session = fields.Many2one('megapgms.session.timings', string='Session')
    ishanga_count = fields.Integer(string='Ishangas Count',default=1)
    co_ishanga_count = fields.Integer(string='Co-Ishangas Count',default=1)
    tech_support_count = fields.Integer(string='Tech Support Count',default=1)
    hall_volunteer_count = fields.Integer(string='Hall Volunteers Count',default=50)
    vol_registrations = fields.One2many('event.registration','opt_session',string='Registered Volunteers')
    ishanga_needed = fields.Integer(string='Ishangas Needed',compute='set_remaining_count',store=True)
    co_ishanga_needed = fields.Integer(string='Co-Ishangas Needed',compute='set_remaining_count',store=True)
    tech_support_needed = fields.Integer(string='Tech Support Needed',compute='set_remaining_count',store=True)
    hall_volunteer_needed = fields.Integer(string='Hall Volunteers Needed',compute='set_remaining_count',store=True)

    @api.depends('ishanga_count','co_ishanga_count','tech_support_count','hall_volunteer_count',
                 'vol_registrations','vol_registrations.state')
    def set_remaining_count(self):
        for rec in self:
            rec.ishanga_needed = rec.ishanga_count - len(rec.vol_registrations.filtered(lambda x: x.mega_pgm_vol_options == 'main_ishanga' and x.state in ['open','done']))
            rec.co_ishanga_needed = rec.co_ishanga_count - len(rec.vol_registrations.filtered(lambda x: x.mega_pgm_vol_options == 'co_ishanga' and x.state in ['open','done']))
            rec.tech_support_needed = rec.tech_support_count - len(rec.vol_registrations.filtered(lambda x: x.mega_pgm_vol_options == 'tech_support' and x.state in ['open','done']))
            rec.hall_volunteer_needed = rec.hall_volunteer_count - len(rec.vol_registrations.filtered(lambda x: x.mega_pgm_vol_options == 'Hall_volunteer' and x.state in ['open','done']))

    def get_session_allocation_info(self, pgm_schedule_ids):
        reg_sessions = self.search([('pgm_schedule','in',pgm_schedule_ids)])
        result_list = []
        for reg in reg_sessions:
            result_list.append(
                {
                    'pgm_schedule':reg.pgm_schedule,
                    'pgm_session':reg.pgm_session,
                    'ishanga_needed':reg.ishanga_needed,
                    'co_ishanga_needed':reg.co_ishanga_needed,
                    'tech_support_needed':reg.tech_support_needed,
                    'hall_volunteer_needed':reg.hall_volunteer_needed
                }
            )
        return result_list
