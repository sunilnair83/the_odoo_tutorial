from odoo import models, fields, api, _


class VirtualRooms(models.Model):
    _name = 'iec.virtual.room'
    _description = 'IECSO Virtual Room'
    _order = 'create_date desc'

    name = fields.Char(string='Virtual Room', readonly=True,copy=False,index=True,default=lambda self: _('Virtual Room'))
    hall_volunteer = fields.Many2many('event.registration','iec_vr_event_reg_rel',string='Hall Volunteer')
    session_allocation = fields.Many2one('session.allocation', string='Session')
    session_volunteers = fields.One2many(related='session_allocation.vol_registrations',string='Session Volunteers')
    schedule_id = fields.Many2one('megapgms.program.schedule', string='Schedule')
    schedule_volunteers = fields.One2many(related='schedule_id.vol_registrations')
    participants = fields.One2many('iec.mega.pgm','virtual_room',string='Participants')
    participants_count = fields.Integer(string='Participants Count')
    active = fields.Boolean(string='Active',default=True)

    @api.model
    def create(self, vals):
        if vals.get('sequence', _('Virtual Room')) == _('Virtual Room'):
            vals['name'] = self.env['ir.sequence'].next_by_code('iec.virtual.room') or _('Virtual Room')
        return super(VirtualRooms,self).create(vals)


class VirtualRoomWizard(models.TransientModel):
    _name = 'iec.virtual.room.wizard'
    _description = 'IECSO Virtual Room Wizard'

    schedules = fields.Many2many('megapgms.program.schedule',string='Schedules')
    participant_count = fields.Integer(string='Participants per Room', default=24)
    volunteer_count = fields.Integer(string='Hall Volunteer per Room',default=1)
    timezone = fields.Selection([('Asia/Kolkata', 'IST'), ('America/New_York', 'EST')], string="Timezone")

    @api.model
    def default_get(self, fields):
        res = super(VirtualRoomWizard, self).default_get(fields)
        res['schedules'] = self.env['megapgms.program.schedule'].browse(self._context.get('active_ids')).sorted(lambda x:x.pgmschedule_startdate).ids
        return res

    def allocate_vr(self):
        room_data_dict = []
        for schedule in self.schedules:

            """
                For each schedule go over the session allocation where the timezone matches sorted by session timings
            """
            session_allocations = schedule.pgmschedule_session_allocation.filtered(
                lambda x:x.pgm_session.timezone == self.timezone).sorted(
                lambda x: x.pgm_session.session_timing)
            for session_allocation in session_allocations:

                """
                    Allocation starts region wise. Get the participant grouped by region
                """
                group_by_regions = self.env['iec.mega.pgm'].read_group(domain=[('partner_region_id','!=',False),('schedule_id','=',schedule.id),('session_id','=',session_allocation.pgm_session.id),('status','in',['A','X'])]
                                                    ,fields=['partner_region_id'], groupby=['partner_region_id'])

                """
                    Allocate participants to the last room if there is any vacancy
                """
                prev_room_vacancy = 0
                for region in group_by_regions:

                    """ 
                        Hall volunteers is picked by the following filter
                        1> volunteers region is the current allocation region,
                        2> state is confirmed
                        3> session_opted is the current session_allocation
                        4> volunteering option is Hall_volunteer
                    """
                    hall_volunteers = self.env['event.registration'].search([('partner_region_id','=',region['partner_region_id'][0]),('state','=','open'),
                                                                             ('opt_session','=',session_allocation.id),('mega_pgm_vol_options','=','Hall_volunteer')]).ids

                    """
                        Participants are picked by filter
                        1> schedule is current schedule
                        2> session is currenc session_allocation's session
                        3> status is 'Active' or 'Transfer'
                        4> participant region is current region
                    """
                    participants = self.env['iec.mega.pgm'].search([('schedule_id','=',schedule.id),('session_id','=',session_allocation.pgm_session.id),
                                                                    ('status','in',['A','X']),('partner_region_id','=',region['partner_region_id'][0])]
                                                ).sorted(lambda x: x.create_date).ids

                    """
                        Split logic volunteers:
                            If any volunteer is available for the current region
                                - split the volunteers into chunks of size volunteer_count
                            Else
                                - allocate rooms without volunteers
                    """
                    split_vol_ids = []
                    split_vol_index = 0
                    if self.volunteer_count > 0:
                        split_vol_ids = [hall_volunteers[i:i + self.volunteer_count] for i in range(0, len(hall_volunteers), self.volunteer_count)]

                    """
                        Split logic for participants:
                            If the last room allocation has some vacancy 
                                - First chunk should be to fill the last room's vacancy
                                - Then split into chunks of size participant_count
                            Else
                                - Split participants into chunks of size participant_count
                            
                    """
                    split_participant_ids = [participants[i:i + self.participant_count] for i in range(prev_room_vacancy, len(participants), self.participant_count)]
                    if prev_room_vacancy > 0:
                        split_participant_ids.insert(0,participants[0:prev_room_vacancy])

                    for room_participants in split_participant_ids:

                        """
                            Check to see if we still have volunteers to be allocated for the current region
                        """
                        if 0 < len(split_vol_ids) > split_vol_index:
                            room_vol = split_vol_ids[split_vol_index]
                        else:
                            room_vol = []

                        """
                            If previous room has participant vacancy 
                                - add the current participants to the previous room
                            Else
                                - Create a new room   
                        """
                        if prev_room_vacancy > 0:
                            room_data_dict[-1]['participants'] = [(6,0,room_data_dict[-1]['participants'][0][2] + room_participants)]
                            room_data_dict[-1]['participants_count'] = len(room_data_dict[-1]['participants'][0][2])
                            """
                                Check if the previous rooms volunteer needed is satisfied 
                                if not take the current regions volunteers 
                            """
                            if len(room_data_dict[-1]['hall_volunteer'][0][2]) < self.volunteer_count:
                                room_data_dict[-1]['hall_volunteer'] = [(6,0,room_vol)]
                                split_vol_index += 1
                        else:
                            room_data_dict.append(
                                {
                                    'session_allocation':session_allocation.id,
                                    'schedule_id':schedule.id,
                                    'hall_volunteer':[(6,0,room_vol)],
                                    'participants':[(6,0,room_participants)],
                                    'participants_count':len(room_participants),
                                    'active':True
                                }
                            )
                            split_vol_index += 1

                        """
                            Compute last rooms vacancy for participant
                        """
                        prev_room_vacancy = self.participant_count - len(room_data_dict[-1]['participants'][0][2])
                        if len(room_data_dict[-1]['participants'][0][2]) != self.participant_count:
                            print('VACANCY')
        if len(room_data_dict)>0:
            vr = self.env['iec.virtual.room'].sudo().create(room_data_dict)

        action = self.env.ref('isha_megapgms.iec_virtual_room_action_window').read()[0]
        return action
