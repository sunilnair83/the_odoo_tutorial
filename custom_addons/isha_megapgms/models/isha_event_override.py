from odoo import models, fields, api


class Event(models.Model):
    _inherit = 'event.event'

    # mega programs fields
    megapgms_schedule = fields.Many2one('megapgms.program.schedule', string='Schedule Name')

    @api.onchange('megapgms_schedule')
    def computeMegaPgmName(self):
        for rec in self:
            temp = self.getEventName(rec)
            rec.name = rec.megapgms_schedule.pgmschedule_programname
            rec.date_begin = rec.megapgms_schedule.pgmschedule_startdate
            rec.date_end = rec.megapgms_schedule.pgmschedule_enddate
            rec.center_id = False

class EventReg(models.Model):
    _inherit = 'event.registration'

    megapgms_schedule = fields.Many2one(related='event_id.megapgms_schedule')
    mega_pgm_vol_options = fields.Selection([('main_ishanga','Main Ishanga'),
                                             ('co_ishanga', 'Co-Ishanga'),
                                             ('tech_support','Tech support'),
                                             ('Overall_coordinator','Overall coordinator'),
                                             ('Registration_and_email', 'Registration and email'),
                                             ('Registration_and_email_coordinator','Registration and email coordinator'),
                                             ('Enrollment','Enrollment'),
                                             ('Enrollment_coordinator','Enrollment coordinator'),
                                             ('Participant_support_nurturing','Participant support nurturing'),
                                             ('Participant_support_nurturing_coordinator','Participant support nurturing coordinator'),
                                             ('Participant_support_technical','Participant support technical'),
                                             ('Participant_support_technical_coordinator','Participant support technical  coordinator'),
                                             ('CRM_support', 'CRM support'),
                                             ('CRM_support_coordinator', 'CRM support coordinator'),
                                             ('Communication', 'Communication'),
                                             ('Communication_coordinator', 'Communication coordinator'),
                                             ('Medical', 'Medical'),
                                             ('Medical_coordinator', 'Medical coordinator'),
                                             ('Hall_volunteer', 'Hall volunteer'),
                                             ('Hall_volunteer_coordinator', 'Hall volunteer coordinator'),
                                             ('Volunteer_coordinator', 'Volunteer coordinator'),
                                             ('Test_support', 'Test support'),
                                             ('Test_support_coordinator','Test support coordinator')],
                                            string='Volunteer Category')

    opt_session = fields.Many2one('session.allocation',string='Opted Session', domain="[('pgm_schedule', '=', megapgms_schedule)]")
    opt_schedule = fields.Many2one('megapgms.program.schedule',string='Opted Schedule', domain="[('id', '=', megapgms_schedule)]")

    state = fields.Selection(selection_add=[('email_sent', 'Email Sent')])
    partner_center_id = fields.Many2one(related='partner_id.center_id', store=True)
    partner_region_id = fields.Many2one(related='partner_center_id.region_id', store=True)
