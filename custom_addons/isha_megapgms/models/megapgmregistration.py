import logging
import uuid
from datetime import datetime

from werkzeug import urls

from odoo import models, fields, api, _, exceptions
from odoo.exceptions import except_orm
from .. import isha_crm_importer

_logger = logging.getLogger(__name__)
try:
    from html2text import HTML2Text
except ImportError as error:
    _logger.debug(error)

def html2text(html):
    """ covert html to text, ignoring images and tables
    """
    if not html:
        return ""

    ht = HTML2Text()
    ht.ignore_images = True
    ht.ignore_tables = True
    ht.ignore_emphasis = True
    ht.ignore_links = True
    return ht.handle(html)

class ValidationError(except_orm):
    """Violation of python constraints.

                 .. admonition:: Example

                     When you try to create a new user with a login which already exist in the db.
                 """

    def __init__(self, msg):
        super(ValidationError, self).__init__(msg)

class MegapgmsRegistration(models.Model):
    _name = 'megapgms.registration'
    _description = 'Mega Program Registration Forms'
    _rec_name = 'first_name'
    #_order = 'last_modified_dt'
    # participant_id = fields.Integer(string="ParticipantId", default=lambda self: self.env['ir.sequence'].next_by_code('increment_your_field'))

    def _get_default_access_token(self):
        return str(uuid.uuid4())

    #basic profile info
    first_name = fields.Char(string="First Name", required=True)
    last_name = fields.Char(string="Last Name", required=True)
    name_called = fields.Char(string="Name you would like to be called")
    gender = fields.Selection([('Male', 'Male'), ('Female', 'Female'), ('Others', 'Others')], string='Gender', required=False)
    marital_status = fields.Selection(
        [('Single', 'Single'), ('Partnered', 'Partnered'), ('Married', 'Married'), ('Divorced', 'Divorced'),
         ('Widowed', 'Widowed')], string='Marital Status')
    dob = fields.Date(string='Date Of Birth', required=False)
    age = fields.Char('Age',  compute="_set_age", store=True, required=False)

    @api.depends('dob')
    def _set_age(self):
        for rec in self:
            if rec.dob != False:
                rec.age = int((datetime.now().date() - self.dob).days/ 365.25);
            else:
                rec.age = 0

    isopnumber = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Did you attend any rejuvenation programs?')
    opnumber = fields.Char(string='OP Number', required=False, index=False)
    # contact details
    countrycode = fields.Char(string='Country Tel code', required=False, index=False)
    phone = fields.Char(string='Mobile Number', required=False, index=True)
    country = fields.Many2one('res.country', 'Country of residence')
    state = fields.Many2one('res.country.state', 'State/province')
    pincode = fields.Char(string="Pincode/Zipcode", required=False, index=True)
    address_line = fields.Char(string='Address Line', required=False)
    city = fields.Char(string='City/Town/District', required=False)
    education_qualification = fields.Char(String="Educational Qualification")
    nationality_id = fields.Many2one('res.country', 'Nationality Id')
    occupation = fields.Char(string='Occupation', required=False)

    reference_id = fields.Char(string='Reference Number', required=False)
    proof_ids = fields.One2many('idproof.attachment', 'idproof_id', string="ID Proof")
    attachment_ids = fields.One2many('megapgm.request.attachment', 'generic_request_id', string="Attachment(s)")

    # Emergency contacts
    name1 = fields.Char(String="Name 1", required=False)
    relationship1 = fields.Char(String="Relationship 1", required=False)
    phone1 = fields.Char(string='Mobile Number 1', required=False, index=True)
    email1 = fields.Char(string='Email 1', required=False, index=True)
    name2 = fields.Char(String="Name 2", required=False)
    relationship2 = fields.Char(String="Relationship 2", required=False)
    phone2 = fields.Char(string='Mobile Number 2', required=False, index=True)
    email2 = fields.Char(string='Email 2', required=False, index=True)

    # photo and id/addr proofs
    photo_file = fields.Binary(string="Upload Photo")
    file_name = fields.Char(string="Photo file")
    idproof_file = fields.Binary(string="Upload IdProof")
    idfile_name = fields.Char(string="IdProof file")
    idproof_type = fields.Many2one('idproof.list', 'IdProof Type')
    addrproof_file = fields.Binary(string="Upload AddressProof")
    addrfile_name = fields.Char(string="AddrProof file")

    # Program history
    programhistorynote = fields.Char(string=' ', size=500, readonly="1",
                                     default='Pls provide the history of Isha Yoga Programs you have undergone')
    programhistory = fields.One2many('mega.program.history','programregistration', string="Program History", required=False)

    # @api.model
    # def fields_view_get(self, view_id=None, view_type='form',
    #                     toolbar=False, submenu=False):
    #     res = super(ProgramRegistration, self).fields_view_get(
    #         view_id, view_type,
    #         toolbar=toolbar, submenu=submenu)
    #     # if view_type != 'search' and self.env.uid != SUPERUSER_ID:
    #     # Check if user is in group that allow creation
    #     desired_group_name = self.env['res.groups'].sudo().search([('name', '=', 'Backoffice User')])
    #     has_my_group = self.env.user.id in desired_group_name.users.ids
    #     root = etree.XML(res['arch'])
    #     if  has_my_group:
    #         root.set('create', 'false')
    #         if view_type == 'form':
    #             for field in res['fields']:
    #                 for node in root.xpath("//field[@name='%s']" % field):
    #                   if not node.values().__contains__('gender') and not  node.values().__contains__('phone') and\
    #                           not node.values().__contains__('addrproof_file') and not  node.values().__contains__('idproof_file') and not node.values().__contains__('photo_file'):
    #                         node.set("readonly", "1")
    #                         modifiers = json.loads(node.get("modifiers"))
    #                         modifiers['readonly'] = True
    #                         node.set("modifiers", json.dumps(modifiers))
    #     res['arch'] = etree.tostring(root)
    #
    #     return res

    # medical profile - basic info

# Program and package details

    programapplied = fields.Many2one('megapgms.program.schedule',string='Select program to register')
#   programapplied = fields.One2many('program.schedule.pgmschedule_programname',string='Select program to register', default='Program Selected from ISO')
    packageselection = fields.Many2one('megapgms.program.schedule.roomdata',string='Select Accommodation', required=False)
#    selectpackage = fields.Many2one('package.parameters',string='Select Accommodation', required=False)
    show_change_pgm = fields.Boolean(compute="_compute_programdateflag")    
    show_cop = fields.Boolean(compute="_compute_show_cop")
    cop_url = fields.Char(string='Change of participant URL')
    cop_access_token = fields.Char(string='Change of participant access token')

    compute_field = fields.Boolean(string="check field", compute='get_user')

    # Physical ailments
    is_disease_diabetes = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Diabetes')
    diabetes_details = fields.Text(string='Please give more details:', required=False)
    is_disease_hypertension = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='HyperTension / Blood pressure')
    hypertension_details = fields.Text(string='Please give more details:', required=False)
    is_disease_heartconditions = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Heart Conditions')
    heartconditions_details = fields.Text(string='Please give more details:', required=False)
    is_disease_stroke = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Stroke')
    stroke_details = fields.Text(string='Please give more details:', required=False)
    is_disease_chronicpain = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Chronic Pain')
    chronicpain_details = fields.Text(string='Please give more details:', required=False)
    is_disease_ligamentinjuries = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Ligament Injuries')
    ligamentinjuries_details = fields.Text(string='Please give more details:', required=False)
    is_disease_seizuresepilepsy = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Seizures/Epilepsy')
    seizuresepilepsy_details = fields.Text(string='Please give more details:', required=False)
    is_disease_spinalconditions = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Spinal Conditions')
    spinalconditions_details = fields.Text(string='Please give more details:', required=False)
    is_disease_glaucoma = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Glaucoma')
    glaucoma_details = fields.Text(string='Please give more details:', required=False)
    is_disease_hernia = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Hernia')
    hernia_details = fields.Text(string='Please give more details:', required=False)
    is_disease_communicabledisease = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Communicable Disease')
    communicabledisease_details = fields.Text(string='Please give more details:', required=False)
    is_disease_surgery = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Surgery in the Last Three Years')
    surgery_details = fields.Text(string='Please give more details:', required=False)
    is_disease_surgery = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Surgery in the Last Three Years')
    surgery_details = fields.Text(string='Please give more details:', required=False)
    is_disease_seriousillness = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Serious Illness in the Last Three Years')
    seriousillness_details = fields.Text(string='Please give more details:', required=False)
    is_disease_injuryinthelastthreeyears = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Injury in the Last Three Years')
    injuryinthelastthreeyears_details = fields.Text(string='Please give more details:', required=False)
    is_disease_lungdisease = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Lung Disease')
    lungdisease_details = fields.Text(string='Please give more details:', required=False)


    # Mental ailments
    is_disease_psychotherapy = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Psychotherapy in the Last 5 years')
    psychotherapy_details = fields.Text(string='Please give more details:', required=False)
    is_disease_psychologicalcounselling = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Psychological Counselling in the Last Five Years')
    psychologicalcounselling_details = fields.Text(string='Please give more details:', required=False)
    is_disease_depression = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Depression')
    depression_details = fields.Text(string='Please give more details:', required=False)
    is_disease_dementia = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Dementia')
    dementia_details = fields.Text(string='Please give more details:', required=False)
    is_disease_schrizophrenia = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Schrizophrenia')
    schrizophrenia_details = fields.Text(string='Please give more details:', required=False)
    is_disease_anxiety = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Anxiety')
    anxiety_details = fields.Text(string='Please give more details:', required=False)
    is_disease_bipolardisorder = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Bipolar Disorder')
    bipolardisorder_details = fields.Text(string='Please give more details:', required=False)
    is_disease_ocd = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Obsessive Compulsive Disorder')
    ocd_details = fields.Text(string='Please give more details:', required=False)
    is_disease_panicattack = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Panic Attack')
    panicattack_details = fields.Text(string='Please give more details:', required=False)
    is_disease_hallucinations = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Hallucinations')
    hallucinations_details = fields.Text(string='Please give more details:', required=False)
    is_disease_othermental = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Others')
    othermental_details = fields.Text(string='Please give more details:', required=False)


    @api.depends('compute_field')
    def get_user(self):
        retval = True;
        try:
            res_user = self.env['res.users'].search([('id', '=', self._uid)])
            if res_user.has_group('isha_megapgms.group_megapgms_backofficeuser'):
                retval = False
            else:
                retval = True
        except Exception as e:
            retval = True
            _logger.debug(error)
        finally:
            self.compute_field = retval

    #program schedule
    participant_programtype = fields.Many2one(related='programapplied.pgmschedule_programtype')
    participant_int_sch= fields.Many2one(related='programapplied.initiation_schedule')
    ha_status = fields.Selection([('Pending','Pending'),('HA Approved','HA Approved'),('Rejected','Rejected'),('Request More Info','Request More Info'),('HA Resubmitted','HA Resubmitted')], string='HA Status', default='Pending', size=100)
    registration_status = fields.Selection([('Pending','Pending'),('Rejected','Rejected'),('Rejection Review','Rejection Review'),('Payment Link Sent','Payment Link Sent'),('Add To Waiting List','Add To Waiting List'),('Cancel Applied','Cancel Applied'),('Cancel Approved','Cancel Approved'),('Paid','Paid'),('Confirmed','Confirmed'),('Approved','Approved'),('Withdrawn','Withdrawn'),('Refunded','Refunded'),('Incomplete','Incomplete'),('Change Of Participant','Change Of Participant')], string='Registration Status',  size=100, default='Pending')
    participant_comments =  fields.Char(string = 'Comments', size=300)
    participant_email = fields.Char(string = 'Email', size=100)
    participant_email_alt = fields.Char(string = 'Alternate Email', size=500)
    selected_package = fields.Many2one('megapgm.package.parameters', string='Requested Package')

    #approval fields
    seats_available = fields.Char(string="Seats Available", default='No', store=True)
    oco_status = fields.Char(string="OCO Status", default='Pending')
    oco_id = fields.Integer(string="OCO Id")
    bl_status = fields.Char(string="Blacklisted", default='Pending')

    prerequisite_pgm = fields.Char(string="Pre-requisite Practices", default='Not Satisfied')
    package_availablity = fields.Integer(string="Package Availablity")

    use_emrg_quota =  fields.Boolean(string="Use Emergency Quota", default=False)
    oco_approval_overide =  fields.Boolean(string="Override", default=False)
    blacklisted_overide =  fields.Boolean(string="Override", default=False)	
    prerequisite_pgm_overide =  fields.Boolean(string="Override", default=False)

    ####QP
    question_ids = fields.Many2many('megapgms.questionbank',string='Questions To The Participant')
    question_bank = fields.One2many('megapgms.participant.qa', 'first_name', string = 'HA Assessment QA')
    addinfo_needed =  fields.Text(string = 'Additional Info Needed')    
    addinfo_needed_answer =  fields.Text(string = 'Additional Info Needed')    

    ## HA Reports
    #ha_reports = fields.One2many('participant.hadocs','participant',string="HA Reports")

    rejection_reason =  fields.Many2one('megapgms.rejection.reason', string='Rejection Reason')
    status_comments =  fields.Text(string = 'Comments', size=500)   

    accompanying_name =  fields.Many2one('megapgms.registration', string='Accompanying With')
    accompanying_name_text = fields.Char(string='Accompany Guest', size=250)
    accompanying_email = fields.Char(string='Email Id of Accompany')
    allocated_roomnumber = fields.Integer(string = 'Room Number')
    roomtype = fields.Many2one('megapgms.roomtype',string = 'Room Type',related="packageselection.pgmschedule_roomtype")
    attendance_status = fields.Selection([('Present','Present'),('Absent','Absent'),('Not Marked','Not Marked')], string='Attendance')
		
    # other status fields
    incompleteregistration_pagescompleted = fields.Integer(string='Registration pages completed', default=1)
    incompleteregistration_email_send = fields.Boolean(string="Incomplete Registration Email Send", default=False)
    incompleteregistration_sms_send = fields.Boolean(string="Incomplete Registration SMS Send", default=False)
    registration_email_send = fields.Boolean(string="Registration Email Send", default=False)
    registration_sms_send = fields.Boolean(string="Registration SMS Send", default=False)
    rejection_email_send = fields.Boolean(string="Rejection Email Send", default=False)
    rejection_sms_send = fields.Boolean(string="Rejection SMS Send", default=False)
    harequest_email_send = fields.Boolean(string="HA Addinfo Email Send", default=False)
    harequest_sms_send = fields.Boolean(string="HA Addinfo SMS Send", default=False)
    cancel_email_send = fields.Boolean(string="cancel_email_send")
    access_token = fields.Char('Access Token', default=lambda self: self._get_default_access_token(), copy=False)
    public_url = fields.Char("Public link", compute="_compute_call_public_url")
    #
    addtowaitlist_email_send = fields.Boolean(string="Add To WaitList Email Send", default=False)
    addtowaitlist_sms_send = fields.Boolean(string="Add To WaitList SMS Send", default=False)
    waitinglist_date = fields.Date()
    waitinglist_email_count = fields.Integer(default=0)
    waitinglist_period_inmonth = fields.Integer(default=0)
    waitinglist_last_communicate_sch = fields.Many2one('megapgms.program.schedule')
    waitinglist_last_communicate_dt = fields.Date(string="Future Programs Last Comm Date")
    waitinglist_email_send = fields.Boolean(string="Future Programs Email Send", default=False)
    waitinglist_sms_send = fields.Boolean(string="Future Programs SMS Send", default=False)
    #
    paymentlink_email_send = fields.Boolean(string="Payment Link Email Send", default=False)
    paymentlink_sms_send = fields.Boolean(string="Payment Link SMS Send", default=False)
    auto_approval_comments = fields.Char("Auto Approval Comments", size=100)
    #
    registration_agreed = fields.Boolean(string="I Agree", default=False)    
    # amount_paid  = fields.Float(string = 'Amount Paid')
    # date_paid =  fields.Date()
    date_confirm  =  fields.Date()

    #
    date_withdrawn = fields.Date()
    date_cancelapproved = fields.Datetime()
    refund_eligible_ondaysbefore = fields.Integer(string = 'Days Before')
    refund_eligible_onpercentage = fields.Float(string = 'Eligible Percentage') 
    refund_eligible = fields.Float(string = 'Refund Amount Eligible')    

    #
    ha_resubmit_date = fields.Date()
    ha_resubmitted = fields.Boolean(string="HA Resubmitted", default=False)    

    #
    date_refund = fields.Datetime(string="Refund Done On")
    refund_amount = fields.Float(string = 'Actual Refunded Amount')
    refund_comments = fields.Text(string = 'Comments')

    contact_id_fkey = fields.Many2one(comodel_name='res.partner',string='Contact')
    
    confirmation_email_send = fields.Boolean(string="Confirmation Email Send", default=False)
    confirmation_sms_send = fields.Boolean(string="Confirmation SMS Send", default=False)

    pushedtocico = fields.Boolean(string="Pushed to CICO", default=False)
    
    last_modified_dt = fields.Datetime(string="Last Modified DT")
    checkindatetime  = fields.Datetime(string="Check-in Date Time from CICO")
	
    cancelapplied_email_send = fields.Boolean(string="Cancellation Applied Email Send", default=False)
    cancelapproved_email_send = fields.Boolean(string="Cancellation Approved Email Send", default=False)
    cancelrefund_email_send = fields.Boolean(string="Cancellation Refunded Email Send", default=False)

    cancelapplied_sms_send = fields.Boolean(string="Cancellation Applied Email Send", default=False)
    cancelapproved_sms_send = fields.Boolean(string="Cancellation Approved Email Send", default=False)
    cancelrefund_sms_send = fields.Boolean(string="Cancellation Refunded Email Send", default=False)

    # payment info
    orderid = fields.Char(string='Order ID', size=80)
    billinginfoconfirmation = fields.Boolean(string="Billing Info Confirmation", default=False)
    billing_name = fields.Char(string='Billing Name', size=60)
    billing_address = fields.Char(string='Billing Address', size=150)
    billing_city = fields.Char(string='Billing City', size=30)
    billing_state = fields.Char(string='Billing State', size=30)
    billing_zip = fields.Char(string='Billing Zip', size=15)
    billing_country = fields.Char(string='Billing Country', size=50)
    billing_tel = fields.Char(string='Billing Tel', size=20)
    billing_email = fields.Char(string='Billing Email', size=70)
    payment_status = fields.Char(string='Payment Status', size=70)
    total_paid  = fields.Float(string = 'Total Amount Paid', default=0)
    amount_paid  = fields.Float(string = 'Amount Paid', default=0)
    date_paid = fields.Datetime()
    ereceiptgenerated = fields.Boolean(string="eReceipt Generated", default=False)
    ereceiptreference = fields.Char(string="eReceipt Reference", size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=500)
    paymenttransactions = fields.One2many('megapgms.paymenttransaction', 'programregistration', string="Payment Transaction", required=False)
    payment_initiated_dt = fields.Datetime()

    ischangeofparticipant = fields.Boolean(string="Change of Participant Applied", default=False)
    changeofparticipant_applied_dt = fields.Datetime(string="Change of Participant Applied DT")
    changeofparticipant_ref = fields.Integer(string="Change of Participant Reference ID")
    transferred_amount = fields.Float(string="Change of Participant Transfered Amount")

    cancelapplied_emailsms_pending = fields.Boolean(default=False)
    copapplied_emailsms_pending = fields.Boolean(default=False)
    active = fields.Boolean(string='active', default=True)

    show_upgrade = fields.Boolean(compute="_compute_show_upgrade")
    othertransactions = fields.One2many('megapgms.registration.transactions', 'programregistration', string="Transactions", required=False)

    @api.model
    def fields_get(self, fields=None):
        fields_to_show = ['first_name','last_name','phone','country','state','pincode','city','nationality_id', 'programapplied', 'participant_email', 'oco_id', 'access_token', 'pushedtocico', 'checkindatetime', 'ereceiptgenerated', 'ereceiptreference', 'ereceipturl', 'registration_status', 'ha_status', 'id']
        res = super(MegapgmsRegistration, self).fields_get()
        for field in res:
            if (field in fields_to_show):
                res[field]['selectable'] = True	
                res[field]['sortable'] = True
            else:
                res[field]['selectable'] = False
                res[field]['sortable'] = False
        return res

    @api.constrains('programapplied')
    def validate_repititionallowed(self):
        repititionallowed=self.programapplied.pgmschedule_repetitionallowed;

        type = self.programapplied.pgmschedule_programtype.id;
        sql = "SELECT prg.participant_email, rps.pgmschedule_enddate  FROM public.mega_program_type  as ptp  inner join public.megapgms_program_schedule rps on rps.pgmschedule_programtype = ptp.id   and  ptp.id =  %s   inner join public.megapgms_registration prg  on prg.programapplied = rps.id and prg.id != %s where prg.participant_email = '%s' and attendance_status ='Present' order  by  rps.pgmschedule_enddate desc;"% (type, self.id,self.participant_email)
        self.env.cr.execute(sql)
        res_all = self.env.cr.fetchall()
        reccount = len(res_all)
        if(reccount >= repititionallowed):
            if(reccount > 0 and res_all is not None and res_all[0] is not None):
                if (res_all[0][1] is not None):
                    raise exceptions.ValidationError("You have already gone through the program %s, %s times which is the max times allowed and last time you attended is on  %s " % (self.programapplied.pgmschedule_programtype.programtype, reccount, res_all[0][1]))
            elif(reccount > 0):
                raise exceptions.ValidationError(
                    "You have already gone through the program %s, %s times which is the max times allowed" % (
                    self.programapplied.pgmschedule_programtype.programtype, reccount))
        else:
            num_months =0;
            if (reccount > 0 and res_all is not None and res_all[0] is not None):
                if (res_all[0][1] is not None):
                    enddate = res_all[0][1]
                    startdate = self.programapplied.pgmschedule_startdate
                    if(enddate < startdate):
                        temp = (startdate.year - enddate.year) * 12 + (startdate.month - enddate.month)
                        num_months = temp
                        print('months: ', num_months)

        if(self.programapplied.pgmschedule_gapbetweenprgm >= num_months ):
            if (reccount > 0 and res_all is not None and res_all[0] is not None):
                if(res_all[0][1] is not None):
                    raise exceptions.ValidationError("You have already gone through the program %s "% self.programapplied.pgmschedule_programtype.programtype+" on %s " %  res_all[0][1] +". "
                    "This program requires a gap of %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months before re-applying,"
                    " Pls re-apply on or after %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months from previous attendance")
            elif (reccount > 0):
                raise exceptions.ValidationError(
                    "You have already gone through the program %s " % self.programapplied.pgmschedule_programtype.programtype +
                    ".This program requires a gap of %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months before re-applying,"
                    " Pls re-apply on or after %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months from previous attendance")

    @api.depends('programapplied')
    def _compute_programdateflag(self):
        for rec in self:
            rec.show_change_pgm = False
            if rec.create_date == False:
                rec.show_change_pgm = False
            else:
                if (rec.programapplied.exists()):
                    if (rec.programapplied.pgmschedule_startdate < fields.Date.today() or 
                        rec.registration_status == 'Withdrawn' or 
                        rec.registration_status == 'Cancel Applied' or 
                        rec.registration_status == 'Cancel Approved' or 
                        rec.registration_status == 'Refunded' or 
                        rec.registration_status == 'Rejected' or 
                        rec.registration_status == 'Incomplete' or 
                        rec.registration_status == 'Change Of Participant' or 
                        rec.payment_status == 'Initiated'):
                        rec.show_change_pgm = False
                    else:
                        if (rec.ischangeofparticipant == True):
                            if (rec.registration_status == 'Paid' or rec.registration_status == 'Confirmed'):
                                rec.show_change_pgm = False
                            else:
                                rec.show_change_pgm = True
                        else:
                            rec.show_change_pgm = True
                else:
                    rec.show_change_pgm = False
            
    
    def checkCOP1(rec):
        try:
            if rec.create_date == False:
                return False
            else:
                if (rec.programapplied.exists()):
                    if (rec.programapplied.pgmschedule_startdate < fields.Date.today()):
                        return False
                    else:
                        if (rec.ischangeofparticipant == False):
                            if (rec.registration_status == 'Paid' or rec.registration_status == 'Confirmed'):
                                return True
                            else:
                                return False
                        else:
                            return False
                else:
                    return False
        except Exception as e:
            print(e)
            return False

    @api.depends('show_cop')
    def _compute_show_cop(self):
        try:
            for rec in self:
                if rec.create_date == False:
                    rec.show_cop=False
                else:
                    if (rec.programapplied.exists()):
                        if (rec.programapplied.pgmschedule_startdate < fields.Date.today()):
                            rec.show_cop=False
                        elif (((rec.programapplied.pgmschedule_startdate - (datetime.now()).date()).days) <= 7):
                            rec.show_cop=False
                        else:
                            if (rec.ischangeofparticipant == False):
                                if (rec.registration_status == 'Paid' or rec.registration_status == 'Confirmed'):
                                    rec.show_cop=True
                                else:
                                    rec.show_cop=False
                            else:
                                rec.show_cop=False
                    else:
                        rec.show_cop=False
        except Exception as e:
            print(e)

    #
    @api.depends('show_upgrade')
    def _compute_show_upgrade(self):
        try:
            for rec in self:
                rec.show_upgrade=self.env['megapgms.commonvalidations'].checkPackageUpgradeEligiblity(rec)
        except Exception as e:
            print(e)
            rec.show_upgrade=False

    #
    def _compute_call_public_url(self):
        """ Computes a public URL  """
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for rec in self:
            rec.public_url = urls.url_join(base_url, "megaprograms/registration/%s" % (rec.access_token))

    #def callReportRegistration(self):
    #    return self.env.ref('isha_megapgms.report_registration').report_action(self)

    def open_changeprogramdate(self):
        print('Open Change Program')
        print(self.id)
        if (self.payment_status == 'Initiated'):
            raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
        else:
            return {
                'name':'Change Program Date',
                'context':{'default_active_id':self.id,'default_participant_name':self.first_name,'default_programapplied':self.programapplied.id,'default_participant_programtype':self.participant_programtype.id},
                'view_type': 'form',
                'res_model': 'megapgms.registration.changeprogramdate',
                'view_id': False,
                'view_mode': 'form',
                'type': 'ir.actions.act_window',
                'target':'new'
            }

    def load_resend_mail(self):
        print('call method')
        return {
			'name':'Resend Email/SMS To Participant',  
			'context':{'default_active_id': self.id, 'default_participant_name': self.first_name },
			'view_type': 'form',
			'res_model': 'megapgms.registration.resendmail',
			'view_id': False,
			'view_mode': 'form',
			'type': 'ir.actions.act_window',
			'target':'new'
        }

    def initiate_upgrade(self):
        print('inside initiate_upgrade')
        if (self.payment_status == 'Initiated'):
            raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
        else:
            return {
                'name':'Package Upgrade',
                'context':{'default_active_id':self.id,'default_participant_name':self.first_name,'default_programapplied':self.programapplied.id,'default_package_cost': self.packageselection.pgmschedule_packagetype.packagecost},
                'view_type': 'form',
                'res_model': 'megapgms.registration.packageupgrade',
                'view_id': False,
                'view_mode': 'form',
                'type': 'ir.actions.act_window',
                'target':'new'
            }

    def initiate_cop(self):
        print('inside initiate_cop')
        reclist = self.env['megapgms.registration'].sudo().search([('id','=',self.id)])
        for rec in reclist:
            if (rec.show_cop):
                base_url = self.env['ir.config_parameter'].sudo().get_param('megapgms.ccavcallbackurl')
                cop_token = self._get_default_access_token()
                public_url = urls.url_join(base_url, "megaprograms/registration/changeofparticipant/%s" % (cop_token))
                rec.write({'cop_access_token':cop_token, 'cop_url': public_url})
                self.env['megapgms.notification'].sudo()._sendChangeOfParticipantEmailCore(rec)
                message = {
                        'type':'ir.actions.client',
                        'tag':'display_notification',
                        'params':
                        {
                            'title':_('Information!'),
                            'message': 'Change of participant request sent to participant',
                            'sticky':False
                        }
                    }
                return message
            else:
                raise exceptions.ValidationError("Change of participant is not allowed, pre-requisite not matched")


    def get_contact_dict(self, vals):

        return  {
            'name': vals['first_name']+ ' '+ vals['last_name'],
            'phone_country_code':vals['countrycode'] if 'countrycode' in vals else None,
            'phone': vals['phone'] if 'phone' in vals else None,
            'email':vals['participant_email'] if 'participant_email' in vals else None,
            'gender': isha_crm_importer.isha_crm.ContactProcessor.getgender(vals['gender']) if 'gender' in vals else None,
            'marital_status': isha_crm_importer.isha_crm.ContactProcessor.getMaritalStatus(vals['marital_status']) if 'marital_status' in vals else None,
            'dob': vals['dob'] if 'dob' in vals else None,
            'occupation': vals['occupation'] if 'occupation' in vals else None,
            'street': vals['address_line'] if 'address_line' in vals else None,
            'city':vals['city'] if 'city' in vals else None,
            'state_id': vals['state'] if 'state' in vals else None,
            'state': self.env['res.country.state'].sudo().browse(vals['state']).name if 'state' in vals and vals['state'] else None,
            'zip': vals['pincode'] if 'pincode' in vals else None,
            'country_id': vals['country'] if 'country' in vals else None,
            'country':self.env['res.country'].sudo().browse(vals['country']).code if 'country' in vals and vals['country'] else None,
            'nationality_id':vals['nationality_id'] if 'nationality_id' in vals else None,
            'nationality': self.env['res.country'].sudo().browse(vals['nationality_id']).code if 'nationality_id' in vals and vals['nationality_id'] else None,
        }



    @api.model
    def create(self, vals):
        _logger.info(str(self.env.uid))
        contact_rec = self.env['res.partner'].sudo().create( self.get_contact_dict(vals) )
        vals['contact_id_fkey'] = contact_rec.id
        vals['last_modified_dt'] = datetime.now()
        reference = self.env['ir.config_parameter'].sudo().get_param('megapgms.referenceid')
        reference = int(reference)

        vals['reference_id'] = datetime.now().strftime("%d-%m-%Y") + "-00" + str(reference);
        reference = reference + 1;
        self.env['ir.config_parameter'].sudo().set_param('megapgms.referenceid', reference)

        rec = super(MegapgmsRegistration, self).create(vals)
        self.env['megapgms.notification'].sudo()._sendRegistrationEmailSMSCore(rec)
        # print( vals['reference_id'])

        ha_preapprove_status = True

        for each_field in vals:
            field_name = each_field
            if field_name.startswith("is_disease"):
                field_value = vals[field_name]
                if field_value:
                    ha_preapprove_status = False
                    break

        #rec.ha_status = ha_preapprove_status
        print('Preapprove Status',ha_preapprove_status)
        return rec

    def load_print(self):
        self.sendPaymentLinkEmail()
       
    def load_print1(self):
        _logger.info('************************ load print is called ************************ ')
        print('report')
        #return self.env.ref('isha_megapgms.report_registration').report_action(self)
        #self.statusUpdateandSendPaymentLinkEmail()
        #self.env['megapgms.cleanup'].sudo().cleanup()
        '''rec = self.env['megapgms.registration'].sudo().search([('id','=',5)])
        source = self.env['megapgms.registration'].sudo().search([('id','=',4)])
        
        refund_data = self.env['megapgms.payments']._calculateRefundAmount(source, rec.changeofparticipant_applied_dt)

        print(refund_data)'''

        list = self.env['megapgms.registration'].sudo().search([('id','=',4)])

        for rec in list:
            rec.write({ 'ha_status': 'Medical Approved', 'registration_status': 'Confirmed',
                    'paymentlink_email_send': False,
					'paymentlink_sms_send' : False
                    })
        
        '''rec.write({'ereceiptgenerated': False})
        rec.write({'registration_status': 'Confirmed'})
        rec.write({'amount_paid': 10})
        rec.write({ 'transferred_amount': 0, 'payment_status': 'Success', 'ha_status': 'Medical Approved'})
        '''
        #rec.write({'registration_status': 'Confirmed', 'payment_status': 'Success', 'pushedtocico': True })
        #'amount_paid': 100, 'date_paid': datetime.now()})
        #rec.write({'registration_status': 'Change Of Participant'})
        # rec.write({ 'ischangeofparticipant': True,
        #             'changeofparticipant_applied_dt': datetime.now(),
        #             'changeofparticipant_ref': 1 })
        #rec.write({'payment_status': ''})
        #return self.env.ref('isha_megapgms.report_registration').report_action(self)

    def call_all_jobs(self):
        for rec in self:
            print(rec.public_url)
        self.checkPaymentStatusForPendingRecords()
        #self.checkAndGenerateEreceipt()
        #self.updateAndSendProgramConfirmation()
        #self.env['megapgms.cicoapi']._pushRegistrationsToCICO()
        #self.sendWaitingListScheduledProgram()
        #self.sendRegistrationEmail()
        #self.statusUpdateandSendPaymentLinkEmail()
        #self.sendRegistrationHAAdditionalInfoEmail()
        #self.sendRegistrationRejectionEmail()
	
    def changetrigger(self):
        print('trigger')


    #
    def checkPaymentStatusForPendingRecords(self):
        self.env['megapgms.paymentstatus'].sudo()._checkPendingPaymentStatus()
	
    #
    
    def checkAndGenerateEreceipt(self):
        self.env['megapgms.paymentstatus'].sudo()._checkAndGenerateEreceipt()
	
    #

    #    
    def updateAndSendProgramConfirmation(self):
        self.env['megapgms.notification'].sudo()._updateAndSendProgramConfirmation()
	
    def sendCancelAppliedConfirmation(self):
        self.env['megapgms.notification'].sudo()._sendCancelAppliedConfirmation()
	
    def sendCOPAppliedConfirmation(self):
        self.env['megapgms.notification'].sudo()._sendCOPAppliedConfirmation()
	
    #
    def statusUpdateandSendPaymentLinkEmail(self):
        foundrec = 0
        rejlist = self.env['megapgms.registration'].sudo().search([('ha_status','=','Medical Approved'),('registration_status','=','Pending')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            try:
                self.statusUpdateandSendPaymentLinkEmailCore(rec)
            except Exception as e:
                print(e)                

        if (foundrec == 1):
            print('registration status update completed, calling email and sms program')
        else:
            print('no records found to update registration status, calling email and sms program')
        self.sendPaymentLinkEmail()

    #
    def statusUpdateandSendPaymentLinkEmailCore(self, rec):
        print('inside statusUpdateandSendPaymentLinkEmailCore')
        auto_approval_comments = ''

        #
					
        if (rec.country.name != 'India' or rec.nationality_id.name != 'India'):
            print('checking overseas')
            if (rec.oco_status == 'Pending' and rec.oco_approval_overide == False):
                print('oco status is pending and override is not selected')
                return
            elif (rec.oco_status != 'Approved' and rec.oco_approval_overide == False):
                auto_approval_comments = 'Rejected due to oco rejection'
                rec.write({
                    'registration_status': 'Rejection Review',
                    'auto_approval_comments': auto_approval_comments,
                    'last_modified_dt': datetime.now()
                })
                print('oco rejection review marked')
                return
        
        #   

        if (rec.bl_status == 'Pending' and rec.blacklisted_overide == False):
            print('waiting for bl status')
            return
        elif (rec.bl_status != 'Verified' and rec.blacklisted_overide == False):
            auto_approval_comments = 'Rejected due to bl rejection'
            rec.write({
                'registration_status': 'Rejection Review',
                'auto_approval_comments': auto_approval_comments,
                'last_modified_dt': datetime.now()
            })
            print('bl rejection review marked')
            return

        #

        if (rec.prerequisite_pgm != 'Matched' and rec.prerequisite_pgm_overide == False):
            print('pre request not matched marking rejection review')
            auto_approval_comments = 'Rejected due to pre-requisite not matched'
            rec.write({
                'registration_status':  'Rejection Review',
                'auto_approval_comments': auto_approval_comments,
                'last_modified_dt': datetime.now()
            })
            return

        #
        print('checking for seat avail')

        if (rec.use_emrg_quota == True):
            emergency_seatscount = (rec.packageselection.pgmschedule_packageemergencyseats - rec.packageselection.pgmschedule_emergencyseatspaid)
            if (emergency_seatscount > 0):
                rec.write({
                    'registration_status': 'Approved',
                    'last_modified_dt': datetime.now(),
                    'auto_approval_comments': 'Seats approved from emergency quota',
                    'paymentlink_email_send': False,
					'paymentlink_sms_send' : False
                })
                return
            else:
                rec.write({
                    'registration_status': 'Rejection Review',
                    'auto_approval_comments': 'Rejected due to seats not available',
                    'last_modified_dt': datetime.now()
                })
                print('seats not available rejection review marked')
                return
        else:
            seatcount = rec.packageselection.pgmschedule_packagenoofseats - rec.packageselection.pgmschedule_regularseatspaid
            if (seatcount > 0):
                rec.write({
                    'registration_status': 'Approved',
                    'last_modified_dt': datetime.now(),
                    'paymentlink_email_send': False,
					'paymentlink_sms_send' : False
                })
                return
            else:
                rec.write({
                    'registration_status': 'Rejection Review',
                    'auto_approval_comments': 'Rejected due to seats not available',
                    'last_modified_dt': datetime.now()
                })
                print('seats not available rejection review marked')
                return
        
        #

    #
    def sendPaymentLinkEmail(self):
        foundrec = 0
        rejlist = self.env['megapgms.registration'].sudo().search([('registration_status','=','Approved'),
        '|',('paymentlink_email_send','=',False),('paymentlink_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['megapgms.notification'].sudo()._sendPaymentLinkEmailCore(rec)
        if foundrec == 1:
            print('paymentlink: sending email and sms completed for all request')
        else:
            print('paymentlink: no records found for sending email and sms..')

    #
    def sendRegistrationEmail(self):
        foundrec = 0
        rejlist = self.env['megapgms.registration'].sudo().search([('registration_status','=','Pending'),
        '|',('registration_email_send','=',False),('registration_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['megapgms.notification'].sudo()._sendRegistrationEmailSMSCore(rec)
        if foundrec == 1:
            print('registrations: sending email and sms completed for all request')
        else:
            print('registrations: no records found for sending email and sms..')

    #
    def sendPartialRegistrationEmail(self):
        foundrec = 0
        rejlist = self.env['megapgms.registration'].sudo().search([('registration_status','=','Incomplete'),
        '|',('incompleteregistration_email_send','=',False),('incompleteregistration_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        _logger.info("Inside sendPartialRegistrationEmail")
        _logger.info(str(rejlist))
        for rec in rejlist:
            foundrec = 1
            self.env['megapgms.notification'].sudo()._sendRegistrationPartiallyCompletedFormEmailSMS(rec)
        if foundrec == 1:
            print('partial registrations: sending email and sms completed for all request')
            _logger.info('partial registrations: sending email and sms completed for all request')
        else:
            print('partial registrations: no records found for sending email and sms..')
            _logger.info('partial registrations: no records found for sending email and sms..')

    def sendWaitingListScheduledProgram(self):
        self.env['megapgms.notification'].sudo()._sendWaitingListScheduledProgram()
    #    
    def sendRegistrationRejectionEmail(self):
        foundrec = 0
        rejlist = self.env['megapgms.registration'].sudo().search([('registration_status','=','Rejected'),
         '|',('rejection_email_send','=',False),('rejection_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['megapgms.notification'].sudo()._sendRegistrationRejectionEmailCore(rec)
        if foundrec == 1:
            print('rejections: sending email and sms completed for all request')
        else:
            print('rejections: no records found for sending email and sms..')

    #
    def sendRegistrationHAAdditionalInfoEmail(self):
        foundrec = 0
        rejlist = self.env['megapgms.registration'].sudo().search([('ha_status','=','Request More Info'),
        '|',('harequest_email_send','=',False),('harequest_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['megapgms.notification'].sudo()._sendRegistrationHAAdditionalInfoEmailCore(rec)
        if foundrec == 1:
            print('HA addinfo: sending email and sms completed for all request')
        else:
            print('HA addinfo: no records found for sending email and sms..')

    def onchange_package(self,val):
        print('package')
        print(self.id)

    def getProgramAttendance(self, contact_id, pgm_category = None):
        pgm_recs = self.env['program.attendance'].getProgramAttendance(contact_id, pgm_category)
        print("pgm-recs", pgm_recs)
        return pgm_recs


    # @api.onchange('programapplied')
    # def programapplied_onchange(self):
    #     print('on change program')
    #     recs = self.env['megapgms.program.schedule.roo                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        mdata'].search([('pgmschedule_programname','=',self.programapplied.id)])
    #     print(recs)

class ProgramHistory(models.Model):
    _name = 'mega.program.history'
    _description = 'Program History'
    #_order = 'packagecode'
    #_rec_name = 'packagecode'
    #_sql_constraints = [('unique_pkgparameter_packagecode','unique(programtype, packagecode)','Cannot have duplicate package code, give different code')]
    # program history

    programregistration = fields.Many2one('megapgms.registration', string='program registration')
  #   programname = fields.Many2one('mega.program.type', string='Program Type', required=True, domain=['&',('lob','=','IPC'),('displaypgmhistory','=',True)])
    programname = fields.Many2one('master.program.category', string='Program Type', required=True)
    pgmdate = fields.Date(string='Program Date',required=True)
    teachername = fields.Char(string='Teacher name', size=100, required=False)
    pgmcountry = fields.Many2one('res.country', string = 'Program Country', required=True)
    pgmstate = fields.Many2one('res.country.state', 'State/province')
    pgmlocation = fields.Many2one('isha.center', string = 'Program Location', required=True)
    # domain=['lob','=','IPC'])


class IdproofList(models.Model):
    _name = "idproof.list"
    _description = "List of Id proofs accepted by IYC"
    _rec_name = 'idproof_type'
#    _sql_constraints = ['unique_idproof','unique(idproof_type)','Id proof type given already exists']

    idproof_type = fields.Char(string='Id proof name')

class MasterLob(models.Model):
    _name = 'master.lob'
    _description = "Master list of line of businesses"
    _rec_name = 'lobname'

    lobname = fields.Char(string='Lob Name', required=False)

class TeacherNames(models.Model):
    _name = 'mega.program.teachernames'
    _description = 'Program teacher names'
    _rec_name = 'teachername'
    _sql_constraints = [('unique_teacher_name','unique(teacherlob,teachername)','Cannot have duplicate teacher name in same LOB pls modify the name')]

    teachername = fields.Char(string='Teacher name', size=100, required=True)
    teacherlob = fields.Many2one('master.lob')
    pgms_trained = fields.Many2one('mega.program.type',string = 'Program Type')
#
# class MasterRegions(models.Model):
#     _name = 'master.regions'
#     _description = "Master list of regions"
#     _rec_name = 'regionname'
#
#     regionname = fields.Char(string='Region Name', required=True)
#     countryname = fields.Many2one('res.country')
#     lobname = fields.Many2one('master.lob')
#
# class MasterCenters(models.Model):
#     _name = 'master.centers'
#     _description = "Master list of program centers"
#     _rec_name = 'centername'
#
#     centername = fields.Char(string='Center Name',required=True)
#     regionname = fields.Many2one('master.regions','regionname')
#     countryname = fields.Many2one('res.country')
#     statename = fields.Many2one('res.country.state')
#     lob = fields.Many2one('master.lob')
#

class idproof_attach(models.Model):
    _name = "idproof.attachment"
    _description = "ID Proof Attachment"
    idproof_id = fields.Many2one('res.partner', string='idproof_attach')
    attached_file = fields.Many2many(comodel_name="ir.attachment",
                                     relation="m2m_ir_attached_file_rel",
                                     column1="m2m_id", column2="attachment_id", string="File Attachment",
                                     required=True)
    idproof_document_type = fields.Many2one('documenttype', string="ID Type", required=True)
    # Removing as per the UAT feedback
    idproof_file = fields.Char(string="File Name", required=True)
    active = fields.Boolean('Active', default=True)

    state = fields.Boolean('State', default=True)

    @api.onchange('attached_file')
    def limit_attached_file(self):
        attachement = len(self.attached_file)
        if attachement > 1:
            raise ValidationError(_('You can add only 2 files per ID Proof'))


    # self.packagetotalseats = (self.packagenoofseats + self.packageemergencyseats)

class DocumentType(models.Model):
    _name = "documenttype"
    _description = "document_type"

    name = fields.Char(string="Document Type Name", required=True)
    active = fields.Boolean('Active', default=True)


class RequestAttachment(models.Model):
    _name = "megapgm.request.attachment"
    _description = "Generic Request Attachment"

    generic_request_id = fields.Many2one('megapgms.registration', string='attachment')
    attached_file = fields.Many2many(comodel_name="ir.attachment",
                                     relation="megapgm_m2m_ir_attached_file_rel",
                                     column1="request_m2m_id", column2="attachment_id", string="Attachment(s)", required=True)
    attachment_type_id = fields.Many2one('documenttype', string="Attachment Type", required=True)
    active = fields.Boolean('Active', default=True)

    # @api.onchange('attached_file')
    # def limit_attached_file(self):
    #     attachement = len(self.attached_file)
    #     if attachement > 1:
    #         raise ValidationError(_('You can add only 2 files per ID Proof'))

class MegapgmsParticipantQuestionBank(models.Model):
    _name = "megapgms.participant.qa"
    _description = "Participant HA Question and Answers"
    _rec_name = 'question_bank'
    _sql_constraints = [('unique_participant_qa','unique(first_name,question_bank)','Cannot have duplicate question, give different data')]


    first_name = fields.Many2one('megapgms.registration',string = 'Participant Name')
    question_bank = fields.Many2one('megapgms.questionbank', string='Questions')
    answer_text = fields.Text(string='Answer')

class MegapgmsParticipantHAReports(models.Model):
    _name = 'megapgms.participant.hadocs'
    description = 'Mega pgms Participant HA Reports'
    _rec_name = 'ha_attachment'

    participant = fields.Many2one('megapgms.registration',string = 'Participant Name')
    ha_attachment = fields.Binary(string="HA Report",)
    ha_filename = fields.Char(string='HA Report')

class PaymentTransaction(models.Model):
    _name = "megapgms.paymenttransaction"
    _description = "Mega programs Payment Transaction"
    
    programregistration = fields.Many2one('megapgms.registration', string='program registration')
    first_name = fields.Char(related="programregistration.first_name",store=False)
    last_name = fields.Char(related="programregistration.last_name",store=False)
    programtype = fields.Char(related="programregistration.programapplied.pgmschedule_programtype.programtype",store=False)
    programname = fields.Char(related="programregistration.programapplied.pgmschedule_programname",store=False)
    # ereceiptgenerated = fields.Boolean(related="programregistration.ereceiptgenerated",store=False)
    # ereceiptreference = fields.Char(related="programregistration.ereceiptreference",store=False)
    # ereceipturl = fields.Char(related="programregistration.ereceipturl",store=False)
    ereceiptgenerated = fields.Boolean(default=False)
    ereceiptreference = fields.Char(string='Receipt No', size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=500)
    ereceiptgenerated_dt = fields.Datetime(string="Receipt No Raised Date")
    orderid = fields.Char(string='Transaction ID', size=80)
    billing_name = fields.Char(string='Participant Name', size=60)
    billing_address = fields.Char(string='Address', size=150)
    billing_city = fields.Char(string='City', size=30)
    billing_state = fields.Char(string='State', size=30)
    billing_zip = fields.Char(string='Zip', size=15)
    billing_country = fields.Char(string='Country', size=50)
    billing_tel = fields.Char(string='Mobile Number', size=20)
    billing_email = fields.Char(string='Email', size=70)
    payment_status = fields.Char(string='Payment Status', size=100)
    amount = fields.Float(string = 'Amount')
    comments = fields.Char(string='Comments', size=200)
    transactiontype = fields.Char(string='Transaction Type', size=50)
    paymenttrackingid = fields.Char(string='PaymentTracking', size=80, default='0')
    transaction_dt = fields.Datetime(string="Transaction Date", compute="_compute_transaction_dt", store=True)
    ebook = fields.Char(string='E-Book', compute="_compute_ebook", store=True)
    status = fields.Char(string='Status', compute="_compute_status", store=True)
    program_purpose = fields.Char(string='Purpose_StatDate_EndDate_ProgramPlace', compute="_compute_programpurpose", store=True)
    gateway = fields.Char(string='Gateway', compute="_compute_gateway", store=True)
    billing_tel1 = fields.Char(string='Phone No1', size=20)
    billing_tel2 = fields.Char(string='Phone No2', size=20)
    billing_pan = fields.Char(string='PAN Number', size=20)
    pre_regid = fields.Char(string='Pre Reg Id', size=20)
    #nooftrees = fields.Char(string='noOfTrees', size=20)
    #recurringdonations = fields.Char(string='recurringDonation', size=20)

    @api.depends('payment_status')
    def _compute_transaction_dt(self):
        for rec in self:
            if rec.create_date != False:
                rec.transaction_dt = rec.create_date
            else:
                rec.transaction_dt = False
    
    @api.depends('ereceiptreference')
    def _compute_ebook(self):
        for rec in self:
            if rec.ereceiptreference != None and rec.ereceiptreference != '' and rec.ereceiptreference != False:
                rec.ebook = rec.ereceiptreference[5:8]
            else:
                rec.ebook = ''
    
    @api.depends('payment_status')
    def _compute_status(self):
        for rec in self:
            if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                rec.status = 'Confirmed'
            else:
                rec.status = ''
    
    @api.depends('payment_status')
    def _compute_programpurpose(self):
        try:
            for rec in self:
                if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                    rec.program_purpose = rec.programregistration.programapplied.pgmschedule_programname + ' - ' + rec.programregistration.programapplied.pgmlocation.centername
                else:
                    rec.program_purpose = ''
        except:
            rec.program_purpose = ''

    @api.depends('payment_status')
    def _compute_gateway(self):
        for rec in self:
            if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                rec.gateway = 'ccavenue'
            else:
                rec.gateway = ''

    
    @api.model
    def fields_get(self, fields=None):
        fields_notto_show = ['programregistration']
        res = super(PaymentTransaction, self).fields_get()
        for field in res:
            if (field in fields_notto_show):
                res[field]['selectable'] = False
                res[field]['sortable'] = False
            else:
                res[field]['selectable'] = True	
                res[field]['sortable'] = True
                
        return res

class MegapgmRegistrationTransactions(models.Model):
    _name = "megapgms.registration.transactions"
    _description = "Mega Program Registration Transactions"

    programregistration = fields.Many2one('megapgms.registration', string='program registration')
    transactiontype = fields.Char(string='Transaction Type', size=50)
    upgrade_url = fields.Char(string='Package Upgrade URL')
    upgrade_access_token = fields.Char(string='Package Upgrade Access Token')
    upgrade_packageselection = fields.Many2one('megapgms.program.schedule.roomdata',string='Upgrade Package Selected', required=False)
    upgrade_applied = fields.Boolean(default=False)
    upgrade_applied_dt = fields.Datetime(string="Package Upgrade Applied DT")
    upgrade_email_send = fields.Boolean(default=False)
    upgrade_sms_send = fields.Boolean(default=False)
    isactive = fields.Boolean(default=True)
    upgrade_use_emrg_quota = fields.Boolean(default=False)
    comments = fields.Char(string='Comments')
    last_modified_dt = fields.Datetime(string="Last Modified DT")
    old_packageselection = fields.Many2one('megapgms.program.schedule.roomdata',string='Old Package Selected', required=False)
    upgrade_completed = fields.Boolean(default=False)
    upgrade_completed_email_send = fields.Boolean(default=False)
    upgrade_completed_sms_send = fields.Boolean(default=False)
    seats_block_days =  fields.Integer(compute="_compute_seats_block_days")

    def sendPackageUpgradeRequestSMSEmail(self):
        plist = self.env['megapgms.registration.transactions'].sudo().search([('isactive','=',True),('upgrade_email_send','=',False)])
        for rec in plist:
            self.env['megapgms.notification'].sudo()._sendRegistrationPackageUpgradeEmailSMSSend(rec)

    def sendPackageUpgradeConfirmation(self):
        plist = self.env['megapgms.registration.transactions'].sudo().search([('upgrade_completed','=',True),('upgrade_completed_email_send','=',False)])
        for rec in plist:
            self.env['megapgms.notification'].sudo()._sendPackageUpgradeConfirmation(rec)

    def cancelPackageUpgradeRequest(self):
        print('cancelPackageUpgradeRequest')
        self.env['megapgms.paymentstatus'].sudo()._checkPackageUpgradePendingAndUnblockSeats()

    @api.depends('upgrade_applied')
    def _compute_seats_block_days(self):
        try:
            seats_block_days = int(self.env['ir.config_parameter'].sudo().get_param('megapgms.seats_block_days'))
            if (seats_block_days == False or seats_block_days == None or seats_block_days == '' or seats_block_days == 0):
                seats_block_days = 15
            for rec in self:
                rec.seats_block_days=seats_block_days
        except Exception as e:
            print(e)
