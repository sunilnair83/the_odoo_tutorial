from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime

class MegaProgramMatches(models.TransientModel):
	_name = 'megaprogram.matches'
	_description = 'MegaProgram Matches'

	programregistration = fields.Many2one('megapgms.registration', string='program registration')
	programname = fields.Many2one('mega.program.type', string='Program Type', required=True)
	pgmdate = fields.Date(string='Program Date',required=True)
	teachername = fields.Many2one('mega.program.teachernames',string = 'Teacher Name', required=False)
	pgmcountry = fields.Many2one('res.country', string = 'Program Country', required=True)
	pgmstate = fields.Many2one('res.country.state', 'State/province')
	pgmlocation = fields.Many2one('isha.center', string = 'Program Location', required=True)

	def submit_selection(self):
		print('submit clicked')