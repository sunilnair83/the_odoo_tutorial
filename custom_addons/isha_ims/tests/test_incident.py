import logging

from odoo import exceptions
from odoo.osv import expression

from .common import IncidentCase, freeze_time
from ..models.incident_incident import html2text

_logger = logging.getLogger(__name__)


class TestIncidentBase(IncidentCase):
    """Test incident base
    """

    def test_090_type_counters(self):
        access_type = self.env.ref(
            'isha_ims.incident_type_access')
        self.assertEqual(access_type.stage_count, 4)
        self.assertEqual(access_type.route_count, 3)

    def test_100_stage_previous_stage_ids(self):
        self.assertEqual(
            self.stage_draft.previous_stage_ids,
            self.stage_rejected)
        self.assertEqual(
            self.stage_sent.previous_stage_ids,
            self.stage_draft)
        self.assertEqual(
            self.stage_confirmed.previous_stage_ids,
            self.stage_sent)
        self.assertEqual(
            self.stage_rejected.previous_stage_ids,
            self.stage_sent)

    def test_110_stage_route_display_name_noname(self):
        self.route_draft_to_sent.name = False
        self.non_ascii_route_draft_to_sent.name = False

        self.assertEqual(
            self.route_draft_to_sent.display_name,
            "Draft -> Sent")

        self.assertEqual(
            self.non_ascii_route_draft_to_sent.display_name,
            u"Чорновик -> Відправлено")

    def test_115_stage_route_display_name_name(self):
        self.assertEqual(
            self.route_draft_to_sent.display_name,
            "Draft -> Sent [Send]")

    def test_117_stage_route_display_name_name_only(self):
        self.assertEqual(
            self.route_draft_to_sent.with_context(name_only=True).display_name,
            "Send")

    def test_117_stage_route_display_name_name_only_no_name(self):
        self.route_draft_to_sent.name = False
        self.assertEqual(
            self.route_draft_to_sent.with_context(name_only=True).display_name,
            "Draft -> Sent")

    def test_120_route_ensure_route__draft_sent(self):
        Route = self.env['incident.stage.route']
        route = Route.ensure_route(
            self.incident_1, self.stage_sent.id)
        self.assertTrue(route)
        self.assertEqual(len(route), 1)

    def test_125_route_ensure_route__draft_confirmed(self):
        Route = self.env['incident.stage.route']

        with self.assertRaises(exceptions.ValidationError):
            Route.ensure_route(self.incident_1, self.stage_confirmed.id)

    def test_130_incident_create_simple(self):
        Incident = self.env['incident.incident']

        incident = Incident.create({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'incident_text': 'Incident Text',
        })

        self.assertTrue(incident.name.startswith('Req-'))
        self.assertEqual(incident.stage_id, self.stage_draft)

    def test_135_incident_create_and_assign(self):
        Incident = self.env['incident.incident']

        incident = Incident.create({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'incident_text': 'Incident Text',
            'user_id': self.incident_manager.id,
        })

        self.assertEqual(incident.user_id, self.incident_manager)
        self.assertTrue(incident.date_assigned)

    def test_140_incident_write_stage_sent(self):
        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        self.incident_1.write({'stage_id': self.stage_sent.id})

        self.assertEqual(self.incident_1.stage_id, self.stage_sent)

    def test_145_incident_write_stage_confirmed(self):
        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        with self.assertRaises(exceptions.ValidationError):
            self.incident_1.write({'stage_id': self.stage_confirmed.id})

    def test_150_incident_type_sequence(self):
        Incident = self.env['incident.incident']

        incident = Incident.create({
            'type_id': self.sequence_type.id,
            'category_id': self.resource_category.id,
            'incident_text': 'Incident Text',
        })

        self.assertTrue(incident.name.startswith('RSR-'))
        self.assertEqual(incident.stage_id, self.stage_new)

    def test_155_incident__type_changed(self):
        Incident = self.env['incident.incident']

        # Create new (empty) incident
        incident = Incident.new({})
        self.assertFalse(incident.stage_id)
        self.assertFalse(incident.type_id)

        # Run onchange type_id:
        incident.onchange_type_id()
        self.assertFalse(incident.stage_id)
        self.assertFalse(incident.type_id)

        incident.type_id = self.simple_type
        self.assertFalse(incident.stage_id)
        self.assertEqual(incident.type_id, self.simple_type)

        incident.onchange_type_id()
        self.assertEqual(incident.stage_id, self.stage_draft)
        self.assertEqual(incident.type_id, self.simple_type)

    def test_157_incident__type_changed_restricts_category(self):
        Incident = self.env['incident.incident']

        category_tech = self.env.ref(
            'isha_ims.incident_category_demo_technical_configuration')

        # Create new (empty) incident
        incident = Incident.new({})
        self.assertFalse(incident.stage_id)
        self.assertFalse(incident.type_id)

        incident.onchange_type_id()

        res = incident._onchange_category_type()
        self.assertFalse(incident.stage_id)
        self.assertFalse(incident.type_id)
        self.assertFalse(incident.category_id)

        # This domain have to disallow category selection when incident type is
        # not selected
        self.assertEqual(res['domain']['category_id'], expression.FALSE_DOMAIN)

        # Choose incident type 'Grant Access'
        incident.type_id = self.access_type
        incident.onchange_type_id()
        self.assertEqual(incident.stage_id, self.access_type.start_stage_id)

        res = incident._onchange_category_type()
        self.assertFalse(incident.category_id)
        self.assertEqual(
            res['domain']['category_id'],
            [('incident_type_ids', '=', self.access_type.id)])

        # Choose category Demo / Technical / Configuration
        self.assertIn(self.access_type, category_tech.incident_type_ids)
        incident.category_id = category_tech

        # Change incident type to *Запит на встановлення ОС* which have same
        # related categories, and ensure, that category not changed, but domain
        # changed
        incident.type_id = self.non_ascii_type

        incident.onchange_type_id()
        self.assertEqual(incident.stage_id, self.non_ascii_type.start_stage_id)

        res = incident._onchange_category_type()
        self.assertTrue(incident.category_id)
        self.assertEqual(
            res['domain']['category_id'],
            [('incident_type_ids', '=', self.non_ascii_type.id)])
        self.assertIn(self.non_ascii_type, category_tech.incident_type_ids)
        self.assertEqual(incident.category_id, category_tech)

        # Change reqyest type to *Simple incident*, which have different
        # categories available, and ensure, that category set to null.
        incident.type_id = self.simple_type

        incident.onchange_type_id()
        self.assertEqual(incident.stage_id, self.simple_type.start_stage_id)

        res = incident._onchange_category_type()
        self.assertFalse(incident.category_id)
        self.assertEqual(
            res['domain']['category_id'],
            [('incident_type_ids', '=', self.simple_type.id)])
        self.assertNotIn(self.simple_type, category_tech.incident_type_ids)

    def test_160_incident_category_display_name(self):

        self.assertEqual(
            self.tec_configuration_category.display_name,
            u"Demo / Technical / Configuration")

    def test_170_incident_type_category_change(self):
        incident = self.env['incident.incident'].create({
            'type_id': self.simple_type.id,
            'category_id': self.resource_category.id,
            'incident_text': 'test',
        })

        with self.assertRaises(exceptions.ValidationError):
            incident.write({'type_id': self.sequence_type.id})

        # Change category
        incident.write({'category_id': self.tec_configuration_category.id})
        last_event = incident.incident_event_ids.sorted()[0]
        self.assertEqual(last_event.event_code, 'category-changed')
        self.assertEqual(last_event.old_category_id, self.resource_category)
        self.assertEqual(
            last_event.new_category_id, self.tec_configuration_category)

    def test_180_incident_close_via_wizard(self):
        incident = self.env.ref(
            'isha_ims.incident_incident_type_sequence_demo_1')
        incident.stage_id = self.env.ref(
            'isha_ims.incident_stage_type_sequence_sent')

        close_stage = self.env.ref(
            'isha_ims.incident_stage_type_sequence_closed')
        close_route = self.env.ref(
            'isha_ims.incident_stage_route_type_sequence_sent_to_closed')

        incident.response_text = 'test response 1'

        incident_closing = self.env['incident.wizard.close'].create({
            'incident_id': incident.id,
            'close_route_id': close_route.id,
        })
        incident_closing.onchange_incident_id()
        self.assertEqual(incident_closing.response_text, incident.response_text)
        self.assertEqual(incident_closing.response_text,
                         '<p>test response 1</p>')

        incident_closing.close_route_id = close_route
        incident_closing.response_text = 'test response 42'
        incident_closing.action_close_incident()

        self.assertEqual(incident.stage_id, close_stage)
        self.assertEqual(incident.response_text, '<p>test response 42</p>')

    def test_incident_html2text(self):
        self.assertEqual(html2text(False), "")
        self.assertEqual(html2text(None), "")
        self.assertEqual(
            html2text("<h1>Test</h1>").strip(), "# Test")

    def test_190_type_default_stages(self):
        type_default_stages = self.env['incident.type'].with_context(
            create_default_stages=True).create({
                'name': "test-default-stages",
                'code': "test-default-stages",
            })
        self.assertEqual(type_default_stages.route_count, 1)
        self.assertEqual(
            type_default_stages.route_ids.stage_from_id.name, 'New')
        self.assertEqual(
            type_default_stages.route_ids.stage_to_id.name, 'Closed')

    def test_200_type_no_default_stages(self):
        type_no_default_stages = self.env['incident.type'].create({
            'name': "test-no-default-stages",
            'code': "test-no-default-stages",
        })
        self.assertEqual(type_no_default_stages.route_count, 0)
        self.assertEqual(len(type_no_default_stages.stage_ids), 0)

        with self.assertRaises(exceptions.ValidationError):
            self.env['incident.incident'].create({
                'type_id': type_no_default_stages.id,
                'incident_text': 'test',
            })

    def test_210_test_kaban_readonly_fields(self):
        res = self.env['incident.incident'].fields_view_get(view_type='kanban')
        self.assertTrue(res['fields']['type_id']['readonly'])
        self.assertTrue(res['fields']['stage_id']['readonly'])
        self.assertTrue(res['fields']['category_id']['readonly'])

    def test_220_delete_stage_with_routes(self):
        with self.assertRaises(exceptions.ValidationError):
            self.stage_confirmed.unlink()

    def test_230_delete_stage_without_routes(self):
        stage = self.env['incident.stage'].create({
            'name': 'Test',
            'code': 'test',
            'incident_type_id': self.simple_type.id,
        })
        stage.unlink()  # no errors raised

    def test_240_incident_events(self):
        with freeze_time('2018-07-09'):
            incident = self.env['incident.incident'].create({
                'type_id': self.simple_type.id,
                'incident_text': 'Test',
            })
            self.assertEqual(incident.incident_event_count, 1)
            self.assertEqual(
                incident.incident_event_ids.event_type_id.code, 'created')

        with freeze_time('2018-07-25'):
            # Change incident text
            incident.incident_text = 'Test 42'

            # Refresh cache. This is required to read incident_event_ids in
            # correct order
            incident.refresh()

            # Check that new event generated
            self.assertEqual(incident.incident_event_count, 2)
            self.assertEqual(
                incident.incident_event_ids[0].event_type_id.code, 'changed')
            self.assertEqual(
                incident.incident_event_ids[0].old_text, '<p>Test</p>')
            self.assertEqual(
                incident.incident_event_ids[0].new_text, '<p>Test 42</p>')

            # Test autovacuum of events (by default 90 days old)
            # No events removed, date not changed
            cron_job = self.env.ref(
                'isha_ims.ir_cron_incident_vacuum_events')
            cron_job.method_direct_trigger()
            self.assertEqual(incident.incident_event_count, 2)

        with freeze_time('2018-08-09'):
            # Test autovacuum of events (by default 90 days old)
            # No events removed, events not older that 90 days
            cron_job = self.env.ref(
                'isha_ims.ir_cron_incident_vacuum_events')
            cron_job.method_direct_trigger()
            self.assertEqual(incident.incident_event_count, 2)

        with freeze_time('2018-10-19'):
            # Test autovacuum of events (by default 90 days old)
            # One events removed, created event is older that 90 days
            cron_job = self.env.ref(
                'isha_ims.ir_cron_incident_vacuum_events')
            cron_job.method_direct_trigger()
            self.assertEqual(incident.incident_event_count, 1)
            self.assertEqual(
                incident.incident_event_ids.event_type_id.code, 'changed')
            self.assertEqual(
                incident.incident_event_ids.old_text, '<p>Test</p>')
            self.assertEqual(
                incident.incident_event_ids.new_text, '<p>Test 42</p>')

        incident.created_by_id.company_id.incident_event_auto_remove = False

        with freeze_time('2018-10-27'):
            # Test autovacuum of events (by default 90 days old)
            # All events removed, all events older that 90 days
            cron_job = self.env.ref(
                'isha_ims.ir_cron_incident_vacuum_events')
            cron_job.method_direct_trigger()
            self.assertEqual(incident.incident_event_count, 1)

        with freeze_time('2018-12-27'):
            # Test autovacuum of events (by default 90 days old)
            # All events removed, all events older that 90 days
            cron_job = self.env.ref(
                'isha_ims.ir_cron_incident_vacuum_events')
            cron_job.method_direct_trigger()
            self.assertEqual(incident.incident_event_count, 1)

        incident.created_by_id.company_id.incident_event_auto_remove = True

        with freeze_time('2018-12-27'):
            # Test autovacuum of events (by default 90 days old)
            # All events removed, all events older that 90 days
            cron_job = self.env.ref(
                'isha_ims.ir_cron_incident_vacuum_events')
            cron_job.method_direct_trigger()
            self.assertEqual(incident.incident_event_count, 0)

    def test_unlink_just_created(self):
        incident = self.env['incident.incident'].with_user(
            self.incident_manager
        ).create({
            'type_id': self.simple_type.id,
            'incident_text': 'test',
        })
        with self.assertRaises(exceptions.AccessError):
            incident.unlink()

        # Managers with group *can delete incidents* are allowed to delete
        # incidents
        self.incident_manager.groups_id |= self.env.ref(
            'isha_ims.group_incident_manager_can_delete_incident')
        incident.unlink()

    def test_unlink_processed(self):
        incident = self.env['incident.incident'].with_user(
            self.incident_manager
        ).create({
            'type_id': self.simple_type.id,
            'incident_text': 'test',
        })
        incident.incident_text = 'test 42'
        incident.user_id = self.incident_manager

        with self.assertRaises(exceptions.AccessError):
            incident.unlink()

        # Managers with group *can delete incidents* are allowed to delete
        # incidents
        self.incident_manager.groups_id |= self.env.ref(
            'isha_ims.group_incident_manager_can_delete_incident')
        incident.unlink()

    def test_incident_can_change_category(self):
        self.assertEqual(self.incident_1.stage_id, self.stage_draft)
        self.assertTrue(self.incident_1.can_change_category)

        # move incident to sent stage
        self.incident_1.stage_id = self.stage_sent

        # Check that changing category not allowed
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)
        self.assertFalse(self.incident_1.can_change_category)
