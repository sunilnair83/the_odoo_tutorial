import logging
from contextlib import contextmanager
from odoo.tests.common import SavepointCase
from odoo.addons.generic_mixin.tests.common import (
    ReduceLoggingMixin,
    AccessRulesFixMixinST,
)


try:
    # pylint: disable=unused-import
    from freezegun import freeze_time  # noqa
except ImportError:  # pragma: no cover
    logging.getLogger(__name__).warning(
        "freezegun not installed. Tests will not work!")


@contextmanager
def disable_mail_auto_delete(env):
    def patched_method(self, vals):
        vals = dict(vals, auto_delete=False)
        return patched_method.origin(self, vals)
    env['mail.mail']._patch_method(
        'create', patched_method)

    yield

    env['mail.mail']._revert_method('create')


class IncidentCase(AccessRulesFixMixinST,
                  ReduceLoggingMixin,
                  SavepointCase):
    """ BAse tests case for tests related to generic incident
    """

    @classmethod
    def setUpClass(cls):
        super(IncidentCase, cls).setUpClass()
        cls.general_category = cls.env.ref(
            'isha_ims.incident_category_demo_general')
        cls.resource_category = cls.env.ref(
            'isha_ims.incident_category_demo_resource')
        cls.tec_configuration_category = cls.env.ref(
            'isha_ims.incident_category_demo_technical_configuration')

        # Incident type
        cls.simple_type = cls.env.ref('isha_ims.incident_type_simple')
        cls.sequence_type = cls.env.ref(
            'isha_ims.incident_type_sequence')
        cls.non_ascii_type = cls.env.ref(
            'isha_ims.incident_type_non_ascii')
        cls.access_type = cls.env.ref(
            'isha_ims.incident_type_access')

        # Stages
        cls.stage_draft = cls.env.ref(
            'isha_ims.incident_stage_type_simple_draft')
        cls.stage_sent = cls.env.ref(
            'isha_ims.incident_stage_type_simple_sent')
        cls.stage_confirmed = cls.env.ref(
            'isha_ims.incident_stage_type_simple_confirmed')
        cls.stage_rejected = cls.env.ref(
            'isha_ims.incident_stage_type_simple_rejected')
        cls.stage_new = cls.env.ref(
            'isha_ims.incident_stage_type_sequence_new')
        # Routes
        cls.route_draft_to_sent = cls.env.ref(
            'isha_ims.incident_stage_route_type_simple_draft_to_sent')
        cls.non_ascii_route_draft_to_sent = cls.env.ref(
            'isha_ims.incident_stage_route_type_non_ascii_draft_to_sent')

        # Incidents
        cls.incident_1 = cls.env.ref(
            'isha_ims.incident_incident_type_simple_demo_1')
        cls.incident_2 = cls.env.ref(
            'isha_ims.incident_incident_type_access_demo_1')

        # Users
        cls.demo_user = cls.env.ref('base.user_demo')
        cls.incident_user = cls.env.ref(
            'isha_ims.user_demo_incident')
        cls.incident_manager = cls.env.ref(
            'isha_ims.user_demo_incident_manager')
        cls.incident_manager_2 = cls.env.ref(
            'isha_ims.user_demo_incident_manager_2')

    def _close_incident(self, incident, stage, response_text=False, user=None):
        if user is None:
            user = self.env.user

        close_route = self.env['incident.stage.route'].with_user(user).search([
            ('incident_type_id', '=', incident.type_id.id),
            ('stage_to_id', '=', stage.id),
        ])
        close_route.ensure_one()
        wiz = self.env['incident.wizard.close'].with_user(user).create({
            'incident_id': incident.id,
            'close_route_id': close_route.id,
            'response_text': response_text,
        })
        wiz.action_close_incident()

        self.assertEqual(incident.stage_id, stage)
        self.assertEqual(incident.response_text, response_text)


class AccessRightsCase(AccessRulesFixMixinST,
                       ReduceLoggingMixin,
                       SavepointCase):

    @classmethod
    def setUpClass(cls):
        super(AccessRightsCase, cls).setUpClass()
        cls.simple_type = cls.env.ref('isha_ims.incident_type_simple')

        # Users
        cls.demo_user = cls.env.ref('isha_ims.user_demo_incident')
        cls.demo_manager = cls.env.ref(
            'isha_ims.user_demo_incident_manager')

        # Envs
        cls.uenv = cls.env(user=cls.demo_user)
        cls.menv = cls.env(user=cls.demo_manager)

        # Incident Type
        cls.usimple_type = cls.uenv.ref('isha_ims.incident_type_simple')
        cls.msimple_type = cls.menv.ref('isha_ims.incident_type_simple')

        # Incident category
        cls.ucategory_demo_general = cls.uenv.ref(
            'isha_ims.incident_category_demo_general')
        cls.mcategory_demo_general = cls.menv.ref(
            'isha_ims.incident_category_demo_general')

        # Incident view action
        cls.incident_action = cls.env.ref(
            'isha_ims.action_incident_window')

    def _read_incident_fields(self, user, incident):
        fields = list(
            self.env['incident.incident'].with_user(
                user
            ).load_views(
                self.incident_action.views
            )['fields_views']['form']['fields']
        )
        return incident.with_user(user).read(fields)
