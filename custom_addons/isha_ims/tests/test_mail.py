import logging
from odoo.tests import (
    HttpCase,
    HOST,
    PORT,
)
from odoo.tools.misc import mute_logger
from .common import disable_mail_auto_delete

_logger = logging.getLogger(__name__)


class TestIncidentMailNotificationLinks(HttpCase):

    def setUp(self):
        super(TestIncidentMailNotificationLinks, self).setUp()

        self.incident_demo_user = self.env.ref(
            'isha_ims.user_demo_incident')
        self.base_url = "http://%s:%s" % (HOST, PORT)
        self.user_root = self.env.ref('base.user_root')

        with self.registry.cursor() as cr:
            # Fix access rules
            #
            # Deactivate rules created by modules that are not initialized to
            # prevent errors raised when rule use field defined in
            # uninitialized addon
            env = self.env(cr=cr)
            rule_ids = env['ir.model.data'].search([
                ('model', '=', 'ir.rule'),
                ('module', 'not in', tuple(self.registry._init_modules)),
            ]).mapped('res_id')
            env['ir.rule'].browse(rule_ids).write({'active': False})

            # Subscribe demo user to printer incident
            env.ref(
                'isha_ims.incident_type_sequence'
            ).message_subscribe(self.incident_demo_user.partner_id.ids)

    @mute_logger('odoo.addons.mail.models.mail_mail',
                 'incidents.packages.urllib3.connectionpool',
                 'odoo.models.unlink')
    def test_assign_employee(self):
        with self.registry.cursor() as cr:
            env = self.env(cr=cr)
            incident = env['incident.incident'].with_context(
                mail_create_nolog=True,
                mail_notrack=True,
            ).create({
                'type_id': env.ref('isha_ims.incident_type_sequence').id,
                'incident_text': 'Test',
            })

            with disable_mail_auto_delete(env):
                incident.with_context(
                    mail_notrack=False,
                ).write({
                    'user_id': self.incident_demo_user.id,
                })

            assign_messages = env['mail.mail'].search([
                ('model', '=', 'incident.incident'),
                ('res_id', '=', incident.id),
                ('body_html', 'ilike',
                 '%%/mail/view/incident/%s%%' % incident.id),
            ])
            self.assertEqual(len(assign_messages), 1)

        self.authenticate(self.incident_demo_user.login, 'demo')
        with mute_logger('odoo.addons.base.models.ir_model'):
            # Hide errors about missing menus
            res = self.url_open('/mail/view/incident/%s' % incident.id)
        self.assertEqual(res.status_code, 200)
        self.assertNotRegex(res.url, r'^%s/web/login.*$' % self.base_url)
        self.assertRegex(
            res.url, r'^%s/web#.*id=%s.*$' % (self.base_url, incident.id))

    @mute_logger('odoo.addons.mail.models.mail_mail',
                 'incidents.packages.urllib3.connectionpool',
                 'odoo.models.unlink')
    def test_change_employee(self):
        with self.registry.cursor() as cr:
            env = self.env(cr=cr)
            incident = env['incident.incident'].with_user(
                self.incident_demo_user
            ).with_context(
                mail_create_nolog=True,
                mail_notrack=True,
            ).create({
                'type_id': env.ref('isha_ims.incident_type_sequence').id,
                'incident_text': 'Test',
            })

            incident.message_subscribe(
                partner_ids=self.incident_demo_user.partner_id.ids,
                subtype_ids=(
                    env.ref('mail.mt_comment') +
                    env.ref(
                        'isha_ims.mt_incident_stage_changed')
                ).ids)
            with disable_mail_auto_delete(env):
                incident.with_user(self.user_root).with_context(
                    mail_notrack=False,
                ).write({
                    'stage_id': env.ref(
                        'isha_ims.'
                        'incident_stage_type_sequence_sent').id,
                })

            messages = env['mail.mail'].search([
                ('model', '=', 'incident.incident'),
                ('res_id', '=', incident.id),
                ('body_html', 'ilike',
                 '%%/mail/view/incident/%s%%' % incident.id),
            ])
            self.assertEqual(len(messages), 1)

        self.authenticate(self.incident_demo_user.login, 'demo')
        with mute_logger('odoo.addons.base.models.ir_model'):
            # Hide errors about missing menus
            res = self.url_open('/mail/view/incident/%s' % incident.id)
        self.assertEqual(res.status_code, 200)
        self.assertNotRegex(res.url, r'^%s/web/login.*$' % self.base_url)
        self.assertRegex(
            res.url, r'^%s/web#.*id=%s.*$' % (self.base_url, incident.id))
