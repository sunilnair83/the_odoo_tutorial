from odoo import models, fields


class ResCompany(models.Model):
    _inherit = 'res.company'

    incident_event_live_time = fields.Integer(default=90)
    incident_event_live_time_uom = fields.Selection([
        ('days', 'Days'),
        ('weeks', 'Weeks'),
        ('months', 'Months')], default='days')
    incident_event_auto_remove = fields.Boolean(
        string='Automatically remove events older then',
        default=True)
