import logging
from odoo import models, fields, api, _, exceptions
from odoo.osv import expression
from datetime import timedelta
from lxml import etree
import time
import json
from .. import isha_base_importer
_logger = logging.getLogger(__name__)


class ResPartner(models.Model):
    _inherit = 'res.partner'

    # person_type_id = fields.Many2one('incident.persontype', 'Person Type', required=False)
    # gender = fields.Selection([('M', "Male"), ('F', "Female"),('O','Others')], required=False)
    # is_overseas = fields.Boolean('Is Overseas')
    # occupation = fields.Char('Occupation')
    # dob = fields.Date(string='DOB')

    incident_match_id = fields.Many2one('incident.incident', readonly=True, groups="isha_ims.group_ims")
    
    donotallow_stayarea_ids = fields.Many2many('incident.donotallows','donotallow_stayarea_incident_rel',
                                               column1='incident_stayarea_ids',column2='donotallow_stayarea_ids',
                                               domain=[('donotallows_category','=','stayarea')], track_visibility='always',
                                               groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader",
                                               string="DNA StayArea")

    donotallow_program_ids = fields.Many2many('incident.donotallows','donotallow_program_incident_rel', column1='incident_program_ids',
                                              column2='donotallow_program_ids', domain=[('donotallows_category','=','program')], track_visibility='always',
                                              groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader",
                                              string="DNA Programs")
    donotallow_volunteering_ids = fields.Many2many('incident.donotallows','donotallow_volunteering_incident_rel',
                                                   column1='incident_volunteering_ids',column2='donotallow_volunteering_ids',
                                                   domain=[('donotallows_category','=','volunteering')], track_visibility='always',
                                                   groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader",
                                                   string="DNA Volunteering")
    donotallow_general_ids = fields.Many2many('incident.donotallows','donotallow_general_incident_rel',
                                              column1='incident_general_ids',column2='donotallow_general_ids',
                                              domain=[('donotallows_category','=','general')], track_visibility='always',
                                              groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader",
                                              string="DNA General")

    dna_stayarea_comments = fields.Text(string='DNA Stayarea Comments',
                                        groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader")
    dna_program_comments = fields.Text(string='DNA Program Comments',
                                       groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader")
    dna_volunteering_comments = fields.Text(string='DNA Volunteering Comments',
                                            groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader")
    dna_general_comments = fields.Text(string='DNA General Comments',
                                       groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader")

    incident_by_partner_ids = fields.One2many(
        'incident.incident', 'partner_id',
        readonly=True, copy=False, groups="isha_ims.group_ims")
    incident_by_author_ids = fields.One2many(
        'incident.incident', 'author_id',
        readonly=True, copy=False, groups="isha_ims.group_ims")
    approved_incident_by_partner_ids = fields.One2many(
        'incident.incident', 'partner_id',
        readonly=True, copy=False, domain=[('stage_id', 'in', ['Approved', 'Freeze', 'Unfreeze'])], groups="isha_ims.group_ims")
    approved_incident_by_author_ids = fields.One2many(
        'incident.incident', 'author_id',
        readonly=True, copy=False, domain=[('stage_id', 'in', ['Approved', 'Freeze', 'Unfreeze'])], groups="isha_ims.group_ims")
    # incident_ids = fields.Many2many(
    #     'incident.incident',
    #     'incident_incident_partner_author_rel',
    #     'partner_id', 'incident_id',
    #     readonly=True, copy=False, store=True,
    #     compute='_compute_incident_data')
    incident_count = fields.Integer(
        'Incidents',
        compute='_compute_incident_data',
        readonly=True, groups="isha_ims.group_ims")

    partner_severity = fields.Selection([('1', 'LOW'), ('2', 'MEDIUM'),
                                        ('3', 'HIGH'), ('4', 'SEVERE')], string='Severity', track_visibility='always', groups="isha_ims.group_ims")

    starmark_category_id = fields.Many2one('starmark.category', string='Star Mark Category', track_visibility='always',
                                           groups="isha_ims.group_ims_basic_search,isha_ims.group_ims_advanced_search,"
                                                  "isha_ims.group_ims_adv_reporter,isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader"
                                           )
    starmark_name = fields.Char(related="starmark_category_id.name", store=False, readonly=True)
    starmark_color = fields.Char(related="starmark_category_id.sm_color", store=False, readonly=True)

    incident_date_closed = fields.Datetime('Star Mark Expiry Date', copy=False, track_visibility='always',
                                           groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_adv_reporter,"
                                                  "isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader")
    incident_date_assigned = fields.Datetime('Star Mark Assigned Date', copy=False, track_visibility='always',
                                             groups="isha_ims.group_ims_advanced_search,isha_ims.group_ims_adv_reporter,"
                                                    "isha_ims.group_ims_moderator,isha_ims.group_ims_bulk_uploader")

    is_adv_user = fields.Boolean(compute='_compute_adv_user', groups="isha_ims.group_ims")

    @api.depends('starmark_category_id')
    def _compute_adv_user(self):
        # self.ensure_one()
        for rec in self:
            rec.is_adv_user = self.env.user.has_group('isha_ims.group_ims_adv_reporter') \
                              and not self.env.user.has_group('isha_ims.group_ims_moderator')

    @api.model
    def fields_view_get(self, view_id=None, view_type='tree',
                        toolbar=False, submenu=False):
        result = super(ResPartner, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)
        # Disabling the create and import buttons for all users in the kanban view
        user = self.env.user
        if user.has_group('isha_ims.group_ims') and not user.has_group('base.user_root') and not user.has_group('base.user_admin'):
            if view_type == 'kanban':
                doc = etree.XML(result['arch'])
                for node in doc.xpath("//kanban"):
                    node.set('import', 'false')
                    node.set('create', 'false')
                result['arch'] = etree.tostring(doc)
            if view_type == 'form':
                doc = etree.XML(result['arch'])
                for node in doc.xpath("//form"):
                    node.set('create', 'false')
                    node.set('edit', 'false')
                result['arch'] = etree.tostring(doc)
        return result

    #Events
    partner_event_ids = fields.One2many(
        'res.partner.event', 'partner_id', 'Events', readonly=True)

    # This method enables copying selected ROW in incident screen
    def select_contact(self, default=None):
        for rec in self:
            if 'params' in self.env.context and 'id' in self.env.context['params']:
                self.env['incident.incident'].search([('id', '=', self.env.context['params']['id'])])[0].partner_id = rec

    @api.onchange('starmark_category_id')
    def update_assign_expiry_dates(self):
        for rec in self:

            if rec.starmark_category_id.id:
                cur_date = fields.Datetime.now()
                if not rec.incident_date_assigned:
                    rec.incident_date_assigned = cur_date
                rec.incident_date_closed = cur_date + timedelta(days=rec.starmark_category_id.time_limit)

    @api.depends('incident_count', 'approved_incident_by_partner_ids', 'approved_incident_by_author_ids')
    def _compute_incident_data(self):
        for record in self:
            record.incident_ids = (
                record.sudo().approved_incident_by_partner_ids + record.sudo().approved_incident_by_author_ids)
            record.incident_count = len(record.incident_ids)

    def action_show_related_incidents(self):
        self.ensure_one()
        if self.self.env.user.has_group('isha_ims.group_ims_moderator') or self.env.user.has_group('isha_ims.group_ims_advanced_search'):
            action = self.env.ref(
                'isha_ims.action_incident_window').read()[0]
            action.update({
                'domain': expression.OR([
                    [('partner_id', 'in', self.ids)],
                    [('author_id', 'in', self.ids)],
                ]),
                'context': dict(
                    self.env.context,
                    default_partner_id=self.commercial_partner_id.id,
                    default_author_id=self.id,
                ),
            })
            return action
        else:
            return None

    def _get_all_moderators_on_partner(self):
        moderators = set()
        for incident in self.approved_incident_by_partner_ids:
            moderators |= self._get_moderators(incident)
        return moderators
    
    def trigger_event(self, event_type=None, composition_mode='mass_mail', event_data=None):
        event_type_id = self.env['incident.event.type'].get_event_type_id(event_type)
        event_data = event_data if event_data is not None else {}
        event_data.update({
            'event_type_id': event_type_id,
            'partner_id': self.id,
            'user_id': self.env.user.id,
            'date': fields.Datetime.now(),
        })
        event = self.env['res.partner.event'].sudo().create(event_data)
        if event_type == 'starmark-updated':
            self.handle_starmark_change_event(event, composition_mode)
        if event_type == 'starmark-expired':
            self.handle_starmark_expiry(event, composition_mode)
    
    def handle_starmark_change_event(self, event, composition_mode):
        moderators = self._get_all_moderators_on_partner()
        subject = 'The contact %s has transitioned to a new star mark category.'
        # for partner in moderators:
        if len(moderators) > 0:
            self._send_default_notification__send(
                'isha_ims.message_contact_star_mark_changed',
                moderators,
                event,
                composition_mode,
                lazy_subject=lambda self: _(
                    subject) % self.name
            )
    
    def handle_starmark_expiry(self, event, composition_mode):
        moderators = self._get_all_moderators_on_partner()
        subject = 'The contact %s has star mark category which has expired.'
        # for partner in moderators:
        if len(moderators) > 0:
            self._send_default_notification__send(
                'isha_ims.message_contact_star_mark_expired',
                moderators,
                event,
                composition_mode,
                lazy_subject=lambda self: _(
                    subject) % self.name
            )


    # Default notifications
    def _send_default_notification__get_email_from(self, **kw):
        """ To be overloaded to change 'email_from' field for notifications
        """
        return 'ims@ishafoundation.org'

    def _send_default_notification__get_context(self, event):
        """ Compute context for default notification
        """
        values = event.get_context()
        values.update({
            'company': self.env.user.company_id,
        })
        return values

    def _send_default_notification__get_msg_params(self, composition_mode, **kw):
        return dict(
            composition_mode=composition_mode,
            auto_delete=True,
            auto_delete_message=False,
            parent_id=False,  # override accidental context defaults
            # subtype_id=self.env.ref('mail.mt_note').id,
            **kw,
        )

    def _creation_subtype(self):
        return self.env.ref('isha_ims.mt_res_partner_updated')

    def get_mail_url(self):
        """ Get incident URL to be used in mails
        """
        url = "/web#id=%s&model=res.partner"
        menu = self.env['ir.ui.menu'].search([('name', 'ilike', 'Isha IMS')])
        if menu:
            menu_id = menu[0].id
            url += "&menu_id=%s"
        return url % (self.id, menu_id)
        # return "/web#id=%s&model=res.partner" % self.id

    def _send_default_notification__send(self, template, partners,
                                         event, composition_mode, **kw):
        """ Send default notification

            :param str template: XMLID of template to use for notification
            :param Recordset partners: List of partenrs that have to receive
                                       this notification
            :param Recordset event: Single record of 'incident.event'
            :param function lazy_subject: function (self) that have to return
                                          translated string for subject
                                          for notification
        """
        values_g = self._send_default_notification__get_context(event)
        message_data_g = self._send_default_notification__get_msg_params(composition_mode, **kw)
        try:
            ims_config = isha_base_importer.Configuration('IMS')
            email_from = ims_config['IMS_EMAIL_FROM']
        except:
            email_from = self._send_default_notification__get_email_from(**kw)
        if email_from:
            message_data_g['email_from'] = email_from

        # In order to handle translatable subjects, we use specific argument:
        # lazy_subject, which is function that receives 'self' and returns
        # string.
        lazy_subject = message_data_g.pop('lazy_subject', None)

        # for partner in partners.sudo():
        #     # Skip partners without emails to avoid errors
        #     if not partner.email:
        #         continue
        #     values = dict(
        #         values_g,
        #         partner= partner
        #     )
        #     if partner.lang:
        #         self_ctx = self_ctx.with_context(lang=partner.sudo().lang)
        self_ctx = self.sudo()
        # self_ctx = self_ctx.with_context(tracking_disable=True, mail_notrack=True)
        message_data = dict(
            message_data_g,
            partner_ids=[(4,partner.id) for partner in partners if partner.email],
            values=dict(values_g,partner=self.sudo()))
        if lazy_subject:
            message_data['subject'] = lazy_subject(self_ctx)
        self_ctx.message_post_with_view(
            template,
            **message_data)

    def _scheduler_update_partner_starmark(self):
        _logger.info('isha_ims : res_partner : _scheduler_update_partner_starmark : cron job - start')
        partners = self.search([('incident_date_closed', '<', fields.Datetime.now()), ('incident_date_closed', '>', fields.Datetime.now() - timedelta(days=1))])
        for partner in partners:
            current_category = partner.starmark_category_id
            next_category = current_category.transition_to_id
            if next_category:
                vals = ({
                    'starmark_category_id': next_category,
                    'incident_date_closed': fields.Datetime.now() + timedelta(days=partner.starmark_category_id.time_limit),
                    'cron_job': True
                })
                partner.write(vals)
            else:
                partner.trigger_event('starmark-expired', 'mass_post')
        _logger.info('isha_ims : res_partner : _scheduler_update_partner_starmark : cron job - partner count %s' % len(partners))
        _logger.info('isha_ims : res_partner : _scheduler_update_partner_starmark : cron job - end')


    def _get_moderators(self, incident):
        send_list = set()
        if incident.team_ids:
            for team in incident.team_ids:
                if team.users:
                    for user in team.users:
                        if user.has_group('isha_ims.group_ims_moderator'):
                            send_list.add(user.partner_id)
        # collect all the moderators directly assigned to this incident.
        # if incident.user_ids:
        #     for moderator in incident.user_ids:
        #         send_list.add(moderator.partner_id)
        return send_list

    def write(self, vals):
        composition_mode = 'mass_mail'
        if vals.get('cron_job'):
            composition_mode = 'mass_post'
            del vals['cron_job']
        res = super(ResPartner, self).write(vals)
        if 'starmark_category_id' in vals:
            self.trigger_event('starmark-updated', composition_mode)
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('black_listed'):
            if len(args) == 0:
                return
            args += [('starmark_category_id', '!=', False)]
        results = super(ResPartner, self)._search(
            args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)
        # if len(results) == 0:
        #     raise exceptions.Warning("No results found!")
        return results

    # overridden _message_get_suggested_recipients from mail.threads to remove suggested mail recipient
    # if partner has a star marking
    def _message_get_suggested_recipients(self):
        recipients = super(ResPartner, self)._message_get_suggested_recipients()
        if len(self.sudo().starmark_category_id):
            recipients = {}
        return recipients
