from odoo import models, fields, api
from .incident_stage import DEFAULT_BG_COLOR, DEFAULT_LABEL_COLOR


class IncidentStageType(models.Model):
    _name = 'incident.stage.type'
    _inherit = [
        'generic.mixin.name_with_code',
        'generic.mixin.uniq_name_code',
    ]
    _description = 'Incident Stage Type'

    # Defined in generic.mixin.name_with_code
    name = fields.Char()
    code = fields.Char()

    active = fields.Boolean(index=True, default=True)

    bg_color = fields.Char(default=DEFAULT_BG_COLOR, string="Backgroung Color")
    label_color = fields.Char(default=DEFAULT_LABEL_COLOR)
    incident_ids = fields.One2many('incident.incident', 'stage_type_id')
    incident_count = fields.Integer(
        compute='_compute_incident_count', readonly=True)

    @api.depends('incident_ids')
    def _compute_incident_count(self):
        for rec in self:
            rec.incident_count = len(rec.incident_ids)

    def action_show_incidents(self):
        self.ensure_one()
        action = self.env.ref(
            'isha_ims.action_incident_window').read()[0]
        ctx = dict(self.env.context)
        ctx.update({
            'default_stage_type_id': self.id,
        })
        return dict(
            action,
            context=ctx,
            domain=[('stage_type_id', '=', self.id)]
        )
