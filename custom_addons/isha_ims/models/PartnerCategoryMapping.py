from odoo import models, fields, api

class PartnerCategoryMapping(models.Model):
    _name = 'partner.category.mapping'
    _description = 'Partner Category Mapping'

    # date_closed = fields.Datetime('Expiry Date', copy=False)
    # date_assigned = fields.Datetime('Assigned Date', copy=False)
    partner_id = fields.Many2one('res.partner',string="Partner")
    category_id = fields.Many2one('partner.category',string='Category')

