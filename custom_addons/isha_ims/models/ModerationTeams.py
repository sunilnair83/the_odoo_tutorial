from odoo import models, fields, api

class ModerationTeams(models.Model):
    _name = 'moderation.team'
    _description = 'Moderation Teams'

    name = fields.Char(string="Team Name", required=True)
    # user_ids = fields.One2many('res.users', 'team_id', string='Moderation Team Users')
    users = fields.Many2many('res.users', column1='moderation_team_ids', column2='res_user_ids', string='Moderation Team Users', relation='res_users_moderation_team_rel')
    # incident_ids = fields.One2many('incident.incident', 'team_id', string='Incidents On Team')
    india_person_type_ids = fields.Many2many('incident.persontype', column1='moderation_team_ids', column2='person_type_ids', string='Person Types', relation='person_type_india_moderation_team_rel')
    overseas_person_type_ids = fields.Many2many('incident.persontype', column1='moderation_team_ids', column2='person_type_ids', string='Person Types', relation='person_type_overseas_moderation_team_rel')
    incident_ids = fields.Many2many('incident.incident', column1='moderation_team_ids', column2='incident_incident_ids', string='Incidents')
    active = fields.Boolean(default=True, help="To make this record in active.", track_visibility='onchange')

    _sql_constraints = [
        ('unique_moderation_team_name',
         'unique(name)', 'Sorry! this Moderation Team Name already exists! Name should be unique!!')
    ]