// /**
//  * List of constants used in the logic in this file
//  * Make sure that these constants are kept up to date.
//  * Otherwise the functionalities written here may not function as intended.
//  */
const constants = {
    moduleName: "Isha IMS",
    moduleBrandClassName: '.o_menu_brand',
    editButtonClassName: '.o_form_button_edit'
}

function isThisIms() {
    let moduleName = $(constants.moduleBrandClassName).text();
    console.log({ moduleName, className: constants.moduleBrandClassName })
    if (moduleName) {
        if (moduleName === constants.moduleName) {
            return true
        } else {
            return false
        }
    }
}

odoo.define('isha_ims.hide_buttons', function(require) {
    "use strict";

    var FormController = require('web.FormController');

    FormController.include({
    /**
     * @override
     * @private
     */
    _updateButtons: function () {
        if (this.$buttons) {
            if (this.footerToButtons) {
                var $footer = this.renderer.$('footer');
                if ($footer.length) {
                    this.$buttons.empty().append($footer);
                }
            }          
            if (this.modelName === 'incident.incident') {
                setTimeout(() => {
                    if (isThisIms()) {
                        console.log("We're in IMS!")
                        if (this.initialState.fieldsInfo.form.partner_name.modifiersValue['readonly'] === true) {
                            $(constants.editButtonClassName).css({ 'display': 'none' });
                        } else {
                            $(constants.editButtonClassName).css({ 'display': '' });
                        }
                    }
                }, 0)
            }
            var edit_mode = (this.mode === 'edit');
            this.$buttons.find('.o_form_buttons_edit')
                            .toggleClass('o_hidden', !edit_mode);
            this.$buttons.find('.o_form_buttons_view')
                            .toggleClass('o_hidden', edit_mode);
        }
    }
    });
});