from odoo import api, SUPERUSER_ID


def migrate(cr, installed_version):
    env = api.Environment(cr, SUPERUSER_ID, {})
    incident = env.ref(
        'isha_ims.incident_incident_type_sequence_demo_1',
        raise_if_not_found=False)
    partner = env.ref(
        'base.res_partner_2',
        raise_if_not_found=False)

    if incident and partner and incident.partner_id != partner:
        incident.partner_id = partner
