def migrate(cr, installed_version):
    cr.execute("""
        UPDATE ir_model_data
        SET module = 'isha_ims'
        WHERE module = 'isha_ims_dashboard'
          AND model IN (
                 'ir.model.fields',
                 'ir.model.constraint',
                 'ir.model.relation',
                 'ir.ui.menu',
                 'ir.model.access',
                 'ir.actions.act_window');

        -- Delete views
        DELETE FROM ir_ui_view WHERE id IN (
            SELECT res_id
            FROM ir_model_data
            WHERE model = 'ir.ui.view'
              AND module = 'isha_ims_dashboard'
        );

        -- DELETE references to ir_model
        DELETE FROM ir_model_data
        WHERE model = 'ir.model'
          AND module = 'isha_ims_dashboard';

        -- DELETE removed modules from database
        DELETE FROM ir_module_module WHERE name = 'isha_ims_dashboard';
    """)
