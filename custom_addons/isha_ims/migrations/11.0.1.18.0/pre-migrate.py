def migrate(cr, installed_version):
    cr.execute("""
        DELETE FROM ir_model
        WHERE model IN (
            'incident.mixin.name_with_code',
            'incident.mixin.uniq_name_code'
        );
    """)
