from odoo.tools.sql import column_exists


def migrate(cr, installed_version):
    if not column_exists(cr, 'incident_incident', 'author_id'):
        cr.execute("""
            ALTER TABLE incident_incident
            ADD COLUMN author_id INTEGER;

            UPDATE incident_incident r
            SET author_id=(
                SELECT partner_id
                FROM res_users u
                WHERE u.id=r.created_by_id);

            UPDATE incident_incident r
            SET partner_id=(SELECT parent_id
                FROM res_partner p
                WHERE p.id=r.author_id)
            WHERE r.partner_id IS NULL;
        """)
