def migrate(cr, installed_version):
    cr.execute("""
        DROP TABLE IF EXISTS incident_wizard_assign CASCADE;
        DELETE FROM ir_model WHERE model = 'incident.wizard.assign';
    """)
