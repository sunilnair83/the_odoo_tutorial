def migrate(cr, installed_version):
    cr.execute("""
        UPDATE ir_model_data
        SET module = 'isha_ims_assignment'
        WHERE module = 'isha_ims'
          AND name IN (
               'incident_assign_policy_model_ir_model',
               'incident_assign_policy_model');
    """)
