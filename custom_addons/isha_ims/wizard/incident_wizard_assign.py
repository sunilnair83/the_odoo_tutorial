from odoo import models, fields


class IncidentWizardAssign(models.TransientModel):
    _name = 'incident.wizard.assign'
    _description = 'Incident Wizard: Assign'

    def _default_user_id(self):
        return self.env.user

    incident_id = fields.Many2one('incident.incident', 'Incident', requeired=True)
    user_id = fields.Many2one(
        'res.users', string="User", default=_default_user_id, required=True)
    partner_id = fields.Many2one(
        'res.partner', related="user_id.partner_id",
        readonly=True, store=False)

    def do_assign(self):
        for rec in self:
            rec.incident_id.ensure_can_assign()
            rec.incident_id.user_id = rec.user_id
