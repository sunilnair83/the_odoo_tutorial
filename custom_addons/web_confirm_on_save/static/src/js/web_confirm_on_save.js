odoo.define('web_confirm_on_save.web_confirm_on_save', function (require) {
"use strict";

var ajax = require('web.ajax');
var AbstractView = require('web.AbstractView');
var FormController = require('web.FormController');
var Dialog = require('web.Dialog');
var session = require('web.session');

AbstractView.include({
	
	init: function (viewInfo, params) {
    	var self = this;
    	this._super.apply(this, arguments);
    	var confirm =  this.arch.attrs.confirm ? this.arch.attrs.confirm : false;
    	var alert =  this.arch.attrs.alert ? this.arch.attrs.alert : false;
    	self.controllerParams.activeActions.confirm = confirm;
    	self.controllerParams.activeActions.alert = alert;
    },

});

FormController.include({
	
	check_condition: function (modelName, record_id ,data_changed) {
        var def = this._rpc({
            "model": modelName,
            "method": "check_condition_show_dialog",
            "args": [record_id ,data_changed],
            "context": this.initialState.context
        });
        return def;
    },
	
	checkCanBeSaved: function (recordID) {
        var fieldNames = this.renderer.canBeSaved(recordID || this.handle);
        if (fieldNames.length) {
            return false;
        }
        return true;
    },
	
	_onSave: function (ev) {
		var self = this;
		var modelName = this.modelName ? this.modelName : false;
		var record = this.model.get(this.handle, {raw: true});
		var data_changed = record ? record.data : false;
		var record_id = data_changed && data_changed.id ? data_changed.id : false;
		var confirm = self.activeActions.confirm;
		var alert =  self.activeActions.alert;
		var canBeSaved = record && record.id ? self.checkCanBeSaved(record.id) : false;
		function saveAndExecuteAction () {
			ev.stopPropagation(); // Prevent x2m lines to be auto-saved
			self._disableButtons();
			self.saveRecord().then(self._enableButtons.bind(self)).guardedCatch(self._enableButtons.bind(self));
	    }
		if(canBeSaved && modelName && (confirm || alert)){
			self.check_condition(modelName, record_id, data_changed).then(function(opendialog){
	        	if(!opendialog){
	        		saveAndExecuteAction();
	        	}else{
	        		if(confirm){
	        			var def = new Promise(function (resolve, reject) {
	        	            Dialog.confirm(self, confirm, {
	        	                confirm_callback: saveAndExecuteAction,
	        	            }).on("closed", null, resolve);
	        	        });
	        		}else{
        				var def = new Promise(function (resolve, reject) {
        		            Dialog.alert(self, alert, {
        		                confirm_callback: saveAndExecuteAction,
        		            }).on("closed", null, resolve);
        		        });
	        			saveAndExecuteAction();
	        		}
	        	}
	        });
		}else{
			return saveAndExecuteAction();
		}
    },
    
    _onSaveCustom: function (ev) {
		var self = this;
		var modelName = this.modelName ? this.modelName : false;
		var record = this.model.get(this.handle, {raw: true});
		var data_changed = record ? record.data : false;
		var record_id = data_changed && data_changed.id ? data_changed.id : false;
		var confirm = self.activeActions.confirm;
		var alert =  self.activeActions.alert;
		var canBeSaved = record && record.id ? self.checkCanBeSaved(record.id) : false;
		function saveAndExecuteAction () {
			ev.stopPropagation(); // Prevent x2m lines to be auto-saved
			self._disableButtons();
			return self.saveRecord().then(function() {
				var record = self.model.get(ev.data.record.id);
                self._callButtonAction(ev.data.attrs, record);
				return self._enableButtons.bind(self)
			}).guardedCatch(self._enableButtons.bind(self));
	    }
		if(canBeSaved && modelName && (confirm || alert)){
			return self.check_condition(modelName, record_id, data_changed).then(function(opendialog){
	        	if(!opendialog){
	        		return saveAndExecuteAction();
	        	}else{
	        		if(confirm){
	        			return new Promise(function (resolve, reject) {
	        	            Dialog.confirm(self, confirm, {
	        	                confirm_callback: saveAndExecuteAction,
	        	            }).on("closed", null, resolve);
	        	        });
	        		}else{
        				return new Promise(function (resolve, reject) {
        		            Dialog.alert(self, alert, {
        		                confirm_callback: saveAndExecuteAction,
        		            }).on("closed", null, resolve);
        		        });
	        			saveAndExecuteAction();
	        		}
	        	}
	        });
		}else{
			return saveAndExecuteAction();
		}
	},

	/**
     * @private
     * @param {OdooEvent} ev
     */
    _onButtonClicked: function (ev) {
        // stop the event's propagation as a form controller might have other
        // form controllers in its descendants (e.g. in a FormViewDialog)
        ev.stopPropagation();
        var self = this;
        var def;

        this._disableButtons();

        function saveAndExecuteAction () {
            return self.saveRecord(self.handle, {
                stayInEdit: true,
            }).then(function () {
                // we need to reget the record to make sure we have changes made
                // by the basic model, such as the new res_id, if the record is
                // new.
                var record = self.model.get(ev.data.record.id);
                return self._callButtonAction(attrs, record);
            });
        }


        var attrs = ev.data.attrs;
        if (attrs.confirm) {
            def = new Promise(function (resolve, reject) {
                Dialog.confirm(this, attrs.confirm, {
                    confirm_callback: saveAndExecuteAction,
                }).on("closed", null, resolve);
            });
        } else if (attrs.special === 'cancel') {
            def = this._callButtonAction(attrs, ev.data.record);
        } else if (!attrs.special || attrs.special === 'save') {
            // save the record but don't switch to readonly mode
            def = saveAndExecuteAction();
        } else if (!attrs.special || attrs.special === 'trigger-default-save') {
            def = this._onSaveCustom(ev).then(function () {
				return;
            });
		} else {
            console.warn('Unhandled button event', ev);
            return;
        }

        // Kind of hack for FormViewDialog: button on footer should trigger the dialog closing
        // if the `close` attribute is set
        def.then(function () {
            self._enableButtons();
            if (attrs.close) {
                self.trigger_up('close_dialog');
            }
        }).guardedCatch(this._enableButtons.bind(this));
    },

});

});
