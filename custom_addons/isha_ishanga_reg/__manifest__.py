{
    'name': 'Isha Ishanga7%',
    'summary': "Join us for powerful meditations, musical performances and Satsangs with Sadhguru.",
    'description': "Join us for powerful meditations, musical performances and Satsangs with Sadhguru.",
    'author': "Isha Foundation",
    'license': "AGPL-3",
    'website': "https://isha.sadhguru.org/",
    'category': 'Isha Donation',
    'version': '20.0.0.1',
    'external_dependencies': {
        'python': [
            'html2text',
        ],
    },
    'depends': ['mail', 'sms', 'auth_signup', 'http_routing', 'base', 'website', 'isha_crm'],
    'data':
        [
            'data/ir_config_param.xml',
            'data/mail_template.xml',
            'data/nu_program_mail_template.xml',
            'data/usd_conversation_currency.xml',
            'security/security.xml',
            'security/ir.model.access.csv',
            'views/common_template.xml',
            'views/ishanga_nu_program_registration.xml',
            'views/ishanga_mail_support_page.xml',
            'views/onlinedonation_first_page.xml',
            'views/online_personal_details.xml',
            'views/ishanga_nanmai_template.xml',
            'views/donation_user_template.xml',
            'views/out_reach_redirect_tmp.xml',
	        'views/ishanga_success_template.xml',
	        'views/nu_porgram_reg_entry.xml',
            'views/ishanga_config.xml',
            'menus/menus.xml'

        ],
    'application': True,
    'auto_install': False,
    'installable': True,
}
