from dateutil.relativedelta import relativedelta

from odoo import api, fields, models


class support_ticket(models.Model):

    _inherit = "support.ticket"

    color = fields.Integer(string='Color Index')
    
    is_open_day = fields.Boolean('Open Day',default=False)
    is_open_week = fields.Boolean('Open Week',default=False)
    is_open_month = fields.Boolean('Open Month',default=False)
    is_open_last_month = fields.Boolean('Last Month',default=False) 

    is_unassigned_day = fields.Boolean('Unassigned Day',default=False)
    is_unassigned_week = fields.Boolean('Unassigned Week',default=False)
    is_unassigned_month = fields.Boolean('Unassigned Month',default=False)
    is_unassigned_last_month = fields.Boolean('Unassigned Last Month',default=False)

    is_closed_day = fields.Boolean('Closed Day',default=False)
    is_closed_week = fields.Boolean('Closed Week',default=False)
    is_closed_month = fields.Boolean('Closed Month',default=False)
    is_closed_last_month = fields.Boolean('Closed Last Month',default=False)

    is_deadline_day = fields.Boolean('Deadline Day',default=False)
    is_deadline_week = fields.Boolean('Deadline Week',default=False)
    is_deadline_month = fields.Boolean('Deadline Month',default=False)
    is_deadline_last_month = fields.Boolean('Deadline Month',default=False)


class support_ticket(models.Model):

    _inherit = "support.team"

    color = fields.Integer('Color Picker')
    
    @api.depends('parent_team_ids')
    def _new_count(self):
        today = fields.Date.context_today(self)
        this_week = today - relativedelta(weeks=1)
        this_month = today - relativedelta(days=30)
        last_month = today - relativedelta(days=60)
        for team in self.env['support.team'].search([('id','in', self.ids)]):

            day_list = self.env['support.ticket'].search_count([('date_create', '>=', today.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])
            week_list = self.env['support.ticket'].search_count([('date_create', '>=', this_week.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])
            month_list = self.env['support.ticket'].search_count([('date_create', '>=', this_month.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])
            last_month_list = self.env['support.ticket'].search_count([('date_create', '>=', last_month.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])

            team.open_ticket_today = day_list
            team.open_ticket_week = week_list
            team.open_ticket_month = month_list
            team.open_ticket_last_month = last_month_list


    @api.depends('parent_team_ids')
    def _unassigned_count(self):
        today = fields.Date.context_today(self)
        this_week = today - relativedelta(weeks=1)
        this_month = today - relativedelta(days=30)
        last_month = today - relativedelta(days=60)
        for team in self.env['support.team'].search([('id','in', self.ids)]):

            day_list = self.env['support.ticket'].search_count([('date_create', '>=', today.strftime("%Y-%m-%d 00:00:00")),('user_id', '=', False),('support_team_id','=',team.id)])
            week_list = self.env['support.ticket'].search_count([('date_create', '>=', this_week.strftime("%Y-%m-%d 00:00:00")),('user_id', '=', False),('support_team_id','=',team.id)])
            month_list = self.env['support.ticket'].search_count([('date_create', '>=', this_month.strftime("%Y-%m-%d 00:00:00")),('user_id', '=', False),('support_team_id','=',team.id)])
            last_month_list = self.env['support.ticket'].search_count([('date_create', '>=', last_month.strftime("%Y-%m-%d 00:00:00")),('user_id', '=', False),('support_team_id','=',team.id)])

            team.unassigned_ticket_today = day_list
            team.unassigned_ticket_week = week_list
            team.unassigned_ticket_month = month_list
            team.unassigned_ticket_last_month = last_month_list


    @api.depends('parent_team_ids')
    def _closed_count(self):
        today = fields.Date.context_today(self)
        this_week = today - relativedelta(weeks=1)
        this_month = today - relativedelta(days=30)
        last_month = today - relativedelta(days=60)
        for team in self.env['support.team'].search([('id','in', self.ids)]):

            day_list = self.env['support.ticket'].search_count([('date_create', '>=', today.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',True)])
            week_list = self.env['support.ticket'].search_count([('date_create', '>=', this_week.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',True)])
            month_list = self.env['support.ticket'].search_count([('date_create', '>=', this_month.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',True)])
            last_month_list = self.env['support.ticket'].search_count([('date_create', '>=', last_month.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',True)])
            
            team.closed_ticket_today = day_list
            team.closed_ticket_week = week_list
            team.closed_ticket_month = month_list
            team.closed_ticket_last_month = last_month_list


    def _deadline_count(self):
        today = fields.Date.context_today(self)
        this_week = today - relativedelta(weeks=1)
        this_month = today - relativedelta(days=30)
        last_month = today - relativedelta(days=60)
        for team in self.env['support.team'].search([('id','in', self.ids)]):

            day_list = self.env['support.ticket'].search_count([('deadline_date', '>=', today.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])
            week_list = self.env['support.ticket'].search_count([('deadline_date', '>=', this_week.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])
            month_list = self.env['support.ticket'].search_count([('deadline_date', '>=', this_month.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])
            last_month_list = self.env['support.ticket'].search_count([('deadline_date', '>=', last_month.strftime("%Y-%m-%d 00:00:00")),('support_team_id','=',team.id),('is_ticket_closed','=',False)])

            team.deadline_ticket_today = day_list
            team.deadline_ticket_week = week_list
            team.deadline_ticket_month = month_list
            team.deadline_ticket_last_month = last_month_list
        
    open_ticket_today = fields.Integer(compute='_new_count')
    open_ticket_week = fields.Integer(compute='_new_count')
    open_ticket_month = fields.Integer(compute='_new_count')
    open_ticket_last_month = fields.Integer(compute='_new_count')

    unassigned_ticket_today = fields.Integer(compute='_unassigned_count')
    unassigned_ticket_week = fields.Integer(compute='_unassigned_count')
    unassigned_ticket_month = fields.Integer(compute='_unassigned_count')
    unassigned_ticket_last_month = fields.Integer(compute='_unassigned_count')

    closed_ticket_today = fields.Integer(compute='_closed_count')
    closed_ticket_week = fields.Integer(compute='_closed_count')
    closed_ticket_month = fields.Integer(compute='_closed_count')
    closed_ticket_last_month = fields.Integer(compute='_closed_count')

    deadline_ticket_today = fields.Integer(compute='_deadline_count')
    deadline_ticket_week = fields.Integer(compute='_deadline_count')
    deadline_ticket_month = fields.Integer(compute='_deadline_count')
    deadline_ticket_last_month = fields.Integer(compute='_deadline_count')


    def action_view_support_ticket_stats_kanban(self):
        kanban_id = self.env.ref("bi_helpdesk_support_ticket_dashbord.support_ticket_kanban_dashboard")
        return {
            'type': 'ir.actions.act_window',
            'name': 'Dashboard',
            'view_mode': 'kanban',
            'res_model': 'support.team',
            'domain': [('id', '=', self.id)],
            # if you don't want to specify form for example
            # (False, 'form') just pass False
            'views': [(kanban_id.id, 'kanban')],
            'target': 'current'
        }
