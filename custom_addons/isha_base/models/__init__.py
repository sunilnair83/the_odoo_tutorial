# -*- coding: utf-8 -*-


from ..configuration import Configuration

ir_attachment_config = Configuration('IR_ATTACHMENT')
s3_enabled = ir_attachment_config.getboolean('CLOUD_ATTACHMENT_STORAGE')
s3_enabled = False if s3_enabled is None else s3_enabled

if s3_enabled:
    from . import s3_attachment
