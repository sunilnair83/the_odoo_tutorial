import hashlib
import base64
import re

from odoo import models

from ..utils.s3 import S3Bucket
from ..configuration import Configuration


class S3Attachment(models.Model):
    """
    Extends ir.attachment to implement the S3 storage engine
    """

    DEFAULT_PREFIX = 'sushumna/'

    _inherit = "ir.attachment"
    _s3_initialized = False
    _s3_bucket = None
    _file_prefix = None
    _s3_enabled = None
    _no_image = None
    _s3_mime_type_filter = None

    def _initialize(self):
        if not self._s3_initialized:
            with open('etc/assets/no-image.jpeg', 'rb') as no_image:
                self._no_image = no_image.read()

            s3_config = Configuration('S3')
            bucket_name = s3_config['S3_BUCKET_NAME']
            access_key = s3_config['S3_ACCESS_KEY']
            secret_key = s3_config['S3_SECRET_KEY']
            file_prefix = s3_config['S3_KEY_PREFIX']

            self._file_prefix = file_prefix if file_prefix is not None \
                and len(file_prefix) > 0 else self.DEFAULT_PREFIX

            ir_attachment_config = Configuration('IR_ATTACHMENT')
            s3_enabled = ir_attachment_config.getboolean('CLOUD_ATTACHMENT_STORAGE')
            s3_mime_type_filter_regex = ir_attachment_config['CLOUD_MIME_TYPE_FILTER_REGEX']

            self._s3_mime_type_filter = '.*' if s3_mime_type_filter_regex is None or s3_mime_type_filter_regex == '' \
                else s3_mime_type_filter_regex

            self._s3_enabled = False if s3_enabled is None else s3_enabled

            if self._s3_enabled:
                self._s3_bucket = S3Bucket(bucket_name, access_key, secret_key)
                if self._s3_bucket is None:
                    raise ConnectionError('Could not connect to S3 Bucket.')

            self._s3_initialized = True

    def _file_read(self, fname, bin_size=False):

        # Because there is no direct control over how this class is
        # instantiated, it's best to check for initialisation routine every time
        self._initialize()

        # Use fname prefix check to see if the file is to be retrieved from s3
        if fname.startswith(self._file_prefix):

            data = self._s3_bucket[fname]
            if data is None:
                data = self._no_image

            return base64.b64encode(data)

        else:
            return super(S3Attachment, self)._file_read(fname, bin_size)

    def _file_delete(self, fname):

        if self._s3_bucket is not None \
                and fname.startswith(self._file_prefix):
            del self._s3_bucket[fname]

        else:
            return super(S3Attachment, self)._file_delete(fname)

    def _get_datas_related_values(self, data, mimetype):
        # Because there is no direct control over how this class is
        # instantiated, it's best to check for initialisation routine every time
        self._initialize()

        # Apply mimetype REGEX filter
        match = re.search(self._s3_mime_type_filter, mimetype)

        if self._s3_enabled and match is not None:

            bin_data = base64.b64decode(data) if data else b''

            # Files stored in S3 will have the prefix
            fname = self._file_prefix + hashlib.sha1(bin_data).hexdigest()

            # store file in S3
            self._s3_bucket[fname] = bin_data

            values = {
                'file_size': len(bin_data),
                'checksum': self._compute_checksum(bin_data),
                'index_content': self._index(bin_data, mimetype),
                'db_datas': False,
                'store_fname': fname,
            }

            return values

        else:
            return super(S3Attachment, self)._get_datas_related_values(data, mimetype)

    def _inverse_datas(self):
        for attach in self:
            vals = self._get_datas_related_values(attach.datas, attach.mimetype)
            # take current location in filestore to possibly garbage-collect it
            fname = attach.store_fname
            # write as superuser, as user probably does not have write access
            super(S3Attachment, attach.sudo()).write(vals)
            # FIXME: if user uploads the same image twice, this below call will delete the actual file in s3
            # if fname:
            #     self._file_delete(fname)
