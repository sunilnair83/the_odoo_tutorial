import logging
import sys
from confluent_kafka import Producer, Consumer
from ..configuration import Configuration


class KafkaHelper:
    __instance = None
    _producer = None
    _consumer = None
    _logger = None

    def __new__(cls):
        """
        Singleton implementation for Kafka Producer
        """

        if KafkaHelper.__instance is None:
            kafka_config = Configuration('KAFKA')
            instance = object.__new__(cls)

            # Create the Kafka producer
            instance._producer = Producer({
                'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
                'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
                'acks': 'all',
                'debug': "topic,msg,broker",
            })

            instance._consumer = Consumer({
                'enable.auto.commit': True,
                'group.id': 'my_group2',
                'auto.offset.reset': 'latest',
                'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
                'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
            })

            # Set up logger
            logger = logging.getLogger('Kafka-Producer-Logger')
            logger.setLevel(logging.DEBUG)
            ch = logging.StreamHandler(sys.stdout)
            ch.setFormatter(logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(threadName)s:%(message)s"))
            logger.addHandler(ch)
            instance._logger = logger

            KafkaHelper.__instance = instance

        return KafkaHelper.__instance

    def publish(self, topic, message):
        """
        Publishes a Kafaka message to a topic
        """
        self._producer.produce(topic, message)
        self._producer.flush(30)
        self._logger.debug("Message Produced...")

    def consume_from_kafka(self, topic):
        self._consumer.subscribe([topic])
        while True:
            msg = self._consumer.poll(1.0)
            if msg is None:
                break
            if msg.error():
                print("Consumer error: {}".format(msg.error()))
                break
            print('Received message: {}'.format(msg.value().decode('utf-8')))