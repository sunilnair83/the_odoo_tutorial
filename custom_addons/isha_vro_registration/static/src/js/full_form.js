odoo.define('isha_vro_registration.FullForm', ['isha_website_utils.BaseForm', 'web.ajax', 'web.rpc'], function (require) {
	'use strict';

    var ajax = require('web.ajax');
	var BaseForm = require('isha_website_utils.BaseForm');
	var FullForm = BaseForm.extend({
	    validateForm($form) {
			let valid = true

			// Validate Email Addresses
            this.validateEmailAddress($form);

            // Do the Validation
			if (! $form[0].checkValidity() && !$('.is-invalid').length) {
				$form[0].classList.add('was-validated');
				setTimeout(() => {
					if ($form.find('input:invalid, input.is-invalid').length) {
						let $error_field = $form.find('input:invalid, input.is-invalid').eq(0)
						$error_field.focus();
						$('html, body').animate({scrollTop: $error_field.offset().top - 100}, 500);
					} else if ($form.find('.form-control:invalid').filter('select').length) {
						$form.find('.form-control:invalid').filter('select').eq(0).select2('open');
					}
				}, 100);
				valid = false
			}
			return valid
		},

		/**
         * Validate Email Addresses Manually
         *
         * NOTE: HTML5 email validity support without .<domain>
         */
        isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
        },

		validateEmailAddress($form){
		    // Validate Email - NOTE: HTML5 email validity support without .<domain>
		    let that = this;
			$form.find('input[type=email]').each(function(){
			    let v = $(this).val(),
			        em = $(this)[0];
			    if (v != '' && !that.isEmail(v) ){
                    em.setCustomValidity('Please enter a valid email address.');
			    }else{
			        em.setCustomValidity('');
			    }
			})
		},

	    onFormSaveSuccess(response) {
			let $success_box = $('#alert-success-box').clone()
                $success_box.removeClass('hide');
			$('#registration-box').html($success_box);
			$('#vro-form-title').html('Success!')
			$('html, body').animate({
                'scrollTop' : $("#alert-success-box").position().top
            });
		}
	});

    function parentConsentRequired(){
        let $dob = $('#dateOfBirth'),
            dob_val = $dob.val(),
            years = 0;

        if ( dob_val !== '' ){
            let start_dt = moment(dob_val),
                today = moment();
            years =  today.diff(start_dt, "years");
        }

        return years >= 14 && years <= 18;
    }

	var fullForm = new FullForm({});
	$('.btn-save').on('click', function() {
		let $form = $('form'),
		    $saveConfirmModal = $('#saveConfirmation'),
		    $parentConsentConfirmModal = $('#parentConsentConfirmation');

        $('.toast').toast('hide'); // hide previous toast if any
        if (fullForm.validateForm($form)){
            // Parents consent required ??
            if( parentConsentRequired() ){
                $parentConsentConfirmModal.modal('show');
            }else{
                // Show Save Confirmation Modal directly
                $saveConfirmModal.modal('show');
            }
        }
	});

    /**
     * Action on Parent consent
     */
	$('#parentConsentConfirmation .btn-ok').on('click', function (e){
	    e.preventDefault();
	    let $parentConsentConfirmModal = $('#parentConsentConfirmation'),
		    $saveConfirmModal = $('#saveConfirmation');

        // Hide parent consent model
        $parentConsentConfirmModal.modal('hide');

        // Show Save Confirmation Modal
        $saveConfirmModal.modal('show');
	});

	$('#saveConfirmation .btn-ok').on('click', function (e){
	    e.preventDefault();
	    let $form = $('form'),
		    $saveConfirmModal = $('#saveConfirmation');

        // Save the Form
        fullForm.saveForm($form, false);
        $saveConfirmModal.modal('hide');
	});


    /**
     * Render Seva Schedules on Seva type change
     */
	$('#sevaType').on('change', async function () {
	    var v = $(this).val(),
	        $opt = $(this).children("option:selected"),
	        schedule_required = $opt.data('schedule-required');
	        console.log(schedule_required)
	    $("#sevaSchedule").html('<option value=""></option>');
	    if(v != '' && schedule_required == 'Yes'){
            ajax.jsonRpc('/vro-registration/seva-schedules-json/'+v, 'call', {})
                .then(function (data) {
                    $.each(data, function(){
                        $("#sevaSchedule").append(
                            '<option value="'+ this.id +'" data-arrival-date="'+this.arrival_date+'" data-departure-date="'+this.departure_date+'">' +
                                this.name +
                            '</option>');
                    });
                    // Show seva schedule
                    $('#seva-schedule-box').fadeIn('normal')
                    // update select2
                    $("#sevaSchedule").select2("destroy");
                    $("#sevaSchedule").select2() // initialize
                    $("#sevaSchedule").select2('open'); // open the select2 dropdown
                })
                .guardedCatch(function () {
                });
	    }else{
	        // Hide seva schedule
	        $('#seva-schedule-box').fadeOut('fast')
	    }
	});


	/**
     * Update Visit Dates on Seva Schedule change
     */
	$('#sevaSchedule').on('change', async function () {
	    var $opt = $(this).children("option:selected")
	    var v = $opt.val()
	    if(v != ''){
           $('#dateOfArrival').val($opt.data('arrival-date'))
           $('#dateOfDeparture').val($opt.data('departure-date'))
	    }
	});

	/**
     * Validate Phone Number
     */
//	$('input.phone-field_number').on('blur', function(event) {
//        var $phone = $(this),
//            $frm = $phone.closest('form'),
//            id = $phone.attr('id'),
//            $cc = $('#country-code-'+id),
//            phone_number = $phone.val().trim(),
//            country_code = $cc.val().trim();
//        if (phone_number != "" && country_code != "") {
//            var rpc = require('web.rpc');
//            rpc.query({
//                route: '/vro-registration/validate_phone_number/',
//                params: {
//                    'phone_number': phone_number,
//                    'country_code': country_code
//                },
//            }).then(function(is_valid) {
//                var ph = $phone[0];
//                $frm.addClass('was-validated');
//                if (is_valid) {
//                    ph.setCustomValidity('');
//                } else {
//                    console.log('Invalid phone');
//                    ph.setCustomValidity('Invalid phone number');
//                }
//            }).catch(function(error) {
//                console.log(error)
//            });
//        }
//    });

    /**
     * Visit Info - Arrival/Departure Change
     */
	$('#sevaType').on('change', function () {
	    toggle_visit_info_error_msg()
	});
    // Datetimepicker change
    $('.input-group.date').on('dp.change', function(e) {
        let id = $(this).find('input[type="text"]').attr('id');
        if (id == 'dateOfArrival' || id == 'dateOfDeparture')
            toggle_visit_info_error_msg();
        else if (id == 'dateOfBirth')
            toggle_dob_error_msg();
    });

    /**
     * Nationality Change - Show error if Not India
     */
	$('#nationality').on('change', function () {
	    toggle_nationality_cor_error_msg()
	});

	/**
     * Load States on Country change
     */
	$('#countryOfResidence').on('change', async function () {
	    var v = $(this).val(),
	        name = $(this).children("option:selected").html().trim();
	    $("#stateId").html('<option value=""></option>');
	    toggle_nationality_cor_error_msg()
	    if(v !== '' && name === 'India'){
            ajax.jsonRpc('/vro-registration/country-states-json/'+v, 'call', {})
                .then(function (data) {
                    $.each(data, function(){
                        $("#stateId").append('<option value="'+ this.key +'">'+ this.value +'</option>');
                    });
                    // update select2
                    $("#stateId").select2("destroy");
                    $("#stateId").select2();
                })
                .guardedCatch(function () {
                });
	    }
	});

    /**
     * Update state and city based on postal code change
     */
    $('input[name="postalCode"]').on('blur', async function () {
		var $pc        = $(this),
		    postalCode = $pc.val(),
		    input = $pc[0];
        input.setCustomValidity(''); // remove validity
		if (postalCode != '' && postalCode.length == 6 ) {
			let countryCode = $('#countryOfResidence option:selected').attr('data-code');
			let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${countryCode}/pincodes/${postalCode}`)
			let address_details_json = await address_details.json()
			// We are matching with name becuase state id is different in API and in options
			let stateId = $('#stateId option').filter(function () {
			    let state_txt = $(this).html().trim()
			    let state_txt_remote = address_details_json.state ? address_details_json.state.trim() : ''
			    return state_txt == state_txt_remote;
            }).val();

            $('select#stateId').val(stateId).trigger('change');
			if (address_details_json.defaultcity)
			    $('input[name="city"]').val(address_details_json.defaultcity);
		}else{
		    input.setCustomValidity('Minimum 6 digits required!');
		    $pc.closest('form').addClass('was-validated');
		}
    });

	/**
	 * Cancel Visit
	 */
	 $('a.cancel-visit').on('click', async function (e) {
	    e.preventDefault();
	    var $a = $(this);
	    var url = $(this).data('url');
	    var $modal = $('#cancelConfirmationModel')
	    $modal.modal('show');
	    // click events firing multiple times - use unbind()
        $('.modal-btn-primary', $modal).unbind().click(function(e) {
            e.preventDefault()
            $('.loader').show();
            ajax.jsonRpc(url, 'call', {})
                .then(function (response) {
                    var $row = $a.closest('tr');
                    $row.find('td.registration-stage').html('Canceled');
                    $a.closest('td').html('')
					$('.loader').hide();
				    $modal.modal('hide');
                })
                .guardedCatch(function (reason) {
					$('.loader').hide();
					$modal.modal('hide');
                });
        });
	 });
});

/**
 * Toggle Visibility of Nationality/COR Error Message
 */
function toggle_nationality_cor_error_msg(){
    let $error_box = $('#nationality-cor-error-msg'),
        $nat = $('#nationality').children("option:selected"),
        $cor = $('#countryOfResidence').children("option:selected"),
        nat_name = $nat.html() ? $nat.html().trim() : '',
        cor_name = $cor.html() ? $cor.html().trim() : '';
    if ( (nat_name !== '' && nat_name !== 'India') || (cor_name !== '' && cor_name !== 'India') ){
        $error_box.removeClass('hide');
        $('html, body').animate({
            'scrollTop' : $error_box.position().top
        });
        $('.btn-save').hide()
    }else{
        $error_box.addClass('hide');
        $('.btn-save').show()
    }
}

/**
 * Toggle Visibility of Visit Info Error Message
 */
function toggle_visit_info_error_msg(){
    let $error_box = $('#visit-info-error-msg'),
        $doa_err_box = $('#date-of-arrival-error-msg'),
        ref_seva_type_id = $error_box.data('ref-seva-type-id'),
        seva_type_id = $('#sevaType').val(),
        $arrivalDt = $('#dateOfArrival'),
        $departureDt = $('#dateOfDeparture'),
        start_dt = $arrivalDt.val(),
        end_dt = $departureDt.val(),
        days = 0,
        $frm = $arrivalDt.closest('form'),
        inputArrivalDt = $arrivalDt[0];

    // hide error box initially
    $error_box.addClass('hide');
    $doa_err_box.addClass('hide');
    $('.btn-save').show();
    inputArrivalDt.setCustomValidity(''); // remove validity
    if ( start_dt != '' && end_dt != '' ){
        let start = moment(start_dt),
            end = moment(end_dt),
            today = moment().startOf('day'),
            duration =  end.diff(start, "days");

        // Do not allow past date
        console.log(start_dt)
        let err_msg = '';
        if ( today.isAfter(start_dt) ){
            err_msg = 'Arrival Date can not be past date!';
            inputArrivalDt.setCustomValidity(err_msg);
            $frm.addClass('was-validated');
            $doa_err_box.html(err_msg).removeClass('hide');
        }
        else if ( start.isAfter(end) ){
            err_msg = 'Arrival Date must be less than Departure Date!';
            inputArrivalDt.setCustomValidity(err_msg);
            $frm.addClass('was-validated');
            $doa_err_box.html(err_msg).removeClass('hide');
        }

        // Ashram Volunteering Min 30 Days Required
        if ( ref_seva_type_id == seva_type_id ){
            // Show error box
            if (duration < 30 ){
                $error_box.removeClass('hide');
                $('.btn-save').hide()
            }
        }
    }



}

/**
 * Toggle Visibility of Visit DOB Error Message
 */
function toggle_dob_error_msg(){
    let $dob = $('#dateOfBirth'),
        $error_box = $('#dob-validation-error-msg'),
        dob_val = $dob.val(),
        dob_input = $dob[0];

    // hide error box initially
    $error_box.addClass('hide');
    $('.btn-save').show();
    dob_input.setCustomValidity(''); // remove validity
    if ( dob_val !== '' ){
        let start_dt = moment(dob_val),
            today = moment(),
            years =  today.diff(start_dt, "years");

        // Toggle Validity and Error Message
        if (years < 14 ){
            let $frm = $dob.closest('form');
            $frm.addClass('was-validated');
            dob_input.setCustomValidity('Minimum age should be 14 years!');
            $error_box.removeClass('hide');
            $('.btn-save').hide();
        }
    }
}

/**
 * Validate Postal Code on load
 */
function validate_postal_code(){
    let $pc = $('#postalCode'),
        postalCode = $pc.val(),
        input = $pc[0];

    input.setCustomValidity(''); // remove validity
    if ( postalCode == '' || postalCode.length != 6 ) {
        input.setCustomValidity('Minimum 6 digits required!');
    }
}

function bootstrap_vro_form(){
    /**
     * Task 1: Hide Seva Schedule if Seva type does not require schedule
     */
    let v = $('#sevaType').val(),
        $opt = $('#sevaType').children("option:selected"),
        schedule_required = $opt.data('schedule-required');

    if ( v == '' || schedule_required == 'No' )
        $('#seva-schedule-box').fadeOut('fast');

    /**
     * Task 2: Seva Type - General Volunteering, Min 30 Days Required
     */
    if ( $('#visit-info-error-msg').length)
        toggle_visit_info_error_msg();

    /**
     * Task 3: Nationality or CoR Error
     */
    if ($('#nationality-cor-error-msg').length)
        toggle_nationality_cor_error_msg();

    /**
     * Task 4: DOB Error Validation
     */
    if ($('#dob-validation-error-msg').length)
        toggle_dob_error_msg();

    /**
     * Task 5: Postal Code Validation
     */
    if ($('#postalCode').length)
        validate_postal_code();
}
$(document).ready(function(){
    bootstrap_vro_form()
})
