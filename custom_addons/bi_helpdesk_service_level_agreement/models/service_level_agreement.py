# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import datetime
from datetime import timedelta

from dateutil.relativedelta import relativedelta

from odoo import models, fields, api


class ServiceLevelAssignment(models.Model):
    _name = "service.level.config"

    name = fields.Char(string="Name")
    workhistory_lines = fields.One2many('work.history', 'work_id', string="Working Time History")


class SLABreachNotification(models.Model):
    _name = 'sla.breach.notification'

    sla = fields.Many2one('service.agreement')
    breached_tickets = fields.Many2many('support.ticket','sla_breach_notif_rel')



class ServiceLevelTeam(models.Model):
    _name = "service.level.team"

    name = fields.Char(string="Name")
    helpdesk_team = fields.Many2one('support.team', string="Helpdesk Team")
    # user_id = fields.Many2one('res.users', string="User")
    company_id = fields.Many2one('res.company', string='Company', required=True, default=lambda self: self.env.user.company_id)
    service_agreement_lines = fields.One2many('service.agreement', 'service_agreement_id', string="Service Agreement")
    notes = fields.Text(string="Notes",default="Time To Live\n - Maximum time a ticket can stay on the 'Target stage'\n"
                                               " - The time considered here is from the time the ticket moved to the 'Target stage'\n"
                                               "Throughput time\n - Maximum time within which it should have moved to 'Target stage' from any of the selected 'From stage' \n"
                                               " - The time considered here from the creation date of the ticket")

    def action_notify_sla_breach1(self):
        for sla_team in self.search([]):
            for sla in sla_team.service_agreement_lines:
                if sla.sla_type == 'max_ttl':
                    breach_datetime = (datetime.datetime.now() - timedelta(seconds=sla.service_stage_time * 3600)).strftime('%Y-%m-%d %H:%M:%S')

                    domain = [('stage_id','=',sla.stage_id.id),
                              ('support_team_id','=',sla_team.id),
                              ('stage_change_datetime','<',breach_datetime)]
                    breach_tickets = self.env['support.ticket'].search(domain)
                    if breach_tickets:
                        mail_template = self.env.ref('bi_helpdesk_service_level_agreement.sla_breach_template1')
                        notif_obj = self.env['sla.breach.notification'].create({
                            'sla':sla.id,
                            'breached_tickets':[(6,0,breach_tickets.ids)]
                        })
                        values = mail_template.generate_email(notif_obj.id, fields=None)
                        values['recipient_ids'] = [(6,0,sla.notify_users.mapped('partner_id').ids)]
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.sudo().create(values)
                        msg_id.send(False)
                        notif_obj.unlink()
                elif sla.sla_type == 'throughput':
                    breach_datetime = (
                                datetime.datetime.now() - timedelta(seconds=sla.service_stage_time * 3600)).strftime(
                        '%Y-%m-%d %H:%M:%S')

                    domain = [('stage_id', '!=', sla.stage_id.id),
                              ('stage_id', 'in', sla.from_stage_id.ids),
                              ('support_team_id', '=', sla_team.id),
                              ('date_create', '<', breach_datetime)]
                    breach_tickets = self.env['support.ticket'].search(domain)
                    if breach_tickets:
                        mail_template = self.env.ref('bi_helpdesk_service_level_agreement.sla_breach_template2')
                        notif_obj = self.env['sla.breach.notification'].create({
                            'sla': sla.id,
                            'breached_tickets': [(6, 0, breach_tickets.ids)]
                        })
                        values = mail_template.generate_email(notif_obj.id, fields=None)
                        values['recipient_ids'] = [(6, 0, sla.notify_users.mapped('partner_id').ids)]
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.sudo().create(values)
                        msg_id.send(False)
                        notif_obj.unlink()
        return True


class TicketAnalysis(models.Model):
    _name = "service.level.analysis"

    name = fields.Char()
    id_off = fields.Integer('Record')
    team_id = fields.Many2one('support.team', string="Helpdesk Team")
    source_stage = fields.Char('Stage')
    start_at = fields.Datetime('Start At')
    end_at = fields.Datetime('End At')
    estimate_time = fields.Float(string='Estimated Time (HH:MM)', default=0.0)
    working_time = fields.Float(string='Working Time (HH:MM)', default=0.0)
    overtime_time = fields.Float(string='Overtime Time (HH:MM)', default=0.0)
    delay_time = fields.Float(string='Delay Time (HH:MM)', default=0.0)


class ServiceAgreement(models.Model):
    _name = "service.agreement"

    service_agreement_id = fields.Many2one('service.level.team', string="Service Agreement")
    stage_id = fields.Many2one('support.stage', string="Target Stage")
    from_stage_id = fields.Many2many('support.stage', string="From Stage")

    def _get_managers_domain(self):
        return [('groups_id','in',[self.env.ref('bi_website_support_ticket.group_support_manager').id])]

    notify_users = fields.Many2many('res.users','sla_breach_users_rel',string='Notify to',domain=_get_managers_domain)
    service_stage_time = fields.Float(string="Estimated Time (HH:MM)", default=0.0,help="Enter days also in hours. E.g. 3 days is 72:00")
    sla_type = fields.Selection([('max_ttl','Time To Live'),('throughput','Throughput time')], string='SLA Type')

    @api.onchange('stage_id')
    def populate_from_stage_ids(self):
        if self.stage_id and self.sla_type == 'throughput':
            stage_ids = self.env['support.stage'].sudo().search([('sequence','<',self.stage_id.sequence)])
            self.from_stage_id = [(6,0,stage_ids.ids)]

class WorkHistory(models.Model):
    _name = "work.history"

    category = fields.Many2one('support.ticket.type', string="Category")
    priority = fields.Selection([('0', 'Low'), ('1', 'Normal'), ('2', 'High')], string='Priority', default='0')
    gap = fields.Integer(string='Gap', required=True, default=1)
    period_type = fields.Selection([('hours',  'Hours'),  ('days',  'Days'), ('weeks', 'Weeks')], required=True, default='hours')
    work_id = fields.Many2one('service.level.config','Helpdesk SLA Level')


class ResPartner(models.Model):
    _inherit = 'res.partner'

    sla_level_id = fields.Many2one('service.level.config', 'Helpdesk SLA Level')


class SupportTicketInherit(models.Model):
    _inherit = 'support.ticket'

    stage_change_datetime = fields.Datetime(string='Stage Changed On')

    @api.depends('stage_id')
    def set_stage_change_datetime(self):
        self.write({'stage_change_datetime':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')})


    @api.onchange('stage_id')
    def sla_log(self):
        ticket_id = self.env['support.ticket'].browse(self.env.context.get('active_id'))
        log_list = []
        record_list = []
        analysis_list = []        
        log_obj = self.env['support.slalog']
        analysis_obj = self.env['service.level.analysis']
        for log in self:
            if log.support_log_line:
                for record in log.support_log_line:
                    record_list.append(record.id)
            date_close = None    
            sla_team = self.env['service.level.team'].search([('helpdesk_team', '=', self.support_team_id.id)])
            if log.support_team_id:
                for time in sla_team.service_agreement_lines:
                    if log.stage_id == time.stage_id:
                        log_list.append((0, 0, {
                            'name' : log.sequence, 
                            'stage_id' : log.stage_id.id, 
                            'start_date' : datetime.datetime.now(), 
                            'estimate_time' : time.service_stage_time, 
                            }))
            work_time = overtime = delay = 0.0
            for record in record_list:
                slalog = log_obj.browse(record)
                if slalog:
                    if not slalog.end_date:
                        slalog.update({'end_date' : datetime.datetime.now()})
                    if slalog.end_date and slalog.start_date:
                        work = (slalog.end_date - slalog.start_date).total_seconds()
                        work_time = work / 3600
                        if work_time != 0.0:
                            if slalog.estimate_time < work_time:
                                overtime = work_time - float(slalog.estimate_time)
                                delay = work_time - float(slalog.estimate_time)
                        slalog.update({'working_time' : work_time, 'overtime_time' : overtime, 'delay_time' : delay})

            log.support_log_line = log_list 
            for i in log.support_log_line:
                record_list.append(i.id)
            log.support_log_line = [(6, 0, record_list)]
            
        ### Analysis view for ticket task changes. ###
        analysis = None
        analysis_ids =  analysis_obj.search([])

        id_off_list = []
        for ids in analysis_ids:
            id_off_list.append(ids.id_off)

        for record in record_list:
            slalog = log_obj.browse(record)
            if not int(slalog.id) in list(set(id_off_list)):
                if slalog:
                    vals = {
                        'name' : self.sequence, 
                        'id_off' : int(slalog.id), 
                        'team_id' : self.support_team_id.id, 
                        'source_stage' : slalog.stage_id.name, 
                        'start_at' : slalog.start_date, 
                        'end_at' : slalog.end_date, 
                        'estimate_time' : slalog.estimate_time, 
                        'working_time' : slalog.working_time, 
                        'overtime_time' : slalog.overtime_time, 
                        'delay_time' : slalog.delay_time, 
                        }
                    analysis = analysis_obj.create(vals)
                    analysis_list.append(slalog.id)

    @api.onchange('category', 'priority', 'sla_level_id','date_create')
    def deadline_date_(self):
        for ticket in self:
            date_close = None
            sla_config = self.env['service.level.config'].search([('name', '=', ticket.sla_level_id.name)])
            if ticket.sla_level_id:
                if ticket.category and ticket.priority:
                    for time in sla_config.workhistory_lines:
                        if (time.category == ticket.category):
                            time_priority = time.priority
                            ticket_priority = ticket.priority
                            if (ticket_priority == time_priority):
                                if ticket.date_create:
                                    date_create = datetime.datetime.combine(ticket.date_create, datetime.time.min)   
                                    if time.period_type == 'hours':
                                            date_close = date_create + relativedelta(hours=time.gap)
                                    if time.period_type == 'days':
                                            date_close = date_create + relativedelta(days=time.gap)
                                    if time.period_type == 'weeks':
                                            date_close = date_create + relativedelta(weeks=time.gap)
            ticket.deadline_date = date_close

    support_log_line = fields.One2many('support.slalog', 'ticket_log_id', string="SLA Log", store=True)
    sla_level_id = fields.Many2one('service.level.config', 'Helpdesk SLA Level')
    deadline_date = fields.Datetime('Deadline Date', store=True)

    @api.model
    def create(self, vals):
        res = super(SupportTicketInherit, self).create(vals)
        log_list = []
        if res.partner_id.sla_level_id:
            res.update({'sla_level_id' : res.partner_id.sla_level_id.id})
        sla_team = self.env['service.level.team'].search([('helpdesk_team', '=', self.support_team_id.id)])
        if res.stage_id.name == 'New':
            if res.support_team_id:
                for time in sla_team.service_agreement_lines:
                    if res.stage_id == time.stage_id:
                        log_list.append((0, 0, {
                            'name' : self.sequence, 
                            'stage_id' : self.stage_id.id, 
                            'start_date' : datetime.datetime.now(), 
                            'estimate_time' : time.service_stage_time, 
                            }))
            res.support_log_line = log_list
        return res

    def write(self, vals):
        res = super(SupportTicketInherit, self.sudo()).write(vals)
        log_list = []
        sla_team = self.env['service.level.team'].search([('helpdesk_team', '=', self.support_team_id.id)])
        for log in self:
            if self.stage_id.name == 'New':
                if self.support_team_id:
                    if not self.support_log_line:
                        for time in sla_team.service_agreement_lines:
                            if self.stage_id == time.stage_id:
                                log_list.append((0, 0, {
                                    'name' : log.sequence, 
                                    'stage_id' : log.stage_id.id, 
                                    'start_date' : datetime.datetime.now(), 
                                    'estimate_time' : time.service_stage_time, 
                                    }))
                                log.support_log_line = log_list
                    else:
                        return res
        return res

    @api.onchange('partner_id')
    def sla_team(self):
        for ticket in self:
            if ticket.partner_id:
                if ticket.partner_id.sla_level_id:
                    ticket.sla_level_id = ticket.partner_id.sla_level_id.id


class SlaLog(models.Model):
    _name = 'support.slalog'
    _order = 'ticket_log_id'

    name = fields.Char()
    ticket_log_id = fields.Many2one('support.ticket', string="Log")
    stage_id = fields.Many2one('support.stage', string="Stage")
    start_date = fields.Datetime(string='Start At')
    end_date = fields.Datetime(string='Done At')
    estimate_time = fields.Float(string='Estimated Time (HH:MM)', default=0.0)
    working_time = fields.Float(string='Working Time (HH:MM)', default=0.0)
    overtime_time = fields.Float(string='Overtime Time (HH:MM)', default=0.0)
    delay_time = fields.Float(string='Delay Time (HH:MM)', default=0.0)
    

