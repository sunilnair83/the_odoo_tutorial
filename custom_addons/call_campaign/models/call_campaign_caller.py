# -*- coding: utf-8 -*-
# Part of Isha. See LICENSE file for full copyright and licensing details.

import datetime
import logging
import re
import uuid

from werkzeug import urls

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

from dateutil.relativedelta import relativedelta

email_validator = re.compile(r"[^@]+@[^@]+\.[^@]+")
_logger = logging.getLogger(__name__)


class CallCampaignCaller(models.Model):
    _name = 'call_campaign.caller'
    _description = 'Campaign Caller Rel'
    _rec_name = 'partner_id'

    _sql_constraints = [
        ('unique_token', 'UNIQUE (token)', 'A token must be unique!'),
    ]

    call_campaign_id = fields.Many2one('call_campaign.call_campaign', 'Campaign', ondelete='cascade', index=True)
    partner_id = fields.Many2one('res.partner', 'Caller', index=True)
    active = fields.Boolean("Active", default=True)
    lead_ids = fields.One2many('call_campaign.user_input', 'caller_id', 'People contacted')
    contacts_assigned_count = fields.Integer('Assigned',compute='_compute_leads_count')
    contacts_completed_count = fields.Integer('Completed',compute='_compute_leads_count')
    contacts_skipped_count = fields.Integer('Skipped',compute='_compute_leads_count')

    start_datetime = fields.Datetime('Start date and time', readonly=True)
    # identification and access
    token = fields.Char('Identification token', default=lambda self: str(uuid.uuid4()), readonly=True, required=True, copy=False)
    public_url = fields.Char("Unique link", compute="_get_call_campaign_url")

    @api.depends('lead_ids','lead_ids.state')
    def _compute_leads_count(self):
        for rec in self:
            rec.contacts_assigned_count = len(rec.lead_ids.ids)
            rec.contacts_completed_count = len(rec.lead_ids.filtered(lambda x: x.state == 'done').ids)
            rec.contacts_skipped_count = len(rec.lead_ids.filtered(lambda x: x.state == 'skip').ids)

    def action_resend(self):
        partners = self.env['res.partner']
        emails = []
        for user_answer in self:
            if user_answer.partner_id:
                partners |= user_answer.partner_id
            elif user_answer.email:
                emails.append(user_answer.email)

        return self.call_campaign_id.with_context(
            default_existing_mode='resend',
            default_partner_ids=partners.ids,
            default_emails=','.join(emails)
        ).action_send_call_campaign()

    def action_print_calls(self):
        """ Open the website page with the call_campaign form """
        self.ensure_one()
        return {
            'type': 'ir.actions.act_url',
            'name': "View calls",
            'target': 'self',
            'url': '/call_campaign/print/%s?caller_token=%s' % (self.call_campaign_id.access_token, self.token)
        }

    def _get_call_campaign_url(self):
        """ Computes a public URL for the call_campaign """
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for caller in self:
            caller.public_url = urls.url_join(base_url, "call_campaign/start/%s?caller_token=%s" % (caller.call_campaign_id.access_token, caller.token))

