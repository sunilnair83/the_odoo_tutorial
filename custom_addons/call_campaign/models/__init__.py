# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import call_campaign_call_campaign
from . import call_campaign_caller
from . import call_campaign_question
from . import call_campaign_user
from . import res_partner
