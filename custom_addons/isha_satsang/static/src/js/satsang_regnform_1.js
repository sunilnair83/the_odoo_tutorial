function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
var jspgremstatecoll=[];
var jspgremcountrycoll=[];
defer(jspgmsatsangdocumentready);
function jspgmsatsangdocumentready(){
  $(document).ready(function () {
        jsgstngfmwtsapp();
        jspgmreginitfunction();
        jspgmregfmselectstates();
        jspgmregfmcountryandcodechange();
        jspgmregfmcountrypincode();
        jspgmregfmcountryphonecode();
        jspgmregfmwatsappcountryphonecode();
        jspgmregsetlangpref();onchangeofpournamitz();jspgmregfmpackagecost();
//        jsgstngfmloadaddressforpincode()
        $("form :input").attr("autocomplete", "off");
});
}

function jspgmregsetlangpref() {
    try {
        var paramsvars = new URLSearchParams(location.search);
        var langvar = paramsvars.get('language').toUpperCase()
        $("select#jsgstngfmpreflang option").each(function() {
            if ($(this).text().toUpperCase() == langvar) {
                $("select#jsgstngfmpreflang").val($(this).val());
                return false;
            }

        });
    } catch (e) {}
}

function jspgmreginitfunction() {
    var initObjvar;
    var addobjvar;
    $('#jsgstngfmstate option').each(function(ivar,jvar){
       var objvar ={
        "valueattr":$(jvar).attr("value"),
        "statenameattr":$(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
}

function jspgmregfmselectstates() {
    var state = document.getElementById("jsgstngfmstate");

    var countryname = document.getElementById("jsgstngfmcountry");
    var countryid = countryname.value;

    var idvar = 0;
    Array.from(state.options).forEach(item => {
        var trend = item.getAttribute("countryid");
        if (trend == countryid) {
            item.style.display = "block";
            item.disabled = false;
            if (idvar == 0)
                idvar = item.index;
        } else
            {
            item.style.display = "none";
            item.disabled = true;
            }
    });
    if (idvar != 0) {
        state.options.selectedIndex = idvar;
        state.disabled = false;
        state.height = 35;
    }
     else if (idvar == 0) {
       state.options.selectedIndex = 0;
        state.disabled = true;
    }
}

function jsgstngfmwtsapp(){
    $('#jsgstngfmwtsappyes').change(function () {
     if ($(this).is(":checked"))
     {
      $('.jsgstngfmwtsappcls').css("display","none")
      $(this).val('YES')
      //$('#jsgstngfmwtsappcountrycode').prop('required',  false)
      //$('#jsgstngfmwtsappphone').prop('required',  false)

        }
    });
    $('#jsgstngfmwtsappno').change(function () {
     if ($(this).is(":checked"))
     {
      $('.jsgstngfmwtsappcls').css("display","flex")
      $(this).val('NO')
      //$('#jsgstngfmwtsappcountrycode').prop('required',  true)
      //$('#jsgstngfmwtsappphone').prop('required',  true)
        }
    });
}

function jspgmregfmcountryandcodechange(){
$('#jsgstngfmcountry').change(function () {
jspgmregfmcountrypincode()
});

$('#jsgstngfmcountrycode').change(function () {
jspgmregfmcountryphonecode()
});

$('#jsgstngfmwtsappcountrycode').change(function () {
jspgmregfmwatsappcountryphonecode()
});
}

function jspgmregfmwatsappcountryphonecode(){
     if( $('#jsgstngfmwtsappcountrycode').val() == '91'){
        $('#jsgstngfmwtsappphone').attr('maxlength', '10')
        $('#jsgstngfmwtsappphone').attr('minlength', '10')
        $('#jsgstngfmwtsappphone').attr('pattern', '[0-9]+')
    }
    else{
    $('#jsgstngfmwtsappphone').attr('maxlength', '15')
    $('#jsgstngfmwtsappphone').attr('minlength', '5')
    $('#jsgstngfmwtsappphone').attr('pattern', '[1-9]{1}[0-9]+')
    }
}

function jspgmregfmcountrypincode(){

  if(  $('#jsgstngfmcountry').val() == '104'){
        $('#jsgstngfmpincode').attr('maxlength', '6')
        $('#jsgstngfmpincode').attr('minlength', '6')
        $('#jsgstngfmpincode').attr('pattern', '[1-9]{1}[0-9]+')
    }
    else{
    $('#jsgstngfmpincode').attr('maxlength', '10')
    $('#jsgstngfmpincode').attr('minlength', '4')
    $('#jsgstngfmpincode').attr('pattern', '[A-Za-z0-9]*')
    }
}

function jspgmregfmcountryphonecode(){
 if( $('#jsgstngfmcountrycode').val() == '91'){
    $('#jsgstngfmphone').attr('maxlength', '10')
    $('#jsgstngfmphone').attr('minlength', '10')
    $('#jsgstngfmphone').attr('pattern', '[0-9]+')
}
else{
$('#jsgstngfmphone').attr('maxlength', '15')
$('#jsgstngfmphone').attr('minlength', '5')
$('#jsgstngfmphone').attr('pattern', '[1-9]{1}[0-9]+')
}
$('#jsgstngfmphone').prop('required',  true)
}

function jspgmsatsangfmvalidateform(){
    if(validateemptystring("#jsgstngfmfirstname")){
         alert($('#jsgstngfmfirstnamewarning').val())
         return false;
         }
    if(validateemptystring("#jsgstngfmlastname")){
         alert($('#jsgstngfmlastnamewarning').val())
         return false;
         }

        $("#submit").attr("disabled", true);
        $("#submit-spinner").attr("style", "display:inline-block")
//        document.getElementById('submit').disabled=true;
//        document.getElementById('submit-spinner').style.display='inline-block';
        return true;//jspgmregfmvalidateiswatsapp();
}


function jspgmregfmvalidateiswatsapp(){
if(!$('#jsgstngfmwtsappyes').is(':checked') && !$('#jsgstngfmwtsappno').is(':checked')){
    //alert($('#jsgstngfmwhatsappwarning'.val()))
    return false;
}
return true;

}
function jspgmregfmpackagecost() {
 $('#timezone_preferred').change(onchangeofpournamitz);
}
function onchangeofpournamitz()
   {
   try{
    var packageselection = document.getElementById("timezone_preferred");
    var optselected = packageselection.options[packageselection.selectedIndex].value;
    if(optselected == 'None') return;
    var programfee = document.getElementById("language_preferred");

    Array.from(programfee.options).forEach(item => {
        //console.log(item.value)
        //console.log(item.text)
        var trend = item.getAttribute("time_zone_id");
        var displayname = item.getAttribute("pckgdisplayname");
        if (trend == optselected)
            {item.style.display = "block";
            item.selected = 'selected'}
        else
            item.style.display = "none";
    });
}
catch(e){debugger;}
 }

function validateemptystring(element){
return $.trim($(element).val()) == "";
}

$(document).ready(function(){


    $('#jsgstngfmpincode').focusout(async () => {
        //let all_countries = await fetch('https://cdi-gateway.isha.in/contactinfovalidation/api/countries');
        //let all_countries_json = await all_countries.json()
        let selected_country = $('#jsgstngfmcountry option:selected').text();
        //let selected_country_iso2_code = _.filter(all_countries_json, (country) => {
        //    return country.name === selected_country;
        //})[0].iso2;
        let selected_country_iso2_code = $('#jsgstngfmcountry option:selected').attr("ccode");

        let pincode = $('#jsgstngfmpincode').val();
        pincode = pincode.trim();
        if( pincode !="")
        {
            let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
            let address_details_json = await address_details.json()

            if (! _.isEmpty(address_details_json)) {
                //let all_states = document.querySelectorAll('#stateid option')
                //let state = _.filter(all_states, (a) => a.innerText == address_details_json.state)[0]
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0]
                //state.setAttribute('selected', 'selected')
                $('#jsgstngfmstate').val(statevar["valueattr"]);

                $('#jsgstngfmcity').val(address_details_json.defaultcity)
//                $('#district').val(address_details_json.defaultcity)
            }
        }
    })
})
