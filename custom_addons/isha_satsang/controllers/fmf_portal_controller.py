from odoo.addons.portal.controllers.portal import CustomerPortal

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR


class FMFPortalController(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(FMFPortalController, self)._prepare_portal_layout_values()
        return values

    @http.route(['/onlinemegaprograms/fmf'], type='http', auth="user", website=True)
    def query_fmf_data(self, page=1, date_begin=None, date_end=None, sortby=None, search_in='record_name', search='', **kw):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'create_date'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'create_date desc'},
            'write_date_asc': {'label': _('Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Updated On (Desc)'), 'order': 'write_date desc'},
        }
        searchbar_inputs = {
            'record_name': {'input': 'record_name', 'label': _('Search in Contact Name')},
            'record_email': {'input': 'record_email', 'label': _('Search in Email')},
            'record_phone': {'input': 'record_phone', 'label': _('Search in Phone')},
            # 'ip_country': {'input': 'ip_country', 'label': _('Search in Country')},
            # 'all': {'input': 'all', 'label': _('Search in All')},
        }

        domain = [('active','=',True),('program_name','=','Online Pournami Satsang')] if search else []
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('record_email', 'all'):
                search_domain = OR([search_domain, [('record_email', 'ilike', search)]])
            if search_in in ('record_phone', 'all'):
                search_domain = OR([search_domain, [('record_phone', 'ilike', search)]])
            if search_in in ('record_name', 'all'):
                search_domain = OR([search_domain, [('record_name', 'ilike', search)]])

            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']

        pl_model_sudo = request.env['program.lead'].sudo()

        fmf_recs = pl_model_sudo.search(domain, order=order, limit=12) if search else pl_model_sudo

        values.update({
            'iecso_recs': fmf_recs,
            'page_name': 'iecso',
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            'default_url': '/onlinemegaprograms/fmf',
        })
        return request.render("isha_satsang.portal_fmf_view", values)
