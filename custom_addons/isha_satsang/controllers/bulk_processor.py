import datetime
import json
import logging
import traceback

import requests
import werkzeug
import werkzeug.wrappers

import odoo
from odoo.http import request, Response

_logger = logging.getLogger(__name__)
null = None
class FMFDataAPI(odoo.http.Controller):
    @odoo.http.route([
        '/api/fmf/registration'
    ], auth="none", type='json', methods=['POST'],csrf=False,cors='*')
    def fmf_data_api(self):
        _logger.info(str(request.jsonrequest))
        event_type = 'pournami'
        event_program_type = "Online Pournami Satsang"
        pgm_lead_data = []
        for post in request.jsonrequest.get('registrations'):
            profileid = post.get('jsgstngfmprofileid')
            first_name = post.get('jsgstngfmfirstname')
            last_name = post.get('jsgstngfmlastname')
            countrycode = post.get('jsgstngfmcountrycode')
            phone = post.get('jsgstngfmphone')
            wtsappyesno = post.get('jsgstngfmwtsappyesno')
            wtsappcountrycode = post.get('jsgstngfmwtsappcountrycode')
            wtsappphone = post.get('jsgstngfmwtsappphone')
            if (wtsappyesno == 'YES' or not wtsappcountrycode or not wtsappphone):
                wtsappcountrycode = countrycode
                wtsappphone = phone
            participantemail = post.get('jsgstngfmparticipantemail')
            country = post.get('jsgstngfmcountry')
            pincode = post.get('jsgstngfmpincode')
            city = post.get('jsgstngfmcity')
            state = post.get('jsgstngfmstate')
            question = post.get('pquestion')

            if (state):
                state = int(state)
            else:
                state = 0
            preflang = post.get('jsgstngfmpreflang')
            preftime = post.get('jsgstngfmpreftime')
            name = first_name + ' ' + last_name
            if event_type == 'pournami':
                pournami_trans_lang = post.get('language_preferred')
                pournami_timezone = post.get('timezone_preferred')
            else:
                pournami_trans_lang = ""
                pournami_timezone = ""

            if (event_type == "anandaalai"):
                prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', preftime)])
            else:
                prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', preflang)])

            countryrec = request.env['res.country'].sudo().search([('id', '=', country)], order="code asc")
            countries_scl = request.env['res.country'].sudo().search(
                ['|', ('name', '=', 'India'), ('name', '=', 'Nepal')], order="code asc")
            countries = request.env['res.country'].sudo().search(
                ['|', '|', '|', ('name', '=', 'Sri Lanka'), ('name', '=', 'Bangladesh'), ('name', '=', 'Pakistan'),
                 ('name', '=', 'Afghanistan')], order="code asc")
            result_countries = countries_scl | countries
            if event_type == 'pournami':
                if question:
                    p_ques_value = question
                else:
                    p_ques_value = ''
                if state != 0:
                    staterec = request.env['res.country.state'].sudo().search([('id', '=', state)])
                pournami_timezone = request.env['satsang.tzlang'].sudo().search([('id', '=', int(pournami_timezone))])
                pournami_trans_lang = request.env['res.lang'].sudo().search([('id', '=', int(pournami_trans_lang))])

                rec_dict = {
                    'name': name,
                    'phone_country_code': countrycode,
                    'phone': phone,
                    'email': participantemail,
                    'city': city,
                    'state_id': state,
                    'state': staterec.name if state != 0 else '',
                    'zip': pincode,
                    'country_id': country,
                    'country': countryrec.code,
                    'whatsapp_country_code': wtsappcountrycode,
                    'whatsapp_number': wtsappphone,
                    'sso_id': profileid
                }
                pgm_schedule_info = {
                    'teacher_id': None,
                    'teacher_name': None,
                    'center_id': None,
                    'center_name': None,
                    'location': None,
                    'country': None,
                    'remarks': None,
                    'schedule_name': prgapplied.pgmschedule_programname,
                    'start_date': prgapplied.pgmschedule_startdate,
                    'end_date': prgapplied.pgmschedule_enddate,
                    'program_schedule_guid': None,
                    'localScheduleId': None,
                    'pii': rec_dict
                }
                recdynlead = request.env['program.lead'].sudo().create({
                    'program_schedule_info': pgm_schedule_info,
                    'program_name': prgapplied.pgmschedule_programtype.program_type,
                    'start_date': prgapplied.pgmschedule_startdate,
                    'reg_status': 'CONFIRMED',
                    'reg_date_time': datetime.datetime.now(),
                    'active': True,
                    'utm_medium': '',
                    'utm_campaign': '',
                    'utm_term': '',
                    'utm_source': '',
                    'utm_content': '',
                    'utm_referer': '',
                    'is_ie_done': False,
                    'record_name': name,
                    'record_phone': phone,
                    'record_email': participantemail,
                    'record_city': city,
                    'record_state': staterec.name if state != 0 else '',
                    'record_zip': pincode,
                    'record_country': countryrec.code,
                    'trans_lang': pournami_trans_lang.name.split('/')[0].strip(),
                    'bay_name': pournami_timezone.tzlang_timezone,
                    'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                    'contact_id_fkey': 2,  # rec.id,
                    'pgm_remark': 'First timer',
                    'query_from_user': p_ques_value,
                    'local_program_type_id': event_program_type,
                    'sso_id': profileid
                })
                try:
                    date_time_vl = ""

                    if pournami_timezone.tzlang_displaytext:
                        date_time_vl = str(pournami_timezone.tzlang_displaytext)

                    fmfemailnextsatsangdate = request.env['ir.config_parameter'].sudo().get_param(
                        'satsang.fmfemailnextsatsangdate')
                    d = datetime.datetime.strptime(fmfemailnextsatsangdate, '%Y-%m-%d')
                    date_time_vl = date_time_vl + " on " + str(d.strftime('%d-%b-%Y'))
                    gl_rd_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    ic_rd_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    time_zone = pournami_timezone.tzlang_displaytext if pournami_timezone.tzlang_displaytext else 'IST',

                    transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                        'record_name': name,
                        'record_phone': phone,
                        'record_email': participantemail,
                        'trans_lang': pournami_trans_lang.name.split('/')[0].strip(),
                        'time_zone': time_zone,
                        'time_ist': pournami_timezone.time_ist if pournami_timezone.time_ist else '7 PM',
                        'time_local': pournami_timezone.time_local if pournami_timezone.time_local else '7 PM',
                        'datetime_text': date_time_vl,
                        'program_name': event_program_type,
                        'contact_text': '',
                        'gl_rd_url': str(gl_rd_url) + "/onlinesatsang/calendarevent?tvalue=" + str(time_zone),
                        'ic_rd_url': str(ic_rd_url) + "/onlinesatsang/downloadicsfile?tvalue=" + str(time_zone),
                    })

                    selected_tzlangregion = False
                    if pournami_timezone.tzlangregion:
                        selected_tzlangregion = pournami_timezone.tzlangregion.tzlang_region.id

                    key_value_rec = request.env['satsang.key.value.config'].sudo().search(
                        [("key", "=", 'confirmation_addtional_text')])
                    if key_value_rec and selected_tzlangregion:
                        for r in key_value_rec:
                            if selected_tzlangregion in r.regionname.ids:
                                transprogramlead_rec.sudo().write({"contact_text": r.value})
                                break
                    _logger.info("..........Before Mail.................")
                    mail_template_id = request.env.ref('isha_satsang.satsang_porumani_sucess_mail_template')
                    # send confirmation mail depends upon the language choose on web page
                    try:
                        if pournami_trans_lang:
                            lang_mail_template = request.env['satsang.lang.mailtempate'].sudo().search(
                                [("key", "=", 'Satsang: FMS Registration Confirmation'),
                                 ("tz_lang_id", "=", pournami_trans_lang.id)])
                            if lang_mail_template:
                                mail_template_id = lang_mail_template.mail_template_id.id
                    except:
                        pass
                    request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(transprogramlead_rec.id,
                                                                                              force_send=True)
                    _logger.info("..........After Mail.................")
                except Exception as ex2:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
                    _logger.error(tb_ex)

