import traceback
from datetime import timedelta, datetime

import requests

from odoo import api, models, fields


class Cleanup(models.AbstractModel):
    _name = 'satsang.cleanup'
    _description = 'Isha Satsang DB Cleanup'

    @api.model
    def cleanup(self):
        templogdays = self.env['ir.config_parameter'].sudo().get_param('satsang.templogdays')
        templogdays = int(templogdays)
        if (templogdays == 0 or templogdays == False):
            templogdays = 90
        diff = (datetime.now() - timedelta(days=templogdays))
        print (diff)
        self.env['satsang.tempdebuglog'].search([('create_date','<', diff)]).unlink()
        print('cleanup completed..')

class SMS(models.AbstractModel):
    _name = 'satsang.smsapi'
    _description = 'Isha Satsang SMS API'

    @api.model
    def _send_smcountry_sms(self, data):

        url = self.env['ir.config_parameter'].sudo().get_param('satsang.sms_provider_base_url')
        params = { 'User': self.env['ir.config_parameter'].sudo().get_param('satsang.sms_provider_username'),
                  'passwd': self.env['ir.config_parameter'].sudo().get_param('satsang.sms_provider_password'),
                  'mobilenumber': data['number'],
                  'message':data['message'],
                  'sid': self.env['ir.config_parameter'].sudo().get_param('satsang.sms_provider_sender_id'),
                  'mtype':'N','DR':'Y'}
        try:
            result = requests.get(url, params)
        except Exception as e:
            self._raise_query_error(e)
        if 'Invalid Password' in result.text:
            self._raise_query_error('Unexpected issue. Please contact Administrator.')
        if 'ERROR:' in result.text:
            self._raise_query_error(result.text)
        return True

    def _raise_query_error(self, error):
        print('Error with sending sms: %s' % error)
        #raise ValueError('Error with sending sms: %s' % error)

class SatsangNotification(models.AbstractModel):
    _name = 'satsang.notification'
    _description = 'Isha Satsang Notification'

    @api.model
    def sendRegistrationEmailSMSCore(self, rec):
        try:
            self.env['satsang.tempdebuglog'].sudo().create({
                'logtext': 'before inside email sms core'
            })
            eventdetail = self.env['satsang.notificationsettingtemplate'].search([('language','=',rec.trans_lang),
            ('notificationevent.notificationevent','=', rec.program_name)])
            if eventdetail.emailtemplateid.id==False:
                eventdetail = self.env['satsang.notificationsettingtemplate'].search([('language', '=', 'English'),
                                ('notificationevent.notificationevent', '=', rec.program_name)])

            if (eventdetail.sendemail == True):
                self.env['satsang.tempdebuglog'].sudo().create({
                    'logtext': 'before email true inside email sms core'
                })
                print('sending email enabled')
                templateid = eventdetail.emailtemplateid.id
                self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
                self.env['satsang.tempdebuglog'].sudo().create({
                    'logtext': 'after email true inside email sms core' + str(templateid)
                })
                print('sending email completed')
            if (eventdetail.sendsms == True ):
                print('sending sms enabled')
                templateid = eventdetail.smstemplateid.id
                smstemplate = self.env['sms.template'].browse(templateid)
                subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'transprogramlead', rec.id)
                print(subject)
                self.env['satsang.smsapi']._send_smcountry_sms({
                        'number': rec.record_phone,
                        'message': subject
                    })
                print('sending sms completed')
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            self.env['satsang.tempdebuglog'].sudo().create({
                'logtext': "satsang mail sms core exception"
            })
            self.env['satsang.tempdebuglog'].sudo().create({
                'logtext': tb_ex
            })


class TransProgramLead(models.TransientModel):
    _name = 'transprogramlead'
    _description = 'Trans Program Lead'

    record_name = fields.Char(string='Contact Name')
    record_phone = fields.Char(string='Contact Phone')
    record_email = fields.Char(string='Contact Email')
    trans_lang = fields.Char(string='Language')
    program_name = fields.Char(string='program_name')
    time_zone = fields.Char(string='TimeZone')
    time_ist = fields.Char('IST Time ')
    time_local = fields.Char('Local time ')
    datetime_text = fields.Char()
    contact_text = fields.Char("Contact values depends upon region")
    tzlangregion = fields.Char("Region value")
    gl_rd_url=fields.Char("Google Calendar event api redirect url")
    ic_rd_url=fields.Char("ICAL redirect url")
