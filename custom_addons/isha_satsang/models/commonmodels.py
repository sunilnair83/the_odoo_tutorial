import hashlib
import hmac
import json
import logging

import jwt
import requests

from odoo import api, models, fields
from odoo.http import request
from ..isha_crm_importer import Configuration

DEFAULT_ENDPOINT = 'http://api.smscountry.com'
_logger = logging.getLogger(__name__)

class tmp_calendar_content(models.TransientModel):
    _name="satsang.calevent"

    event_content=fields.Char(string="dict content")

class uicaptionlist(models.Model):
    _name='satsang.uicaptionlist'
    _description = 'caption texts'

    name=fields.Char(string='Language Name')
    title=fields.Char(string="Title String")
    first_name=fields.Char(string="First Name")
    last_name=fields.Char(string="Last Name")
    mobile_no=fields.Char(string="Mobile Number")
    watsup_no=fields.Char(string="Whatsapp Number")
    countrycode=fields.Char(string="Country Code")
    email=fields.Char(string="EMail")
    language =fields.Char(string="Langugage")
    pref_lang =fields.Char(string="Preferred Langugage")
    nationality =fields.Char(string="Nationality")
    state =fields.Char(string="State")
    district =fields.Char(string="District")
    pincode =fields.Char(string="Pincode / Zipcode")
    residential_Country =fields.Char(string="Country of residence")
    submittext =fields.Char(string="Save and Continue")
    watsupq =fields.Char(string="In certain countries, Isha volunteers may need to communicate with you via WhatsApp.Is your whatsapp number same as your mobile number?")
    yes =fields.Char(string="Yes")
    no =fields.Char(string="No")

class NotificationTemplateMaster(models.Model):
	_name = 'satsang.notificationtemplate'
	_description = 'Notification Template'
	##_order = 'lob, programtype'
	_rec_name = 'notificationevent'
	_sql_constraints = [('unique_notificationevent1','unique(satsangschedule, notificationevent)','Cannot have duplicate notification event')]

	programtype = fields.Many2one('satsang.schedule', string = 'Satsang Schedule')
	notificationevent = fields.Many2one('satsang.notificationevent', string = 'Notification Event')
	sendemail = fields.Boolean(string='Send Email', default=False)
	emailtemplateid = fields.Many2one('mail.template',string='Email Template',track_visibility='onchange')

class NotificationMaster(models.Model):
	_name = 'satsang.notificationevent'
	_description = 'Notification Event'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'
	_sql_constraints = [('unique_notificationevent','unique(notificationevent)','Cannot have duplicate notification event, give different name')]

	notificationevent = fields.Char(string = 'Notification Event Code', size=100, required=True)
	#programname = fields.Selection([('Online Satsang', 'Online Satsang'), ('Online SCK Satsang', 'Online SCK Satsang'), ('Online IEO Satsang', 'Online IEO Satsang'),  ('Online Samyama Satsang', 'Online Samyama Satsang')], string='Satsang Program Name', required=False)

class NotificationTemplateSettingMaster(models.Model):
	_name = 'satsang.notificationsettingtemplate'
	_description = 'Notification settings templateTemplate'
	#_order = 'lob, programtype'
	#_rec_name = 'notificationevent'
	_sql_constraints = [('unique_notificationsettingevent1','unique(notificationevent, language)','Cannot have duplicate notificationevent language')]

	#programtype = fields.Many2one('satsang.schedule', string = 'Satsang Schedule')
	notificationevent = fields.Many2one('satsang.notificationevent', string = 'Notification Event')
	sendemail = fields.Boolean(string='Send Email', default=False)
	emailtemplateid = fields.Many2one('mail.template',string='Email Template',track_visibility='onchange')
	sendsms = fields.Boolean(string='Send SMS', default=False)
	smstemplateid = fields.Many2one('sms.template', string='SMS Template', track_visibility='onchange')
	language = fields.Selection(
		[('English', 'English'), ('Hindi', 'Hindi'), ('Kannada', 'Kannada'), ('Tamil', 'Tamil'),
		 ('Telugu', 'Telugu')], string='Language')

class TempDebugLog(models.Model):
    _name = 'satsang.tempdebuglog'
    _description = 'Temp debug log'

    logtext = fields.Text(string='logtext')

class SSO(models.AbstractModel):
    _name = 'satsang.ssoapi'
    _description = 'Isha Satsang SSO API'

    @api.model
    def get_consent_info(self, data):
        consentgrantStatusflag = False
        profidvarforconsent = data["profileidvar"]
        requestconsent_url = data['pms_url'] + "/api/consents?legalEntity=" + data["legal_entity"] + "&profileId=" + profidvarforconsent
        headersconsent = {
            "x-user": profidvarforconsent,
            "X-legal-entity": data["legal_entity"],
            "Authorization": "Bearer "+data['system_token']
        }
        try:
            if profidvarforconsent !="0" :
                result = requests.get(requestconsent_url, headers=headersconsent, verify=False)
                result.raise_for_status()
                consentreq_dict = result.json()
                consentgrantStatusflag = consentreq_dict[0]["grantStatus"]

        except Exception as e:
            self._raise_query_error(e)
        return consentgrantStatusflag

    def get_profileid_fromtoken(self, data):

        profidvar="0"
        try:
            decodedtoken = jwt.decode(data, verify=False, algorithms=['RS256'])
            profidvar = decodedtoken['sub']
        except Exception as e:
            self._raise_query_error(e)
        return profidvar

    def get_fullprofile_info(self, data):
        profidvar = data["profileidvar"]
        legal_entity = data["legalEntity"]
        fullprofileresponsedat = {}
        request_url = data['pms_url'] + "/api/full-profiles/" + profidvar
        headers = {
            "x-user": profidvar,
            "X-legal-entity": legal_entity,
            "Authorization": "Bearer "+data['system_token']
        }
        try:
                req = requests.get(request_url, headers=headers, verify=False)
                req.raise_for_status()
                fullprofileresponsedat = req.json()

        except Exception as e:
            self._raise_query_error(e)

        return fullprofileresponsedat

    def post_fullprofile_info(self, fullprofiledat, profidvar, legal_entity):
        responsecode = 0
        headers2 = {
            "x-user": profidvar,
            "X-legal-entity": legal_entity,
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJlbnYiOiJVQVQiLCJraWQiOiJlMmYwZWRjYjU0YWQ0NzVhODZmNGI4YmNmNGE5MzQxZCIsImFsZyI6IlJTMjU2In0.eyJpYXQiOjE1OTYwMjQ5MTQsImlzcyI6InBtcyIsInN1YiI6IlJlanV2ZW5hdGlvbiIsIl90dCI6InN5c3RlbSIsInJvbGVzIjpbXX0.Bq-2grTvpaq1T7KS4VjkbEE_30ECBTBcusrxkvohmgXIIPKkQsm_mnXslqmuZbI9DEQAVENqK48jSuINwPM7qpbVR52qUVtWndhkzoTBQ9DzbiAQeratwVzl2ahgvlT54D0WfR8mKvN2ZDYatAAowL76jiSVHU6R0BU2PIy26tVOqJNdeWDtvi6Pbx1cMpyko7TT6mclXuYSImPI7KzoCKpmE3pRgRRB2nW8TJlU0olEiFJCmUNFtVveuIi3dOzGS9pzQLjCbHYyRF4pECmoSGtRYBn_gkWMk_ai8YRRhrRSaboiWdFEGkYpDhjgsjjsGeRUMs7oBEu_DPjMB5WBqQ"
        }
        request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + profidvar
        try:
            #datavarpost = json.dumps(fullprofiledat)
            r = requests.post(request_url, files={'profile': (None, fullprofiledat)}, headers=headers2)
            responsecode =  r.status_code
        except Exception as e:
            self._raise_query_error(e)

        return responsecode

    def get_fullprofileresponse_DTO(self):
        return  {
            "profileId": "",
            "basicProfile": {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "profileId": "",
                "email": "",
                "firstName": "",
                "lastName": "",
                "profilePic": "",
                "phone": {
                    "countryCode": "",
                    "number": ""
                },
                "gender": "",
                "dob": "",
                "countryOfResidence": ""
            },
            "extendedProfile": {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "passportNum": "",
                "nationality": "",
                "pan": "",
                "profileId": ""
            },
            "documents": [],
            "attendedPrograms": [],
            "profileSettingsConfig": {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "nameLocked": False,
                "isPhoneVerified": False,
                "phoneVerificationDate": None,
                "profileId": ""
            },
            "addresses": [
                {
                    "createdBy": "",
                    "createdDate": "",
                    "lastModifiedBy": "",
                    "lastModifiedDate": "",
                    "id": 0,
                    "addressType": "",
                    "addressLine1": "",
                    "addressLine2": "",
                    "townVillageDistrict": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "pincode": "",
                    "profileId": ""
                }
            ]
        }

    def _raise_query_error(self, error):
        print('Error with SSO api: %s' % error)
        #raise ValueError('Error with sending sms: %s' % error)

    def get_ssoimage_froms3(self,ssoidproofuri):
        req = requests.get(ssoidproofuri, headers={}, verify=False)
        req.raise_for_status()
        return req._content

    # SSO Logout
    def sso_logout(self):
        sso_config = Configuration('SSO')
        ret = sso_config['SSO_API_SECRET']
        cururls = str(request.httprequest.url).replace("http://", "http://", 1)
        # base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        # callbackurl = base_url + '/onlinesatsang/registrationform?is_logout=yes'
        # clbkurl=callbackurl
        ret_list = {'request_url': str(cururls),
                    "callback_url": self.env['ir.config_parameter'].sudo().get_param(
                        'satsang_pournami_sso_logoutcallback_url'),
                    "api_key": sso_config['SSO_API_KEY'],
                    }
        data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
        data_enc = data_enc.replace("/", "\\/")
        signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                             digestmod=hashlib.sha256).hexdigest()
        ret_list["hash_value"] = signature
        ret_list['sso_logouturl'] = sso_config['SSO_LOGIN_URL'] + 'cm'
        print(ret_list)
        return ret_list

    # def anandaalai_sso_logout(self):
    #     base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
    #     sso_login_callbackurl = base_url + '/onlinesatsang/registrationform?eventtype=anandaalai&is_logout=yes'
    #     return (self.sso_logout(sso_login_callbackurl))
	#
    # def pournami_sso_logout(self):
    #     clbckurl=self.env['ir.config_parameter'].sudo().get_param('satsang_pournami_sso_logoutcallback_url')
    #     return(self.sso_logout(clbckurl))

