# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

import logging
_logger = logging.getLogger(__name__)


class support_ticket(models.Model):
	_inherit = "support.ticket"

	note1 = fields.Text(string='Solution Note 1',)
	note2 = fields.Text(string='Solution Note 2',)
	note3 = fields.Text(string='Solution Note 3',)
	image1 = fields.Binary(string='Solution Image 1',)
	image2 = fields.Binary(string='Solution Image 2',)
	image3 = fields.Binary(string='Solution Image 3',)
	image4 = fields.Binary(string='Solution Image 4',)
	image5 = fields.Binary(string='Solution Image 5',)
	image6 = fields.Binary(string='Solution Image 6',)