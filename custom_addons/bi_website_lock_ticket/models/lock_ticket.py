# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

class MultiInvoicePayment(models.TransientModel):
	_name = 'lock.ticket'

	lock_date = fields.Datetime(string="Ticket Lock Date",required=True,default=fields.Datetime.now)
	lock_end_date = fields.Datetime(string="Ticket Lock End Date",required=True,default=fields.Datetime.now)
	is_sure = fields.Boolean(string="Are You Sure???",default=False,required=True)

	def lock_ticket(self):
		for lock in self:
			if not lock.is_sure:
				raise UserError(_('Please be sure you want to lock this Ticket!!...!!'))
			l_date = datetime.strptime(str(lock.lock_date),DEFAULT_SERVER_DATETIME_FORMAT)
			un_date =  datetime.strptime(str(lock.lock_end_date),DEFAULT_SERVER_DATETIME_FORMAT)
			if un_date <= l_date:
				raise UserError(_('Please make sure you enter the correct dates!!...!!'))
			if lock.is_sure == True:
				ticket = self.env['support.ticket'].browse(self._context.get('active_id'))
				stage_obj = self.env['support.stage'].search([('name','=','Lock')])
				ticket.write({
					'lock_date' : str(lock.lock_date),
					'lock_end_date' : str(lock.lock_end_date),
					'is_lock' : True,
					'lock_by_user' : self.env.user.name,
					'unlock_by_user' : '',
					'stage_id' : stage_obj.id,
					'lock_note' : 'This ticket has been lock by ' + self.env.user.name,
					})


