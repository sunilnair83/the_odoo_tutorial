# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class InheritResConfigSettings(models.TransientModel):
	_inherit = 'res.config.settings'

	auto_cancel = fields.Boolean('Auto cancel Ticket Lock',default=False)

	@api.model
	def get_values(self):
		res = super(InheritResConfigSettings, self).get_values()
		ICPSudo = self.env['ir.default'].sudo()
		auto_cancel_setting = ICPSudo.get("res.config.settings",'auto_cancel')
		res.update(
			auto_cancel=auto_cancel_setting,
		)
		return res


	def set_values(self):
		super(InheritResConfigSettings, self).set_values()
		ICPSudo = self.env['ir.default'].sudo()
		ICPSudo.set("res.config.settings",'auto_cancel',self.auto_cancel)

	