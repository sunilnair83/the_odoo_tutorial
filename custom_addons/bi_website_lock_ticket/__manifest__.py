# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Support Ticket Lock System in Odoo',
    'version': '13.0.0.1',
    'category': 'website',
    'author' : 'BrowseInfo',
    'summary': "Manage Customer Helpdesk Support Ticket lock Helpdesk Support Ticket timesheet Lock and unlock feature for support ticket helpdesk service system website support request lock website portal support website ticket handling online Support Request ticketing",
    'description': '''A helpdesk / support ticket system for your website
    support management from website
    website support ticket
    website helpdesk management
    Support system for your website
    website support management
    submit support request
    support form
    Ticket support
    website support ticket
    website issue
    website project issue
    website crm management
    website ticket handling
    support management
    project support, crm support, online support management, online support, support product, 
    support services, issue support, fix issue, raise ticket by website, 
    raise support ticket by website, view support request, display support on website, 
    list support on website, helpdesk system for your website, website helpdesk management, 
    submit helpdesk, helpdesk form, Ticket helpdesk, website support ticket, website issue, 
    website project issue, website crm management, website ticket handling,support management, 
    project support, crm support, online support management, online helpdesk, helpdesk product, 
    helpdesk services, issue helpdesk, fix helpdesk, raise ticket by website, raise issue by website, 
    view helpdesk, display helpdesk on website, list helpdesk on website, website customer support Ticket
    website support Ticket with timesheet, website support Ticket invoice, website helpdesk Ticket invoice, 
    website support helpdesk Ticket invoice, website helpdesk support Ticket with timesheet, 
    website support tickit, website helpdesk tickit
    Customer Website Helpdesk Support Ticket for Customer
    client Website Helpdesk Support Ticket for client
    Website portal Helpdesk Support Ticket for Customer
    customer portal helpdesk support, customer portal support management, portal customer support management
    website portal helpesk support for customer, website portal support, website portal helpdesk
    website portal support Ticket,website portal helpdesk Ticket , website portal support request , website support request
    Submit Support Request, online Support Request, manage Support Request, manage support team
    create task from support request, create task from helpdesk request, online service request, create task from service request
    website portal service request, website portal helpdesk service request, create invoice from timesheet
    invoice from service request, invoice from helpdesk request, invoice from helpdesk timesheet
    timesheet for helpdesk support Ticket, invoice for helpdesk support request, project sub-task management
    Website Helpdesk Ticket locking System
    website Helpdesk Ticket Lock-unlock System
    website support request lock unlock
    website support request locking system
    portal helpdesk locking system
    support management from website
    website support ticket
    website helpdesk management
    Support system for your website
    website support management
    submit support request
    Website Helpdesk Support Ticket for Customer
    support form
    technical support
    tech support
    administration
    receptionist
    customer support
    service desk
    Helpdesk Support Ticket
    Helpdesk Ticket
    Help desk Ticket
    request management 
    issue tracking system
     help desk or 
     call center
     Website Help desk Support
     Website tech Support
     service request
      customer services
      Customer support
      Remote support
This Odoo apps almost contain everything you need for Service Desk, 
Technical support team, Call center management, Issue ticketing system which include issue tracking, 
billing payment, tech support portal, service request with timesheet to be managed in Odoo project management app. 
Website customer helpdesk support Ticketing System is used for give customer an interface where he/she can send support ticket request 
and attach documents from website.Support ticket will send by email to customer and admin. 
for Online ticketing system for customer support in Odoo Support. Also its allow to create invoice 
easily from timesheet logged for the project issue/helpdesk support ticketing system. 
Customer can view their ticket from the website portal and easily see stage of the reported ticket also 
customer can communicate with help-desk support team from website communication option.
    Help Desk Reporting Systems
    Ticketing Systems
    Ticket Systems
    Ticket support
    website support ticket
    website issue
    website project issue
    website crm management
    website ticket handling
    support management
    Website Help desk Support ticket
    support ticket
    helpdesk request
    odoo service desk
    customer service

    odoo helpdesk  
    website support ticket
    support ticket 
    helpdesk support ticket
    Online ticketing system for customer support
    Help Desk Ticketing System
    support Help Desk Ticketing System
    support HelpDesk Ticketing System
    support Ticketing System
    project support, crm support, online support management, online support, support product, 
    support services, issue support, fix issue, raise ticket by website, 
    raise support ticket by website, view support request, display support on website, 
    list support on website, helpdesk system for your website, website helpdesk management, 
    submit helpdesk, helpdesk form, Ticket helpdesk, website support ticket, website issue, 
    website project issue, website crm management, website ticket handling,support management, 
    project support, crm support, online support management, online helpdesk, helpdesk product, 
    helpdesk services, issue helpdesk, fix helpdesk, raise ticket by website, raise issue by website, 
    view helpdesk, display helpdesk on website, list helpdesk on website, website customer support Ticket
    website support Ticket with timesheet, website support Ticket invoice, website helpdesk Ticket invoice, 
    website support helpdesk Ticket invoice, website helpdesk support Ticket with timesheet, 
    website support tickit, website helpdesk tickit
    Customer Website Helpdesk Support Ticket for Customer
    client Website Helpdesk Support Ticket for client
    Website portal Helpdesk Support Ticket for Customer
    customer portal helpdesk support, customer portal support management, portal customer support management
    website portal helpesk support for customer, website portal support, website portal helpdesk
    website portal support Ticket,website portal helpdesk Ticket , website portal support request , website support request
    Submit Support Request, online Support Request, manage Support Request, manage support team
    create task from support request, create task from helpdesk request, online service request, create task from service request
    website portal service request, website portal helpdesk service request, create invoice from timesheet
    invoice from service request, invoice from helpdesk request, invoice from helpdesk timesheet
    timesheet for helpdesk support Ticket, invoice for helpdesk support request, project sub-task management

     

        ''',
    'website': 'https://www.browseinfo.in',
    'price': 10,
    'currency': "EUR",
    'depends': ['base','website',
                # 'website_sale',
                'bi_website_support_ticket'],
    'data': [
    'data/support_ticket_unlock.xml',
    'views/lock_ticket_view.xml',
    'views/website_stage_view.xml',
    'views/website_support_view.xml',
    'views/res_config_settings_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
    'live_test_url':'https://youtu.be/2wKiXSm1KdI',
    "images":['static/description/Banner.png'],
}
