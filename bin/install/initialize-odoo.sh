#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################


if [[ ( -n $1 ) && ( -d $1 )]]; then
  SUSHUMNA_ROOT=$1
fi

if [[ ( -n $2 ) && ( -d $2 )]]; then
  APP_ROOT=$2
fi

ENVIRONMENT=$3
ODOO_DB=$4

echo
echo "--------------------------------------------------------------------------"
echo "  >> Initializing Odoo with parameters:"
echo "--------------------------------------------------------------------------"
echo "      Sushumna Root: $SUSHUMNA_ROOT"
echo "      App Root: $APP_ROOT"
echo "      Environment: $ENVIRONMENT"
echo "      DB: $ODOO_DB"
echo "--------------------------------------------------------------------------"
echo

if [[ ( -z $SUSHUMNA_ROOT ) || ( -z $APP_ROOT ) || ( -z $ENVIRONMENT ) || ( -z $ODOO_DB ) ]]; then
  echo
  echo "Mandatory parameters missing. Usage:"
  echo
  echo "  bash ${BASH_SOURCE[0]} <sushumna-root> <application-root-path> <environment> <odoo-database>"
  echo

  exit 1
fi

# Activate the virtual environment
cd $APP_ROOT
source env/bin/activate

PYTHON=python3.7
ODOO=odoo/odoo-bin

export ODOO_RC=etc/conf/$ENVIRONMENT/odoo.conf

$PYTHON $ODOO --database $ODOO_DB -i base --stop-after-init

echo "Odoo Database '$ODOO_DB' initialized with Base modules."
