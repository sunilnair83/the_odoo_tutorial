#!/bin/bash

################################################################################
# Author: Nanda Kumar Mangati
# Date: Nov 14, 2020
################################################################################

brew doctor
brew update

#--------------------------------------------------
# Install Packages
#--------------------------------------------------

echo -e "\n--- Installing Developer tools and libraries --"

# Dev tools for building python packages
brew install gcc fontconfig wkhtmltopdf wget

# Install Postgres if not installed

if ! [ -x "$(command -v psql)" ]; then
  echo -e "\n---- Installing Postgres ----"
  brew install postgresql@12
fi

# Install python3.7 if not installed

if ! [ -x "$(command -v python3.7)" ]; then
  echo -e "\n---- Installing python3.7 ----"
  brew install pyenv
  pyenv install 3.7.7
  pyenv global 3.7.7
  pip3 install virtualenv
fi

# Install node if not installed

if ! [ -x "$(command -v node)" ]; then
  echo -e "\n---- Installing nodeJS + NPM ----"
  brew install node
fi
