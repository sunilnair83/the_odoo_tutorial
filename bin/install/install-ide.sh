#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

#--------------------------------------------------
# Install IDE
#--------------------------------------------------

echo
echo "Installing PyCharm..."
echo
sudo snap install pycharm-community --classic
echo
echo "Installing Visual Studio Code..."
echo
sudo snap install code --classic
echo
echo "Installing PgAdmin4..."
echo
sudo apt install -y pgadmin4


