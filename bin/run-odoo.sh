#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################



if [[ ( -n $1 ) && ( -d $1 )]]; then
  SUSHUMNA_ROOT=$1
fi

if [[ ( -n $2 ) && ( -d $2 )]]; then
  APP_ROOT=$2
fi

ENVIRONMENT=$3
ODOO_PORT=$4
ODOO_DB=$5
SECRET=$6
ODOO_SHELL=$7

if [[ ( -z $SUSHUMNA_ROOT ) || ( -z $APP_ROOT ) || ( -z $ENVIRONMENT ) || ( -z $ODOO_PORT ) || ( -z $ODOO_DB ) || ( -z $SECRET ) ]]; then
  echo
  echo "Mandatory parameters missing. Usage:"
  echo
  echo "  bash ${BASH_SOURCE[0]} <sushumna-root> <application-root-path> <environment> <odoo-port> <odoo-database> <configuration-secret>"
  echo

  exit 1
fi

# Activate the virtual environment
echo $APP_ROOT
cd $APP_ROOT
source env/bin/activate

# Install PIP dependencies for addons
bash bin/install/install-addon-pip-requirements.sh

echo
echo "--------------------------------------------------------------------------"
echo "  Starting Odoo"
echo "--------------------------------------------------------------------------"
echo "      Sushumna Root: $SUSHUMNA_ROOT"
echo "      App Root: $APP_ROOT"
echo "      Environment: $ENVIRONMENT"
echo "      Port: $ODOO_PORT"
echo "      DB: $ODOO_DB"
echo "--------------------------------------------------------------------------"
echo

PYTHON=python3.7
ODOO=odoo/odoo-bin

export ODOO_RC=etc/conf/$ENVIRONMENT/odoo.conf
export SUSHUMNA_CONFIG=etc/conf/$ENVIRONMENT/sushumna.conf
export SUSHUMNA_CONFIG_SECRET=$SECRET

# Start Odoo Application
if [[ $ODOO_SHELL == "shell" ]]; then
  # Launch odoo Shell
  $PYTHON $ODOO shell --database $ODOO_DB --dev all,reload,qweb,werkzeug,xml

else

  # Launch browser with the Odoo start delay
  sleep 5 && echo "Launching ..." &
  sleep 4 && echo "Launching Browser in 1 ..." &
  sleep 3 && echo "Launching Browser in 2 ..." &
  sleep 2 && echo "Launching Browser in 3 ..." &
  # If it is mac, use open.
  sleep 5 &&
  # If it is mac, use open.
  if [[ "$(uname -a)" == *"Darwin"* ]]; then
    open http://localhost:$ODOO_PORT/web/login?debug=1 &
  else
    xdg-open http://localhost:$ODOO_PORT/web/login?debug=1 &
  fi

  $PYTHON $ODOO --database $ODOO_DB --dev all,reload,qweb,werkzeug,xml
fi
