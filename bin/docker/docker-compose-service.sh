#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

if [[ ( -z $1 ) || ( -z $2 ) || ( -z $3)]]; then
  echo
  echo "Usage:"
  echo
  echo "  bash ${BASH_SOURCE[0]} <applicaton-name> <docker-compose-file> <action>"
  echo
  echo "  Actions:"
  echo "    start     Brings up the containers in the compose file"
  echo "    stop      Takes down the containers in the compose file"
  echo "    restart   Restarts all the containers in the compose file"
  echo

  exit 1
fi

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." >/dev/null 2>&1 && pwd )
DOCKER_COPMPOSE_PATH=$APP_ROOT/etc/docker-compose
APPLICATION_NAME=$1
DOCKER_COMPOSE_FILE=$2
ACTION=$3

echo
echo "--------------------------------------------------------------------------"
echo " DOCKER COMPOSE SERVICE MANAGER"
echo "--------------------------------------------------------------------------"
echo "    Service: $APPLICATION_NAME"
echo "    Docker Compose Path: $DOCKER_COPMPOSE_PATH/$DOCKER_COMPOSE_FILE.yml"
echo "    Action: $ACTION"
echo "--------------------------------------------------------------------------"
echo

# Start database if script is run with the start parameter or without parameters
if [[ $ACTION == "start" ]]; then
  echo ">> Starting $APPLICATION_NAME..."
  echo
  docker-compose  -f $DOCKER_COPMPOSE_PATH/$DOCKER_COMPOSE_FILE.yml up -d
  echo
  echo ">> $APPLICATION_NAME has STARTED."
  echo

elif [[  $ACTION == "stop" ]] ; then
  echo ">> Stopping $APPLICATION_NAME... --"
  echo
  docker-compose  -f $DOCKER_COPMPOSE_PATH/$DOCKER_COMPOSE_FILE.yml down
  echo
  echo ">> $APPLICATION_NAME has STOPPED."
  echo

elif [[  $ACTION == "restart" ]] ; then
  echo ">> Stopping $APPLICATION_NAME... "
  echo
  docker-compose  -f $DOCKER_COPMPOSE_PATH/$DOCKER_COMPOSE_FILE.yml down
  echo
  echo ">> Starting $APPLICATION_NAME... "
  echo
  docker-compose  -f $DOCKER_COPMPOSE_PATH/$DOCKER_COMPOSE_FILE.yml up -d
  echo
  echo ">> $APPLICATION_NAME has RESTARTED."
  echo

fi

echo