#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

echo " -- Stopping all containers --"

docker container stop $(docker container ls -aq)

echo " -- Remove all containers --"

docker container prune -f

echo " -- Remove all volumes --"

docker volume prune -f