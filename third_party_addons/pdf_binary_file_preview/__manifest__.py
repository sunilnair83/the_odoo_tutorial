# -*- coding: utf-8 -*-
{
    'name': "PDF and Image Document Preview",

    'summary': """
        Document Preview allows users to preview a document without downloading it that leads to saving
         time and storage of users.
        """,

    'description': """
       This module is mainly used to show the Preview of the Image and PDF file for binary fields.
    """,

    "author": "Isha Foundation",
    "license": "LGPL-3",
    "website": "https://isha.sadhguru.org/",

    'category': 'Tools',

    'images': ['static/description/icon.png'],

    'version': '13.0.1.0.1',

    'depends': ['base', 'web', 'mail'],

    'data': [
        'views/pdf_preview_templates.xml',
    ],
    'qweb': ['static/src/xml/pdf_binary_preview.xml',
    ],
}
